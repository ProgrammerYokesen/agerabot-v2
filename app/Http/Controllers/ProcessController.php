<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Cart;
use Alert;
use Cookie;
use App\Sender;
use Jenssegers\Agent\Agent;
use CRUDBooster;
use Session;
use Illuminate\Support\Facades\Hash;
use Illuminate\Pagination\LengthAwarePaginator;
use carbon\carbon;

class ProcessController extends Controller
{

  public function addtocart($id)
  {
      $product = DB::table('products')->where('id', $id)->first();
      $images = unserialize($product->productImages);
      $cart = Cart::add($product->id, $product->productName, 1, $product->productPrice3, ['promo' => 'none','img' => $images[0]]);
      $cookie = Cookie::get('booking');
      $check_kode = DB::table('pesanan')->where('booking',$cookie)->where('flagCartExpired','1')->orderby('id','desc')->first();
      if($check_kode){
        $token = $check_kode->booking;
        date_default_timezone_set("Asia/Jakarta");
        $date = date('Y-m-d H:i:s');
        $orderExpired = date('Y-m-d H:i:s',strtotime($date.'+ 1 hours'));
        $cartExpired = date('Y-m-d H:i:s',strtotime($date.'+ 3 hours'));
        $jumlah = DB::table('pesanan')->where('booking',$cookie)->where('flagCartExpired','1')->where('produk_id',$product->id)->orderby('id','desc')->first();
        if($jumlah){
          $jmhl = $jumlah->jumlah + 1;
          $update = DB::table('pesanan')->where('booking',$cookie)->where('flagCartExpired','1')->where('produk_id',$product->id)->update([
            'jumlah' => $jmhl
          ]);
        }else{
          $insert = DB::table('pesanan')->insert([
            'phone' => '0000000',
            'produk_id' => $product->id,
            'flag_jmlh' => '1',
            'harga' => $product->productPrice3,
            'orderExpired' => $orderExpired,
            'cartExpired' => $cartExpired,
            'jumlah' => '1',
            'flagOrderExpired' => '1',
            'flagCartExpired' => '1',
            'booking' => $token
          ]);
        }

      }else{
        $time = time();
        $token = "";
        $codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        mt_srand($time);
        for($i=0;$i<6;$i++){
            $token .= $codeAlphabet[mt_rand(0,strlen($codeAlphabet)-1)];
        }
        $check_unique = DB::table('pesanan')->where('booking',$token)->count();
        if($check_unique == 0){
          $token = $token;
        }else{
          $token = time();
        }

        Cookie::queue('booking', $token, 3243200);
        date_default_timezone_set("Asia/Jakarta");
        $date = date('Y-m-d H:i:s');
        $orderExpired = date('Y-m-d H:i:s',strtotime($date.'+ 1 hours'));
        $cartExpired = date('Y-m-d H:i:s',strtotime($date.'+ 3 hours'));
        $insert = DB::table('pesanan')->insert([
          'phone' => '0000000',
          'produk_id' => $product->id,
          'flag_jmlh' => '1',
          'harga' => $product->productPrice3,
          'orderExpired' => $orderExpired,
          'cartExpired' => $cartExpired,
          'jumlah' => '1',
          'flagOrderExpired' => '1',
          'flagCartExpired' => '1',
          'booking' => $token
        ]);
      }



      $check_bookingan = DB::table('bookingan')->where('kode',$token)->orderby('id','desc')->first();
      if($check_bookingan){
        $jmlh_baru = DB::table('pesanan')->where('booking',$token)->sum('jumlah');
        $update = DB::table('bookingan')->where('kode',$token)->update([
          'jmlh_item' => $jmlh_baru,
          'time_status_keranjang' => date('Y-m-d H:i:s')
        ]);
      }else{
        $insert_bookingan = DB::table('bookingan')->insert([
          'phone' => $phone,
          'nama_pemesan' => $senderName,
          'kode' => $token,
          'status' => 'keranjang',
          'status_keranjang' => 'keranjang_berisi',
          'time_status_keranjang' => date('Y-m-d H:i:s')
        ]);
      }

      Alert::warning('Produk '.$product->productName.' Berhasil dimasukkan ke keranjang','Success')->persistent('Close');

      return redirect()->back();
  }

  public function addCart($slug)
  {
    // dd($slug);
    // dd(Cart::content());
      Cart::destroy();
      $cart = DB::table('package')->where('slug',$slug)->first();
      Cart::add($cart->id, $cart->nmPackage, 1, $cart->price, ['type'=>'subs', 'slug'=>$slug]);
      // foreach(Cart::content() as $key=>$value){
      //   dd($key, $value->options['type']);

      // }

      return redirect()->route('checkoutPage');
  }
  public function addCartVps($slug)
  {
    
    Cart::destroy();
    $cart = DB::table('vps')->where('slug',$slug)->leftjoin('catvps', 'vps.catVpsId', 'catvps.id')->select('vps.id', 'catvps.nmVps', 'vps.price', 'vps.interval')->first();
    // dd($cart);
      Cart::add($cart->id, $cart->nmVps, $cart->interval, $cart->price, ['type'=>'vps', 'slug'=>$slug]);
      // dd(Cart::content());
      // dd('dd');
      return redirect()->route('checkoutVpsPage');
  }
  public function addCartInstall()
  {
    // dd($slug);
    // dd('masuk sini');
      Cart::destroy();
      $cart = DB::table('installment')->first();
      // dd($cart);
      Cart::add($cart->id, 'installation', 1, $cart->price, ['type'=>'install', 'slug'=>$cart->slug]);
      // dd(Cart::content());
      // dd('dd');
      return redirect()->route('checkoutInstallPage');
  }
  public function addCartUnpaid($id)
  {
    // dd($id);
    // id,name,qty,price
      Cart::destroy();
      $cart = DB::table('subscription')->where('subscription.id',$id)->leftjoin('package', 'subscription.packageId', 'package.id')->first();
      Cart::add($cart->id, $cart->nmPackage, 1, $cart->price, ['type'=>'unpaidsubs']);
      // dd(Cart::content());
      // dd('dd');
      return redirect()->route('checkoutPage');
  }
  public function addCartUnpaidVps($id)
  {
    // dd($id);

      Cart::destroy();
      $cart = DB::table('paymentvps')->where('paymentvps.id',$id)->leftjoin('vps', 'paymentvps.vpsId', 'vps.id')->leftjoin('catvps', 'vps.catVpsId', 'catvps.id')->select('paymentvps.id', 'catvps.nmVps', 'vps.price')->first();
      Cart::add($cart->id, $cart->nmVps, 1, $cart->price, ['type'=>'unpaidvps']);
      // dd(Cart::content());
      // dd('dd');
      return redirect()->route('checkoutVpsPage');
  }
  public function addCartUnpaidInstall($id)
  {
    // dd($id);

      Cart::destroy();
      $cart = DB::table('paymentinstall')->where('id',$id)->first();
      Cart::add($cart->id, 'Install', $cart->quotaTotal, $cart->priceTotal, ['type'=>'unpaidinstall']);
      // dd(Cart::content());
      // dd('dd');
      return redirect()->route('checkoutInstallPage');
  }

  public function addCartRender($id){
    Cart::destroy();
    $cart = DB::table('package')->where('id',$id)->first();
    //ganti cart->slug
    Cart::add($cart->id, $cart->nmPackage, 1, $cart->price);
    $datas = DB::table('package')->get();
    $datasLength = $datas->count();
    $view = view('pages-v2.checkout-ajax',compact('cart', 'datas', 'datasLength'))->render();

    $orderDetail = view('pages-v2.detail-order-ajax')->render();

    //dd(Cart::content(),$view);

    return response()->json(['html'=>$view, 'orderDetail' => $orderDetail]);
  }

  public function addCartVpsRender($id){
    Cart::destroy();
    $cart = DB::table('vps')->where('vps.id',$id)
           ->join('catvps', 'vps.catVpsId', 'catvps.id')
           ->select('vps.id', 'catvps.nmVps', 'vps.interval', 'vps.price', 'vps.slug')
           ->first();
    // dd($cart);
    Cart::add($cart->id, $cart->nmVps, $cart->interval, $cart->price, ['type'=>'vps', 'slug'=>$cart->slug]);

    $datas = \DB::table('vps')->select('catvps.id', 'catvps.nmVPS', 'catvps.detail')->leftJoin('catvps','catvps.id','vps.catVpsId')->groupBy('catvps.id')->get();
        foreach($datas as $key => $data){
            $value = \DB::table('vps')->select('vps.id as vpsId', 'vps.interval','vps.price','vps.slug')->where('catVpsId',$data->id)->get();
            $data->vps=$value;
        }
    $view = view('pages-v2.checkout-vps-ajax',compact('datas'))->render();

    $orderDetail = view('pages-v2.detail-order-vps-ajax', compact('datas'))->render();

    //dd(Cart::content(),$view);

    return response()->json(['html'=>$view, 'orderDetail'=>$orderDetail]);
  }

  public function removeFromCart($rowId){

    Cart::remove($rowId);
    
    return redirect()->back();
  }

  public function addService($requestService,$rowId){
    //dd(Cart::content(),Cart::get($rowId));
    $cart = Cart::get($rowId);
    $existing = $cart->options;
    $jasa = DB::table('jasa')->where('jasaStatus','active')->get();
    $additional = $cart->options->service;
    $promo = $cart->options->promo;
    $periode = $cart->options->periode;

    foreach ($jasa as $service) {
      if($service->jasaPeriode == "monthly"){
        $kali = $periode['kali'];
      }else{
        $kali = 1;
      }
      if($service->jasaName == $requestService){
        $additional[$service->jasaName] = $service->jasaPrice * $kali;
      }
    }

    $ganti = ['periode' => $periode, 'service' => $additional,'promo' => $promo];

    $next = Cart::update($rowId,['options' => $ganti]);

    $view = view('web_pages.pages.cart-ajax',compact('jasa'))->render();

    //dd(Cart::content(),$view);

    return response()->json(['html'=>$view]);

  }

  public function removeService($requestService,$rowId){
    //dd(Cart::content(),Cart::get($rowId));
    $cart = Cart::get($rowId);
    $existing = $cart->options;
    $jasa = DB::table('jasa')->where('jasaStatus','active')->get();
    $additional = $cart->options->service;
    $promo = $cart->options->promo;
    $periode = $cart->options->periode;

    foreach ($jasa as $service) {
      if($service->jasaName == $requestService){
        $additional[$service->jasaName] = 0;
      }
    }

    $ganti = ['periode' => $periode,'service' => $additional,'promo' => $promo];

    $next = Cart::update($rowId,['options' => $ganti]);

    $view = view('web_pages.pages.cart-ajax',compact('jasa'))->render();

    return response()->json(['html'=>$view]);

  }

  public function rentPeriode($requestService,$rowId){
    $cart = Cart::get($rowId);
    $robot = DB::table('robots')->where('id',$cart->id)->first();

    switch ($requestService) {
      case '1': $price = $robot->robotPrice1;$sebutan= '1 bulan';$kali = 1;break;
      case '6': $price = $robot->robotPrice2*6;$sebutan= '6 bulan';$kali = 6;break;
      case '12': $price = $robot->robotPrice3*12;$sebutan= '1 tahun';$kali = 12;break;

      default:
        $price = $robot->robotPrice1;
        $sebutan = '1 bulan';
        $kali = 1;
        break;
    }

    $promo = $cart->options->promo;

    $jasa = DB::table('jasa')->where('jasaStatus','active')->get();
    $additional = $cart->options->service;
    foreach ($jasa as $service) {
      if($service->jasaPeriode != "monthly"){
        $multi = 1;
      }else{
        $multi = $kali;
      }
      if ($additional[$service->jasaName] != 0) {
        $additional[$service->jasaName] = $service->jasaPrice * $multi;
      }

    }

    $periode = [
      'kali' => $kali,
      'sebutan' => $sebutan,
      'harga' => $price
    ];

    $ganti = ['periode'=>$periode,'service' => $additional,'promo' => $promo];
    //dd($ganti);
    $next = Cart::update($rowId,['price' => $price, 'options' => $ganti]);
    $view = view('web_pages.pages.cart-ajax',compact('jasa'))->render();

    return response()->json(['html'=>$view]);
  }

  public function kodeVoucher($kode){
    foreach (Cart::content() as $cart) {
      $jasa = DB::table('jasa')->where('jasaStatus','active')->get();

      $service = $cart->options->service;
      $promo = $cart->options->promo;
      $periode = $cart->options->periode;

      $voucher = DB::table('vouchers')->where('kodeVoucher',$kode)
                ->where('expireVoucher','>',date('Y-m-d H:i:s'))
                ->where('sisaVoucher','>',0)
                ->first();

      $promo = [
        'voucher' => $kode,
        'amount' => $voucher->diskonVoucher
      ];
      $ganti = ['periode'=>$periode,'service' => $service,'promo' => $promo];

      $next = Cart::update($cart->rowId,['options' => $ganti]);
      $view = view('web_pages.pages.cart-ajax',compact('jasa'))->render();

      return response()->json(['html'=>$view]);
    }
  }

  public function removeVoucher(){
    foreach (Cart::content() as $cart) {
      $jasa = DB::table('jasa')->where('jasaStatus','active')->get();

      $service = $cart->options->service;
      $promo = $cart->options->promo;
      $periode = $cart->options->periode;

      $promo = [
        'voucher' => 'none',
        'amount' => 0
      ];
      $ganti = ['periode'=>$periode,'service' => $service,'promo' => $promo];

      $next = Cart::update($cart->rowId,['options' => $ganti]);
      $view = view('web_pages.pages.cart-ajax',compact('jasa'))->render();

      return response()->json(['html'=>$view]);
    }
  }

  public function processCheckout(){
    if(Cart::count()>0){
      foreach (Cart::content() as $n => $cart){
        $biayaService = 0;
        $jasa = DB::table('jasa')->get();
        foreach ($jasa as $service){
          if($cart->options->service[$service->jasaName] != 0){
            $biayaService += $cart->options->service[$service->jasaName];
          }
        }
        $totalService += $biayaService;
        $totalRobot += $cart->price;
        if ($cart->options->promo['amount']>0) {
          $potongan = $cart->options->promo['amount'];
          $kodenya = $cart->options->promo['voucher'];
        }
      }
      $subtotal = $totalRobot + $totalService;
      $diskon = $potongan * $totalRobot * 0.01;
      $total = $subtotal - $diskon;

      $insert = DB::table('sales')->insertGetId([
        'salesCode' => 'SW-'.CRUDBooster::myID().'-'.$cart->id.'-'.time(),
        'user_id' => CRUDBooster::myID(),
        'salesSart' => serialize(Cart::content()) ,
        'salesSubtotal' => $subtotal,
        'salesDiscount' => $diskon,
        'salesVoucher' => $kodenya,
        'salesTotal' => $total,
        'salesStatus' => 'menunggu pembayaran',
        'salesTime' => date('Y-m-d H:i:s'),
        'salesPaymentStatus' => 'menunggu',
        'salesDownloadStatus' => 'menunggu'
      ]);

      Cart::destroy();
      return redirect()->route('checkout');
    }else{
      return redirect()->route('dashboard');
    }
  }

  public function userLogout(){
    Session::flush();
    return redirect()->route('webPagesIndex');
  }

  // Controller Eza
  public function postRegister(Request $request)
  {
    // dd($request);
    $validator = $request->validate([
      'fullName' => 'required|string|min:3',
      'email' => 'required|email|unique:cms_users,email',
      'password' => 'required|min:6|confirmed',
      'password_confirmation' =>'required|same:password',
      'disclaimer' => 'required'
    ],
    [
      'fullName.min' => 'Full Name must containt at least 3 characters',
      'email.unique' => 'The Email has already been taken',
      'password.min' => 'Password must containt at least 6 characters',
      'password_confirmation.same' => 'Password are not matching',
    ]);
      $username = explode("@", $request->email);
      // dd($username[0]);
      $photo = 'uploads/46/2020-11/user_default.jpg';
    $insert = DB::table('cms_users')->insertGetId([
      'name' => $request->fullName,
      'username'=>$username[0],
      'email' => $request->email,
      'password' => Hash::make($request->password),
      'id_cms_privileges' => '2',
      'created_at' => date('Y-m-d H:i:s'),
      'photo'=>$photo,
    ]);

    if($insert){

      $users = DB::table(config('crudbooster.USER_TABLE'))->where("id", $insert)->first();
      $priv = DB::table("cms_privileges")->where("id", $users->id_cms_privileges)->first();

      $roles = DB::table('cms_privileges_roles')->where('id_cms_privileges', $users->id_cms_privileges)->join('cms_moduls', 'cms_moduls.id', '=', 'id_cms_moduls')->select('cms_moduls.name', 'cms_moduls.path', 'is_visible', 'is_create', 'is_read', 'is_edit', 'is_delete')->get();

      $photo = ($users->photo) ? asset($users->photo) : asset('vendor/crudbooster/avatar.jpg');
      Session::put('admin_id', $users->id);
      Session::put('admin_is_superadmin', $priv->is_superadmin);
      Session::put('admin_name', $users->name);
      Session::put('admin_photo', $photo);
      Session::put('admin_privileges_roles', $roles);
      Session::put("admin_privileges", $users->id_cms_privileges);
      Session::put('admin_privileges_name', $priv->name);
      Session::put('admin_lock', 0);
      Session::put('theme_color', $priv->theme_color);
      Session::put("appname", CRUDBooster::getSetting('appname'));

      CRUDBooster::insertLog(trans("crudbooster.log_login", ['email' => $users->email, 'ip' => $request->ip()]));

      $cb_hook_session = new \App\Http\Controllers\CBHook;
      $cb_hook_session->afterLogin();
      if($users->id_cms_privileges == 2 && session('checkout')){
        Session::forget('checkout');
        return redirect()->route("checkoutPage");
      }else{
        return redirect()->route('dashboard');
      }
    }else{
      // dd('login gagal');
      return redirect()->back()->withInput();
    }
  }
  //function andika
  public function getVoucher($kode){
    $voucher = DB::table('vouchers')->where('kodeVoucher',$kode)
    ->where('expireVoucher','>',date('Y-m-d H:i:s'))
    ->where('sisaVoucher','>',0)
    ->first();
    return response()->json($voucher);
  }


  // function Dimas
  public function postChat(Request $request){
    DB::table('report')
      ->insert(['ticketId' => $request->input('ticketId'), 'userId' => CRUDBooster::myID(),'chat' => $request->input('chatContent')]);
  
      $chatData = DB::table('report') 
      ->where('report.ticketId', $request->input('ticketId'))
      ->leftjoin('cms_users','report.userId', 'cms_users.id')
      ->select('cms_users.id_cms_privileges', 'report.chat', 'report.created_at')
      ->orderBy('report.id','desc')
      ->first();
    
    return response()->json(['chatData'=>$chatData, 'ticketId'=>$request->input('ticketId')]);
  }

  //Function from andika
  public function useVoucher($kode){
      if(Cart::content()->isNotEmpty()){
          foreach (Cart::content() as $cart) {
            $jasa = DB::table('jasa')->where('jasaStatus','active')->get();
      
            $service = $cart->options->service;
            $promo = $cart->options->promo;
            $periode = $cart->options->periode;
      
            $voucher = DB::table('vouchers')->where('kodeVoucher',$kode)
                      ->where('expireVoucher','>',date('Y-m-d H:i:s'))
                      ->where('sisaVoucher','>',0)
                      ->first();
      
            $promo = [
              'voucher' => $kode,
              'amount' => $voucher->diskonVoucher
            ];
            $ganti = ['periode'=>$periode,'service' => $service,'promo' => $promo];
      
            $next = Cart::update($cart->rowId,['options' => $ganti]);
          }
      }
      return redirect()->route('cart');
  }

  //Function from andika
  public function buyAgain($id){
    $checkout = DB::table('sales')
    ->where('user_id',CRUDBooster::myId())
    ->where('salesStatus','menunggu pembayaran')
    ->where('id',$id)
    ->orderby('id','desc')
    ->first();
    $robot = unserialize($checkout->salesSart);
    foreach($robot as $robots){
      // dd($robots->name->robotSlugName);
      $cart = DB::table('robots')->where('robotSlugName',$robots->name->robotSlugName)->first();
      $jasa = DB::table('jasa')->where('jasaStatus','active')->get();
      foreach ($jasa as $service) {
          $additional[$service->jasaName] = 0;
      }
      $promo = [
        'voucher' => 'none',
        'amount' => 0
      ];
      $periode = [
        'kali' => 1,
        'sebutan' => '1 bulan',
        'harga' => $cart->robotPrice1
      ];
      Cart::add($cart->id, $cart, 1, $cart->robotPrice1, ['periode' => $periode, 'service' => $additional,'promo' => $promo]);
    }
    return redirect()->route('cart');
  }

  public function downloadRobot($id, $code, $generate){
  // public function downloadRobot(){
    // dd($id, $code);
    
    $userId = CRUDBooster::myID();
    $robotId = $id;
    $generateCode = $generate;
    // $userId = 89;
    // $generateCode = 'yes';
    // $robotId = 11;
    if($generateCode == 'yes'){
      // cek all the license code that user have for the robot
      $userRobotCode = DB::table('robotlicense')->where('userId', $userId)->where('robotId', $robotId)->distinct()->select('robotLicense')->get();
      $userLicenseCode = DB::table('robotlicense')->where('userId', $userId)->distinct()->select('robotLicense')->get();
      // dd($userLicenseCode, $userRobotCode);
      if($userRobotCode->count() == 0 && $userLicenseCode->count() == 0 || $userRobotCode->count() == $userLicenseCode->count() && $userLicenseCode->count() < 3){
        $insertLicenseCode = DB::table('robotlicense')->insertGetId([
          'userId'=> $userId,
          'robotId'=> $robotId,
          'robotLicense'=> $code,
        ]);
        $getLicenseAndName = DB::table('robotlicense')->where('robotlicense.id', $insertLicenseCode)->leftjoin('robots', 'robotlicense.robotId', 'robots.id')->first();
        $insertIntoTrader = DB::table('trader')->insert([
          "userId"=>$userId,
          "licenseKey"=>$getLicenseAndName->robotLicense,
          "robotName"=>$getLicenseAndName->robotSlugName,
          "status"=>'waiting',
          "created_at"=>date('Y-m-d H:i:s')
        ]);
      }elseif ($userRobotCode->count() < $userLicenseCode->count() && $userLicenseCode->count() <= 3){
        // dd('masuk lagi');
        $qtyRobotLicense = $userRobotCode->count();
        // dd($qtyRobotLicense, $userLicenseCode[$qtyRobotLicense]->robotLicense);
        $insertLicenseCode = DB::table('robotlicense')->insertGetId([
          'userId'=> $userId,
          'robotId'=> $robotId,
          'robotLicense'=> $userLicenseCode[$qtyRobotLicense]->robotLicense,
        ]);
        $getLicenseAndName = DB::table('robotlicense')->where('robotlicense.id', $insertLicenseCode)->leftjoin('robots', 'robotlicense.robotId', 'robots.id')->first();
        $insertIntoTrader = DB::table('trader')->insert([
          "userId"=>$userId,
          "licenseKey"=>$getLicenseAndName->robotLicense,
          "robotName"=>$getLicenseAndName->robotSlugName,
          "status"=>'waiting',
          "created_at"=>date('Y-m-d H:i:s')
        ]);
      }else{
        // dd('else');
        return redirect()->route('myrobot');
      }
    }
    
    // condition untuk max 3x generate license

    $url = DB::table('robots')->where('id', $robotId)->first();
    if($url){
      $file_path = storage_path().'/app/'.$url->robotFile;

      if(file_exists($file_path)){
        // send download
          return \Response::download($file_path,$url->robotLongName.'.ex4', [
              'Content-Type: application/ex4',
          ]);
      }else {
          // Error
            return redirect()->route('myrobot');
      }
    }else{
      return redirect()->route('myrobot');
    }
  }

  public function submitComment(Request $request)
  { 
    $userId = CRUDBooster::myID();
    // dd($request);
    // $userId = 89;
    $insertComment = DB::table('comment')->insertGetId([
      'blogId' => $request->input('blogId'),
      'userId' => $userId,
      'comment' => $request->input('comment'),
      'created_at' => date('Y-m-d H:i:s')
    ]);

    $comment = DB::table('comment')
    ->where('comment.id', $insertComment)
    ->leftjoin('cms_users', 'comment.userId', 'cms_users.id')
    ->select('comment.id','comment.comment', 'comment.created_at', 'comment.likesCount', 'comment.dislikesCount', 'cms_users.name', 'cms_users.photo', 'cms_users.providerOrigin')
    ->first();
    $comment->time = Carbon::createFromTimeStamp(strtotime($comment->created_at))->diffForHumans();
    return response()->json($comment);
  }

  public function like(Request $request) 
  {
    $commentId = $request->input('commentId');
    $like = $request->input('like');
    // return response()->json($tes);  

    // dd($request);
    $userId = CRUDBooster::myID();
    // $userId = 89;
    $cekLike = DB::table('likes')->where('userId', $userId)->where('commentId', $commentId)->first();
    // dd($cekLike);
    if($cekLike == null){
      $insertLike = DB::table('likes')->where('commentId', $commentId)->insert([
        'userId'=>$userId,
        'commentId'=>$commentId,
        'like'=>$like,
      ]);
      if($like == 1){
        $data = 'like';
      }else {
        $data = 'dislike';
      }
      // dd('masuk');
    }elseif($cekLike->like == $like){
      DB::table('likes')->where('userId', $userId)->where('commentId',$commentId)->delete();
      if($like == 1){
        $data = 'unlike';
      }else {
        $data = 'undislike';
      }
      // dd('sana');
    }else {
      $updateLike = DB::table('likes')->where('userId', $userId)->where('commentId', $commentId)->update([
        'like'=>$like,
        ]);
        if($like == 1){
          $data = 'like';
        }else {
          $data = 'dislike';
        }
        // dd('sini');
      }
      return response()->json($data);  
  }

  public function blogAjax(Request $request){
    if($request->ajax()){
      if ($request->query('slug') == 'all'){
        // most read
        $mostRead=DB::table('blogs')->where('blogs.blogStatus','active')->join('cms_users','cms_users.id','blogs.blogAuthor')->select('blogs.id as blogId', 'blogs.blogTitle', 'blogs.blogSlug', 'blogs.blogContent', 'blogs.blogImage1', 'blogs.blogAuthor', 'blogs.blogStatus', 'cms_users.id', 'cms_users.name', 'cms_users.photo')->orderBy('blogRead','desc')->first();
        $totalWord=str_word_count($mostRead->blogContent);
        $mostReadTime=$totalWord/255;
        $mostReadTime=ceil($mostReadTime);
        $readingTime=$mostReadTime;

        
        return response()->json([$mostRead, $readingTime]);
      };
      
    }
  } 

  public function blogCardAjax (Request $request){
    // if($request->ajax()){
      if ($request->query('slug') == 'all'){
        $mostRead=DB::table('blogs')->where('blogs.blogStatus','active')->join('cms_users','cms_users.id','blogs.blogAuthor')->select('blogs.id as blogId', 'blogs.blogTitle', 'blogs.blogSlug', 'blogs.blogContent', 'blogs.blogImage1', 'blogs.blogAuthor', 'blogs.blogStatus', 'cms_users.id', 'cms_users.name', 'cms_users.photo')->orderBy('blogRead','desc')->first();
        $exclude = [$mostRead->blogId];
        $slug = $request->query('slug');
        $getAllBlog = DB::table('blogs')->join('cms_users','cms_users.id','blogs.blogAuthor')->select('blogs.id as blogId', 'blogs.blogTitle', 'blogs.blogSlug', 'blogs.blogContent', 'blogs.blogImage1', 'blogs.blogAuthor', 'blogs.blogStatus', 'cms_users.id', 'cms_users.name', 'cms_users.photo')->get();
        $blogsData = collect($getAllBlog)->whereNotIn('blogId',$exclude);
        foreach ($blogsData as $key => $value) {
            $totalWord=str_word_count($value->blogContent);
            $readTime=$totalWord/255;
            $readTime=ceil($readTime);
            $readingTime=$readTime;
            $blogsData[$key]->readingTime = $readingTime;
        }
        $finalBlogsData = $this->paginate3($blogsData);
        // return response()->json($finalBlogsData);
        // dd($finalBlogsData);
        return view('pages-v2.blog-blade-card', compact('finalBlogsData'));
      }

    // }
  }

  public function paginate2($items, $perPage = 9, $page = null, $options = [])
  {
      $currentPage = LengthAwarePaginator::resolveCurrentPage();
      $currentResults = $items->slice(($currentPage - 1) * $perPage, $perPage)->all();
      $results = new LengthAwarePaginator($currentResults, $items->count(), $perPage);
      $results->withPath('blog')->links();

      return $results;
  }
  public function paginate3($items, $perPage = 9, $page = null, $options = [])
  {
      $currentPage = LengthAwarePaginator::resolveCurrentPage();
      $currentResults = $items->slice(($currentPage - 1) * $perPage, $perPage)->all();
      $results = new LengthAwarePaginator($currentResults, $items->count(), $perPage);
      $results->withPath('blogsAll')->links();

      return $results;
  }

  public function loginSession() {
    Session::put('checkout', 'true');
    return 'success';
  }
  
}

