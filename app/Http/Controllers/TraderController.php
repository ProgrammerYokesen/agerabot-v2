<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class TraderController extends Controller
{
  public function dashboard(){

    return view('web_pages.pages.dashboard');
  }

  public function upsertTrader(Request $request){

    $check = DB::table('trader')->where('account',$request->account)->where('server',$request->server)->where('pair',$request->pair)->first();

    if(count($check) <1){
      $insert = DB::table('trader')->insert([
        'broker' => $request->broker,
        'server' => $request->server,
        'account' => $request->account,
        'pair' => $request->pair,
        'status' => 'inactive',
      ]);

      return ('Registering to Signal Yokesen, Success');
    }else{
      return ('Robot Has Been Registered to Signal Yokesen');
    }
  }

  public function verifyToTrade(Request $request){
    $check = DB::table('trader')->where('account',$request->username)->where('server',$request->server)->first();
    return ($check->status);
  }
}
