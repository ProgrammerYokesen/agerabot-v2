<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class RobotController extends Controller
{
  public function masterLuke(Request $request){

    if($request['method'] == "initiate"){
            $check =  DB::table('trader')->where('server',$request['server'])
                    ->where('account',$request['account'])
                    ->count();


            if($check > 0){
              $return = "Either active or suspend, but you are already registered";
            }else{
              $insert = DB::table('trader')->insert([
                'broker' => $request['broker'],
                'server' => $request['server'],
                'account' => $request['account'],
                'pair' => $request['pair'],
                'status' => 'active'
              ]);
              $return = "Server Receive Request to Trade, Wait Admin to Approve";
            }

            return $return;
        }elseif($request['method'] == "cantrade"){
          $get = DB::table('trader')->where('server',$request['server'])
                  ->where('account',$request['account'])
                  ->first();
          $status = $get->status;
        return $status;
        }elseif($request['method'] == "checkprofit"){

          $check = DB::table('trade_profit')->where('server',$request['server'])
          ->where('login',$request['account'])
          ->where('bln',date('m'))
          ->where('thn',date('Y'))
          ->count();

          if($check<1){
            $return = "notyet";
          }else{
            $return = "already";
          }
          return $return;
        }elseif($request['method'] == "insertprofit"){

          $insert = DB::table('trade_profit')->insert([
            'server' => $request['server'],
            'login' => $request['account'],
            'bln' => date('m'),
            'thn' => date('Y'),
            'profit' => $request['profit'],
            'fee' => $request['profit'] * 0.2,
            'lunas' => 'no'
          ]);
          $return = "Reporting Last Month Profit";

          return $return;
        }elseif($request['method'] == "pushTrade"){
          $insert = DB::table('trade')->insert([
            'ticket' => $request['ticket'],
            'op' => $request['op'],
            'pair' => $request['pair'],
            'magicNumber' => $request['magicNumber'],
            'status' => 'open'
          ]);

          $data = DB::table('trade')->where('status','closed')->orderby('update_at','desc')->limit(7)->get();
          foreach ($data as $value) {
            if($value->profit > 0){
              $follow++;
            }
            if($value->profit < 0){
              $againts++;
            }
          }

          $equity = DB::table('equity')->orderby('id','desc')->first();

          if($equity->type == "follow"){
            //type = follow
            if($request['op'] == "OP_BUY"){
              $trade = "OP_BUY";
            }elseif($request['op'] == "OP_SELL"){
              $trade = "OP_SELL";
            }
          }else{
            //type = againts
            if($request['op'] == "OP_BUY"){
              $trade = "OP_SELL";
            }elseif($request['op'] == "OP_SELL"){
              $trade = "OP_BUY";
            }
          }

          $clients = DB::table('trader')->where('status','active')->where('master','no')->get();

          foreach($clients as $client){
            $insert_client = DB::table('trade_client')->insert([
              'id_client' => $client->account,
              'ticket' => 0,
              'op' => $trade,
              'pair' => $request['pair'],
              'magicNumber' => $request['ticket'],
              'status' => 'new'
            ]);
          }

          $return = "Trade OP listed";

          return $return;
        }elseif($request['method'] == "listTicket"){


          $twodayago = date('Y-m-d H:i:s',strtotime(' - 8 hours'));
          $last = DB::table('equity')->where('update_at','<',$twodayago)->orderby('id','desc')->first();
          if($request->equity > $last->equity){
            $type = "follow";
          }else{
            $type = "againts";
          }

          $insert = DB::table('equity')->insert([
            'mt4id' => $request->mt4id,
            'type' => $type,
            'equity' => $request->equity,
            'balance' => $request->balance,
            'equity_before' => $last->equity,
            'balance_before' => $last->balance
          ]);


          $delete = DB::table('equity')->where('id','<',$last->id)->delete();

          $tickets = DB::table('trade')->where('status','open')->select('ticket')->get();
          foreach ($tickets as $key => $value) {
            $tiket .= $value->ticket."|";
          }
          $return = $tiket;

          return $return;
        }elseif($request['method'] == "closeTicket"){
          $update = DB::table('trade')->where('ticket',$request['ticket'])->update([
            'status'=>'closed',
            'profit' => $request->profit
          ]);

          $update_client = DB::table('trade_client')->where('magicNumber',$request['ticket'])->where('status','open')->update([
            'status'=>'closing'
          ]);

          if($update){
            $status = "okay";
          }else{
            $status = ".";
          }

          return $status;
        }elseif($request['method'] == "listNewPosition"){
          $value = DB::table('trade_client')->where('id_client',$request['account'])->where('status','new')->first();
          $pair = explode(".",$value->pair);
          $trade = $value->op;

          if($value){
            $position = $value->magicNumber."|".$trade."|".$pair[0];
          }else{
            $position = "0|0|0";
          }

          $return = $position;

          return $return;
        }elseif($request['method'] == "NewTrade"){
          $update = DB::table('trade_client')->where('id_client',$request['account'])->where('magicNumber',$request['magicNumber'])->update([
            'status'=>'open',
            'ticket' => $request['ticket']
          ]);
          if($update){
            $status = "okay";
          }else{
            $status = ".";
          }

          return $status;
        }elseif($request['method'] == "WrongSymbol"){
          $update = DB::table('trade_client')->where('id_client',$request['account'])->where('magicNumber',$request['magicNumber'])->update([
            'status'=>'No Symbol'
          ]);
          if($update){
            $status = "okay";
          }else{
            $status = ".";
          }

          return $status;
        }elseif($request['method'] == "closePositionClient"){
          $closethis = DB::table('trade_client')->where('id_client',$request['account'])->where('status','closing')->select('ticket')->first();
          if ($closethis) {
            $return = $closethis->ticket;
          }else{
            $return = 0;
          }


          return $return;
        }elseif($request['method'] == "updateClosingan"){

          $update_client = DB::table('trade_client')->where('ticket',$request['ticket'])->update([
            'status'=>'closed',
            'profit' => $request['profit']
          ]);

          if($update){
            $status = "okay";
          }else{
            $status = "Error waktu closing";
          }

          return $status;
        }

      }

      public function auto_expire(){
        $date = date('Y-m-d H:i:s',strtotime('-1 month'));

        $expire = DB::table('trade_client')->where('created_at','<',$date)->where('status','Active')->get();
        foreach($expire as $time){
          $close = DB::table('trade_client')->where('id',$time->id)->update([
            'status' => 'expired'
          ]);
        }
      }

      public function validate_license(Request $request){

        if($request['method'] == "initiate"){

          $check =  DB::table('trader')->where('licenseKey',$request['license'])->where('robotName',$request['name'])->count();
          $license = DB::table('trader')->where('licenseKey',$request['license'])->where('robotName',$request['name'])->first();
          $status = $license->status;
          $broker = $request['broker'];
          $server = $request['server'];
          $account = $request['account'];
          $pair = $request['pair'];

          if($check > 0){

            if($status == 'Waiting'){
              $update = DB::table('trader')->where('licenseKey',$request['license'])->where('robotName',$request['name'])->update([
                'broker' => $request['broker'],
                'server' => $request['server'],
                'account' => $request['account'],
                'pair' => $request['pair'],
                'status' => 'Active'
              ]);

              $return = "Server $server and Account $account updated. Now you are good to go to TRADE on $pair";
            }elseif($status == 'Active'){
              $usage = DB::table('trader')->where([
                'licenseKey' => $request['license'],
                'robotName' => $request['name'],
                'broker' => $request['broker'],
                'account' => $request['account'],
                'status' => 'Active'
                ])->count();

                if($usage == 1){
                  $return = "Your license is ACTIVE";
                }else{
                  $return = "Your license is ALREADY USED on server $license->broker with account $license->account on pair $license->pair please buy another license";
                }
              }elseif($status == 'Suspended'){
                $return = "Your license is SUSPENDED";
              }elseif($status == 'Expired'){
                $return = "Your license is EXPIRED please pay your SUBSCRIPTION";
              }

            }else{
              $return = "Your license key is INVALID";
            }

            return $return;

          }elseif($request['method'] == "cantrade"){

            $usage = DB::table('trader')->where([
              'licenseKey' => $request['license'],
              'robotName' => $request['name'],
              'broker' => $request['broker'],
              'account' => $request['account'],
              'status' => 'Active'
              ])->count();

              if($usage == 1){
                $return = "Active";
              }else{
                $return = "Not-Valid-Server-Account-And-Pair";
              }
              return $return;
            }

      }
    }
