<?php

namespace App\Http\Controllers\Auth;

use Socialite, DB, Crudbooster, Session, Input, Cookie;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Jenssegers\Agent\Agent;
use Illuminate\Http\Request;
use Cart;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function redirectToFacebook($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    public function handleFacebookCallback($provider)
    {
        $users = Socialite::driver($provider)->user();
        //dd($users);
        $exist = DB::table(config('crudbooster.USER_TABLE'))->where('email',$users->email)->first();

        $agent = new Agent();
        if(count($exist) < 1){
          $insert = DB::table(config('crudbooster.USER_TABLE'))->insertGetId([
            'name' => $users->name,
            'email' => $users->email,
            'photo' => $users->avatar,
            'id_cms_privileges' => '2',
            'providerId' => $users->id,
            'providerOrigin' => $provider,
            'parent' => Cookie::get('parent'),
            'utm_source' => Cookie::get('utm_source'),
            'utm_medium' => Cookie::get('utm_medium'),
            'utm_campaign' => Cookie::get('utm_campaign'),
            'utm_term' => Cookie::get('utm_term'),
            'utm_content' => Cookie::get('utm_content'),
            'uuid' => Cookie::get('uuid'),
            'ipaddress' => Cookie::get('ipaddress'),
            'screen' => Cookie::get('screen'),
            'platform' => Cookie::get('platform'),
            'platformVersion' => Cookie::get('platformVersion'),
            'device' => Cookie::get('device'),
            'browser' => Cookie::get('browser'),
            'browserVersion' => Cookie::get('browserVersion'),
            'language' => Cookie::get('language'),
            'robot' => Cookie::get('robot')
          ]);
          $id = $insert;
        }else{
          $update = DB::table(config('crudbooster.USER_TABLE'))->where('id',$exist->id)->update([
            'name' => $users->name,
            'email' => $users->email,
            'photo' => $users->avatar,
            'providerId' => $users->id,
            'providerOrigin' => $provider,
            'id_cms_privileges' => '2'
          ]);
          $id = $exist->id;
        }


        $priv = DB::table("cms_privileges")->where("id", "2")->first();

        $roles = DB::table('cms_privileges_roles')->where('id_cms_privileges', "2")->join('cms_moduls', 'cms_moduls.id', '=', 'id_cms_moduls')->select('cms_moduls.name', 'cms_moduls.path', 'is_visible', 'is_create', 'is_read', 'is_edit', 'is_delete')->get();

        $photo = $users->avatar;
        Session::put('admin_id', $id);
        Session::put('admin_is_superadmin', $priv->is_superadmin);
        Session::put('admin_name', $users->name);
        Session::put('admin_photo', $photo);
        Session::put('admin_privileges_roles', $roles);
        Session::put("admin_privileges", $users->id_cms_privileges);
        Session::put('admin_privileges_name', $priv->name);
        Session::put('admin_lock', 0);
        Session::put('theme_color', $priv->theme_color);
        Session::put("appname", CRUDBooster::getSetting('appname'));
        Session::put("email", $users->email);

        CRUDBooster::insertLog(trans("crudbooster.log_login", ['email' => $users->email, 'ip' => \Request::ip()]));

        $cb_hook_session = new \App\Http\Controllers\CBHook;
        $cb_hook_session->afterLogin();

        if(Cart::count()>0){
          return redirect()->route('processCheckout');
        }else{
          return redirect()->route('collection');
        }

    }

    public function handleFacebookFailed($provider)
    {
      dd("failed");
    }

    public function handleFacebookUnsubscribe($provider)
    {
      dd("unsubscribe");
    }
}
