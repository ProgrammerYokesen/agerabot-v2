<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    public function robotList() {
        return view('user_dashboard.dashboardApp');
    }
}
