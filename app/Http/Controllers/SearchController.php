<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use CRUDBooster;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use Illuminate\Pagination\Paginator;
use carbon\carbon;
// use Validator;


class SearchController extends Controller
{
    public function statistic_robot($periode,$type,$robot){
        $pullPair = DB::table('robotStatistic')->distinct()->select('stat_pair')->get();
        $pairArray = [];
        foreach ($pullPair as $key => $value) {
            // dd($value);
            array_push($pairArray, $value->stat_pair);
        };
        
        $limit = count($pairArray);
            $pullRobot = DB::table('robotstatistic')->where('stat_robot', $robot)->whereIn('stat_pair', $pairArray)->orderby('id','desc')->limit($limit)->get();
        
        $sort = 'month_percent';
  
        $array = collect($pullRobot)->sortBy('stat_monthly')->reverse()->toArray();
        return response()->json($array);
    }
    public function robotSearch(Request $request) {
        // dd($request->query('query'));
        
        // if($request->has('q') && $request->has('pair')) {
        if($request->ajax()) {
            // $cari = 'a';
            // $pair = 'xauusd';
            $userId = CRUDBooster::myId();
            $pullPair = DB::table('robotStatistic')->distinct()->select('stat_pair')->get();
            // $pair = $request->pair;
            $sorting = $request->query('sort');
            $cari = $request->query('query');
            $type = 'percent';
            $periode = 'month';
            $userId = CRUDBooster::myId();
            
            $getRobots = DB::table('robots')
                ->where('robotLongName', 'LIKE', '%'.$cari.'%')
                ->leftjoin('robotStatistic', 'robotStatistic.stat_robot', 'robots.id')
                ->where('robotStatistic.stat_robot', '!=' , 0)
                ->distinct()->select('stat_robot')->get();      
            // ->where('robotLongName', 'LIKE', '%'.$cari.'%')->get();
            // dd($getRobots);
            foreach ($getRobots as $robot) {
                // dd($robot);
                $result = $this->statistic_robot($type,$periode,$robot->stat_robot);
                // dd($result);
                $data = $result->getData();
                // dd($data);

                $urut = [];
                foreach ($data as $key => $value) {
                    // dd($value);
                    $urut[] = $value;
                }
                // $robot = DB::table('robots')->where('id',$urut[0]->robot)->first();
                $robot = DB::table('robots')->where('id',$urut[0]->stat_robot)->first();
                $time = strtotime($robot->created_at);
                if($userId == null){
                    $dataRobot[] = [
                        'robot'=>$robot->id, 
                        'pair'=>$urut[0]->stat_pair,
                        'last_percent'=>$urut[0]->stat_lastten,
                        'last_total'=>$urut[0]->stat_totalten,
                        'week_percent'=>$urut[0]->stat_weekly,
                        'week_total'=>$urut[0]->stat_totalweekly,
                        'month_percent'=>$urut[0]->stat_monthly,
                        'month_total'=>$urut[0]->stat_totalmonthly,
                        'year_percent'=>$urut[0]->stat_yearly,
                        'year_total'=>$urut[0]->stat_totalyearly,
                        'robotName' => $robot->robotLongName,
                        'robotImage' => $robot->robotImage,
                        'tag' => $robot->robotSlugName,
                        'created_at' => $time,
                    ];
                }else {
                    $checkSubs = DB::table('wishlist')
                    ->where('userId', $userId)
                    ->where('robotId', $robot->id)
                    ->first();
                    // dd($checkSubs);
                    if($checkSubs == null){
                        $dataRobot[] = [
                            'robot'=>$robot->id, 
                            'pair'=>$urut[0]->stat_pair,
                            'last_percent'=>$urut[0]->stat_lastten,
                            'last_total'=>$urut[0]->stat_totalten,
                            'week_percent'=>$urut[0]->stat_weekly,
                            'week_total'=>$urut[0]->stat_totalweekly,
                            'month_percent'=>$urut[0]->stat_monthly,
                            'month_total'=>$urut[0]->stat_totalmonthly,
                            'year_percent'=>$urut[0]->stat_yearly,
                            'year_total'=>$urut[0]->stat_totalyearly,
                            'robotName' => $robot->robotLongName,
                            'robotImage' => $robot->robotImage,
                            'tag' => $robot->robotSlugName,
                            'created_at' => $time,
                        ];
                    }else {
                        $dataRobot[] = [
                            'robot'=>$robot->id, 
                            'pair'=>$urut[0]->stat_pair,
                            'last_percent'=>$urut[0]->stat_lastten,
                            'last_total'=>$urut[0]->stat_totalten,
                            'week_percent'=>$urut[0]->stat_weekly,
                            'week_total'=>$urut[0]->stat_totalweekly,
                            'month_percent'=>$urut[0]->stat_monthly,
                            'month_total'=>$urut[0]->stat_totalmonthly,
                            'year_percent'=>$urut[0]->stat_yearly,
                            'year_total'=>$urut[0]->stat_totalyearly,
                            'robotName' => $robot->robotLongName,
                            'robotImage' => $robot->robotImage,
                            'tag' => $robot->robotSlugName,
                            'created_at' => $time,
                        ];
                    }
                    // dd($dataRobot);
                }
            }
            // dd($sorting);
            $sort = $periode."_".$type;
            if($sorting == 'performance'){
                $finalRobotLists = collect($dataRobot)->sortBy($sort)->reverse();
                // dd('choose');
                
            }elseif($sorting == 'asc'){
                $finalRobotLists = collect($dataRobot)->sortBy('robotName');
                // dd('asc');
                
            }elseif($sorting == 'desc'){
                $finalRobotLists = collect($dataRobot)->sortBy('robotName')->reverse();
                // dd('desc');
            }elseif($sorting == 'old'){
                $finalRobotLists = collect($dataRobot)->sortBy('created_at')->reverse();
            }elseif($sorting == 'new'){
                $finalRobotLists = collect($dataRobot)->sortBy('created_at');
            }
            // $finalRobotLists = collect($dataRobot)->sortBy($sort)->reverse()->toArray();
            // dd($finalRobotLists);
            $dataFinal = $this->paginate($finalRobotLists);
            return view('pages-v2.robot-list-card', compact('dataFinal'))->render();
        }

        
            $pullPair = DB::table('robotStatistic')->distinct()->select('stat_pair')->get();
            // $pair = $request->pair;
            $sorting = $request->query('sort');
            $cari = $request->query('query');
            $type = 'percent';
            $periode = 'month';
            $userId = CRUDBooster::myId();
            
            $getRobots = DB::table('robots')
                ->where('robotLongName', 'LIKE', '%'.$cari.'%')
                ->leftjoin('robotStatistic', 'robotStatistic.stat_robot', 'robots.id')
                ->where('robotStatistic.stat_robot', '!=' , 0)
                ->distinct()->select('stat_robot')->get();      
            // ->where('robotLongName', 'LIKE', '%'.$cari.'%')->get();
            // dd($getRobots);
            foreach ($getRobots as $robot) {
                // dd($robot);
                $result = $this->statistic_robot($type,$periode,$robot->stat_robot);
                // dd($result);
                $data = $result->getData();
                // dd($data);

                $urut = [];
                foreach ($data as $key => $value) {
                    // dd($value);
                    $urut[] = $value;
                }
                // $robot = DB::table('robots')->where('id',$urut[0]->robot)->first();
                $robot = DB::table('robots')->where('id',$urut[0]->stat_robot)->first();
                $time = strtotime($robot->created_at);
                if($userId == null){
                    $dataRobot[] = [
                        'robot'=>$robot->id, 
                        'pair'=>$urut[0]->stat_pair,
                        'last_percent'=>$urut[0]->stat_lastten,
                        'last_total'=>$urut[0]->stat_totalten,
                        'week_percent'=>$urut[0]->stat_weekly,
                        'week_total'=>$urut[0]->stat_totalweekly,
                        'month_percent'=>$urut[0]->stat_monthly,
                        'month_total'=>$urut[0]->stat_totalmonthly,
                        'year_percent'=>$urut[0]->stat_yearly,
                        'year_total'=>$urut[0]->stat_totalyearly,
                        'robotName' => $robot->robotLongName,
                        'robotImage' => $robot->robotImage,
                        'tag' => $robot->robotSlugName,
                        'created_at' => $time,
                    ];
                }else {
                    $checkSubs = DB::table('wishlist')
                    ->where('userId', $userId)
                    ->where('robotId', $robot->id)
                    ->first();
                    // dd($checkSubs);
                    if($checkSubs == null){
                        $dataRobot[] = [
                            'robot'=>$robot->id, 
                            'pair'=>$urut[0]->stat_pair,
                            'last_percent'=>$urut[0]->stat_lastten,
                            'last_total'=>$urut[0]->stat_totalten,
                            'week_percent'=>$urut[0]->stat_weekly,
                            'week_total'=>$urut[0]->stat_totalweekly,
                            'month_percent'=>$urut[0]->stat_monthly,
                            'month_total'=>$urut[0]->stat_totalmonthly,
                            'year_percent'=>$urut[0]->stat_yearly,
                            'year_total'=>$urut[0]->stat_totalyearly,
                            'robotName' => $robot->robotLongName,
                            'robotImage' => $robot->robotImage,
                            'subs' => false,
                            'tag' => $robot->robotSlugName,
                            'created_at' => $time,
                        ];
                    }else {
                        $dataRobot[] = [
                            'robot'=>$robot->id, 
                            'pair'=>$urut[0]->stat_pair,
                            'last_percent'=>$urut[0]->stat_lastten,
                            'last_total'=>$urut[0]->stat_totalten,
                            'week_percent'=>$urut[0]->stat_weekly,
                            'week_total'=>$urut[0]->stat_totalweekly,
                            'month_percent'=>$urut[0]->stat_monthly,
                            'month_total'=>$urut[0]->stat_totalmonthly,
                            'year_percent'=>$urut[0]->stat_yearly,
                            'year_total'=>$urut[0]->stat_totalyearly,
                            'robotName' => $robot->robotLongName,
                            'robotImage' => $robot->robotImage,
                            'subs' => false,
                            'tag' => $robot->robotSlugName,
                            'created_at' => $time,
                        ];
                    }
                    // dd($dataRobot);
                }
            }
            // dd($sorting);
            $sort = $periode."_".$type;
            if($sorting != 'asc' && $sorting != 'desc'){
                $finalRobotLists = collect($dataRobot)->sortBy($sort)->reverse();
                // dd('choose');
                
            }elseif($sorting == 'asc'){
                $finalRobotLists = collect($dataRobot)->sortBy('robotName');
                // dd('asc');
                
            }elseif($sorting == 'desc'){
                $finalRobotLists = collect($dataRobot)->sortBy('robotName')->reverse();
                // dd('desc');
            }elseif($sorting == 'old'){
                $finalRobotLists = collect($dataRobot)->sortBy('created_at')->reverse();
            }elseif($sorting == 'new'){
                $finalRobotLists = collect($dataRobot)->sortBy('created_at');
            }
            // $finalRobotLists = collect($dataRobot)->sortBy($sort)->reverse()->toArray();
            // dd($finalRobotLists);
            $dataFinal = $this->paginate($finalRobotLists);
            return view('pages-v2.robot-list', compact('dataFinal'))->render();
        
    }
    public function robotCompare(Request $request) {
        // Request $request
        // dd($request);
        // dd('masuk');
        $validator = $request->validate([
            'robot1' => 'required',
            'robot2' => 'required',
            'pair' => 'required',
            ]);

        $pullPairs = DB::table('robotStatistic')->distinct()->select('stat_pair')->get();
        
        // dd($request->robot1, $request->robot2);
        // isi $robot = id robot
        $robot1 = DB::table('robots')->where('robotLongname', $request->robot1)->first();
        // dd($robot1->id);
        $robot2 = DB::table('robots')->where('robotLongname', $request->robot2)->first();
        // $robot1 = DB::table('robots')->where('robotLongname', 'Zeuz')->first();
        // $robot2 = DB::table('robots')->where('robotLongname', 'Andromeda')->first();
        $pair = $request->pair;
        $robotId1 = $robot1->id;
        $robotId2 = $robot2->id;
        $userId = CRUDBooster::myId();
        // $userId = 89;
        $cekSubs = DB::table('cms_users')->where('id', $userId)->select('status')->first();
        // $robotId2 = 17;
        // $robotId1 = 17;
        // $pair = 'eurgbp';
        // dd($pair,$robotId1);
        $robotData1 = DB::table('robotStatistic')
        ->where('stat_pair', $pair)
        ->where('stat_robot', $robotId1)
        ->orderby('id','desc')
        ->first();
        $data1 = [
            'robot' => $robotId1,
            'pair' => $pair,
            'last_percent' => $robotData1->stat_lastten,
            'last_total' => $robotData1->stat_totalten,
            'week_percent' => $robotData1->stat_weekly,
            'week_total' => $robotData1->stat_totalweekly,
            'month_percent' => $robotData1->stat_monthly,
            'month_total' => $robotData1->stat_totalmonthly,
            'year_percent' => $robotData1->stat_yearly,
            'year_total' => $robotData1->stat_totalyearly,
            'robotName' => $robot1->robotLongName,
            'robotImage' => $robot1->robotImage
            ];
        // dd($data1);

        $robotData2 = DB::table('robotStatistic')
        ->where('stat_pair', $pair)
        ->where('stat_robot', $robotId2)
        ->orderby('id','desc')
        ->first();
        // dd($robotData2, $robotId2);

        $data2 = [
            'robot' => $robotId2,
            'pair' => $pair,
            'last_percent' => $robotData2->stat_lastten,
            'last_total' => $robotData2->stat_totalten,
            'week_percent' => $robotData2->stat_weekly,
            'week_total' => $robotData2->stat_totalweekly,
            'month_percent' => $robotData2->stat_monthly,
            'month_total' => $robotData2->stat_totalmonthly,
            'year_percent' => $robotData2->stat_yearly,
            'year_total' => $robotData2->stat_totalyearly,
            'robotName' => $robot2->robotLongName,
            'robotImage' => $robot2->robotImage
            ];

            $pullRobot = DB::table('robots')->get();

            if ($request->robot5){
                // robot3
                $robot3 = DB::table('robots')->where('robotLongname', $request->robot3)->first();
                $robotId3 = $robot3->id;
                $robotData3 = DB::table('robotStatistic')
                ->where('stat_pair', $pair)
                ->where('stat_robot', $robotId3)
                ->orderby('id','desc')
                ->first();
                $data3 = [
                    'robot' => $robotId3,
                    'pair' => $pair,
                    'last_percent' => $robotData3->stat_lastten,
                    'last_total' => $robotData3->stat_totalten,
                    'week_percent' => $robotData3->stat_weekly,
                    'week_total' => $robotData3->stat_totalweekly,
                    'month_percent' => $robotData3->stat_monthly,
                    'month_total' => $robotData3->stat_totalmonthly,
                    'year_percent' => $robotData3->stat_yearly,
                    'year_total' => $robotData3->stat_totalyearly,
                    'robotName' => $robot3->robotLongName,
                    'robotImage' => $robot3->robotImage
                ];

                // robot4
                $robot4 = DB::table('robots')->where('robotLongname', $request->robot4)->first();
                $robotId3 = $robot4->id;
                $robotData3 = DB::table('robotStatistic')
                ->where('stat_pair', $pair)
                ->where('stat_robot', $robotId4)
                ->orderby('id','desc')
                ->first();
                $data4 = [
                    'robot' => $robotId4,
                    'pair' => $pair,
                    'last_percent' => $robotData4->stat_lastten,
                    'last_total' => $robotData4->stat_totalten,
                    'week_percent' => $robotData4->stat_weekly,
                    'week_total' => $robotData4->stat_totalweekly,
                    'month_percent' => $robotData4->stat_monthly,
                    'month_total' => $robotData4->stat_totalmonthly,
                    'year_percent' => $robotData4->stat_yearly,
                    'year_total' => $robotData4->stat_totalyearly,
                    'robotName' => $robot4->robotLongName,
                    'robotImage' => $robot4->robotImage
                ];

                // robot5
                $robot5 = DB::table('robots')->where('robotLongname', $request->robot5)->first();
                $robotId5 = $robot3->id;
                $robotData5 = DB::table('robotStatistic')
                ->where('stat_pair', $pair)
                ->where('stat_robot', $robotId5)
                ->orderby('id','desc')
                ->first();
                $data5 = [
                    'robot' => $robotId5,
                    'pair' => $pair,
                    'last_percent' => $robotData5->stat_lastten,
                    'last_total' => $robotData5->stat_totalten,
                    'week_percent' => $robotData5->stat_weekly,
                    'week_total' => $robotData5->stat_totalweekly,
                    'month_percent' => $robotData5->stat_monthly,
                    'month_total' => $robotData5->stat_totalmonthly,
                    'year_percent' => $robotData3->stat_yearly,
                    'year_total' => $robotData5->stat_totalyearly,
                    'robotName' => $robot5->robotLongName,
                    'robotImage' => $robot5->robotImage
                ];
                return view('pages-v2.robot-compare', compact('data1', 'data2', 'data3', 'data4', 'data5', 'pullPairs', 'pullRobot', 'cekSubs'));
            }
            if ($request->robot4){
                $robot3 = DB::table('robots')->where('robotLongname', $request->robot3)->first();
                $robotId3 = $robot3->id;
                $robotData3 = DB::table('robotStatistic')
                ->where('stat_pair', $pair)
                ->where('stat_robot', $robotId3)
                ->orderby('id','desc')
                ->first();
                $data3 = [
                    'robot' => $robotId3,
                    'pair' => $pair,
                    'last_percent' => $robotData3->stat_lastten,
                    'last_total' => $robotData3->stat_totalten,
                    'week_percent' => $robotData3->stat_weekly,
                    'week_total' => $robotData3->stat_totalweekly,
                    'month_percent' => $robotData3->stat_monthly,
                    'month_total' => $robotData3->stat_totalmonthly,
                    'year_percent' => $robotData3->stat_yearly,
                    'year_total' => $robotData3->stat_totalyearly,
                    'robotName' => $robot3->robotLongName,
                    'robotImage' => $robot3->robotImage
                ];

                $robot4 = DB::table('robots')->where('robotLongname', $request->robot4)->first();
                $robotId3 = $robot4->id;
                $robotData3 = DB::table('robotStatistic')
                ->where('stat_pair', $pair)
                ->where('stat_robot', $robotId4)
                ->orderby('id','desc')
                ->first();
                $data4 = [
                    'robot' => $robotId4,
                    'pair' => $pair,
                    'last_percent' => $robotData4->stat_lastten,
                    'last_total' => $robotData4->stat_totalten,
                    'week_percent' => $robotData4->stat_weekly,
                    'week_total' => $robotData4->stat_totalweekly,
                    'month_percent' => $robotData4->stat_monthly,
                    'month_total' => $robotData4->stat_totalmonthly,
                    'year_percent' => $robotData4->stat_yearly,
                    'year_total' => $robotData4->stat_totalyearly,
                    'robotName' => $robot4->robotLongName,
                    'robotImage' => $robot4->robotImage
                ];
                return view('pages-v2.robot-compare', compact('data1', 'data2', 'data3', 'data4', 'pullPairs', 'pullRobot', 'cekSubs'));
            }
            if ($request->robot3){
                $robot3 = DB::table('robots')->where('robotLongname', $request->robot3)->first();
                $robotId3 = $robot3->id;
                $robotData3 = DB::table('robotStatistic')
                ->where('stat_pair', $pair)
                ->where('stat_robot', $robotId3)
                ->orderby('id','desc')
                ->first();
                $data3 = [
                    'robot' => $robotId3,
                    'pair' => $pair,
                    'last_percent' => $robotData3->stat_lastten,
                    'last_total' => $robotData3->stat_totalten,
                    'week_percent' => $robotData3->stat_weekly,
                    'week_total' => $robotData3->stat_totalweekly,
                    'month_percent' => $robotData3->stat_monthly,
                    'month_total' => $robotData3->stat_totalmonthly,
                    'year_percent' => $robotData3->stat_yearly,
                    'year_total' => $robotData3->stat_totalyearly,
                    'robotName' => $robot3->robotLongName,
                    'robotImage' => $robot3->robotImage
                ];
                return view('pages-v2.robot-compare', compact('data1', 'data2', 'data3', 'pullPairs', 'pullRobot', 'cekSubs'));
            }else {
                return view('pages-v2.robot-compare', compact('data1', 'data2', 'pullPairs', 'pullRobot', 'cekSubs'));
            }
    }
    
    public function searchProfitableRobot(Request $request) {
        // dd($request->query('pair'),$request->query('timeframe') );
        $validator = $request->validate([
            'pair' => 'required',
            'timeframe' => 'required',
            ]);
        $pair = $request->query('pair');
        $periode = $request->query('timeframe');
        $userId = CRUDBooster::myId();
        // $userId = 4;
        // dd($request->timeframe);
        $checkSubscription = DB::table('subscription')->where('userId', CRUDBooster::myId())->first();
        // dd($checkSubscription);
        if($checkSubscription != NULL){
            $subscribe = true;
        }
        if($checkSubscription == NULL){
            $subscribe = false;
        }

        $pullPair = DB::table('robotStatistic')->distinct()->select('stat_pair')->get();

        // dd($pullPair);
            $pullRobot = DB::table('robotStatistic')->distinct()->select('stat_robot')->get();
            foreach ($pullRobot as $robot) {
                // dd($robot, $key);
              $pullPairs = DB::table('robotStatistic')->where('robotStatistic.stat_robot',$robot->stat_robot)->where('robotStatistic.stat_pair',$pair)->orderby('robotStatistic.id','desc')
              ->leftjoin('robots', 'robotStatistic.stat_robot', 'robots.id')
              ->where('robots.id', $robot->stat_robot)->first();
              $data[] = [
                'robot' => $robot->stat_robot,
                'pair' => $pair,
                'last_percent' => $pullPairs->stat_lastten,
                'last_total' => $pullPairs->stat_totalten,
                'week_percent' => $pullPairs->stat_weekly,
                'week_total' => $pullPairs->stat_totalweekly,
                'month_percent' => $pullPairs->stat_monthly,
                'month_total' => $pullPairs->stat_totalmonthly,
                'year_percent' => $pullPairs->stat_yearly,
                'year_total' => $pullPairs->stat_totalyearly,
                'robotName' => $pullPairs->robotLongName,
                'robotImage' => $pullPairs->robotImage,
                'tag' => $pullPairs->robotSlugName,
                ];
            }
            // dd($data);
            $sort = $periode."_total";
            // dd($sort);
            if($sort == "week_total"){
                
                $array = collect($data)->sortBy($sort)->filter(function ($robot) { return $robot['week_total'] > 0;})->reverse()->toArray();
                foreach ($array as $key => $value) {
                    // dd($key['robot']);
                    $getLastTen = DB::table('mt4')
                    ->where('robot', $value['robot'])
                    ->where('item', $value['pair'])
                    ->select('robot', 'item', 'profit', 'closeTime')
                    ->orderby('closeTime', 'desc')
                    ->limit(10)
                    ->get();

                    $array[$key]['lastTen'] = $getLastTen;

                    if($userId!=null);{
                        $checkRobotSubs = DB::table('wishlist')
                        ->where('userId', $userId)
                        ->where('robotId', $value['robot'])
                        ->first();
                        // dd($robotList);
                        if($checkRobotSubs != null){
                            // $value['subs'] = true;
                            $array[$key]['subs'] = true;
                        }else{
                            $array[$key]['subs'] = false;
                        }
                    }
                    
                    // dd($dataLastTen[0]);
                }
                // dd($array, $getLastTen, $dataLastTen, 'week');
                // $sendData = ['robotData'=>$array, 'subscribe'=>$subscribe, 'pullPair'=>$pullPairs, 'lastTen'=>$dataLastTen];
            }
            elseif($sort == "month_total"){
                
                $array = collect($data)->sortBy($sort)->filter(function ($robot) { return $robot['month_total'] > 0;})->reverse()->toArray();
                foreach ($array as $key => $value) {
                    // dd($key['robot']);
                    $getLastTen = DB::table('mt4')
                    ->where('robot', $value['robot'])
                    ->where('item', $value['pair'])
                    ->select('robot', 'item', 'profit', 'closeTime')
                    ->orderby('closeTime', 'desc')
                    ->limit(10)
                    ->get();

                    $array[$key]['lastTen'] = $getLastTen;
                    // $dataLastTen[] = [
                    //     $key => $getLastTen
                    // ];
                    if($userId!=null);{
                        $checkRobotSubs = DB::table('wishlist')
                        ->where('userId', $userId)
                        ->where('robotId', $value['robot'])
                        ->first();
                        // dd($robotList);
                        if($checkRobotSubs != null){
                            // $value['subs'] = true;
                            $array[$key]['subs'] = true;
                        }else{
                            $array[$key]['subs'] = false;
                        }
                    }
                    // dd($getLastTen);
                }
                // dd($array, 'month');
                // $sendData = ['robotData'=>$array, 'subscribe'=>$subscribe, 'pullPair'=>$pullPairs, 'lastTen'=>$dataLastTen];
            }
            elseif($sort == "year_total"){
                
                $array = collect($data)->sortBy($sort)->filter(function ($robot) { return $robot['year_total'] > 0;})->reverse()->toArray();
                foreach ($array as $key => $value) {
                    // dd($key['robot']);
                    $getLastTen = DB::table('mt4')
                    ->where('robot', $value['robot'])
                    ->where('item', $value['pair'])
                    ->select('robot', 'item', 'profit', 'closeTime')
                    ->orderby('closeTime', 'desc')
                    ->limit(10)
                    ->get();

                    $array[$key]['lastTen'] = $getLastTen;
                    // $dataLastTen[] = [
                    //     $key => $getLastTen
                    // ];
                    if($userId!=null);{
                        $checkRobotSubs = DB::table('wishlist')
                        ->where('userId', $userId)
                        ->where('robotId', $value['robot'])
                        ->first();
                        // dd($robotList);
                        if($checkRobotSubs != null){
                            // $value['subs'] = true;
                            $array[$key]['subs'] = true;
                        }else{
                            $array[$key]['subs'] = false;
                        }
                    }
                }
                // dd($array, 'year');
                // $sendData = ['robotData'=>$array, 'subscribe'=>$subscribe, 'pullPair'=>$pullPairs, 'lastTen'=>$dataLastTen];
            }
            // dd($array, $subscribe,$pullPair, $dataLastTen);
            // dd($array['2']['lastTen'][0]->profit);
            // dd($array);
            $date = date('d/m/y', strtotime(Carbon::now()));
            // dd($date);
            return view('pages-v2.robot', compact('array', 'subscribe', 'pullPair', 'dataLastTen', 'userId', 'date', 'periode'));
        }
    public function paginate($items, $perPage = 12, $page = null, $options = [])
    {
        // $page = $currentPage = LengthAwarePaginator::resolveCurrentPage();
        // $items = $items instanceof Collection ? $items : Collection::make($items);
        // return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
        // set current page
    $currentPage = LengthAwarePaginator::resolveCurrentPage();
    // set limit 
    // $perPage = 20;
    // generate pagination
    $currentResults = $items->slice(($currentPage - 1) * $perPage, $perPage)->all();
    $results = new LengthAwarePaginator($currentResults, $items->count(), $perPage);
    return $results;
    }

    public function dashSearchRobotList(Request $request){
        if($request->ajax()){
            $cari = $request->query('query');
            $getRobots =  DB::table('robots')
            ->where('robotLongName', 'LIKE', '%'.$cari.'%')
            ->leftjoin('robotStatistic', 'robotStatistic.stat_robot', 'robots.id')
            ->where('robotStatistic.stat_robot', '!=' , 0)
            ->distinct()->select('stat_robot')->get();
            $type = 'percent';
            $periode = 'year';
            foreach ($getRobots as $robot) {
                // dd($robot);
                $result = $this->statistic_robot($type,$periode,$robot->stat_robot);
                // dd($result);
                $data = $result->getData();
                // dd($data);
    
                $urut = [];
                foreach ($data as $key => $value) {
                    // dd($value);
                    $urut[] = $value;
                }
                // $robot = DB::table('robots')->where('id',$urut[0]->robot)->first();
                $robot = DB::table('robots')->where('id',$urut[0]->stat_robot)->first();
                    $dataRobot[] = [
                        'robot'=>$robot->id, 
                        'pair'=>$urut[0]->pair,
                        'last_percent'=>$urut[0]->stat_lastten,
                        'last_total'=>$urut[0]->stat_totalten,
                        'week_percent'=>$urut[0]->stat_weekly,
                        'week_total'=>$urut[0]->stat_totalweekly,
                        'month_percent'=>$urut[0]->stat_monthly,
                        'month_total'=>$urut[0]->stat_totalmonthly,
                        'year_percent'=>$urut[0]->stat_yearly,
                        'year_total'=>$urut[0]->stat_totalyearly,
                        'robotName' => $robot->robotLongName,
                        'robotImage' => $robot->robotImage,
                        'tag' => $robot->robotSlugName,
                    ];
            }
    
            $sort = $periode.'_'.$type;
    
            $finalRobot = collect($dataRobot)->sortBy($sort)->reverse()->toArray();
            $robotSubs = DB::table('robotlicense')->where('userId', $userId)->get();
            $getUserSubs = DB::table('cms_users')->where('id', $userId)->select('status')->first();
            foreach ($finalRobot as $key => $value) {
                foreach ($robotSubs as $indicator => $item) {
                    // dd($value['robot'], $item->robotId);
                    if($value['robot'] == $item->robotId){
                        $finalRobot[$key]['subs'] = true;
                    }
                }
            }
        
            // cek user quota
            $quota = DB::table('installquota')->where('userId', $userId)->first();
            $userQuota = $quota->sisaQuota;
            $packageStatus = DB::table('cms_users')->where('id', $userId)->select('status')->first();
            $packageId = DB::table('subscription')->where('userId', $userId)->select('packageId')->orderBy('paymentTime', 'desc')->first();
            // dd($userId, $packageId);
            $time = Carbon::now()->timestamp;
            return view('pages-v2.dashboard.dash-myrobot-list-card', compact('finalRobot', 'getUserSubs', 'packageStatus', 'packageId', 'userId', 'time', 'userQuota'))->render();        
        }
        $cari = $request->query('query');
            $getRobots =  DB::table('robots')
            ->where('robotLongName', 'LIKE', '%'.$cari.'%')
            ->leftjoin('robotStatistic', 'robotStatistic.stat_robot', 'robots.id')
            ->where('robotStatistic.stat_robot', '!=' , 0)
            ->distinct()->select('stat_robot')->get();
            $type = 'percent';
            $periode = 'year';
            foreach ($getRobots as $robot) {
                // dd($robot);
                $result = $this->statistic_robot($type,$periode,$robot->stat_robot);
                // dd($result);
                $data = $result->getData();
                // dd($data);
    
                $urut = [];
                foreach ($data as $key => $value) {
                    // dd($value);
                    $urut[] = $value;
                }
                // $robot = DB::table('robots')->where('id',$urut[0]->robot)->first();
                $robot = DB::table('robots')->where('id',$urut[0]->robot)->first();
                    $dataRobot[] = [
                        'robot'=>$robot->id, 
                        'pair'=>$urut[0]->pair,
                        'last_percent'=>$urut[0]->last_percent,
                        'last_total'=>$urut[0]->last_total,
                        'week_percent'=>$urut[0]->week_percent,
                        'week_total'=>$urut[0]->week_total,
                        'month_percent'=>$urut[0]->month_percent,
                        'month_total'=>$urut[0]->month_total,
                        'year_percent'=>$urut[0]->year_percent,
                        'year_total'=>$urut[0]->year_total,
                        'robotName' => $robot->robotLongName,
                        'robotImage' => $robot->robotImage,
                        'tag' => str_slug($robot->robotLongName),
                    ];
            }
    
            $sort = 'year_percent';
    
            $finalRobot = collect($dataRobot)->sortBy($sort)->reverse()->toArray();
            $robotSubs = DB::table('robotlicense')->where('userId', $userId)->get();
            $getUserSubs = DB::table('cms_users')->where('id', $userId)->select('status')->first();
            foreach ($finalRobot as $key => $value) {
                foreach ($robotSubs as $indicator => $item) {
                    // dd($value['robot'], $item->robotId);
                    if($value['robot'] == $item->robotId){
                        $finalRobot[$key]['subs'] = true;
                    }
                }
            }

            // cek user quota
            $quota = DB::table('installquota')->where('userId', $userId)->first();
            $userQuota = $quota->sisaQuota;
            $packageStatus = DB::table('cms_users')->where('id', $userId)->select('status')->first();
            $packageId = DB::table('subscription')->where('userId', $userId)->select('packageId')->orderBy('paymentTime', 'desc')->first();
            // dd($userId, $packageId);
            $time = Carbon::now()->timestamp;
            return view('pages-v2.dashboard.dash-myrobot-list', compact('finalRobot', 'getUserSubs', 'packageStatus', 'packageId', 'userId', 'time', 'userQuota'))->render();
    }
}


