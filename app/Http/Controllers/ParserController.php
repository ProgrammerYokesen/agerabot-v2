<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use CRUDBooster;

class ParserController extends Controller
{
  public function test(){
        $pullRobot = DB::table('mt4')->get();

        foreach ($pullRobot as $key => $value) {
          $update = DB::table('mt4')->where('id',$value->id)->update([
            'openTime' => str_replace('.','-',$value->openTime),
            'closeTime' => str_replace('.','-',$value->closeTime)
          ]);
        }
  }

  public function statistic_ten(){
    $first_year = date('Y-01-01');
    $first_month = date('Y-m-01');
    $monday    = date('Y-m-d', strtotime("monday this week"));
    $today = date('Y-m-d');
    $day = date('w');
    if($day < 6){
      $pullRobot = DB::table('mt4')->distinct()->select('robot')->get();

      foreach ($pullRobot as $robot) {
        $pullItem = DB::table('mt4')->where('robot',$robot->robot)->distinct()->select('item')->get();

        foreach ($pullItem as $pair) {
          /*last ten*/
          $lastten = DB::table('mt4')->where('robot',$robot->robot)->where('item',$pair->item)->where('status','closed')->orderby('closeTime','desc')->limit(10)->get();
          $tenplus = 0;
          $tenminus = 0;
          $tenprofit = 0;
          foreach ($lastten as $x => $ten) {
            if($ten->profit > 0){
              $tenplus++;
            }
            if($ten->profit < 0){
              $tenminus++;
            }
            $tenprofit += $ten->profit;
          }

          if($tenplus+$tenminus>0){
            $percentten = number_format($tenplus / ($tenplus+$tenminus) *100,2);
          }else{
            $percentten = 0;
          }

          $last = DB::table('robotStatistic')->where('stat_robot',$robot->robot)->where('stat_pair',$pair->item)->orderby('id','desc')->first();

          $insert = DB::table('robotStatistic')->insert([
            'stat_robot' => $robot->robot,
            'stat_pair' => $pair->item,
            'stat_lastten' => $percentten,
            'stat_totalten' => $tenprofit,
            'stat_weekly' => $last->stat_weekly,
            'stat_totalweekly' => $last->stat_totalweekly,
            'stat_monthly' => $last->stat_monthly,
            'stat_totalmonthly' => $last->stat_totalmonthly,
            'stat_yearly' => $last->stat_yearly,
            'stat_totalyearly' => $last->stat_totalyearly,
            'stat_alltime' => $last->stat_alltime,
            'stat_totalprofit' => $last->stat_totalprofit,
            'stat_kodetime' => $robot->robot."-".$pair->item."-Y-".$first_year."-".$today,
            'stat_status' => 'active'
          ]);
        }
      }
    }
  }

  public function statistic_week(){
    $first_year = date('Y-01-01');
    $first_month = date('Y-m-01');
    $monday    = date('Y-m-d', strtotime("monday this week"));
    $today = date('Y-m-d');
    $day = date('w');
    if($day < 6){
      $pullRobot = DB::table('mt4')->distinct()->select('robot')->get();

      foreach ($pullRobot as $robot) {
        $pullItem = DB::table('mt4')->where('robot',$robot->robot)->distinct()->select('item')->get();

        foreach ($pullItem as $pair) {

          /*week*/

          $lastweek = DB::table('mt4')->where('robot',$robot->robot)->where('item',$pair->item)->where('status','closed')->where('closeTime','>',$monday)->get();
          $weekplus = 0;
          $weekminus = 0;
          $weekprofit = 0;
          foreach ($lastweek as $x => $week) {
            if($week->profit > 0){
              $weekplus++;
            }
            if($week->profit < 0){
              $weekminus++;
            }
            $weekprofit += $week->profit;
          }

          if($weekplus+$weekminus>0){
            $percentweek = number_format($weekplus / ($weekplus+$weekminus) *100,2);
          }else{
            $percentweek = 0;
          }

          $last = DB::table('robotStatistic')->where('stat_robot',$robot->robot)->where('stat_pair',$pair->item)->orderby('id','desc')->first();

          $insert = DB::table('robotStatistic')->insert([
            'stat_robot' => $robot->robot,
            'stat_pair' => $pair->item,
            'stat_lastten' => $last->stat_lastten,
            'stat_totalten' => $last->stat_totalten,
            'stat_weekly' => $percentweek,
            'stat_totalweekly' => $weekprofit,
            'stat_monthly' => $last->stat_monthly,
            'stat_totalmonthly' => $last->stat_totalmonthly,
            'stat_yearly' => $last->stat_yearly,
            'stat_totalyearly' => $last->stat_totalyearly,
            'stat_alltime' => $last->stat_alltime,
            'stat_totalprofit' => $last->stat_totalprofit,
            'stat_kodetime' => $robot->robot."-".$pair->item."-Y-".$first_year."-".$today,
            'stat_status' => 'active'
          ]);
        }
      }
    }
  }

  public function statistic_month(){
    $first_year = date('Y-01-01');
    $first_month = date('Y-m-01');
    $monday    = date('Y-m-d', strtotime("monday this week"));
    $today = date('Y-m-d');
    $day = date('w');
    if($day < 6){
      $pullRobot = DB::table('mt4')->distinct()->select('robot')->get();

      foreach ($pullRobot as $robot) {
        $pullItem = DB::table('mt4')->where('robot',$robot->robot)->distinct()->select('item')->get();

        foreach ($pullItem as $pair) {

          /*month*/

          $lastmonth = DB::table('mt4')->where('robot',$robot->robot)->where('item',$pair->item)->where('status','closed')->where('closeTime','>',$first_month)->get();
          $monthplus = 0;
          $monthminus = 0;
          $monthprofit = 0;
          foreach ($lastmonth as $x => $month) {
            if($month->profit > 0){
              $monthplus++;
            }
            if($month->profit < 0){
              $monthminus++;
            }
            $monthprofit += $month->profit;
          }

          if($monthplus+$monthminus>0){
            $percentmonth = number_format($monthplus / ($monthplus+$monthminus) *100,2);
          }else{
            $percentmonth = 0;
          }

          $last = DB::table('robotStatistic')->where('stat_robot',$robot->robot)->where('stat_pair',$pair->item)->orderby('id','desc')->first();

          $insert = DB::table('robotStatistic')->insert([
            'stat_robot' => $robot->robot,
            'stat_pair' => $pair->item,
            'stat_lastten' => $last->stat_lastten,
            'stat_totalten' => $last->stat_totalten,
            'stat_weekly' => $last->stat_weekly,
            'stat_totalweekly' => $last->stat_totalweekly,
            'stat_monthly' => $percentmonth,
            'stat_totalmonthly' => $monthprofit,
            'stat_yearly' => $last->stat_yearly,
            'stat_totalyearly' => $last->stat_totalyearly,
            'stat_alltime' => $last->stat_alltime,
            'stat_totalprofit' => $last->stat_totalprofit,
            'stat_kodetime' => $robot->robot."-".$pair->item."-Y-".$first_year."-".$today,
            'stat_status' => 'active'
          ]);
        }
      }
    }
  }

  public function statistic_year(){
    $first_year = date('Y-01-01');
    $first_month = date('Y-m-01');
    $monday    = date('Y-m-d', strtotime("monday this week"));
    $today = date('Y-m-d');
    $day = date('w');
    if($day < 6){
      $pullRobot = DB::table('mt4')->distinct()->select('robot')->get();

      foreach ($pullRobot as $robot) {
        $pullItem = DB::table('mt4')->where('robot',$robot->robot)->distinct()->select('item')->get();

        foreach ($pullItem as $pair) {

          /*year*/

          $lastyear = DB::table('mt4')->where('robot',$robot->robot)->where('item',$pair->item)->where('status','closed')->where('closeTime','>',$first_year)->get();
          $yearplus = 0;
          $yearminus = 0;
          $yearprofit = 0;
          foreach ($lastyear as $x => $year) {
            if($year->profit > 0){
              $yearplus++;
            }
            if($year->profit < 0){
              $yearminus++;
            }
            $yearprofit += $year->profit;
          }

          if($yearplus+$yearminus>0){
            $percentyear = number_format($yearplus / ($yearplus+$yearminus) *100,2);
          }else{
            $percentyear = 0;
          }

          $last = DB::table('robotStatistic')->where('stat_robot',$robot->robot)->where('stat_pair',$pair->item)->orderby('id','desc')->first();

          $insert = DB::table('robotStatistic')->insert([
            'stat_robot' => $robot->robot,
            'stat_pair' => $pair->item,
            'stat_lastten' => $last->stat_lastten,
            'stat_totalten' => $last->stat_totalten,
            'stat_weekly' => $last->stat_weekly,
            'stat_totalweekly' => $last->stat_totalweekly,
            'stat_monthly' => $last->stat_monthly,
            'stat_totalmonthly' => $last->stat_totalmonthly,
            'stat_yearly' => $percentyear,
            'stat_totalyearly' => $yearprofit,
            'stat_alltime' => $last->stat_alltime,
            'stat_totalprofit' => $last->stat_totalprofit,
            'stat_kodetime' => $robot->robot."-".$pair->item."-Y-".$first_year."-".$today,
            'stat_status' => 'active'
          ]);
        }
      }
    }
  }

  public function parseStatement(){
    $robots = DB::table('robots')->where('robotStatus','active')->get();
    foreach ($robots as $robot) {
      $url = "https://agerabot.com/mt4/".$robot->robotSlugName."/statement.htm";
      $curl = curl_init();
      curl_setopt_array($curl, array(
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_TIMEOUT => 30000,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
          'Content-Type: application/json',
        ),
      ));
      $response = curl_exec($curl);
      $err = curl_error($curl);
      curl_close($curl);

      $account = explode('Account: ',$response);
      $account = explode('<',$account[1]);
      $account = $account[0];

      $closed = explode('Closed Transactions:',$response);
      $closed = explode('Closed P/L:',$closed[1]);
      $closed = explode('</tr>',$closed[0]);
      //dd($closed);
      foreach ($closed as $n => $clo) {
        if($n > 1){
            $column = explode('</td>',$clo);
            //dd($column);
            $type = explode('>',$column[2]);
            $type = $type[1];
            if($type=="sell" or $type=="buy"){
              $ticket = explode('">',$column[0]);
              $ticket = $ticket[1];

              $openTime = explode('>',$column[1]);
              $openTime = $openTime[1];

              $size = explode('>',$column[3]);
              $size = $size[1];

              $item = explode('>',$column[4]);
              $item = $item[1];

              $price = explode('>',$column[5]);
              $price = $price[1];

              $sl = explode('>',$column[6]);
              $sl = $sl[1];

              $tp = explode('>',$column[7]);
              $tp = $tp[1];

              $closeTime = explode('>',$column[8]);
              $closeTime = $closeTime[1];

              $closePrice = explode('>',$column[9]);
              $closePrice = $closePrice[1];

              $profit = explode('>',$column[13]);
              $profit = $profit[1];



              $check = DB::table('mt4')->where('ticket',$ticket)->where('status','closed')->count();

              if($check <1){
                $insert = DB::table('mt4')->insert([
                  'robot' => $robot->id,
                  'akun' => $account,
                  'ticket' => $ticket,
                  'openTime' => str_replace('.','-',$openTime),
                  'type' => $type,
                  'size' => $size,
                  'item' => str_replace('.ffb','',$item),
                  'price' => $price,
                  'stoploss' => $sl,
                  'takeprofit' => $tp,
                  'closeTime' => str_replace('.','-',$closeTime),
                  'closePrice' => $closePrice,
                  'profit' => $profit,
                  'status' => 'closed'
                ]);
              }

            }
            echo $robot->robotSlugName."<br>";
        }
      }
    }
  }

  public function tgphl(){
    $url = "https://agerabot.com/mt4/tgphl/statement.htm";
    $curl = curl_init();
    curl_setopt_array($curl, array(
      CURLOPT_URL => $url,
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_TIMEOUT => 30000,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "GET",
      CURLOPT_HTTPHEADER => array(
        'Content-Type: application/json',
      ),
    ));
    $response = curl_exec($curl);
    $err = curl_error($curl);
    curl_close($curl);

    $account = explode('Account: ',$response);
    $account = explode('<',$account[1]);
    $account = $account[0];

    $closed = explode('Open Trades:',$response);
    $closed = explode('Floating P/L:',$closed[1]);
    $closed = explode('</tr>',$closed[0]);

    foreach ($closed as $n => $clo) {
      if($n > 1){
          $column = explode('</td>',$clo);
          //dd($column);
          $type = explode('>',$column[2]);
          $type = $type[1];
          if($type=="sell" or $type=="buy"){
            $ticket = explode('">',$column[0]);
            $ticket = $ticket[1];

            $openTime = explode('>',$column[1]);
            $openTime = $openTime[1];

            $size = explode('>',$column[3]);
            $size = $size[1];

            $item = explode('>',$column[4]);
            $item = $item[1];

            $price = explode('>',$column[5]);
            $price = $price[1];

            $sl = explode('>',$column[6]);
            $sl = $sl[1];

            $tp = explode('>',$column[7]);
            $tp = $tp[1];

            $closeTime = explode('>',$column[8]);
            $closeTime = $closeTime[1];

            $closePrice = explode('>',$column[9]);
            $closePrice = $closePrice[1];

            $profit = explode('>',$column[13]);
            $profit = $profit[1];

            $check = DB::table('tgpl')->where('ticket',$ticket)->where('status','open')->count();

            if($check <1){
              $brokerTrader = DB::table('tradeBroker')->orderby('count','asc')->first();
              $insert = DB::table('tgpl')->insert([
                'robot' => 'tgphl',
                'akun' => $account,
                'ticket' => $ticket,
                'openTime' => str_replace('.','-',$openTime),
                'type' => $type,
                'size' => $size,
                'item' => str_replace('.ffb','',$item),
                'price' => $price,
                'stoploss' => $sl,
                'takeprofit' => $tp,
                'status' => 'open',
                'tradeUserId' => $brokerTrader->userId,
                'tradeBroker' => $brokerTrader->broker,
                'tradeAkun' => $brokerTrader->login
              ]);

              $increment = DB::table('tradeBroker')->where('id',$brokerTrader->id)->update([
                'count' => $brokerTrader->count + 1
              ]);
              $hour = date('H');
              if($hour > 8 && $hour < 17){
                $config['content'] = "SEGERA TRADE di $brokerTrader->broker";
                $config['to'] = CRUDBooster::adminPath('tgpl');
                $config['id_cms_users'] = [$brokerTrader->userId];
                CRUDBooster::sendNotification($config);
              }
            }

          }

      }
    }
  }

  public function tgphl_close(){
    $url = "https://agerabot.com/mt4/tgphl/statement.htm";
    $curl = curl_init();
    curl_setopt_array($curl, array(
      CURLOPT_URL => $url,
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_TIMEOUT => 30000,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "GET",
      CURLOPT_HTTPHEADER => array(
        'Content-Type: application/json',
      ),
    ));
    $response = curl_exec($curl);
    $err = curl_error($curl);
    curl_close($curl);

    $account = explode('Account: ',$response);
    $account = explode('<',$account[1]);
    $account = $account[0];

    $closed = explode('Closed Transactions:',$response);
    $closed = explode('Closed P/L:',$closed[1]);
    $closed = explode('</tr>',$closed[0]);
    //dd($closed);
    foreach ($closed as $n => $clo) {
      if($n > 1){
          $column = explode('</td>',$clo);
          //dd($column);
          $type = explode('>',$column[2]);
          $type = $type[1];
          if($type=="sell" or $type=="buy"){
            $ticket = explode('">',$column[0]);
            $ticket = $ticket[1];

            $openTime = explode('>',$column[1]);
            $openTime = $openTime[1];

            $size = explode('>',$column[3]);
            $size = $size[1];

            $item = explode('>',$column[4]);
            $item = $item[1];

            $price = explode('>',$column[5]);
            $price = $price[1];

            $sl = explode('>',$column[6]);
            $sl = $sl[1];

            $tp = explode('>',$column[7]);
            $tp = $tp[1];

            $closeTime = explode('>',$column[8]);
            $closeTime = $closeTime[1];

            $closePrice = explode('>',$column[9]);
            $closePrice = $closePrice[1];

            $profit = explode('>',$column[13]);
            $profit = $profit[1];



            $check = DB::table('tgpl')->where('ticket',$ticket)->where('status','closed')->count();

            if($check <1){
              $update = DB::table('tgpl')->where('ticket',$ticket)->update([
                'closeTime' => str_replace('.','-',$closeTime),
                'closePrice' => $closePrice,
                'profit' => $profit,
                'status' => 'closed'
              ]);

              $tradeOpen = DB::table('tgpl')->where('ticket',$ticket)->first();
              $config['content'] = "SEGERA TUTUP POSISI di $tradeOpen->tradeBroker";
              $config['to'] = CRUDBooster::adminPath('tgpl93');
              $config['id_cms_users'] = [$tradeOpen->userId];
              CRUDBooster::sendNotification($config);
            }
          }
      }
    }
  }

  public function expire(){

    $now = date('Y-m-d H:i:s');
    $expire = date('Y-m-d H:i:s',strtotime($now." - 15 minutes"));

    $list = DB::table('tgpl')->where('status','open')->where('tradeStatus','new')->where('created_at','<',$expire)->get();

    foreach ($list as $trade) {
      $update = DB::table('tgpl')->where('id',$trade->id)->update([
        'tradeStatus' => 'expired'
      ]);
    }
  }
}
