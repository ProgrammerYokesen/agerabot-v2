<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Cart;
use Alert;
use Cookie;
use carbon\carbon;
use App\Sender;
use Jenssegers\Agent\Agent;
use CRUDBooster;
use Storage;
use Session;
use App\Http\Controllers\Redirect;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use Illuminate\Pagination\Paginator;
class PagesController extends Controller
{
    public function index()
    {
        $robots = DB::table('robots')->get();
        // $checkout = DB::table('sales')->where('salesStatus','!=','menunggu pembayaran')->get();
        $checkout = DB::table('sales')->get();
        $data=array();
        foreach ($checkout as $value) {
            $cart = unserialize($value->salesSart);
            foreach ($cart as $n => $check){
            // dd($check->name);
            array_push($data, $check->name->id);
            // $data=$data+$check->name;
            }
        }
        $values = array_count_values($data);
        arsort($values);
        $firstKey = array_key_first($values);
        $mostPurchased = DB::table('robots')->where('id',$firstKey)->first();
        // dd($mostPurchased->robotImage);
        return view('web_pages.pages.index',compact('robots','mostPurchased'));
    }

    public function robotTrading()
    {
        $robots = DB::table('robots')->paginate(9);
        return view('web_pages.pages.robot-trading',compact('robots'));
    }

    public function robotTradingSort(Request $request)
    {
        
        if($request){
            $robots = DB::table('robots')
            ->orderBy('robotPrice1', 'desc')
            ->get();
            return view('web_pages.pages.robot-trading',compact('robots'));
        }elseif ($request) {
            $robots = DB::table('robots')
            ->orderBy('robotPrice1')
            ->get();
            return view('web_pages.pages.robot-trading',compact('robots'));
        }else{
            $robots = DB::table('robots')
            ->orderBy('robotPrice1')
            ->get();
            return view('web_pages.pages.robot-trading',compact('robots'));
        }
        
    }

    public function VPS()
    {
        return view('web_pages.pages.vps');
    }

    public function support()
    {
        return view('web_pages.pages.support');
    }

    public function blog()
    {
        $blogs=DB::table('blogs')->where('blogs.blogStatus','active')->join('cms_users','cms_users.id','blogs.blogAuthor')->get();
        $i=0;
        $mostRead=DB::table('blogs')->where('blogs.blogStatus','active')->join('cms_users','cms_users.id','blogs.blogAuthor')->orderBy('blogRead','desc')->first();
        $totalWord=str_word_count($mostRead->blogContent);
        $mostReadTime=$totalWord/255;
        $mostReadTime=ceil($mostReadTime);
        $readingTime=$mostReadTime;
        return view('web_pages.blogs.index',compact('blogs','mostRead','readingTime'));
    }

    public function blogSearch()
    {
        return view('web_pages.blogs.search');
    }

    public function contact()
    {
        return view('web_pages.pages.contact');
    }

    public function storeContact(Request $request)
    {
        dd($request->all());
    }

    public function cart()
    {
        $robots = DB::table('robots')->get();
        //dd(Cart::content());
        if(Cart::content()->isNotEmpty()){
            $jasa = DB::table('jasa')->where('jasaStatus','active')->get();
            return view('web_pages.pages.cart',compact('jasa','robots'));
        }else{
            return view('web_pages.pages.empty-cart',compact('robots'));
        }

    }

    public function payment(){
      return view('web_pages.pages.payment');
    }

    public function checkout(Request $request){
      $a=array("6288908898772","6288908898774","6288908898775");
      $random_keys=array_rand($a,1);
      $handphone = $a[$random_keys];
      //dd($random_keys,$handphone);
      $jasa = DB::table('jasa')->get();
      $checkout = DB::table('sales')->where('user_id',CRUDBooster::myId())->where('salesStatus','menunggu pembayaran')->orderby('id','desc')->first();
      $cart = unserialize($checkout->salesSart);
      return view('web_pages.pages.checkout',compact('checkout','jasa','cart','handphone'));
    }

    public function robotCollection(){
      $jasa = DB::table('jasa')->get();
      $checkout = DB::table('sales')->where('salesPaymentStatus','!=','menunggu')->where('user_id',CRUDBooster::myId())->get();
      $unpaid = DB::table('sales')->where('salesPaymentStatus','menunggu')->where('user_id',CRUDBooster::myId())->get();
      $check = DB::table('sales')->where('user_id',CRUDBooster::myId())->get();
    //   $checkout = DB::table('sales')->where('salesStatus','!=','menunggu pembayaran')->where('user_id',CRUDBooster::myId())->get();
      $robots = DB::table('robots')->get();
      if($check->isEmpty()){
        return view('web_pages.pages.empty-collection',compact('robots'));
      }
      else{
        return view('web_pages.pages.my-robot-collection',compact('checkout','jasa','unpaid'));
      }
    }

    
    // controller from dimas
    public function privacyPolicy(){
        return view('web_pages.pages.privacy-policy');
    }
    
    public function help(){
        return view('web_pages.pages.sub_pages_setting.help');
    }
    public function helpManageAccount(){
        return view('web_pages.pages.sub_pages_setting.sub_pages_help.manage-account');
    }
    public function paymentMethod(){
        return view('web_pages.pages.sub_pages_setting.sub_pages_help.payment-method');
    }
    public function installRobot(){
        return view('web_pages.pages.sub_pages_setting.sub_pages_help.install-robot');
    }
    public function manageVPS(){
        return view('web_pages.pages.sub_pages_setting.sub_pages_help.manage-vps');
    }
    public function complain(){

        // check ticket status
        $cekTicketStatus = DB::table('report')
        ->where('userId', CRUDBooster::myId())
        ->leftjoin('tickets', 'report.ticketId', 'tickets.id')
        ->where('tickets.status', 1)
        ->first();
        // dd($cekTicketStatus);
        if($cekTicketStatus){
            // get data
            $chatData = DB::table('report')
            ->where('ticketId', $cekTicketStatus->ticketId)
            ->leftjoin('cms_users', 'report.userId', 'cms_users.id')
            ->orderBy('report.created_at')
            ->get();
            $ticketId = $cekTicketStatus->ticketId;
            // dd($cekTicketStatus);
            return view('web_pages.pages.sub_pages_setting.complain-chat', compact('ticketId', 'chatData'));
        }else{
             // create the ticket
        $ticketRandomString = bin2hex(random_bytes(15));
        $ticket = $request->product . $ticketRandomString . CRUDBooster::myId();

        DB::table('tickets')
        ->insert(['ticketNumber' => $ticket, 'status' => false, 'created_at' => date('Y-m-d H:i:s')]);

        return view('web_pages.pages.sub_pages_setting.complain', compact('ticket'));
        }
    }

    public function complainContent(Request $request, $ticket){
        $validator = $request->validate([
            'complain' => 'required',
        ]);

        // update ticket status
        DB::table('tickets')
        ->where('ticketNumber', $ticket)
        ->update(['status'=> true]);

        if($request->hasFile('photo')){
            $validator = $request->validate([
            'photo' => 'image|mimes:jpeg,png,jpg'
            ]);

            // get ticketId
            $ticket = DB::table('tickets')
            ->where('ticketNumber', $ticket)
            ->first();
            
            date_default_timezone_set("Asia/Jakarta");
			$file = $request->file('photo');
            // uniquefilename yang dimasukkan ke database
            $fileName = $request->file('photo')->getClientOriginalName();
            $uniqueFileName= str_replace(" ","",time()."_".$fileName);
            
            $filePath = 'uploads/'.CRUDBooster::myId();
            
            $fileNameToStore = $filePath.'/'.$uniqueFileName;
            $tes = Storage::putFileAs($filePath, $file, $uniqueFileName);
            // dd($fileNameToStore);
            // insert into report
            DB::table('report')
            ->insert(['ticketId' => $ticket->id, 'userId' => CRUDBooster::myId(), 'chat' => $request->complain, 'photoComplain' => $fileNameToStore, 'created_at' => date('Y-m-d H:i:s')]);
            return redirect()->route('complainContentView');
            // return view('web_pages.pages.sub_pages_setting.complain-chat', compact('ticket', 'chatData'));

        }else{
            // get ticketId
            $ticket = DB::table('tickets')
            ->where('ticketNumber', $ticket)
            ->first();

            // insert into report
            DB::table('report')
            ->insert(['ticketId' => $ticket->id, 'userId' => CRUDBooster::myId(), 'chat' => $request->complain, 'created_at' => date('Y-m-d H:i:s')]);

            // get data
            $chatData = DB::table('report')
            ->where('ticketId', $ticket->id)
            ->leftjoin('cms_users', 'report.userId', 'cms_users.id')
            ->orderBy('report.created_at')
            ->get();
            return redirect()->route('complainContentView');

        }
    }

    public function complainContentView(){

        $chatData = DB::table('report')
        ->where('userId', CRUDBooster::myId())
        ->leftjoin('tickets', 'tickets.id', 'report.ticketId')
        ->leftjoin('cms_users', 'cms_users.id', 'report.userId')
        ->where('tickets.status', 1)
        ->get();

        $ticketId = $chatData[0]->ticketId;
        return view('web_pages.pages.sub_pages_setting.complain-chat', compact('ticketId', 'chatData'));

    }

    public function reportButton($id){
        $data = DB::table('tickets')
                ->where('id',$id)
                ->first();
                
        // dd($data, $id);
        if($data -> status == '1'){
            DB::table('tickets')
                ->where('id',$id)
                ->update(['status' => 0]);
        }
        if($data -> status == '0'){
            DB::table('tickets')
                ->where('id',$id)
                ->update(['status' => 1]);
        }
        return redirect()->back();
    }

    public function reportDetail($id){
        $chatData = DB::table('report')
        ->where('ticketId', $id)
        ->leftjoin('tickets', 'tickets.id', 'report.ticketId')
        ->leftjoin('cms_users', 'cms_users.id', 'report.userId')
        ->get();

        $ticketId = $chatData[0]->ticketId;
        // dd($ticketId);
        return view('web_pages.pages.sub_pages_setting.complain-chat', compact('ticketId', 'chatData'));
    }

    public function paymentVerification($id){
        DB::table('sales')
        ->where('id', $id)
        ->update(['salesPaymentStatus' => 'paid', 'salesPaymentTime' => Carbon::now()->toDateTimeString() ]);

        return redirect('/arena/sales');
    }

    public function printReceipt($id){
        $salesData = DB::table('sales')
        ->where('sales.id', $id)
        ->leftjoin('cms_users', 'cms_users.id', 'sales.user_id')
        ->select('sales.id', 'cms_users.name', 'sales.salesTime', 'sales.salesSart', 'sales.salesSubtotal', 'sales.salesDiscount', 'sales.salesTotal')
        ->first();

        // $productDetails = unserialize($salesData->salesSart);
        // dd($productDetails);

        // dd($salesData);
        return view('web_pages.pages.admin.print-invoice', compact('salesData'));
    }

    public function submitBlog(Request $request){
        
        // dd($data);

        $validator = $request->validate([
            'blogTitle' => 'required',
            'konten' => 'required',
            'blogMetaDescription' => 'required',
            'blogMetaKeywords' => 'required',
            'blogAuthor' => 'required',
            'bio' => 'required',
            'blogStatus' => 'required', 
            'blogImage1' =>'required|image|mimes:jpeg,png,jpg|max:2048',
            'release_date' => 'required'
        ]);
            
            date_default_timezone_set("Asia/Jakarta");
			$file = $request->file('blogImage1');
            // uniquefilename yang dimasukkan ke database
            $fileName = $request->file('blogImage1')->getClientOriginalName();
            $uniqueFileName= str_replace(" ","",date('y-m').'/'.$fileName);
            
            $filePath = 'uploads/'.CRUDBooster::myId();
            
            $fileNameToStoreImage1 = $filePath.'/'.$uniqueFileName;
            $tes = Storage::putFileAs($filePath, $file, $uniqueFileName);

            $blogAuthor = DB::table('cms_users')
            ->where('name', $request->blogAuthor)
            ->select('id')
            ->first();

            $blogAuthor = $blogAuthor->id;
            $blogSlug = str_slug($request->blogTitle);
            $release_date = $request->release_date.' '.date("H:i:s");
        // dd($request);
        if($request->hasFile('blogImage2')){
            $validator = $request->validate([
                'blogImage2' =>'image|mimes:jpeg,png,jpg|max:2048',
            ]);

            date_default_timezone_set("Asia/Jakarta");
			$file = $request->file('blogImage2');
            // uniquefilename yang dimasukkan ke database
            $fileName = $request->file('blogImage2')->getClientOriginalName();
            $uniqueFileName= str_replace(" ","",date('y-m').'/'.$fileName);
            
            $filePath = 'uploads/'.CRUDBooster::myId();
            
            $fileNameToStoreImage2 = $filePath.'/'.$uniqueFileName;
            $tes = Storage::putFileAs($filePath, $file, $uniqueFileName);
            
        // dd($fileNameToStoreImage1, $fileNameToStoreImage2, $blogSlug, $release_date);
            DB::table('blogs')
            ->insert(['blogTitle' => $request->blogTitle, 'blogSlug' => $blogSlug, 'blogContent' => $request->konten, 'blogImage1' => $fileNameToStoreImage1, 'blogImage2' => $fileNameToStoreImage2, 'blogMetaDescription' => $request->blogMetaDescription, 'blogMetaKeywords' => $request->blogMetaKeywords, 'blogAuthor' => $blogAuthor, 'bio' => $request->bio, 'blogStatus' => $request->blogStatus, 'release_date' => $release_date]);
            
            return redirect(url('/').'/arena/blogs');
        }else{
            
            DB::table('blogs')
            ->insert(['blogTitle' => $request->blogTitle, 'blogSlug' => $blogSlug, 'blogContent' => $request->konten, 'blogImage1' => $fileNameToStoreImage1, 'blogMetaDescription' => $request->blogMetaDescription, 'blogMetaKeywords' => $request->blogMetaKeywords, 'blogAuthor' => $blogAuthor, 'bio' => $request->bio, 'blogStatus' => $request->blogStatus, 'release_date' => $release_date]);
        
            return redirect(url('/').'/arena/blogs');
        }
    }

    public function editBlog(Request $request){

        $validator = $request->validate([
            'blogTitle' => 'required',
            'konten' => 'required',
            'blogMetaDescription' => 'required',
            'blogMetaKeywords' => 'required',
            'blogAuthor' => 'required',
            'bio' => 'required',
            'blogStatus' => 'required', 
            'release_date' => 'required'
            ]);

            $blogAuthor = DB::table('cms_users')
            ->where('name', $request->blogAuthor)
            ->select('id')
            ->first();
        
            $blogAuthor = $blogAuthor->id;
            $blogSlug = str_slug($request->blogTitle);
            $release_date = $request->release_date.' '.date("H:i:s");
            // dd($request->hasFile('_blogImage1'));        

        if($request->hasFile('_blogImage2')){

            $validator = $request->validate([
                '_blogImage2' =>'image|mimes:jpeg,png,jpg|max:2048',
            ]);

            date_default_timezone_set("Asia/Jakarta");
			$file = $request->file('_blogImage2');
            // uniquefilename yang dimasukkan ke database
            $fileName = $request->file('_blogImage2')->getClientOriginalName();
            $uniqueFileName= str_replace(" ","",date('y-m').'/'.$fileName);
            
            $filePath = 'uploads/'.CRUDBooster::myId();
            
            $fileNameToStoreImage2 = $filePath.'/'.$uniqueFileName;
            $tes = Storage::putFileAs($filePath, $file, $uniqueFileName);
            
        // dd($fileNameToStoreImage1, $fileNameToStoreImage2, $blogSlug, $release_date);
            DB::table('blogs')
            ->where('id', $id)
            ->update(['blogTitle' => $request->blogTitle, 'blogSlug' => $blogSlug, 'blogContent' => $request->konten, 'blogImage2' => $fileNameToStoreImage2, 'blogMetaDescription' => $request->blogMetaDescription, 'blogMetaKeywords' => $request->blogMetaKeywords, 'blogAuthor' => $blogAuthor, 'bio' => $request->bio, 'blogStatus' => $request->blogStatus, 'release_date' => $release_date]);
            
            return redirect(url('/').'/arena/blogs');  
        }elseif($request->hasFile('_blogImage1')) {
            
            $validator = $request->validate([
                '_blogImage1' =>'image|mimes:jpeg,png,jpg|max:2048',
                ]);
                
            

            date_default_timezone_set("Asia/Jakarta");
			$file = $request->file('_blogImage1');
            // uniquefilename yang dimasukkan ke database
            $fileName = $request->file('_blogImage1')->getClientOriginalName();            
            $uniqueFileName= str_replace(" ","",date('y-m').'/'.$fileName);            
            $filePath = 'uploads/'.CRUDBooster::myId();            
            $fileNameToStoreImage1 = $filePath.'/'.$uniqueFileName;
            $tes = Storage::putFileAs($filePath, $file, $uniqueFileName);
            
            DB::table('blogs')
            ->where('id', $id)
            ->update(['blogTitle' => $request->blogTitle,
             'blogSlug' => $blogSlug, 
             'blogContent' => $request->konten, 
             'blogImage1' => $fileNameToStoreImage1, 
             'blogMetaDescription' => $request->blogMetaDescription, 
             'blogMetaKeywords' => $request->blogMetaKeywords, 
             'blogAuthor' => $blogAuthor, 
             'bio' => $request->bio, 
             'blogStatus' => $request->blogStatus, 
             'release_date' => $release_date]);
            
            
            return redirect(url('/').'/arena/blogs');
        }elseif ($request->hasFile('_blogImage2') && $request->hasFile('_blogImage1')){
            $validator = $request->validate([
                '_blogImage2' =>'image|mimes:jpeg,png,jpg|max:2048',
                '_blogImage1' =>'image|mimes:jpeg,png,jpg|max:2048',
            ]);

            date_default_timezone_set("Asia/Jakarta");
			$file = $request->file('_blogImage2');
            // uniquefilename yang dimasukkan ke database
            $fileName = $request->file('_blogImage2')->getClientOriginalName();
            $uniqueFileName= str_replace(" ","",date('y-m').'/'.$fileName);
            
            $filePath = 'uploads/'.CRUDBooster::myId();
            
            $fileNameToStoreImage2 = $filePath.'/'.$uniqueFileName;
            $tes = Storage::putFileAs($filePath, $file, $uniqueFileName);

            date_default_timezone_set("Asia/Jakarta");
			$file1 = $request->file('_blogImage1');
            // uniquefilename yang dimasukkan ke database
            $fileName1 = $request->file('_blogImage1')->getClientOriginalName();            
            $uniqueFileName1= str_replace(" ","",date('y-m').'/'.$fileName1);            
            $filePath1 = 'uploads/'.CRUDBooster::myId();            
            $fileNameToStoreImage1 = $filePath1.'/'.$uniqueFileName1;
            $tes1 = Storage::putFileAs($filePath1, $file1, $uniqueFileName1);

            DB::table('blogs')
            ->where('id', $id)
            ->update(['blogTitle' => $request->blogTitle, 'blogSlug' => $blogSlug, 'blogContent' => $request->konten, 'blogImage1' => $fileNameToStoreImage1, 'blogImage2' => $fileNameToStoreImage2, 'blogMetaDescription' => $request->blogMetaDescription, 'blogMetaKeywords' => $request->blogMetaKeywords, 'blogAuthor' => $blogAuthor, 'bio' => $request->bio, 'blogStatus' => $request->blogStatus, 'release_date' => $release_date]);
            return redirect(url('/').'/arena/blogs');

        }else {
            DB::table('blogs')
            ->where('id', $id)
            ->update(['blogTitle' => $request->blogTitle, 'blogSlug' => $blogSlug, 'blogContent' => $request->konten, 'blogMetaDescription' => $request->blogMetaDescription, 'blogMetaKeywords' => $request->blogMetaKeywords, 'blogAuthor' => $blogAuthor, 'bio' => $request->bio, 'blogStatus' => $request->blogStatus, 'release_date' => $release_date]);
            return redirect(url('/').'/arena/blogs');
        }        
    }

    public function blogs(){
        $author = DB::table('cms_users')
        ->where('id_cms_privileges', 3)
        ->get();
        $action = 'add';

        // dd($author);
        return view('web_pages.pages.admin.blog-form', compact('action', 'author'));
    }
    
    public function editBlogs($id){
        $blogData= DB::table('blogs')
        ->where('id', $id)
        ->first();

        $author = DB::table('cms_users')
        ->where('id_cms_privileges', 3)
        ->get();
        
        $action = 'edit';
        return view('web_pages.pages.admin.blog-form', compact('blogData', 'action', 'author'));
    }
    
    public function publishBlogs($id){
        $blogData= DB::table('blogs')
        ->where('id', $id)
        ->select('blogStatus')
        ->first();

        if($blogData->blogStatus == 'active'){
            $blogStatus = 'inactive';
        }else {
            $blogStatus = 'active';
        }
        DB::table('blogs')
        ->where('id', $id)
        ->update(['blogStatus' => $blogStatus]);

        return redirect()->back();
    }
    
    public function disclaimer(){
        return view('web_pages.pages.disclaimer');
    }
    
    //Controller from andika
    public function redeemCoupon(){
        $voucher = DB::table('vouchers')
        ->where('expireVoucher','>',date('Y-m-d H:i:s'))
        ->where('sisaVoucher','>',0)
        ->get();
        return view('web_pages.pages.redeem-coupon',compact('voucher'));
    }

    public function paymentHistory() {
        $jasa = DB::table('jasa')->get();
        $checkout = DB::table('sales')->where('salesPaymentStatus','!=','menunggu')->where('user_id',CRUDBooster::myId())->get();
        // $checkout = DB::table('sales')->where('salesStatus','!=','menunggu pembayaran')->where('user_id',CRUDBooster::myId())->get();
        // dd($checkout);
        return view('web_pages.pages.payment-history',compact('jasa','checkout'));
    }

    //Controller from Eza
    public function register()
    {
        return view('web_pages.pages.register');
    }

    public function forgotPassword()
    {
        return view('web_pages.pages.forgot-pass');
    }

    public function resetPassword()
    {
        return view('web_pages.pages.reset-pass');
    }
    public function article($slug)
    {
        $article=DB::table('blogs')->where('blogs.blogSlug',$slug)->join('cms_users','cms_users.id', 'blogs.blogAuthor')->first();
        $totalWord=str_word_count($article->blogContent);
        $mostReadTime=$totalWord/255;
        $mostReadTime=ceil($mostReadTime);
        $readingTime=$mostReadTime;
        $blogs=DB::table('blogs')->orderBy('blogs.blogRead','desc')->where('blogs.blogStatus','active')->join('cms_users','cms_users.id','blogs.blogAuthor')->paginate(3);
        $totalRead=$article->blogRead;
        $totalRead+=1;
        DB::table('blogs')->where('blogSlug', $slug)
          ->update(['blogRead' => $totalRead]);
        return view('web_pages.pages.article',compact('article','readingTime','blogs'));
    }

    public function checkoutIndex($id){
        $jasa = DB::table('jasa')->get();
        $checkout = DB::table('sales')->where('user_id',CRUDBooster::myId())->where('salesStatus','menunggu pembayaran')->where('id',$id)->first();
        $cart = unserialize($checkout->salesSart);
        return view('web_pages.pages.checkout',compact('checkout','jasa','cart'));
      }



      //Agerabot v2
      public function homePage()
    {
        $robots = DB::table('robots')->get();
        $testimoni = DB::table('testimoni')->get();
        foreach ($robots as $robot) {
            // dd($robot);
            $pullStat = DB::table('robotstatistic')->where('stat_robot',$robot->id)->where('stat_pair','xauusd')->orderby('id','desc')->first();
            $data[] = [
              'robot' => $robot->id,
              'robotLongName' => $robot->robotLongName,
              'pair' => 'xauusd',
              'month_percent' => $pullStat->stat_monthly,
              
            ];
          }
    
          $sort = "month_percent";
    
          $robot = collect($data)->sortBy($sort)->reverse()->toArray();
        // dd($robot);
        return view('pages-v2.homepage', compact('robot', 'testimoni'));
    }


    public function statistic_robot($periode,$type,$robot){
        $pullPair = DB::table('robotstatistic')->where('stat_robot', $robot)->distinct()->select('stat_pair')->get();
        // $pullPair = DB::table('robotstatistic')->where('stat_robot', $robot->stat_robot)->distinct()->select('stat_pair')->get();
        // dd($pullPair);
        // dd($robot);
        $pairArray = [];
        foreach ($pullPair as $key => $value) {
            // dd($value);
            array_push($pairArray, $value->stat_pair);
        };
        // foreach ($pullPair as $pair) {
           
            $limit = count($pairArray);
            $pullRobot = DB::table('robotstatistic')->where('stat_robot', $robot)->whereIn('stat_pair', $pairArray)->orderby('id','desc')->limit($limit)->get();
            // dd($pullRobot, $pairArray);

            
            // dd($robot->stat_robot);
            // $pullRobot = DB::table('robotstatistic')->where('stat_robot', $robot->stat_robot)->where('stat_pair',$pair->stat_pair)->orderby('id','desc')->first();
            // $pullRobot = DB::table('robotstatistic')->where('stat_robot', $robot)->where('stat_pair',$pair->stat_pair)->orderby('id','desc')->first();
            // if($pullRobot->stat_monthly != null){
                // $data[] = [
                //     // 'robot' => $robot->stat_robot,
                //     'robot' => $robot,
                //     'pair' => $pair->stat_pair,
                //     'last_percent' => $pullRobot->stat_lastten,
                //     'last_total' => $pullRobot->stat_totalten,
                //     'week_percent' => $pullRobot->stat_weekly,
                //     'week_total' => $pullRobot->stat_totalweekly,
                //     'month_percent' => $pullRobot->stat_monthly,
                //     'month_total' => $pullRobot->stat_totalmonthly,
                //     'year_percent' => $pullRobot->stat_yearly,
                //     'year_total' => $pullRobot->stat_totalyearly,
                // ];
            // }
            
        // }
        

        $sort = $periode."_".$type;
        // dd($pullRobot);
        // $array = collect($data)->sortBy($sort)->reverse()->toArray();
        $array = collect($pullRobot)->sortBy($sort)->reverse()->toArray();
        // var_dump(count($array));
        // dd($array);

        return response()->json($array);
    }

    public function robotListPage()
    {

        $pullRobot = DB::table('robotstatistic')->distinct()->select('stat_robot')->get();

        $userId = CRUDBooster::myId();
        // $userId = 89;
        // dd($pullRobot);
        $type = 'percent';
        $periode = 'month';
            foreach ($pullRobot as $eachRobot) {
                // dd($eachRobot);
                $result = $this->statistic_robot($type,$periode,$eachRobot->stat_robot);
                $data = $result->getData();

                $urut = [];
                // dd($data);
                foreach ($data as $key => $value) {
                    $urut[] = $value;
                }
                // dd($urut[0]);
                //get Robot
                // $robot = DB::table('robots')->where('id',$urut[0]->stat_robot)->first();
                $robot = DB::table('robots')->where('id',$urut[0]->stat_robot)->first();
                // var_dump($robot);
                // dd($robot, $urut);
                if($userId == null){
                    $dataRobot[] = [
                        // 'robot'=>$robot->id, 
                        // 'pair'=>$urut[0]->pair,
                        // 'last_percent'=>$urut[0]->last_percent,
                        // 'last_total'=>$urut[0]->last_total,
                        // 'week_percent'=>$urut[0]->week_percent,
                        // 'week_total'=>$urut[0]->week_total,
                        // 'month_percent'=>$urut[0]->month_percent,
                        // 'month_total'=>$urut[0]->month_total,
                        // 'year_percent'=>$urut[0]->year_percent,
                        // 'year_total'=>$urut[0]->year_total,
                        // 'robotName' => $robot->robotLongName,
                        // 'robotImage' => $robot->robotImage,
                        // 'tag' => $robot->robotSlugName,
                        'robot'=>$robot->id, 
                        'pair'=>$urut[0]->stat_pair,
                        'last_percent'=>$urut[0]->stat_lastten,
                        'last_total'=>$urut[0]->stat_totalten,
                        'week_percent'=>$urut[0]->stat_weekly,
                        'week_total'=>$urut[0]->stat_totalweekly,
                        'month_percent'=>$urut[0]->stat_monthly,
                        'month_total'=>$urut[0]->stat_totalmonthly,
                        'year_percent'=>$urut[0]->stat_yearly,
                        'year_total'=>$urut[0]->stat_totalyearly,
                        'robotName' => $robot->robotLongName,
                        'robotImage' => $robot->robotImage,
                        'tag' => $robot->robotSlugName,
                    ];
                }else {
                    $checkSubs = DB::table('wishlist')
                    ->where('userId', $userId)
                    ->where('robotId', $robot->id)
                    ->first();
                    // dd($checkSubs);
                    if($checkSubs == null){
                        $dataRobot[] = [
                            'robot'=>$robot->id, 
                            'pair'=>$urut[0]->stat_pair,
                            'last_percent'=>$urut[0]->stat_lastten,
                            'last_total'=>$urut[0]->stat_totalten,
                            'week_percent'=>$urut[0]->stat_weekly,
                            'week_total'=>$urut[0]->stat_totalweekly,
                            'month_percent'=>$urut[0]->stat_monthly,
                            'month_total'=>$urut[0]->stat_totalmonthly,
                            'year_percent'=>$urut[0]->stat_yearly,
                            'year_total'=>$urut[0]->stat_totalyearly,
                            'robotName' => $robot->robotLongName,
                            'robotImage' => $robot->robotImage,
                            'subs' => false,
                            'tag' => $robot->robotSlugName,
                        ];
                    }else {
                        $dataRobot[] = [
                            'robot'=>$robot->id, 
                            'pair'=>$urut[0]->stat_pair,
                            'last_percent'=>$urut[0]->stat_lastten,
                            'last_total'=>$urut[0]->stat_totalten,
                            'week_percent'=>$urut[0]->stat_weekly,
                            'week_total'=>$urut[0]->stat_totalweekly,
                            'month_percent'=>$urut[0]->stat_monthly,
                            'month_total'=>$urut[0]->stat_totalmonthly,
                            'year_percent'=>$urut[0]->stat_yearly,
                            'year_total'=>$urut[0]->stat_totalyearly,
                            'robotName' => $robot->robotLongName,
                            'robotImage' => $robot->robotImage,
                            'subs' => true,
                            'tag' => $robot->robotSlugName,
                        ];
                    }
                }
            }
            // var_dump($dataRobot);
            // dd($dataRobot);
            $sort = "month_percent";
            // $finalRobotLists = collect($dataRobot)->sortBy($sort)->reverse()->toArray();
            $finalRobotLists = collect($dataRobot)->sortBy($sort)->reverse();
            // dd($finalRobotLists);
            $dataFinal = $this->paginate($finalRobotLists);
            // dd($finalRobotLists, $dataFinal);

            // dd($dataFinal);
            // $sendData = ['robotLists'=>$finalRobotLists, 'pair'=>$pullPairs];
        return view('pages-v2.robot-list', compact('dataFinal', 'pair'));
    }

    public function robotPage(){
        $pullPair = DB::table('robotStatistic')->distinct()->select('stat_pair')->get();
        // dd($pullPair);
        return view('pages-v2.robot', compact('pullPair'));

    }

    public function profitableRobot()
    {
        $pullPair = DB::table('robotStatistic')->distinct()->select('stat_pair')->get();
        $pullRobots = DB::table('robots')->select('robotLongName', 'id')->get();
        
        foreach ($pullRobots as $key=>$value) {
            $getDownloads = DB::table('robotlicense')->where('robotId', $value->id)->count();
            $getFavorite = DB::table('wishlist')->where('robotId', $value->id)->count();
            if($getDownloads == null){
                $pullRobots[$key]->downloads = 0;
            }else {
                $pullRobots[$key]->downloads = $getDownloads;
            }
            $pullRobots[$key]->favorite = $getFavorite;
        }

        $sort = 'downloads';
        $popularRobots = collect($pullRobots)->sortBy($sort)->reverse()->slice(0,10)->toArray();
        dd(array_keys($popularRobots), $popularRobots);
        return view('pages-v2.robot', compact('pullPair', 'popularRobots'));
    }

    // for future robot by popularity
    public function popularRobot()
    {
        $pullRobots = DB::table('robots')->select('robotLongName', 'id')->get();
        
        foreach ($pullRobots as $key=>$value) {
            $getDownloads = DB::table('robotlicense')->where('robotId', $value->id)->count();
            $getFavorite = DB::table('wishlist')->where('robotId', $value->id)->count();
            if($getDownloads == null){
                $pullRobots[$key]->downloads = 0;
            }else {
                $pullRobots[$key]->downloads = $getDownloads;
            }
            $pullRobots[$key]->favorite = $getFavorite;
        }

        $sort = 'downloads';
        $popularRobots = collect($pullRobots)->sortBy($sort)->reverse()->slice(0,10);
        return view('pages-v2.robot', compact('popularRobots'));
    }

    public function wishlist(Request $request)
    {
        // dd($request);
        // $robotId = 'robot';
        
        if($request->ajax()){
            $robotId = $request->query('robot');
            
            $checkSubscription = DB::table('wishlist')->where('userId', CRUDBooster::myId())->where('robotId', $robotId)->first();
            $robot = DB::table('robots')->where('id', $robotId)->first();
            $slug = $robot->robotSlugName;
            // dd($checkSubscription);
            if($checkSubscription == null) {
                $subs = true;
                DB::table('wishlist')->insert(['userId'=>CRUDBooster::myId(), 'robotId'=>$robotId]);
            }else{
                $subs = false;
                DB::table('wishlist')->where('userId',CRUDBooster::myId())->where('robotId',$robotId)->delete();
            }
            return response()->json(compact('subs','slug'))->setStatusCode(200);
            // return view('pages-v2.robot-list-card', compact('subs', 'slug'))->render();
            // return redirect()->back();
        };
        
    }

    public function robotComparePage()
    {
        $pullRobot = DB::table('robots')->get();
        $pullPairs = DB::table('robotstatistic')->distinct()->select('stat_pair')->get();

        return view('pages-v2.robot-compare', compact('pullRobot', 'pullPairs'));
    }

    public function subsPage()
    {
        $datas = DB::table('package')->get();

        // dd($datas);
        return view('pages-v2.subscription', compact('datas'));
    }

    public function regisPage()
    {
        return view('pages-v2.register');
    }
    public function paginate($items, $perPage = 12, $page = null, $options = [])
    {   
        // dd($items);
        // $page = LengthAwarePaginator::resolveCurrentPage();
        // $items = $items instanceof Collection ? $items : Collection::make($items);
        // return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
        $currentPage = LengthAwarePaginator::resolveCurrentPage();
    // set limit 
    // $perPage = 20;
    // generate pagination
    $currentResults = $items->slice(($currentPage - 1) * $perPage, $perPage)->all();
    $results = new LengthAwarePaginator($currentResults, $items->count(), $perPage);
    $results->withPath('robot-list')->links();

    // dd($currentPage);
    return $results;
    }

    public function loginPage()
    {
        return view('pages-v2.login');
    }

    public function checkoutPage()
    {
        $datas = DB::table('package')->get();
        $datasLength = $datas->count();
        // dd(Cart::content());
        // dd($datasLength);
        return view('pages-v2.checkout', compact('datas', 'datasLength'));
    }

    public function checkoutVpsPage()
    {
        $datas = \DB::table('vps')->select('catvps.id', 'catvps.nmVPS', 'catvps.detail')->leftJoin('catvps','catvps.id','vps.catVpsId')->groupBy('catvps.id')->get();
        foreach($datas as $key => $data){
            $value = \DB::table('vps')->select('vps.id as vpsId', 'vps.interval','vps.price','vps.slug')->where('catVpsId',$data->id)->get();
            $data->vps=$value;
        }
        // dd($datas[0]->vps);
        return view('pages-v2.checkout-vps', compact('datas'));
    }

    public function checkoutInstallPage()
    {
        return view('pages-v2.checkout-install');
    }

    public function dashboard()
    {   
        $userId = CRUDBooster::myId();
        // $userId = 89;

        // subscription
        $getActiveTime = DB::table('subscription')->where('subscription.userId', $userId)->leftjoin('subscriptionhistory', 'subscription.orderId', 'subscriptionhistory.orderId')->orderby('activeTime', 'desc')->first();
        if($getActiveTime != Null){
            $subscription = DB::table('package')->where('id', $getActiveTime->packageId)->select('nmPackage')->first();
            // dd($subscription->nmPackage);
            $active = strtotime($getActiveTime->activeTime);
            if($subscription->nmPackage == '1 Month'){
                
                // cek berapa lama langganannya
                $subsExpired = date('d M Y', strtotime("+1 month", $active));
            }elseif ($subscription->nmPackage == '3 Months'){
                // cek berapa lama langganannya
                $subsExpired = date('d M Y', strtotime("+3 months", $active));
            }elseif ($subscription->nmPackage == '12 Months'){
                // cek berapa lama langganannya
                $subsExpired = date('d M Y', strtotime("+1 year", $active));
            }
        }else {
            $subsExpired = 'null';
        }
        
        // dd($active);

        $quota = DB::table('installquota')->where('userId', $userId)->select('sisaQuota')->first();
        if($quota != null) {
            // dd($quota);
            $quota = $quota->sisaQuota;
        }else {
            $quota = 0;
        }
        // dd($quota);
        // vps
        $vps = DB::table('paymentvps')->where('userId', $userId)->where('payment', 1)->orderBy('paymentTime', 'desc')->first();
        // dd($vps);
        if($vps){
            
                $paymentTime = $vps->paymentTime;
                // cek berapa lama
                $paymentExpired = date('d M Y', strtotime("+1 month", $paymentTime));
                // $vps->paymentExpired = $paymentExpired;
            $data['status'] = $vps->status;  
            $data['paymentExpired'] = $paymentExpired;  
            
        }
        
        // untuk quota kalau tidak ada 0

        $robotList = DB::table('trader')->where('trader.userId', $userId)->rightjoin('robots', 'trader.robotName', 'robotSlugName')->select('trader.last_transaction_at', 'trader.licenseKey', 'trader.status', 'robots.robotLongName')->get();
        // dd($robotList);
        foreach ($robotList as $robot=>$value){
            $lastTransaction = new \DateTime(date('Y-m-d H:i:s',strtotime($value->last_transaction_at)));
            $currentDate = new \DateTime(date('Y-m-d H:i:s'));
            $getInterval = $lastTransaction->diff($currentDate);
            $interval = (int) $getInterval->format("%R%a");
        
            if($interval <= 2){
                $robotList[$robot]->health = "okay";
            }else{
                $robotList[$robot]->health = "not okay";
            }
        }
        // dd($subscription);
        return view('pages-v2.dashboard.dash-home', compact('subscription', 'subsExpired', 'quota', 'data', 'robotList'));
    }

    public function dashRobotList()
    {
        $userId = CRUDBooster::myId();
        // $userId = 89;
        $getRobots = DB::table('robotStatistic')->distinct()->select('stat_robot')->get();
        $type = 'percent';
        $periode = 'year';
        foreach ($getRobots as $robot) {
            // dd($robot);
            $result = $this->statistic_robot($type,$periode,$robot->stat_robot);
            // dd($result);
            $data = $result->getData();
            // dd($data);

            $urut = [];
            foreach ($data as $key => $value) {
                // dd($value);
                $urut[] = $value;
            }
            // $robot = DB::table('robots')->where('id',$urut[0]->robot)->first();
            $robot = DB::table('robots')->where('id',$urut[0]->stat_robot)->first();
            
            // dd($urut[0]->stat_robot, $cekLastLicense);
                $dataRobot[] = [
                    'robot'=>$robot->id, 
                    'pair'=>$urut[0]->pair,
                    'last_percent'=>$urut[0]->stat_lastten,
                    'last_total'=>$urut[0]->stat_totalten,
                    'week_percent'=>$urut[0]->stat_weekly,
                    'week_total'=>$urut[0]->stat_totalweekly,
                    'month_percent'=>$urut[0]->stat_monthly,
                    'month_total'=>$urut[0]->stat_totalmonthly,
                    'year_percent'=>$urut[0]->stat_yearly,
                    'year_total'=>$urut[0]->stat_totalyearly,
                    'robotName' => $robot->robotLongName,
                    'robotImage' => $robot->robotImage,
                    'tag' => $robot->robotSlugName,
                ];
        }

        $sort = $periode.'_'.$type;

        $finalRobot = collect($dataRobot)->sortBy($sort)->reverse()->toArray();
        $robotSubs = DB::table('robotlicense')->where('userId', $userId)->get();
        $getUserSubs = DB::table('cms_users')->where('id', $userId)->select('status')->first();
        foreach ($finalRobot as $key => $value) {
            foreach ($robotSubs as $indicator => $item) {
                // dd($value['robot'], $item->robotId);
                if($value['robot'] == $item->robotId){
                    $finalRobot[$key]['subs'] = true;
                }
            }
        }
        
        // cek user quota
        // $quota = DB::table('installquota')->where('userId', $userId)->first();
        // $userQuota = $quota->sisaQuota;
        $packageStatus = DB::table('cms_users')->where('id', $userId)->select('status')->first();
        // dd($finalRobot);
        $packageId = DB::table('subscription')->where('userId', $userId)->select('packageId')->orderBy('paymentTime', 'desc')->first();
        // dd($userId, $packageId);
        $time = Carbon::now()->timestamp;
        //   return response()->json($array);
        return view('pages-v2.dashboard.dash-myrobot-list', compact('finalRobot', 'getUserSubs', 'packageStatus', 'packageId', 'userId', 'time'));
    }

    public function dasboardWishlist(){
        $userId = CRUDBooster::myId();
        $type = 'percent';
        $periode = 'year';
        // $userId = 4;
        $wishlistRobots = DB::table('wishlist')->where('userId', $userId)->select('robotId')->get();
        // dd($wishlistRobots);
        if($wishlistRobots){
            foreach ($wishlistRobots as $robot) {
                $result = $this->statistic_robot($type,$periode,$robot->robotId);
                // dd($result);
                $data = $result->getData();
                // dd($data);
    
                $urut = [];
                foreach ($data as $key => $value) {
                    // dd($value);
                    $urut[] = $value;
                }
                // $robot = DB::table('robots')->where('id',$urut[0]->robot)->first();
                $robot = DB::table('robots')->where('id',$urut[0]->robot)->first();
                    $dataRobot[] = [
                        'robot'=>$robot->id, 
                        'pair'=>$urut[0]->pair,
                        'last_percent'=>$urut[0]->last_percent,
                        'last_total'=>$urut[0]->last_total,
                        'week_percent'=>$urut[0]->week_percent,
                        'week_total'=>$urut[0]->week_total,
                        'month_percent'=>$urut[0]->month_percent,
                        'month_total'=>$urut[0]->month_total,
                        'year_percent'=>$urut[0]->year_percent,
                        'year_total'=>$urut[0]->year_total,
                        'robotName' => $robot->robotLongName,
                        'robotImage' => $robot->robotImage,
                        'tag' => str_slug($robot->robotLongName),
                    ];
            }
    
            $sort = 'year_percent';
    
            $finalRobotWish = collect($dataRobot)->sortBy($sort)->reverse()->toArray();
            // dd($finalRobotWish);
            return view('pages-v2.dashboard.dashboard-wishlist', compact('finalRobotWish'));
        }else{

        }
        
    }

    public function dashUpgrade()
    {
        $userId = CRUDBooster::myId();
        // $userId = 89;
        // dd($userId);
        $inv = DB::table('subscription')->where('userId', $userId)->select('externalId')->first();
        $robots = DB::table('trader')->where('trader.userId', $userId)->rightjoin('robots', 'trader.robotName', 'robots.robotSlugName')->select('trader.status', 'robots.robotLongName', 'trader.userId', 'robots.id', 'trader.licenseKey')->get();
        $robotLicense = DB::table('robotlicense')->where('userId', $userId)->distinct()->select('robotlicense')->get();
        $qtyLicense = count($robotLicense);
        if($qtyLicense > 3){
            $licenseQuota = false;
        }else {
            $licenseQuota = true;
        }

        foreach($robots as $key=>$value){
            $robotStatus = DB::table('robotlicense')->where('userId', $value->userId)->where('robotId', $value->id)->where('robotLicense', $value->licenseKey)->first();
            // dd($robotStatus);
            $robots[$key]->vpsStatus = $robotStatus->vps;
            $robots[$key]->installmentStatus = $robotStatus->installment;
        }
        $packageId = DB::table('subscription')->where('userId', $userId)->select('packageId')->orderBy('paymentTime', 'desc')->first();
        $time = Carbon::now()->timestamp;

        // dd($robots);
        return view('pages-v2.dashboard.dash-upgrade', compact('inv', 'robots', 'userId', 'packageId', 'time'));
    }

    public function transactionHistory()
    {
        $userId = CRUDBooster::myId();
        // $userId = 89;
        $unpaidSubs = DB::table('subscription')->where('subscription.payment', 0)->where('subscription.userId', $userId)->leftjoin('package', 'subscription.packageId',  'package.id')->select('package.nmPackage', 'subscription.externalId', 'subscription.id')->get();
        $unpaidVps = DB::table('paymentvps')->where('paymentvps.payment', 0)->where('paymentvps.userId', $userId)->leftjoin('vps', 'paymentvps.vpsId', 'vps.id')->leftjoin('catvps', 'vps.catVpsId', 'catvps.id')->select('paymentvps.id', 'catvps.nmVps', 'paymentvps.externalId')->get();
        $unpaidInstall = DB::table('paymentinstall')->where('payment', 0)->where('paymentinstall.userId', $userId)->get();
        
        return view('pages-v2.dashboard.dash-transaction', compact('unpaidSubs', 'unpaidVps', 'unpaidInstall'));
    }

    public function transactionDone()
    {
        $userId = CRUDBooster::myId();
        // $userId = 89;
        $paidSubs = DB::table('subscription')->where('subscription.payment', 1)->where('subscription.userId', $userId)->leftjoin('subscriptionhistory', 'subscription.orderId', 'subscriptionhistory.orderId')->leftjoin('package', 'subscription.packageId', 'package.id')->select('package.nmPackage', 'subscriptionhistory.dateSubs', 'subscriptionhistory.activeTime', 'subscriptionhistory.paymentMethod', 'package.interval')->get();
        foreach ($paidSubs as $key => $value) {
            // dd($value->interval);
            if($value->interval == 1){
                $activeTime = strtotime($value->activeTime);
                $expiredTime = strtotime('+1 month', $activeTime);
                $expiredDate = date('d M Y',$expiredTime);
                $activeDate = date('d M Y',$activeTime);
                // dd($expiredDate);
                $currentDate = Carbon::now()->timestamp;
                if($currentDate > $expiredTime){
                    $paidSubs[$key]->expired = 'Expired';
                    $paidSubs[$key]->expiredDate = $expiredDate;
                    $paidSubs[$key]->activeTime = $activeDate;
                }else{
                    $paidSubs[$key]->expired = 'Active';
                    $paidSubs[$key]->expiredDate = $expiredDate;
                    $paidSubs[$key]->activeTime = $activeDate;
                }
            }
            if($value->interval == 3){
                $activeTime = strtotime($value->activeTime);
                $expiredTime = strtotime('+3 months', $activeTime);
                $expiredDate = date('d M Y',$expiredTime);
                $activeDate = date('d M Y',$activeTime);
                $currentDate = Carbon::now()->timestamp;
                // dd($expiredDate);
                if($currentDate > $expiredTime){
                    // dd('expired');
                    $paidSubs[$key]->expired = 'Expired';
                    $paidSubs[$key]->expiredDate = $expiredDate;
                    $paidSubs[$key]->activeTime = $activeDate;
                }else{
                    $paidSubs[$key]->expired = 'Active';
                    $paidSubs[$key]->expiredDate = $expiredDate;
                    $paidSubs[$key]->activeTime = $activeDate;
                }
            }
            if($value->interval == 12){
                $activeTime = strtotime($value->activeTime);
                $expiredTime = strtotime('+1 year', $activeTime);
                $currentDate = Carbon::now()->timestamp;
                $expiredDate = date('d M Y',$expiredTime);
                $activeDate = date('d M Y',$activeTime);
                // dd($expiredDate);
                if($currentDate > $expiredTime){
                    // dd('expired');
                    $paidSubs[$key]->expired = 'Expired';
                    $paidSubs[$key]->expiredDate = $expiredDate;
                    $paidSubs[$key]->activeTime = $activeDate;
                }else{
                    $paidSubs[$key]->expired = 'Active';
                    $paidSubs[$key]->expiredDate = $expiredDate;
                    $paidSubs[$key]->activeTime = $activeDate;
                }
            }
        }

        $paidVps = DB::table('paymentvps')->where('paymentvps.payment', 1)->where('paymentvps.userId', $userId)->leftjoin('vps', 'paymentvps.vpsId', 'vps.id')->leftjoin('paymentvpshistory', 'paymentvps.orderId', 'paymentvpshistory.orderId')->get();
        // dd($paidVps);
        foreach ($paidVps as $key=>$value) {
            $vpsName = DB::table('catvps')->where('id', $value->catVpsId)->first();
            $paidVps[$key]->name = $vpsName->nmVps;
        }
        foreach ($paidVps as $key=>$value) {
            if($value->interval == 1){
                $activeTime = strtotime($value->activeTime);
                $expiredTime = strtotime('+1 month', $activeTime);
                $expiredDate = date('d M Y',$expiredTime);
                $activeDate = date('d M Y',$activeTime);
                $currentDate = Carbon::now()->timestamp;
                if($currentDate > $expiredTime){
                    $paidVps[$key]->expired = 'Expired';
                    $paidVps[$key]->expiredDate = $expiredDate;
                    $paidVps[$key]->activeTime = $activeDate;
                }else{
                    $paidVps[$key]->expired = 'Active';
                    $paidVps[$key]->expiredDate = $expiredDate;
                    $paidVps[$key]->activeTime = $activeDate;
                }
            }
            if($value->interval == 3){
                $activeTime = strtotime($value->activeTime);
                $expiredTime = strtotime('+3 months', $activeTime);
                $currentDate = Carbon::now()->timestamp;
                $expiredDate = date('d M Y',$expiredTime);
                $activeDate = date('d M Y',$activeTime);
                // dd($expiredTime, $currentDate);
                if($currentDate > $expiredTime){
                    // dd('expired');
                    $paidVps[$key]->expired = 'Expired';
                    $paidVps[$key]->expiredDate = $expiredDate;
                    $paidVps[$key]->activeTime = $activeDate;
                }else{
                    $paidVps[$key]->expired = 'Active';
                    $paidVps[$key]->expiredDate = $expiredDate;
                    $paidVps[$key]->activeTime = $activeDate;
                }
            }
            if($value->interval == 12){
                $activeTime = strtotime($value->activeTime);
                $expiredTime = strtotime('+1 year', $activeTime);
                $currentDate = Carbon::now()->timestamp;
                $expiredDate = date('d M Y',$expiredTime);
                $activeDate = date('d M Y',$activeTime);
                if($currentDate > $expiredTime){
                    // dd('expired');
                    $paidVps[$key]->expired = 'Expired';
                    $paidVps[$key]->expiredDate = $expiredDate;
                    $paidVps[$key]->activeTime = $activeDate;
                }else{
                    $paidVps[$key]->expired = 'Active';
                    $paidVps[$key]->expiredDate = $expiredDate;
                    $paidVps[$key]->activeTime = $activeDate;
                }
            }
        }

        $paidInstall =  DB::table('paymentinstall')->where('payment', 1)->where('userId', $userId)->get();
        foreach ($paidInstall as $key=>$value) {
            $paymentTime = date('d M Y', strtotime($value->paymentTime));
            $paidInstall[$key]->paymentTime = $paymentTime;
            // dd($paymentTime);
        }
        return view('pages-v2.dashboard.dash-transaction-done', compact('paidSubs', 'paidInstall', 'paidVps'));
    }

    public function productPage()
    {
        $subscription = DB::table('package')->get();
        
        $vps1 = DB::table('vps')->where('catVpsId', 1)->get();
        $vps2 = DB::table('vps')->where('catVpsId', 2)->get();

        // dd($vps1, $subscription, $vps2);
        return view('pages-v2.dashboard.dash-product', compact('subscription', 'vps1', 'vps2'));
    }

    public function vpsPage()
    {
        return view('pages-v2.dashboard.dash-vps');
    }

    
    public function submitChangePassword(Request $request){
        $oldPassword = DB::table('cms_users')
        ->where('id', CRUDBooster::myId())
        ->select('password')
        ->first();

        $validator = $request->validate([
            'passwordLama' => 'required',
            'passwordBaru' => 'required',
            'confirmasiPasswordBaru' =>'same:passwordBaru'
        ],
        [
            'passwordLama.required' => 'check your password Lama!',
            'passwordBaru.required' => 'check your password Baru!',
            'confirmasiPasswordBaru.same' => 'check your password Baru and Lama!'
        ]);

        // dd($request->passwordLama, $oldPassword[0]->password);
        if(\Hash::check($request->passwordLama, $oldPassword->password)){
            DB::table('cms_users')
            ->where('id', CRUDBooster::myId())
            ->update(['password' => \Hash::make($request->passwordBaru)]);

            $success= 'your Password has been successfuly changed';
            return redirect()->back()->with(['success'=>$success]);
        }else{
            return redirect()->back();
            // return redirect()->back()->withErrors(['msg', 'check the input make sure you already fill all the input']);
            
        }
    }

    public function submitProfile(Request $request){
        $validator = $request->validate([
            'name' => 'required|string|min:3',
            'phoneNumber' => 'required|numeric',
            'username'=>'required',
            'country'=>'required'
        ],
        [
            'name.required' => 'please fill out your name field',
            'phoneNumber.required' => 'please fill out your phone number field'
        ]);

        $success = 'your data has been successfully changed';

        if($request->hasFile('photo')){
            $validator = $request->validate([
            'photo' => 'image|mimes:jpeg,png,jpg'
            ]);
            
            date_default_timezone_set("Asia/Jakarta");
			$file = $request->file('photo');
            // uniquefilename yang dimasukkan ke database
            $fileName = $request->file('photo')->getClientOriginalName();
            $uniqueFileName= str_replace(" ","",time()."_".$fileName);
            
            $filePath = 'uploads/'.CRUDBooster::myId();
            
            $fileNameToStore = $filePath.'/'.$uniqueFileName;
            $tes = Storage::putFileAs($filePath, $file, $uniqueFileName);
            // dd($fileNameToStore);
            // insert into report
            DB::table('cms_user')
            ->update(['photo' => $fileNameToStore, 'name'=>$request->name, 'phoneNumber'=>$request->phoneNumber, 'username'=> $request->username, 'country'=>$request->country]);
            return redirect()->with(['success'=>$success]);
        }else{
            DB::table('cms_users')
            ->where('id', CRUDBooster::myId())
            ->update([
                'name' => $request->name,
                'phoneNumber' => $request->phoneNumber,
                'username'=> $request->username, 
                'country'=>$request->country,
            ]);
        

            return redirect()->back()->with(['success'=>$success]);
        }
    }
        
    // DASHBOARD SETTING
    public function settingPage()
    {
        $user = DB::table('cms_users')
        ->where('id', CRUDBooster::myId())
        // ->where('id', 89)
        ->select('name','email', 'phoneNumber', 'username', 'country')
        ->first();
        // dd($user);
        return view('pages-v2.dashboard.dashboard-setting.dash-setting', compact('user'));
    }

    public function setPassword()
    {
        $userData = DB::table('cms_users')
        ->where('id',  CRUDBooster::myId())
        ->select('providerOrigin')
        ->first();
        return view('pages-v2.dashboard.dashboard-setting.dash-password', compact('userData'));
    }
    
    public function termsPage()
    {
        return view('pages-v2.dashboard.dashboard-setting.dash-terms');
    }

    public function planPage()
    {
        return view('pages-v2.dashboard.dashboard-setting.dash-plan');
    }

    public function helpPage()
    {
        return view('pages-v2.dashboard.dash-help');
    }

    public function manualPage()
    {
        return view('pages-v2.dashboard.dash-manual');
    }

    public function manualVpsPage()
    {
        return view('pages-v2.dashboard.dash-manual-vps');
    }

    public function howToPage()
    {
        return view('pages-v2.how-to-use');
    }

    public function howUninstall()
    {
        return view('pages-v2.how-to-uninstall');
    }
    public function howtoVPS()
    {
        return view('pages-v2.how-to-vps');
    }

    public function termsCondition()
    {
        return view('pages-v2.terms');
    }

    public function disclaimerPage()
    {
        return view('pages-v2.disclaimer');
    }

    public function privacyPage()
    {
        return view('pages-v2.privacy-policy');
    }

    public function blogPage(Request $request)
    {
        // all
        if($request->ajax()){
            if ($request->query('slug') == 'all'){
                $mostRead=DB::table('blogs')->where('blogs.blogStatus','active')->join('cms_users','cms_users.id','blogs.blogAuthor')->select('blogs.id as blogId', 'blogs.blogTitle', 'blogs.blogSlug', 'blogs.blogContent', 'blogs.blogImage1', 'blogs.blogAuthor', 'blogs.blogStatus', 'cms_users.id', 'cms_users.name', 'cms_users.photo')->orderBy('blogRead','desc')->first();
                $exclude = [$mostRead->blogId];
                $slug = $request->query('slug');
                $getAllBlog = DB::table('blogs')->join('cms_users','cms_users.id','blogs.blogAuthor')->select('blogs.id as blogId', 'blogs.blogTitle', 'blogs.blogSlug', 'blogs.blogContent', 'blogs.blogImage1', 'blogs.blogAuthor', 'blogs.blogStatus', 'cms_users.id', 'cms_users.name', 'cms_users.photo')->get();
                $blogsData = collect($getAllBlog)->whereNotIn('blogId',$exclude);
                foreach ($blogsData as $key => $value) {
                    $totalWord=str_word_count($value->blogContent);
                    $readTime=$totalWord/255;
                    $readTime=ceil($readTime);
                    $readingTime=$readTime;
                    $blogsData[$key]->readingTime = $readingTime;
                    $blogsData[$key]->type = 'all';
                };
                // $finalBlogsData = $this->paginate2($blogsData, 'all');
                $finalBlogsData = $this->paginate2($blogsData);
                // return response()->json($finalBlogsData);
                // dd($finalBlogsData);
                return view('pages-v2.blog-blade-card', compact('finalBlogsData', 'type'))->render();
            }

        }else {
            
            // weekly
            //mostRead Weekly
            $mostRead=DB::table('blogs')->where('blogs.blogStatus','active')->where('blogs.highlight', 1)->join('cms_users','cms_users.id','blogs.blogAuthor')->select('blogs.id as blogId', 'blogs.blogTitle', 'blogs.blogSlug', 'blogs.blogContent', 'blogs.blogImage1', 'blogs.blogAuthor', 'blogs.blogStatus', 'cms_users.id', 'cms_users.name', 'cms_users.photo')->orderBy('blogRead','desc')->first();
            // dd($mostRead->id);
            $totalWord=str_word_count($mostRead->blogContent);
            $mostReadTime=$totalWord/255;
            $mostReadTime=ceil($mostReadTime);
            $readingTime=$mostReadTime;
            $exclude = [$mostRead->blogId];
            // dd($exclude);
            // Blogs weekly
            $blogs=DB::table('blogs')->where('blogs.blogStatus','active')->where('blogs.highlight', 1)->join('cms_users','cms_users.id','blogs.blogAuthor')->select('blogs.id as blogId', 'blogs.blogTitle', 'blogs.blogSlug', 'blogs.blogContent', 'blogs.blogImage1', 'blogs.blogAuthor', 'blogs.blogStatus', 'cms_users.id', 'cms_users.name', 'cms_users.photo')->get();
            // dd($blogs);
            $blogsData = collect($blogs)->whereNotIn('blogId',$exclude);
            foreach($blogsData as $key=>$value){
                // dd($value);
                $totalWord=str_word_count($value->blogContent);
                $readTime=$totalWord/255;
                $readTime=ceil($readTime);
                $readingTime=$readTime;
                $blogsData[$key]->readingTime = $readingTime;
            };
            // dd($mostRead);
            // dd($blogsData);
            // $finalBlogsData = $this->paginate2($blogsData, 'weekly');
            $finalBlogsData = $this->paginate2($blogsData);
            // $finalBlogsDatas = $this->paginate2($blogsData, 'all');
            // dd($finalBlogsData);
        
            return view('pages-v2.blog', compact('finalBlogsData','mostRead','readingTime'));
        }


        
    }

    public function paginate2($items, $page = null, $perPage = 9,  $options = [] )
    {
        // dd($items);
        // $page = LengthAwarePaginator::resolveCurrentPage();
        // $items = $items instanceof Collection ? $items : Collection::make($items);
        // return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
        $currentPage = LengthAwarePaginator::resolveCurrentPage();
        // set limit 
        // $perPage = 20;
        // generate pagination
        $currentResults = $items->slice(($currentPage - 1) * $perPage, $perPage)->all();
        $results = new LengthAwarePaginator($currentResults, $items->count(), $perPage);
        $results->withPath('blog')->links();
        // if($type == 'weekly'){
        //     $results->withPath('blogWeek')->links();
        // }elseif ($type == 'all'){
        //     $results->withPath('blogAll')->links();
        // }
            // dd($results);
    // dd($currentPage);
    return $results;
    }

    public function blogDetailPage($slug)
    {   
        
        $article = DB::table('blogs')->where('blogs.blogSlug', $slug)->join('cms_users', 'cms_users.id', 'blogs.blogAuthor')->select('blogs.id as blogId', 'blogs.categoriesId', 'blogs.blogTitle', 'blogs.blogSlug', 'blogs.release_date', 'blogs.blogContent', 'blogs.blogImage1', 'blogs.blogAuthor', 'blogs.blogStatus', 'blogs.blogRead', 'cms_users.id', 'cms_users.name', 'cms_users.photo', 'cms_users.providerOrigin')->first();
        $article->release_date = Carbon::parse($article->release_date)->format('d m Y');
        $totalWord=str_word_count($article->blogContent);
        $articleReadTime=$totalWord/255;
        $articleReadTime=ceil($articleReadTime);
        $articleReadingTime=$articleReadTime;
        $article->readingTime = $articleReadingTime;
        $article->blogRead = $article->blogRead + 1;

        // random article,
        $exclude = [$article->blogId];
        // dd($array);
        $getBlogs = DB::table('blogs')->where('categoriesId', $article->categoriesId)->get();
        $blogs = collect($getBlogs)->whereNotIn('id',$exclude);
        $randomBlogsId = [];
        foreach ($blogs as $item){
            array_push($randomBlogsId, $item->id);
        }
        $randomize = array_rand($randomBlogsId, 3);
        foreach ($randomize as $item=>$value) {
            // dd($randomize, $randomBlogsId, $value, $randomBlogsId[$value]);
            $randomBlogs = DB::table('blogs')->where('blogs.id', $randomBlogsId[$value])->join('cms_users', 'cms_users.id', 'blogs.blogAuthor')->select('blogs.id as blogId', 'blogs.blogTitle', 'blogs.blogSlug', 'blogs.blogContent', 'blogs.blogImage1', 'blogs.blogAuthor', 'cms_users.name', 'cms_users.photo', 'cms_users.providerOrigin')->first();
            $totalWord=str_word_count($randomBlogs->blogContent);
            $readTime=$totalWord/255;
            $readTime=ceil($readTime);
            $readingTime=$readTime;
            // $getBlogs->readingTime = $readingTime;
            $data[] = [
                'title'=>$randomBlogs->blogTitle,
                'blogImage'=>$randomBlogs->blogImage1,
                'slug'=>$randomBlogs->blogSlug,
                'content'=>$randomBlogs->blogContent,
                'name'=>$randomBlogs->name,
                'photo'=>$randomBlogs->photo,
                'readingTime' => $readingTime,
                'slug' => $randomBlogs->blogSlug,
                'providerOrigin' =>$randomBlogs->providerOrigin,
            ];
        }

        // comment
        $comments = DB::table('comment')->where('comment.blogId', $article->blogId)->leftjoin('cms_users', 'comment.userId', 'cms_users.id')->select('comment.comment', 'comment.id', 'comment.created_at', 'comment.likesCount', 'comment.dislikesCount', 'cms_users.name', 'cms_users.photo', 'cms_users.providerOrigin')->get();
        if($comments>0){
            foreach ($comments as $comment){
                $comment->time = Carbon::createFromTimeStamp(strtotime($comment->created_at))->diffForHumans();
            }
        };

        
        // cek orang yang login apakah dia nge-like atau dislike di blog ini? 



        // dd($comments);
        // foreach
        // $excludeArticle = collect(rando)
        // dd($article, $randomBlogsId, $data);
        // dd($randomBlogs);
        return view('pages-v2.blog-detail', compact('article', 'data', 'comments'));
    }

    public function robotPerformance()
    {
        return view('pages-v2.robot-performance');
    }

}

