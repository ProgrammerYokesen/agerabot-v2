<?php

namespace App\Http\Controllers\Api\Payment;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Xendit\Xendit;
use Illuminate\Support\Facades\Auth;

class xenditController extends Controller
{
    //
    public function getListVA(){
        Xendit::setApiKey(env('XENDIT_SECRET_KEY'));
        $getVABanks = \Xendit\VirtualAccounts::getVABanks();
        // dd($getVABanks);
        return response()->json([
            'data' => $getVABanks
        ])->setStatusCode(200);
    }

    public function createInvoice($id){
        // dd($id);
        Xendit::setApiKey(env('XENDIT_SECRET_KEY'));
        $params = [
        'external_id' => 'demo_147580196270',
        'payer_email' => 'sample_email@xendit.co',
        'description' => 'Trip to Bali',
        'amount' => 32000
        ];
        $createInvoice = \Xendit\Invoice::create($params);

        //Simpan $createInvoice->id ke database kita

        //End simpan
        
        return response()->json([
            'data' => $createInvoice
        ])->setStatusCode(200);
    }

    public function getRecurring($id){
        Xendit::setApiKey(env('XENDIT_SECRET_KEY'));
        // dd($id);
        // $id = 'invoice-id';
        $getRecurring = \Xendit\Recurring::retrieve($id);
        return response()->json([
            'data' => $getRecurring
        ])->setStatusCode(200);
    }

    public function getAllInvoice(){
        Xendit::setApiKey(env('XENDIT_SECRET_KEY'));
        $getAllInvoice = \Xendit\Invoice::retrieveAll();
        return response()->json([
            'data' => $getAllInvoice
        ])->setStatusCode(200);
    }

    public function requiring($id){
        
        // dd($paid_at);
        // dd('y');

        $external_id ='inv-'.time();
        $userId = \CRUDBooster::myId();
        Xendit::setApiKey(env('XENDIT_SECRET_KEY'));
      

        // dd($userId);
        $subs = \DB::table('subscription')
        ->where('userId', $userId)
        ->where('payment', 0)->get();
        // dd($subs);
        if(!$subs->isEmpty()){
            // return 'data';
            $subscription = \DB::table('subscription')
            ->where('userId', $userId)
            ->where('payment', 0)->delete();
        }
        // return 'empty';
        $email = \DB::table('cms_users')->find($userId)->email;
        // dd($email);
        $package = \DB::table('package')->find($id);
        $client = new \GuzzleHttp\Client();
        $response = $client->request('GET', 'https://free.currconv.com/api/v7/convert?q=USD_IDR&compact=ultra&apiKey=d777c23d07de7559ab15');
        $price = $package->price;
        if ($response->getBody()) {
            // dd($response->getBody()->getContents());
            $currency = json_decode($response->getBody()->getContents());
            $currency = $currency->USD_IDR;
            
            $price = $package->price * $currency;
            // JSON string: { ... }
        }

          // Cek if user already have subscription Edit subscription //

          $countSubs = \DB::table('subscription')
          ->where('userId', $userId)
          ->where('payment', 1)->first();
          if($countSubs != NULL){
              $orderId = $countSubs->orderId;
              $params = [
              'description' => $package->detail,
              'amount' => $price,
              'interval_count' => $package->interval
            ];
              $editRecurring = \Xendit\Recurring::update($orderId, $params);
              $update = \DB::table('subscription')->where('orderId', $orderId)->update([
                'packageId' => $id
            ]);
              return response()->json([
                'message' => 'Edit success',
                'type' => 'update',
                'data' => $editRecurring
                ])->setStatusCode(200);
          }
  
          // End check
        
        // dd($external_id, $email, $package->detail, $package->price, $package->interval);
        $params = [
            'external_id' => $external_id, //userId
            'payer_email' => $email,//user Email
            'description' => $package->detail,
            'amount' => $price,
            'interval' => 'MONTH',
            'interval_count' => $package->interval
          ];
        //   dd($params);
        
          $createRecurring = \Xendit\Recurring::create($params);
        //   dd($createRecurring);

          //Insert id from xendit recuiring to our database here
            //   dd($createRecurring['id']);
            //   $data = response()
            date_default_timezone_set("America/New_York");
            $insert = \DB::table('subscription')->insert([
                'userId' => $userId,
                'packageId' => $id,
                'externalId' => $external_id,
                'orderId' => $createRecurring['id'],
                'paymentUrl' => $createRecurring["last_created_invoice_url"],
                'created_at' => date("Y-m-d h:i:s")
            ]);
            // $check = $insert->externalId;
            // dd($external_id, $createRecurring);


          //end insert
          return response()->json([
            'data' => $createRecurring
            ])->setStatusCode(200);
        // return \Redirect::to($createRecurring["last_created_invoice_url"]);
    }
    
    public function getRequiring($id){
        Xendit::setApiKey(env('XENDIT_SECRET_KEY'));
        $getRecurring = \Xendit\Recurring::retrieve($id);
        return response()->json([
            'message' => 'Save success',
            'type' => 'create',
            'data' => $getRecurring
            ])->setStatusCode(200);
        // var_dump($getRecurring);
      
    }
    public function updateSubscription(Request $request){
        $callbackToken = $header = $request->header('X-CALLBACK-TOKEN');
        if(env('XENDIT_CALLBACK_KEY') == $callbackToken){
        $externalId = $request->external_id;
        $status = $request->status;
        $orderId = $request->recurring_payment_id;

        $paid_at = \Carbon\Carbon::parse($request->paid_at)->format('Y-m-d H:m:s');
        // return $orderId;
        // $data = \DB::table('subscription')->where('orderId',$orderId)->get();
        // $userId = \CRUDBooster::myId();

        // check orderId dari install, subscription, or vps
        $subscount = \DB::table('subscription')->where('orderId',$orderId)->count();
        $vpscount = \DB::table('paymentvps')->where('orderId',$orderId)->count();
        $installcount = \DB::table('paymentinstall')->where('orderId',$orderId)->count();
        if($subscount!=0){
        // Update recurring subscription
        $package = \DB::table('subscription')->where('orderId',$orderId)->first();
        $userId = $package->userId;
        if($status=='PAID'){
            $update = \DB::table('subscription')->where('orderId',$orderId)->update([
                'payment' => 1,
                'paymentTime' => $paid_at,
                'status' => $status
            ]);

            $premium = \DB::table('cms_users')->where('id',$userId)->update([
                'status' => 'active'
            ]);
            $history = \DB::table('subscriptionhistory')->insert([
                'orderId' => $orderId,
                'dateSubs' => $paid_at,
                'description' => $request->description,
                'activeTime' => $paid_at,
                'subTotal' => $request->amount,
                'total' => $request->paid_amount,
                'status' => $status,
                'paymentMethod' => $request->payment_method
            ]);
        }
        else{
            $update = \DB::table('subscription')->where('externalId',$externalId)->update([
                'payment' => 0,
                'paymentTime' => date("Y-m-d h:i:s"),
                'status' => $status
            ]);
        }
        // End update recurring subscription
    }
        

        if($vpscount!=0){
        // Update recurring vps
        $package = \DB::table('paymentvps')->where('orderId',$orderId)->first();
        $userId = $package->userId;
        if($status=='PAID'){
            $update = \DB::table('paymentvps')->where('orderId',$orderId)->update([
                'payment' => 1,
                'paymentTime' => $paid_at,
                'status' => $status,
                'paymentMethod' => $request->payment_method
            ]);

            $history = \DB::table('paymentvpshistory')->insert([
                'orderId' => $orderId,
                'dateSubs' => $paid_at,
                'description' => $request->description,
                'activeTime' => $paid_at,
                'subTotal' => $request->amount,
                'total' => $request->paid_amount,
                'status' => $request->status,
                'paymentMethod' => $request->payment_method
            ]);
        }
        else{
            $update = \DB::table('subscription')->where('externalId',$externalId)->update([
                'payment' => 0,
                'paymentTime' => date("Y-m-d h:i:s"),
                'status' => $status
            ]);
        }
        // End update recurring vps
        }
        if($installcount!=0){
            
            $update = \DB::table('paymentinstall')->where('orderId',$orderId)->update([
                'payment' => 1,
                'paymentTime' => $paid_at,
                'status' => $status,
                'paymentMethod' => $request->payment_method
            ]);
            $quota = \DB::table('installquota')->where('orderId',$orderId)->count();
            if($quota==0){
                $dataQuota = \DB::table('paymentinstall')->where('orderId',$orderId)->first();
                $addQuota = \DB::table('installquota')->insert([
                    'userId' => $userId,
                    'quota' => $dataQuota->quotaTotal,
                    'sisaQuota' => $dataQuota->quotaTotal
                ]);
            }
            else{
                $dataQuota = \DB::table('paymentinstall')->where('orderId',$orderId)->first();
                $quotaSisa = \DB::table('installquota')->where('userId',$userId)->first();
                $totalQuota = $dataQuota->quotaTotal + $quotaSisa->quota;
                $sisaQuota = $dataQuota->quotaTotal + $quotaSisa->sisaQuota;
                $addQuota = \DB::table('installquota')->where('userId', $userId)->update([
                    'quota' => $totalQuota,
                    'sisaQuota' => $sisaQuota
                ]);
            }

        }
        return response()->json([
            'message' => $userId
            ])->setStatusCode(200); 
        }

    }
    
    public function stopRecurringSubscription(){
          Xendit::setApiKey(env('XENDIT_SECRET_KEY'));
          $userId = \CRUDBooster::myId();
          $orderId = \DB::table('subscription')->where('userId', $userId)->where('payment', 1)->pluck('orderId')->first();
        //   dd($orderId);
          if($orderId != NULL){
            $stopRecurring = \Xendit\Recurring::stop($orderId);
            //   dd($stopRecurring);
              if($stopRecurring){
                  
                 $nonactive = \DB::table('cms_users')->where('id',$userId)->update([
                    'status' => 'expired'
                ]);
                 $nonactiveTrader = \DB::table('trader')->where('id',$userId)->update([
                    'status' => 'inactive'
                ]);
                 $nonactiveSubscription = \DB::table('subscription')->where('orderId',$orderId)->update([
                    'status' => 'STOPPED',
                    'payment' => 0
                ]);
                // return 'masuk if';
                return redirect()->back(); 
    
            //   return response()->json([
            //     'message' => $stopRecurring
            //     ])->setStatusCode(200); 
              }
            //   return 'masuk else';
              return response()->json([
                'message' => "failed stop recurring"
                ])->setStatusCode(200); 
          }
        //   return 'masuk else awal';
          return redirect()->back(); 
         
    }

    //Function untuk create recurring vps
    public function recurringVps($id){

        $external_id ='inv-vps'.time();
        $userId = \CRUDBooster::myId();
        // dd($userId);
        $vpsSubs = \DB::table('paymentvps')
        ->where('userId', $userId)
        ->where('payment', 0)->get();
        // dd($subs);
        if(!$vpsSubs->isEmpty()){
            // return 'data';
            $vpsSubscription = \DB::table('paymentvps')
            ->where('userId', $userId)
            ->where('payment', 0)->delete();
        }
        // return 'empty';
        $email = \DB::table('cms_users')->find($userId)->email;
        // dd($email);
        $package = \DB::table('vps')->where('vps.id', $id)->join('catvps','catvps.id','vps.catVpsId')->first();
        $client = new \GuzzleHttp\Client();
         $response = $client->request('GET', 'https://free.currconv.com/api/v7/convert?q=USD_IDR&compact=ultra&apiKey=d777c23d07de7559ab15');
        $price = $package->price;
        if ($response->getBody()) {
            // dd($response->getBody()->getContents());
            $currency = json_decode($response->getBody()->getContents());
            $currency = $currency->USD_IDR;
            
            $price = $package->price * $currency;
            // JSON string: { ... }
        }
        // dd($package);
        $detail = $package->interval."Month subscription for".$package->nmVps.". ".$package->detail;
        // dd($package);
        Xendit::setApiKey(env('XENDIT_SECRET_KEY'));
        // dd($external_id, $email, $package->detail, $package->price, $package->interval);
        $params = [
            'external_id' => $external_id, //userId
            'payer_email' => $email,//user Email
            'description' => $detail,
            'amount' => $price,
            'interval' => 'MONTH',
            'interval_count' => $package->interval
          ];
        
          $createRecurring = \Xendit\Recurring::create($params);
        //   dd($createRecurring);

          //Insert id from xendit recuiring to our database here
            //   dd($createRecurring['id']);
            //   $data = response()
            date_default_timezone_set("America/New_York");
            $insert = \DB::table('paymentvps')->insert([
                'userId' => $userId,
                'vpsId' => $id,
                'vpsDetailId' => $id,
                'externalId' => $external_id,
                'orderId' => $createRecurring['id'],
                'paymentUrl' => $createRecurring["last_created_invoice_url"],
                'created_at' => date("Y-m-d h:i:s")
            ]);
            // $check = $insert->externalId;
            // dd($external_id, $createRecurring);


          //end insert
          return response()->json([
            'data' => $createRecurring
            ])->setStatusCode(200);
        // return \Redirect::to($createRecurring["last_created_invoice_url"]);
    }

    public function installmentCheckout(Request $req){
        $data = \DB::table('installment')->first();
        $qty = $req->qty;
        $totalPrice = $data->price * $qty;

        $external_id ='inv-instl'.time();
        $userId = \CRUDBooster::myId();
        // dd($userId);
        // dd($subs);
        // return 'empty';
        $email = \DB::table('cms_users')->find($userId)->email;
        $client = new \GuzzleHttp\Client();
         $response = $client->request('GET', 'https://free.currconv.com/api/v7/convert?q=USD_IDR&compact=ultra&apiKey=d777c23d07de7559ab15');
        $price = $package->price;
        if ($response->getBody()) {
            // dd($response->getBody()->getContents());
            $currency = json_decode($response->getBody()->getContents());
            $currency = $currency->USD_IDR;
            
            $price = $package->price * $currency;
            // JSON string: { ... }
        }
        Xendit::setApiKey(env('XENDIT_SECRET_KEY'));
        $params = [
        'external_id' => $external_id,
        'payer_email' => $email,
        'description' => $qty.'x Installment',
        'amount' => $price
        ];

        $createInvoice = \Xendit\Invoice::create($params);
            date_default_timezone_set("America/New_York");
            $insert = \DB::table('paymentinstall')->insert([
                'userId' => $userId,
                'externalId' => $external_id,
                'quotaTotal' => $qty,
                'orderId' => $createInvoice['id'],
                'priceTotal' => $totalPrice,
                'invoiceUrl' => $createInvoice["invoice_url"],
                'status' => $createInvoice["status"],
                'created_at' => date("Y-m-d h:i:s")
            ]);
        //Simpan $createInvoice->id ke database kita

        //End simpan
        
        return response()->json([
            'data' => $createInvoice
        ])->setStatusCode(200);
    }

}


