<?php

namespace App\Http\Middleware;

use Closure;

class authAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $priv_id = DB::table('cms_users')->where('id', CRUDBooster::myId())->first();
        if($priv_id->id_cms_privileges == 1){
            return $next($request);
        }else{
            return redirect()->route('webPagesIndex');
        }
    }
}
