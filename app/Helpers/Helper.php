<?php

function getUserPhoto(){
  $user = DB::table('cms_users')
  ->where('id', CRUDBooster::myId())
  ->get();

  return $user;
}

function set_active($uri, $output = 'selected')
{
 if( is_array($uri) ) {
   foreach ($uri as $u) {
     if (Route::is($u)) {
       return $output;
     }
   }
 } else {
   if (Route::is($uri)){
     return $output;
   }
 }
}

function set_active_navbar($uri, $output = 'active')
{
 if( is_array($uri) ) {
   foreach ($uri as $u) {
     if (Route::is($u)) {
       return $output;
     }
   }
 } else {
   if (Route::is($uri)){
     return $output;
   }
 }
}
