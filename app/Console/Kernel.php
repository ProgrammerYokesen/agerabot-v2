<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
      $schedule->call('App\Http\Controllers\RobotController@auto_expire')->everyMinute();
      $schedule->call('App\Http\Controllers\ParserController@parseStatement')->everyFifteenMinutes();
      $schedule->call('App\Http\Controllers\ParserController@tgphl')->everyMinute();
      $schedule->call('App\Http\Controllers\ParserController@tgphl_close')->everyMinute();
      $schedule->call('App\Http\Controllers\ParserController@expire')->everyFifteenMinutes();
      $schedule->call('App\Http\Controllers\ParserController@statistic_ten')->dailyAt('05:40')->timezone('Asia/Jakarta');
      $schedule->call('App\Http\Controllers\ParserController@statistic_ten')->dailyAt('11:53')->timezone('Asia/Jakarta');
      $schedule->call('App\Http\Controllers\ParserController@statistic_ten')->dailyAt('18:13')->timezone('Asia/Jakarta');
      $schedule->call('App\Http\Controllers\ParserController@statistic_ten')->dailyAt('22:13')->timezone('Asia/Jakarta');
      $schedule->call('App\Http\Controllers\ParserController@statistic_week')->dailyAt('05:00')->timezone('Asia/Jakarta');
      $schedule->call('App\Http\Controllers\ParserController@statistic_month')->dailyAt('04:00')->timezone('Asia/Jakarta');
      $schedule->call('App\Http\Controllers\ParserController@statistic_year')->dailyAt('03:00')->timezone('Asia/Jakarta');
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
