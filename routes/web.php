<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::group(['prefix' => \UriLocalizer::localeFromRequest(), 'middleware' => 'localize'], function () {
// 	/* Your routes here */

// Route::get('/', 'PagesController@index')->name('webPagesIndex');
// Route::get('/robot-trading', 'PagesController@robotTrading')->name('robot-trading');
// Route::get('/robot-trading/{value}', 'PagesController@robotTradingSort')->name('robot-trading-sort');
// Route::get('/vps', 'PagesController@VPS')->name('vps');
// Route::get('/support', 'PagesController@support')->name('support');
Route::get('/addService/{service}/{cartid}', 'ProcessController@addService')->name('addService');
Route::get('/removeService/{service}/{cartid}', 'ProcessController@removeService')->name('removeService');
Route::get('/rentPeriode/{service}/{cartid}', 'ProcessController@rentPeriode')->name('rentPeriode');
Route::get('/kodeVoucher/{kodevoucher}', 'ProcessController@kodeVoucher')->name('kodeVoucher');
Route::get('/removeVoucher', 'ProcessController@removeVoucher')->name('removeVoucher');
Route::get('/remove/cart/{id}', 'ProcessController@removeFromCart')->name('removeFromCart');

// Route::group(['as' => 'blog.', 'prefix' => 'blog'], function () {
// 	Route::get('/', 'PagesController@blog')->name('index');
// 	Route::get('/search', 'PagesController@blogSearch')->name('search');
// 	Route::get('/{slug}','PagesController@article')->name('article'); //Page setelah klik reset pass di email
// });

// Route::group(['as' => 'contact.', 'prefix' => 'contact'], function () {
// 	Route::get('/', 'PagesController@contact')->name('index');
// 	Route::post('/', 'PagesController@storeContact')->name('store');
// });

Route::get('/data/mql','DataController@dataMql');
Route::get('/data/ims','DataController@dataIms');

Route::get('/lab/expire','RobotController@auto_expire');
Route::get('/lab/type','LabController@opposite');
Route::get('/lab/last','LabController@researchseven');
Route::get('/lab/lastseven','LabController@last_seven');

Route::group(['as' => 'login.', 'prefix' => 'login'], function () {
	Route::get('/', 'Auth\LoginController@login')->name('index');
	Route::post('/', 'Auth\LoginController@loginCustomers')->name('customers');
	Route::get('oAuth/process/{provider}', 'Auth\LoginController@redirectToFacebook')->name('process-oAuth');
	Route::get('oAuth/callback/{provider}', 'Auth\LoginController@handleFacebookCallback')->name('process-callback-oAuth');
	Route::get('oAuth/failed/{provider}', 'Auth\LoginController@handleFacebookFailed')->name('process-failed-oAuth');
});

/*INI NANTI GROUP MIDDLEWARE LOGIN*/


// Route::get('/disclaimer','PagesController@disclaimer')->name('disclaimer');
// Route::get('/privacy-policy','PagesController@privacyPolicy')->name('privacyPolicy');

//Route from Andika
Route::get('/userLogout','ProcessController@userLogout')->name('userLogout');
Route::middleware(['authUser'])->group(function () {

Route::get('/download/robot/{slug}/{salesCode}','ProcessController@downloadRobot')->name('downloadRobot');
Route::get('/dashboard','TraderController@dashboard')->name('dashboard');
Route::get('/processCheckout','ProcessController@processCheckout')->name('processCheckout');
// Route::get('/checkout','PagesController@checkout')->name('checkout');
// Route::get('/checkout/{id}','PagesController@checkoutIndex')->name('checkoutIndex');
// Route::get('/payment','PagesController@payment')->name('payment');
// Route::get('/my-collection','PagesController@robotCollection')->name('collection');
// Route::get('/redeem-coupon','PagesController@redeemCoupon')->name('redeemCoupon');
Route::get('/get-voucher/{kode}','ProcessController@getVoucher')->name('getVoucher');
// Route::get('/history','PagesController@paymentHistory')->name('paymentHistory');
Route::get('/use-voucher/{kode}','ProcessController@useVoucher')->name('useVoucher');
Route::get('/buy-again/{id}','ProcessController@buyAgain')->name('buyAgain');

// $data = \Mail::to($email)->send(new \App\Mail\sendingEmail($details));
//Route from Dimas
// Route::get('setting/help','PagesController@help')->name('help');
// Route::get('setting/helpManageAccount','PagesController@helpManageAccount')->name('helpManageAccount');
// Route::get('setting/help/paymentMethod','PagesController@paymentMethod')->name('paymentMethod');
// Route::get('setting/help/installRobot','PagesController@installRobot')->name('installRobot');
// Route::get('setting/help/manageVPS','PagesController@manageVPS')->name('manageVPS');
// Route::get('/setting/profile','PagesController@settingProfile')->name('profile');
// Route::get('/setting/changePassword','PagesController@changePassword')->name('changePassword');
// Route::post('/setting/submitProfile','PagesController@submitProfile')->name('submitProfile');
// Route::post('/submit/changePassword','PagesController@submitChangePassword')->name('submitChangePassword');
// Route::get('/setting/complain','PagesController@complain')->name('complain');
// Route::get('/setting/complain/content/view','PagesController@complainContentView')->name('complainContentView');
// Route::post('/setting/complain/content/{ticket}','PagesController@complainContent')->name('complainContent');
});

// middleware buat admin dan system admin
Route::middleware(['authAdminSystem' || 'authAdmin'])->group(function () {
	Route::get('/payment-verification/{id}','PagesController@paymentVerification')->name('paymentVerification');
	Route::get('/print-receipt/{id}','PagesController@printReceipt')->name('printReceipt');
	Route::get('/arena/best-robot/{type}/{periode}','statisticController@bestRobot')->name('bestRobot');
});

// middleware buat user dan admin
// Route::middleware(['authUser' || 'authAdmin'])->group(function () {
// 	Route::get('/report/button/{id}','PagesController@reportButton')->name('reportButton');
// 	Route::get('/report/detail/{id}','PagesController@reportDetail')->name('reportDetail');
// 	Route::post('/post-chat/','ProcessController@postChat')->name('postChat');
// });

// middleware admin dan cms

// Route::middleware(['authAdminCMS' || 'authAdmin' || 'authCopyWriter'])->group(function () {
// 	Route::post('/setting/submitBlog','PagesController@submitBlog')->name('submitBlog');
// 	Route::post('/setting/edittBlog','PagesController@editBlog')->name('editBlog');
//   Route::get('arena/blogs/addBlogs','PagesController@blogs')->name('blogs');
//   Route::get('arena/blogs/edit/{id}','PagesController@editBlogs')->name('editBlogs');
//   Route::get('arena/blogs/publish/{id}','PagesController@publishBlogs')->name('publishBlogs');
// });



//Route from Eza
// Route::get('/userRegister','PagesController@register')->name('userRegister'); //Page Register
Route::post('/registering', 'ProcessController@postRegister')->name('processRegistering'); //Proses Register
// Route::get('/forgot','PagesController@forgotPassword')->name('userForgotpassword'); //Page Setelah Klik Forgot Pass
// Route::get('/reset','PagesController@resetPassword')->name('userResetpassword'); //Page setelah klik reset pass di email


// });


Route::get('/lab/server','statisticController@server_check');
Route::get('/lab/statistic/all/{type}/{periode}','statisticController@best_pair');




//Agerabot v2
// Route::group(['middleware' => 'authUser'], function () {

Route::get('/', 'PagesController@homePage')->name('homePage');
Route::get('/robot', 'PagesController@robotPage')->name('robotPage');
// Route::get('/robot/comparison', 'PagesController@robotComparison')->name('robotComparison');
Route::get('/subscription', 'PagesController@subsPage')->name('subsPage');
Route::get('/register', 'PagesController@regisPage')->name('registerPage');

Route::get('/wishlist', 'PagesController@wishlist')->name('wishlist');
Route::get('/profitable/robot', 'PagesController@profitableRobot')->name('profitableRobot');
Route::get('/popular/robot', 'PagesController@popularRobot')->name('popularRobot');
Route::get('/robot-list', 'PagesController@robotListPage')->name('robotListPage');
Route::get('/robot-compare', 'PagesController@robotComparePage')->name('robotComparePage');
Route::get('/robot-performance', 'PagesController@robotPerformance')->name('robotPerformancePage');

Route::get('/login', 'PagesController@loginPage')->name('loginPage');
Route::get('/login/checkout/session', 'ProcessController@loginSession')->name('loginSession');
Route::get('/how-to-use', 'PagesController@howToPage')->name('howToPage');
Route::get('/how-to-uninstall', 'PagesController@howUninstall')->name('howUninstall');
Route::get('/how-to-vps', 'PagesController@howtoVPS')->name('howtoVPS');
Route::get('/terms', 'PagesController@termsCondition')->name('termsCondition');
Route::get('/disclaimer', 'PagesController@disclaimerPage')->name('disclaimerPage');
Route::get('/privacy-policy', 'PagesController@privacyPage')->name('privacyPage');
Route::get('/blog', 'PagesController@blogPage')->name('blogPage');
Route::get('/blogs/ajax', 'ProcessController@blogAjax')->name('blogAjax');
// Route::get('/blogs-card-ajax', 'ProcessController@blogCardAjax')->name('blogCardAjax');
Route::get('/blog/detail/{slug}', 'PagesController@blogDetailPage')->name('blogDetailPage');
Route::post('/blog/submit/comment', 'ProcessController@submitComment')->name('submitComment');
Route::post('/comment/like', 'ProcessController@like')->name('like');

Route::get('/checkout', 'PagesController@checkoutPage')->name('checkoutPage');
Route::get('/checkout-ajax/{id}', 'ProcessController@addCartRender')->name('checkoutPageAjax');
Route::get('/checkout-vps-ajax/{id}', 'ProcessController@addCartVpsRender')->name('checkoutPageAjax');
Route::get('/checkout/vps', 'PagesController@checkoutVpsPage')->name('checkoutVpsPage');
Route::get('/checkout/install', 'PagesController@checkoutInstallPage')->name('checkoutInstallPage');
Route::get('/dashboardUser', 'PagesController@dashboard')->name('dashboard');
Route::get('/dashboard-myrobot', 'PagesController@dashRobotList')->name('myrobot');
Route::get('/dashboard-wishlist', 'PagesController@dasboardWishlist')->name('dasboardWishlist');
Route::get('/dashboard/upgrade', 'PagesController@dashUpgrade')->name('upgradeRobot');
Route::get('/dashboard/transaction', 'PagesController@transactionHistory')->name('transactionHistory');
Route::get('/dashboard/transaction-done', 'PagesController@transactionDone')->name('transactionDone');
Route::get('/dashboard/product', 'PagesController@productPage')->name('productPage');
Route::get('/dashboard/vps', 'PagesController@vpsPage')->name('vpsPage');
Route::get('/dashboard/help', 'PagesController@helpPage')->name('helpPage');
Route::get('/dashboard/manual', 'PagesController@manualPage')->name('manualPage');
Route::get('/dashboard/manual-vps', 'PagesController@manualVpsPage')->name('manualVpsPage');
Route::get('/download/robot/{id}/{code}/{generate}', 'ProcessController@downloadRobot')->name('downloadRobot');
// Route::get('/download/robot', 'ProcessController@downloadRobot')->name('downloadRobot');
// Route::get('/dashboard/changePassword','PagesController@changePassword')->name('changePassword');
Route::get('/dashboard/setting', 'PagesController@settingPage')->name('settingPage');
Route::post('/dashboard/setting/submit', 'PagesController@submitProfile')->name('submitProfile');
Route::get('/dashboard/setting/password', 'PagesController@setPassword')->name('passwordPage');
Route::post('/submit/changePassword','PagesController@submitChangePassword')->name('submitChangePassword');
Route::get('/dashboard/setting/terms', 'PagesController@termsPage')->name('termsPage');
Route::get('/dashboard/setting/plan', 'PagesController@planPage')->name('planPage');
Route::get('/cart', 'PagesController@cart')->name('cart');
Route::get('/addcart/{slug}', 'ProcessController@addCart')->name('addToCart');
Route::get('/addcart/vps/{slug}', 'ProcessController@addCartVps')->name('addToCartVps');
Route::get('/addcart-install', 'ProcessController@addCartInstall')->name('addToCartInstall');
Route::get('/addcart/unpaid/{id}', 'ProcessController@addCartUnpaid')->name('addToCartUnpaid');
Route::get('/addcart/unpaid/vps/{id}', 'ProcessController@addCartUnpaidVps')->name('addToCartUnpaidVps');
Route::get('/addcart/unpaid/install/{id}', 'ProcessController@addCartUnpaidInstall')->name('addToCartUnpaidInstall');


// Route Search
Route::get('/dashboard-myrobot/search', 'SearchController@dashSearchRobotList')->name('searchMyrobot');
Route::get('/search/robot', 'SearchController@robotSearch')->name('robotSearch');
Route::get('/dashboard/search/robot', 'SearchController@dashboardRobotSearch')->name('dashboardRobotSearch');
Route::post('/robot/compare', 'SearchController@robotCompare')->name('robotCompare');
Route::get('/search/profitable/robot', 'SearchController@searchProfitableRobot')->name('searchProfitableRobot');

//Route Xendit
//Recurring xendit
Route::get('xendit/recurring/vps/create/{id}','Api\Payment\xenditController@recurringVps')->name('createSubscriptionVps');
Route::get('xendit/recurring/create/{id}','Api\Payment\xenditController@requiring')->name('createSubscription');
Route::post('xendit/recurring-vps/create','Api\Payment\xenditController@installmentCheckout')->name('createSubscriptionInstall');
Route::get('xendit/recurring/{id}','Api\Payment\xenditController@getRequiring');
Route::post('xendit/invoice_callback_url','Api\Payment\xenditController@updateSubscription')->name('updateSubscription');
Route::get('xendit/recurring/{id}','Api\Payment\xenditController@getRecurring');
Route::get('xendit/all-invoice','Api\Payment\xenditController@getAllInvoice');
Route::get('xendit/stop-subs-recurring','Api\Payment\xenditController@stopRecurringSubscription')->name('stopSubscription');

//Charge xendit
Route::get('xendit/charge/{id}','Api\Payment\xenditController@createInvoice');
// dashboard
Route::get('/dashboard/robotList', 'DashboardController@robotList')->name('robotList');

// });