<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/equity/yuli','LabController@equityYuli');
Route::post('/test/good','LabController@getPrice');
Route::post('/test/price','LabController@test');
Route::post('v2/access/robot/master-luke','RobotController@masterLuke');
Route::post('v2/access/robot/license','RobotController@validate_license');
Route::get('v2/statistic/pair/{type}/{periode}/{pair}','statisticController@statistic_pair');
Route::get('v2/statistic/robot/{type}/{periode}/{robot}','statisticController@statistic_robot');

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//Api form andika

Route::get('/add-cart/{slug}', 'ProcessController@addCart')->name('apiAddToCart');