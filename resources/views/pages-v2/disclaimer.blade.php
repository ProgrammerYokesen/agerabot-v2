@extends('app')

@section('content')
    <div class="howto-container container">
        <div class="row">
            <div class="col-md-3">
                <div class="side-menu">
                    <a href="{{ route('termsCondition') }}">Terms & Condition</a>
                    <a href="#">Disclaimer</a>
                    <a href="{{ route('privacyPage') }}">Privacy Policy</a>
                </div>
            </div>
            <div class="col-md-9">
                <div class="howto-content">
                    <h1>Disclaimer</h1>
                    <p>Agerabot.com does not promise fixed income, profit or fund management. The robot is meant only a tool (tools) for trading easily.</p>

                    <p>
                        In forex trading there is no guarantee that you will profit, and Forex Trading is a risky business activity "high gain high risk". In forex trading, you can suffer a complete loss on your investment.
                    </p>
                       
                    <p>
                        The information provided in this robot installation guide is intended exclusively for informative purposes and is obtained from a reliable source. Agerabot.com does not guarantee the completeness and accuracy of the information and will not be responsible for direct or indirect damages due to using the information provided in this guide. Agerabot.com does not sell, facilitate or guarantee profits in the use of any type of ROBOT, EA, Copy Trade and other trading strategies. This guide provides education to individuals and companies in information covering risks and trading strategies.</p>
                    
                        <h1>PRIVACY POLICY</h1>
                        <p>For internal purposes, Agerabot.com requires personal information for them to use Agerabot.com services. Agerabot.com and employees are required to maintain the confidentiality of client information and are prohibited from sharing it with third parties. However, if required by law, Agerabot.com may provide information to public authorities.</p>

                        <h1>RISK WARNING ABOUT TRADE</h1>
                        <p>Trading on margin is a leveraged product, carries a high risk and may not be suitable for everyone. There are NO GUARANTEES OF PROFIT on your investment and therefore be careful of those who guarantee profit on trading. You are advised not to use funds if you are not prepared to bear losses. Before deciding to trade, make sure you understand the risks involved and take your experience into account as well.</p>
                    </div>

            </div>
        </div>

    </div>
@endsection
