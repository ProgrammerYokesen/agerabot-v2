@extends('app')

@section('content')
    <div class="robot-hero">
        <div class="robot-hero-txt">
            <h2>Temukan Robot yang tepat untuk Anda</h2>
            <h5>Kami menyediakan berbagai jenis robot yang sesuai dengan karakter dan tipe trading Anda, dan pastinya robot
                kami memiliki performa yang bisa membantu Anda meraih profit!</h5>
        </div>
        <div class="robot-hero-overlay"></div>
    </div>


    <div class="container robot-menu-container">
        <div class="customize_solution">
            <ul class="tabs nav nav-justified">
                <li class="tab-link current nav-pill mt-2">
                    <a href="{{ route('robotPage') }}">
                        <span class="ease-effect">Top Performance Robot</span>
                    </a>
                </li>
                <li class="tab-link nav-pill mt-2">
                    <a href="{{ route('robotListPage') }}">
                        <span class="ease-effect">Robot Detail List</span>
                    </a>
                </li>
                <li class="tab-link nav-pill mt-2">
                    <a href="{{ route('robotComparePage') }}">
                        <span class="ease-effect">Robot Comparison</span>
                    </a>
                </li>
            </ul>
        </div>

        {{-- PILIH MENU POPULARITY/PERFORMANCE --}}
        <div class="customize_solution my-5">
            <ul class="tabs nav nav-justified" style="justify-content: center">
                <li class="tab-link nav-pill mt-2">
                    <a href="{{ route('profitableRobot') }}">
                        <span class="ease-effect">By Popularity</span>
                    </a>
                </li>
                <li class="tab-link current nav-pill mt-2">
                    <a href="{{ route('robotPerformancePage') }}">
                        <span class="ease-effect">By Performance</span>
                    </a>
                </li>
            </ul>
        </div>

        <div class="performance_select">
            <div class="performance_pair">
                <h5>Pair</h5>
                <button data-toggle="collapse" data-target="#multiCollapseExample1" aria-expanded="false"
                    aria-controls="multiCollapseExample2">Select Pair</button>
            </div>
            <div class="performance_period">
                <h5>Period</h5>
                <button data-toggle="collapse" data-target="#multiCollapseExample2" aria-expanded="false"
                    aria-controls="multiCollapseExample2">Select Period</button>
            </div>
            <button>Search</button>
        </div>


        {{-- Profitable ROBOT --}}
        <div>
            <div class="profitable-robot">
                <form action="{{ route('searchProfitableRobot') }}" method="get">
                    @csrf
                    {{-- SELECT PAIR --}}
                    <div class="card mt-5 collapse" id="multiCollapseExample1">
                        <div class="card-body">
                            <h5>Select Pair</h5>
                            <div class="row">

                                {{-- @foreach ($pullPair as $datum) --}}
                                @for ($i = 0; $i < 15; $i++)
                                    <div class="col-md-3 col-lg-3 col-sm-3 col-3 colpadding">
                                        <label class="robot-radio-label">
                                            <input type="radio" name="pair" value="{{ $datum->stat_pair }}"
                                                class="card-input-element" />
                                            <div class="robot-pair">
                                                <p>XAUUSD</p>
                                            </div>
                                        </label>
                                    </div>
                                @endfor
                                {{-- @endforeach --}}

                            </div>
                        </div>
                    </div>
                    {{-- END SELECT PAIR --}}

                    {{-- SELECT TIMEFRAME --}}
                    <div class="card my-5 collapse " id="multiCollapseExample2">
                        <div class="card-body">
                            <h5>Select Period</h5>
                            <div class="row">

                                <div class="col-md-4 col-lg-4 col-sm-4 col-4">
                                    <label class="robot-radio-label">
                                        <input type="radio" name="timeframe" value="week" class="card-input-element" />
                                        <div class="robot-pair">
                                            <p>WEEKLY</p>
                                        </div>
                                    </label>
                                </div>
                                <div class="col-md-4 col-lg-4 col-sm-4 col-4">
                                    <label class="robot-radio-label">
                                        <input type="radio" name="timeframe" value="month" class="card-input-element" />
                                        <div class="robot-pair">
                                            <p>MONTHLY</p>
                                        </div>
                                    </label>
                                </div>
                                <div class="col-md-4 col-lg-4 col-sm-4 col-4">
                                    <label class="robot-radio-label">
                                        <input type="radio" name="timeframe" value="year" class="card-input-element" />
                                        <div class="robot-pair">
                                            <p>YEAR TO DATE</p>
                                        </div>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{-- END SELECT TIMEFRAME --}}

                    {{-- @if ($errors->all()) --}}
                    {{-- <div>
                        <p class="error-profitable-robot">Please select pair and timeframe</p>
                    </div> --}}
                    {{-- @endif --}}

                    {{-- <div class="profitable-search-btn">
                        <button type="submit">Search</button>
                    </div> --}}
                </form>


                <div class="text-center mt-5">
                    <a href="{{ route('subsPage') }}">
                        <button class="subscribe-btn mb-5">SUBSCRIBE NOW</button>
                    </a>
                    <a href="{{ route('howToPage') }}">
                        <button class="subscribe-btn mb-5">LEARN HOW TO USE</button>
                    </a>
                </div>

            </div>
        </div>
    </div>

@endsection
