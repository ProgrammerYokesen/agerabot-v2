@extends('app')

@section('content')
<div class="checkout-container container">
  <div class="row">
    <div class="col-md-7 order-choice">
      <h4>Choose your additional feature</h4>

      <div class="row vps-card">
        <div class="card">
          <div class="card-body">
            <h5>VPS</h5>
            <p>Explanation about VPS system in Agerabot</p>
          </div>
        </div>
      </div>

      <!-- Start foreach here -->
      <div id="checkoutContent">
        @foreach($datas as $data)
        <div class="row vps-card">
          <div class="card">
            <div class="card-body">
              <h5>{{$data->nmVPS}}</h5>
              <p>{{$data->detail}}</p>
            </div>
          </div>
        </div>


        <div class="row">
          <!-- Foreach vps here -->
          @foreach($data->vps as $vps)
          <div class="col-md-4 col-lg-4 col-sm-4 col-4">

            <label class="radio-label">
              <input type="radio" name="product" class="card-input-element" onclick="editCart({{ $vps->vpsId }})" />
              @foreach (Cart::content() as $row)
              @if ($vps->vpsId == $row->id)
              <div class="panel panel-default card-input active-card">
                @else
                <div class="panel panel-default card-input">
                  @endif
                  @endforeach

                  <p>{{$vps->interval}} Month</p>
                  <h4>
                    ${{$vps->price}}
                  </h4>
                </div>

            </label>

          </div>
          @endforeach
          <!-- End foreach VPs -->

        </div>
        @endforeach
      </div>
      <!-- End foreach here -->

    </div>
    <div class="col-md-5 order-detail-card">
      <div class="card" id="detailOrder">
        <!-- foreach here -->
        @foreach (Cart::content() as $cart)
        <div class="card-body">

          <h4>Detail Orders</h4>

          <div class="order-name">
            <h4 id="vpsName">{{$cart->name}}</h4>
            <button>X</button>
          </div>

          <div class="order-type">
            <p>{{$cart->qty}} Months</p>
            <p>${{$cart->price}}</p>
          </div>

          <div class="coupon-code">
            <p>Enter Coupon Code</p>
            <input type="text" class="coupon-code-input" placeholder="Coupon Code">
          </div>

          <!-- <div class="tax">
                            <p>10% Tax</p>
                            <p>$1.5</p>
                        </div> -->

          <div class="total  mt-5">
            <h4>Total</h4>
            <h4>${{$cart->price}}</h4>
          </div>

          <div class="mt-5" style="margin: 0 auto">
            @if (CRUDBooster::myID())
            <button class="checkout-btn" onclick="checkout({{ $cart->id }})">Checkout</button>
            @else
            <a href="{{ route('registerPage') }}">
              <button class="checkout-btn">Checkout</button>
            </a>
            @endif

          </div>
        </div>
      </div>
      @endforeach
      <!-- endforeach -->
    </div>
  </div>
</div>


<!-- Modal checkout -->
<div class="modal" tabindex="-1" role="dialog" id="checkoutModal">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content p-0">
            <iframe width="400" height="700" id="iframe-invoice" class="iframe-invoice" title="Invoice" src=""></iframe>
        </div>
    </div>
</div>
<!-- Endmodal checkout -->

@endsection

@section('scriptJs')
<script type="text/javascript">
  function editCart(id) {
    // console.log(id)
    $.ajax({
        type: "GET",
        url: "/checkout-vps-ajax/" + id,
        beforeSend: function() {
          // $('.ajax-load').show();
        }
      })
      .done(function(data) {
        if (data.html == " ") {
          $('.ajax-load').html("Error");
          return;
        }
        $('.ajax-load').hide();
        document.getElementById("checkoutContent").innerHTML = data.html;
        document.getElementById("detailOrder").innerHTML = data.orderDetail;
      })
      // .fail(function(jqXHR, ajaxOptions, thrownError)
      .fail(function(e) {
        console.log(e);
        alert('server not responding...');
      });
  }


  function checkout(id) {
    console.log('checkout');
    $.ajax({
        type: "GET",
        url: "../xendit/recurring/vps/create/" + id,
        beforeSend: function() {
          // $('.ajax-load').show();
        }
      })
      .done(function(data) {
        data = data.data;
        // console.log(data);
        loadIframe("iframe-invoice", data.last_created_invoice_url);
        $('#checkoutModal').modal('show');
      })
      // .fail(function(jqXHR, ajaxOptions, thrownError)
      .fail(function(e) {
        console.log(e);
        alert('server not responding...');
      });
  }

  function loadIframe(iframeName, url) {
    var $iframe = $('#' + iframeName);
    if ($iframe.length) {
      $iframe.attr('src', url);
      return false;
    }
    return true;
  }
</script>
@endsection