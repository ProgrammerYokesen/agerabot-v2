@extends('app')

@section('content')

    <div class="robot-hero">
        <div class="robot-hero-txt">
            <h2>Temukan Robot yang tepat untuk Anda</h2>
            <h5>Kami menyediakan berbagai jenis robot yang sesuai dengan karakter dan tipe trading Anda, dan pastinya robot
                kami memiliki performa yang bisa membantu Anda meraih profit!</h5>
        </div>
        <div class="robot-hero-overlay"></div>
    </div>

    <div class="container robot-menu-container">
        <div class="customize_solution">
            <ul class="tabs nav nav-justified">
                <li class="tab-link nav-pill mt-2">
                    <a href="{{ route('robotPage') }}">
                        <span class="ease-effect">Profitable Robot</span>
                    </a>
                </li>
                <li class="tab-link nav-pill mt-2">
                    <a href="{{ route('robotListPage') }}">
                        <span class="ease-effect">Robot Detail List</span>
                    </a>
                </li>
                <li class="tab-link current nav-pill mt-2">
                    <a href="{{ route('robotComparePage') }}">
                        <span class="ease-effect">Robot Comparison</span>
                    </a>
                </li>
            </ul>
        </div>

        {{-- ROBOT COMPARISON --}}
        <div>
            <div>
                <h2 class="text-center robot-compare-title my-5">Popular Robot Comparisons</h2>
            </div>
            <form action="{{ route('robotCompare') }}" method="post">
                @csrf
                <div class="robot-compare-search">
                    <div class="search">
                        <i class="fas fa-search search-icon"></i>
                        <<<<<<< HEAD {{-- <input type="search" class="search-box" name="robot1" placeholder="Search..."> --}} <div class="search">
                            <i class="fas fa-search search-icon"></i>
                            {{-- <input type="search" name="robot2" class="search-box" placeholder="Search..."> --}}

                            <select class="robot1 search-box" name="robot1">
                                <option selected disabled>Search...</option>
                                @foreach ($pullRobot as $item)
                                    <option data-thumbnail="{{ asset('images/zeus.png') }}"
                                        value="{{ $item->robotLongName }}">{{ $item->robotLongName }}</option>
                                @endforeach
                            </select>
                    </div>
                    =======
                    <select class="robot1 search-box" name="robot1">
                        <option selected disabled>Search...</option>
                        @foreach ($pullRobot as $item)
                            <option data-thumbnail="{{ asset('images/zeus.png') }}" value="{{ $item->robotLongName }}">
                                {{ $item->robotLongName }}</option>
                        @endforeach
                    </select>
                    >>>>>>> 9b36f14df09ae2ad6a55b5da2623dfc01e1d3748
                </div>

                <div class="input-group mb-2 ml-3" style="width: 250px" id="pair-container">
                    <select name="pair" class="custom-select comparePair" id="inputGroupSelect01">
                        <option class="dropdown-toggle" selected>Choose Pair</option>
                        @foreach ($pullPairs as $pair)
                            <option value="{{ $pair->stat_pair }}">{{ $pair->stat_pair }}</option>
                        @endforeach
                    </select>
                    <div>
                        <p class="error-profitable-robot d-none" id="error-compare">Please select pair and timeframe</p>
                    </div>
                </div>


                <div class="search">
                    <i class="fas fa-search search-icon"></i>
                    {{-- <input type="search" name="robot2" class="search-box" placeholder="Search..."> --}}
                    <div class="search">
                        <i class="fas fa-search search-icon"></i>
                        {{-- <input type="search" name="robot2" class="search-box" placeholder="Search..."> --}}
                        <select class="robot2 search-box" name="robot2">
                            <option selected disabled>Search...</option>
                            @foreach ($pullRobot as $item)

                                <option data-thumbnail="{{ asset('images/Zeus.png') }}"
                                    value="{{ $item->robotLongName }}">{{ $item->robotLongName }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
        </div>

        <div class="compare-btn">
            <button type="submit" id="compare-submit">COMPARE</button>
        </div>
        </form>

    </div>
    @endif
    </div>

    @if ($robotCompare != null)
        <div class="robot-comparison">
            <h3>Popular Robot Comparisons</h3>
            <div class="row robot-comparison-card">

                @foreach ($robotCompare as $item)

                    <div class="col-md-6 col-6" style="padding: 5px">
                        <form action="{{ route('robotCompare') }}" method="post" id="robot-compare-card">
                            @csrf
                            <div class="card" onclick="this.parentNode.submit()">
                                <div class="card-body">
                                    <img src="{{ asset('images/robot-compare-1.png') }}" alt="robot">
                                    <img src="{{ asset('images/robot-compare-2.png') }}" alt="robot">
                                </div>
                                <div class="name-robot-compare">
                                    <h5>{{ $item['name1'] }}</h5>
                                    <h5>{{ $item['name2'] }}</h5>
                                    <input type="hidden" name="robot1" value="{{ $item['name1'] }}">
                                    <input type="hidden" name="robot2" value="{{ $item['name2'] }}">
                                    <input type="hidden" name="pair" value="{{ $item['pair'] }}">
                                </div>
                            </div>
                        </form>
                    </div>
                @endforeach

            </div>
        </div>
        {{-- @endif --}}
        <div class="text-center mt-5">
            @if ($cekSubs->status == null || $cekSubs->status == 'expired')
                <a href="{{ route('subsPage') }}">
                    <button class="subscribe-btn mb-5">SUBSCRIBE NOW</button>
                </a>
            @else
                <a href="{{ route('myrobot') }}">
                    <button class="subscribe-btn mb-5">SUBSCRIBE NOW</button>
                </a>
            @endif

            <button class="subscribe-btn mb-5">LEARN HOW TO USE</button>
        </div>

        </div>

        {{-- Menu Bawah --}}
        <div class="customize_solution my-5">
            <ul class="tabs nav nav-justified">
                <li class="tab-link nav-pill mt-2">
                    <a href="{{ route('robotPage') }}">
                        <span class="ease-effect">Profitable Robot</span>
                    </a>
                </li>
                <li class="tab-link nav-pill mt-2">
                    <a href="{{ route('robotListPage') }}">
                        <span class="ease-effect">Robot Detail List</span>
                    </a>
                </li>
                <li class="tab-link current nav-pill mt-2">
                    <a href="{{ route('robotComparePage') }}">
                        <span class="ease-effect">Robot Comparison</span>
                    </a>
                </li>
            </ul>
        </div>
        {{-- End Menu Bawah --}}

        </div>


    @endsection

    @section('js')
        <script>

        </script>
        <script>
            $(document).ready(function() {
                $('.robot1').select2({
                    templateResult: setPicture,
                    templateSelection: setPicture
                });
                $('.robot2').select2({
                    templateResult: setPicture1,
                    templateSelection: setPicture1
                });
                $('.comparePair').select2();

                function setPicture(picture) {
                    if (!picture.id) {
                        return picture.text;
                    }
                    var img = $(picture.element).data('thumbnail');
                    //  console.log(img)
                    if (!img) {
                        return picture.text;
                    } else {
                        var $picture = $('<img src="' + img +
                            '" alt="" class="image-thumb"><span class="img-changer-text ml-3">' + $(
                                picture.element).text() + '</span>');
                        return $picture;
                    }
                }

                function setPicture1(picture) {
                    if (!picture.id) {
                        return picture.text;
                    }
                    var img = $(picture.element).data('thumbnail');
                    //  console.log(img)
                    if (!img) {
                        return picture.text;
                    } else {
                        var $picture = $('<img src="' + img +
                            '" alt="" class="image-thumb"><span class="img-changer-text ml-3">' + $(
                                picture.element).text() + '</span>');
                        return $picture;
                    }
                }

                $(document).on('click', '#compare-submit', function() {
                    var pair = $('select[name="pair"] option').filter(':selected').val();
                    robot1 = $('.robot1').val();
                    robot2 = $('.robot2').val();
                    console.log(pair, robot1, robot2);
                    if (pair == 'Choose...' || robot1 == null || robot2 == null) {
                        $('#error-compare').removeClass("d-none")
                        $('#pair-container').removeClass("mb-2")
                        $('#pair-container').addClass("mt-5")
                        return false;
                    } else {
                        return true;
                    }
                })
            });

        </script>
        <script>
            function fetch_data(query) {
                // console.log(query)
                window.location.href = "/v2/dashboard-myrobot/search?query=" + query
            }

        </script>
    @endsection
