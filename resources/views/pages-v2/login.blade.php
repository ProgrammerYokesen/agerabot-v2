<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Agerabot</title>
    {{-- Favicon --}}
    <link rel="icon" type="image/png" href="{{ URL::to('images/favicon.ico') }}">

    <script src="{{ asset('js/app.js') }}" defer></script>

    {{-- CSS --}}
    <link rel="stylesheet" href="{{ URL::to('css/style.css') }}">

    {{-- Bootstrap --}}
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css"
        integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">

    {{-- Font Awesome --}}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css">

    <meta name="csrf-token" content="{{ csrf_token() }}">



</head>

<body>

    <div class="register-hero">
        <div class="register-logo">
            <a href="{{ route('homePage') }}">
                <img src="{{ asset('./images/logo.svg') }}" alt="" class="register">
            </a>
        </div>
        <div class="register-overlay">

            <div class="login-hero-container">
                <h1>Login</h1>
                <p class="new-user">New user? Let’s create <a href="{{ route('registerPage') }}">new one</a></p>
                <form>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Email Address</label>
                        <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"
                            required>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Password</label>
                        <input type="password" class="form-control" id="exampleInputPassword1" required>
                    </div>
                    <p class="forgot-pass">Forgot Password?</p>
                    <button type="submit" class="register-btn my-3">LOGIN</button>
                    <h5 class="text-center">Or</h5>
                    <button type="submit" class="login-fb-btn my-3">Login with Facebook</button>
                    <button type="submit" class="login-google-btn my-3">Login with Google</button>
                </form>

            </div>
        </div>


    </div>


    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous">
    </script>

</body>

</html>
