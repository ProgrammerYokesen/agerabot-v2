@extends('app')

@section('content')
    <div class="robot-hero">
        <div class="robot-hero-txt">
            <h2>Temukan Robot yang tepat untuk Anda</h2>
            <h5>Kami menyediakan berbagai jenis robot yang sesuai dengan karakter dan tipe trading Anda, dan pastinya robot
                kami memiliki performa yang bisa membantu Anda meraih profit!</h5>
        </div>
        <div class="robot-hero-overlay"></div>
    </div>

    <div class="container robot-menu-container">
        <div class="customize_solution">
            <ul class="tabs nav nav-justified">
                <li class="tab-link nav-pill mt-2">
                    <a href="{{ route('robotPage') }}">
                        <span class="ease-effect">Profitable Robot</span>
                    </a>
                </li>
                <li class="tab-link current nav-pill mt-2">
                    <a href="{{ route('robotListPage') }}">
                        <span class="ease-effect">Robot Detail List</span>
                    </a>
                </li>
                <li class="tab-link nav-pill mt-2">
                    <a href="{{ route('robotComparePage') }}">
                        <span class="ease-effect">Robot Comparison</span>
                    </a>
                </li>
            </ul>
        </div>

        {{-- ROBOT DETAIL LIST --}}
        <div>
            <h2 class="text-center robot-detail-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit</h2>

            <div class="search-filter">
                <div class="filter">
                    <div>
                        <p>Sort By :</p>
                    </div>
                    {{-- Sort By: <input type="text" class="filter-box"> --}}
                    <div class="input-group mb-3 ml-3" style="width: 200px">
                        <select name="sorting" class="custom-select robot-list-sort" id="sortData">
                            {{-- <option selected disabled>Choose...</option> --}}
                            <option value="performance" selected>Performance</option>
                            <option value="asc">name: A to Z</option>
                            <option value="desc">name: Z to A</option>
                            <option value="new">Newest Robot</option>
                            <option value="old">Oldest Robot</option>
                        </select>
                    </div>
                </div>
                <div class="search">
                    <i class="fas fa-search search-icon"></i>
                    <input type="search" class="search-box" placeholder="Search..." id="search-robot-list">
                </div>
            </div>

            @include('pages-v2.robot-list-card')

            <input type="hidden" name="hidden_page" id="hidden_page" value="1" />

            <div class="text-center">
                <a href="{{ route('subsPage') }}">
                    <button class="subscribe-btn mb-5">SUBSCRIBE NOW</button>
                </a>
                <a href="{{ route('howToPage') }}">
                    <button class="subscribe-btn mb-5">LEARN HOW TO USE</button>
                </a>
            </div>



        </div>

        {{-- Menu Bawah --}}
        <div class="customize_solution my-5">
            <ul class="tabs nav nav-justified">
                <li class="tab-link nav-pill mt-2">
                    <a href="{{ route('robotPage') }}">
                        <span class="ease-effect">Profitable Robot</span>
                    </a>
                </li>
                <li class="tab-link current nav-pill mt-2">
                    <a href="{{ route('robotListPage') }}">
                        <span class="ease-effect">Robot Detail List</span>
                    </a>
                </li>
                <li class="tab-link nav-pill mt-2">
                    <a href="{{ route('robotComparePage') }}">
                        <span class="ease-effect">Robot Comparison</span>
                    </a>
                </li>
            </ul>
        </div>
        {{-- End Menu Bawah --}}

    </div>
@endsection

@section('js')

    <script>
        $(document).ready(function() {
            // console.log('masuk')
            function fetch_data(page, query, sort) {
                $.ajax({
                    url: "/search/robot?page=" + page + "&query=" + query + "&sort=" + sort,
                    success: function(dataFinal) {
                        // console.log(dataFinal)
                        $('#robot-list-card').html('');
                        $('#robot-list-card').html(dataFinal);
                    }
                })
            }

            $(document).on('keyup', '#search-robot-list', function() {
                var query = $('#search-robot-list').val();
                var sort = $('select[name="sorting"] option').filter(':selected').val();
                // console.log(selected_option)
                console.log(sort, query)

                // var column_name = $('#hidden_column_name').val();
                // var sort_type = $('#hidden_sort_type').val();
                var page = $('#hidden_page').val();
                fetch_data(page, query, sort);
            });
            $(document).on('change', '#sortData', function() {
                // console.log('masuk')
                var query = $('#search-robot-list').val();
                var sort = $('select[name="sorting"] option').filter(':selected').val();
                // console.log(sort, query)
                // var column_name = $('#hidden_column_name').val();
                // var sort_type = $('#hidden_sort_type').val();
                var page = $('#hidden_page').val();
                fetch_data(page, query, sort);
            });

            $(document).on('click', '.pagination a', function(event) {
                event.preventDefault();
                var page = $(this).attr('href').split('page=')[1];
                $('#hidden_page').val(page);
                // var column_name = $('#hidden_column_name').val();
                // var sort_type = $('#hidden_sort_type').val();
                var sort = $('select[name="sorting"] option').filter(':selected').val();

                var query = $('#search-robot-list').val();

                $('li').removeClass('active');
                $(this).parent().addClass('active');
                fetch_data(page, query, sort);
            });


        })

    </script>

    <script>
        function wish(robot) {
            console.log(robot)
            $.ajax({
                    url: "/wishlist?robot=" + robot,
                    type: "GET",
                    // data: robot,
                    // headers: {
                    //     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    // },
                    beforeSend: function() {
                        // $('.ajax-load').show();
                    }

                })
                .done(function(data) {
                    // console.log(data)
                    if (data.subs == true) {
                        var source = "{{ asset('./images/wishlist-red.svg') }}"
                        $(`#${data.slug}`).attr('src', source)
                    } else {
                        var source = "{{ asset('./images/wishlist.svg') }}"
                        $(`#${data.slug}`).attr('src', source)
                    }
                })
                .fail(function(e) {
                    console.log(e);

                });
        }

    </script>

@endsection
