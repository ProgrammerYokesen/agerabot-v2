@extends('pages-v2.dashboard.dash-myrobot')
@section('dashboardRobot')
<div class="my-robot-header">
  <div class="row">
      <div class="col-md-6">
          {{-- Menu Bawah --}}
          <div class="customize_solution">
              <ul class="tabs nav nav-justified">
                  <li class=" nav-pill mt-2" >
                    <a href="{{route('myrobot')}}"><span class="ease-effect">Robot List</span></a>
                  </li>
                  <li class=" current nav-pill mt-2" >
                    <a href="{{route('dasboardWishlist')}}"><span class="ease-effect">Wishlist</span></a>
                  </li>
              </ul>
          </div>
          {{-- End Menu Bawah --}}
      </div>
  </div>
  
</div>
<div class="myrobot-list">
  <div class="row" style="row-gap: 75px" id="dashboard-robot-wish">
@if ($finalRobotWish)
@foreach ($finalRobotWish as $robot=>$data)
{{-- <div class="tab-content" id="tab-2"> --}}
  <div class="col-md-4 cardss [ is-collapsed ]">
    <div class="robot-list-card card__inner">
      <img src="{{ asset('./images/robot.png') }}" alt="robot-img" class="myrobot-img">
      <div class="myrobot-name">
          <h5>{{$finalRobot[$robot]['robotName']}}</h5>
          <a href="{{route('downloadRobot', ['id'=> $finalRobotWish[$robot]['robot']])}}"><button>Download</button></a>
          <button class="[ js-expander ]">See Detail</button>
      </div>
    </div>
    {{-- Data Expand --}}
    <div class="card__expander">
        <div class="row">
            <div class="col-md-8">
                <div class="detail-header row align-items-center">
                    <h5 class="col-md-2" style="padding-left:30px">Best Pair</h5>
                    <h4 class="col-md-10" style="padding-left:30px">{{$finalRobotWish[$robot]['pair']}}</h4>
                </div>
                <div class="last-ten row align-items-center">
                    <h5 class="col-md-2">Last Ten</h5>
                    <div class="col-md-8">
                        <div class="progress">
                            <div class="progress-bar" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width:{{$finalRobotWish[$robot]['last_percent']}}%"></div>
                        </div>
                    </div>
                    <h5 class="col-md-2">{{$finalRobotWish[$robot]['last_percent']}}%</h5>
                </div>
                <div class="last-ten row align-items-center">
                    <h5 class="col-md-2">Weekly</h5>
                    <div class="col-md-8">
                        <div class="progress">
                            <div class="progress-bar" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width:{{$finalRobotWish[$robot]['week_percent']}}%"></div>
                        </div>
                    </div>
                    <h5 class="col-md-2">{{$finalRobotWish[$robot]['week_percent']}}%</h5>
                </div>
                <div class="last-ten row align-items-center">
                    <h5 class="col-md-2">Monthly</h5>
                    <div class="col-md-8">
                        <div class="progress">
                            <div class="progress-bar" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width:{{$finalRobotWish[$robot]['month_percent']}}%"></div>
                        </div>
                    </div>
                    <h5 class="col-md-2">{{$finalRobotWish[$robot]['month_percent']}}%</h5>
                </div>
            </div>
            <div class="col-md-4 text-center robot-detail-right">
                <div>
                    <h1>{{$finalRobotWish[$robot]['year_percent']}}%</h1>
                </div>
                <p>Year to date Performance <br> {{$finalRobotWish[$robot]['pair']}}</p>
                <button>Subscribe</button>
                <button class="[ js-collapser ]">Show Less</button>
            </div>
        </div>
    </div>
  </div>
  @endforeach
  </div>
</div>
@else
<h1>You Have No Wishlist Robot</h1>
@endif

{{-- </div> --}}
@endsection
