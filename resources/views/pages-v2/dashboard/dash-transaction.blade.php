@extends('pages-v2.dashboard.dashboard')

@section('dash-content')
    <div class="transaction-container">

        <div class="order_menu">
            <a class="order_link active" href="{{ route('transactionHistory') }}">Unpaid</a>
            <a class="order_link" href="{{ route('transactionDone') }}">Done</a>
        </div>

        <h5>Unpaid Transaction</h5>
        <div class="unpaid-table">
            <table class="table table-striped table-bordered">
                <thead>
                    <tr class="text-center">
                        <th scope="col">Plan/Product</th>
                        <th scope="col">Invoice ID</th>
                        <th scope="col">Active Time</th>
                        <th scope="col">Status</th>
                        <th scope="col">Payment Method</th>
                        <th scope="col"></th>
                    </tr>
                </thead>
                <tbody>
                    @if (count($unpaidSubs) > 0)
                        @foreach ($unpaidSubs as $datum)
                            <tr>
                                <td>{{ $datum->nmPackage }}</td>
                                <td>{{ $datum->externalId }}</td>
                                <td></td>
                                <td>Unpaid</td>
                                <td></td>
                                <td>
                                    <a href="{{ route('addToCartUnpaid', $datum->id) }}"><button
                                            class="checkout-btn">Checkout</button></a>
                                </td>
                            </tr>
                        @endforeach

                    @endif
                    @if (count($unpaidVps) > 0)
                        @foreach ($unpaidVps as $item)
                            <tr>
                                <td>{{ $item->nmVps }}</td>
                                <td>{{ $item->externalId }}</td>
                                <td></td>
                                <td>Unpaid</td>
                                <td></td>
                                <td>
                                    <a href="{{ route('addToCartUnpaidVps', $item->id) }}"><button
                                            class="checkout-btn">Checkout</button></a>
                                </td>
                            </tr>
                        @endforeach

                    @endif
                    @if (count($unpaidInstall) > 0)
                        @foreach ($unpaidInstall as $data)
                            <tr>
                                <td>Install Robot</td>
                                <td>{{ $data->externalId }}</td>
                                <td></td>
                                <td>Unpaid</td>
                                <td></td>
                                <td>
                                    <a href="{{ route('addToCartUnpaidInstall', $data->id) }}"><button
                                            class="checkout-btn">Checkout</button></a>
                                </td>
                            </tr>
                        @endforeach

                    @endif
                    {{-- <tr>
                    <td>Premium</td>
                    <td>INV-1613966294</td>
                    <td></td>
                    <td>Unpaid</td>
                    <td></td>
                    <td>
                        <button class="checkout-btn">Checkout</button>
                    </td>
                  </tr> --}}
                    </tr>
                </tbody>
            </table>
        </div>


    </div>
@endsection
