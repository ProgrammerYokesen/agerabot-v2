<div class="row" id="dashboard-robot-list">
    {{-- @php
      dd($finalRobot);
  @endphp --}}
    {{-- <h1>tes</h1> --}}
    @foreach ($finalRobot as $robot => $data)
        {{-- @php
        dd($finalRobot, $data['robot']);
    @endphp --}}
        <div class="col-md-4 cardss [ is-collapsed ]">
            <div class="robot-list-card card__inner">
                <img src="{{ asset('./images/robot.png') }}" alt="robot-img" class="myrobot-img">
                <div class="myrobot-name">
                    <h5>{{ $data['robotName'] }}</h5>
                    @if ($packageStatus->status != 'active' || $packageStatus->status == null)
                        <a href="{{ route('productPage') }}">
                            <button class="dash-robot-download">Download</button></a>
                        </a>
                    @elseif ($data['subs'] == true)
                        <a href="{{ route('upgradeRobot') }}">
                            <button class="dash-robot-download">Download</button></a>
                        </a>
                    @else
                        <button data-toggle="modal" data-target="#termsCondition" data-id="{{ $data['robot'] }}"
                            data-userId="{{ $userId }}" data-packageId="{{ $packageId->packageId }}"
                            data-time="{{ $time }}" class="dash-robot-download">Download</button></a>
                    @endif
                    <button class="[ js-expander ]">See Detail</button>
                </div>
            </div>
            {{-- Data Expand --}}
            <div class="card__expander">
                <div class="row">
                    <div class="col-md-8">
                        <div class="detail-header row align-items-center">
                            <h5 class="col-md-2" style="padding-left:30px">Best Pair</h5>
                            <h4 class="col-md-10" style="padding-left:30px">{{ $data['pair'] }}</h4>
                        </div>
                        <div class="last-ten row align-items-center">
                            <h5 class="col-md-2">Last Ten</h5>
                            <div class="col-md-8">
                                <div class="progress">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="75" aria-valuemin="0"
                                        aria-valuemax="100" style="width:{{ $data['last_percent'] }}%"></div>
                                </div>
                            </div>
                            <h5 class="col-md-2">{{ $data['last_percent'] }}%</h5>
                        </div>
                        <div class="last-ten row align-items-center">
                            <h5 class="col-md-2">Weekly</h5>
                            <div class="col-md-8">
                                <div class="progress">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="75" aria-valuemin="0"
                                        aria-valuemax="100" style="width:{{ $data['week_percent'] }}%"></div>
                                </div>
                            </div>
                            <h5 class="col-md-2">{{ $data['week_percent'] }}%</h5>
                        </div>
                        <div class="last-ten row align-items-center">
                            <h5 class="col-md-2">Monthly</h5>
                            <div class="col-md-8">
                                <div class="progress">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="75" aria-valuemin="0"
                                        aria-valuemax="100" style="width:{{ $data['month_percent'] }}%"></div>
                                </div>
                            </div>
                            <h5 class="col-md-2">{{ $data['month_percent'] }}%</h5>
                        </div>
                    </div>
                    <div class="col-md-4 text-center robot-detail-right">
                        <div>
                            <h1>{{ $data['year_percent'] }}%</h1>
                        </div>
                        <p>Year to date Performance <br> {{ $data['pair'] }}</p>
                        <button>Subscribe</button>
                        <button class="[ js-collapser ]">Show Less</button>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
    {{-- @endfor --}}


</div>
