@extends('pages-v2.dashboard.dashboard')

@section('dash-content')
    <div class="setting-container">
        <ul class="nav nav-tabs nav-justified" id="myTab" role="tablist">
            <li class="nav-item">
                <a class="nav-link" id="profile-tab" href="{{ route('settingPage') }}">Profile Detail</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="pass-tab" href="{{ route('passwordPage') }}">Change Password</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="2fa-tab" href="#2fa">2FA</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="terms-tab" href="{{ route('termsPage') }}">Term & Condition</a>
            </li>
            <li class="nav-item">
                <a class="nav-link active" id="plans-tab" href="javascript:void(0)">Plan</a>
            </li>
        </ul>

        <div class="setting-content">
            <div class="plan-flex">
                <h5>Current Subscription Plan</h5>
                <h5>1 Month</h5>
            </div>
            <div class="plan-flex">
                <h5>Auto Renewal</h5>
                <div>
                    <label class="tgl">
                        <input type="checkbox" checked/>
                        <span class="tgl_body">
                          <span class="tgl_switch"></span>
                          <span class="tgl_track">
                            <span class="tgl_bgd"></span>
                            {{-- <span class="tgl_bgd tgl_bgd-negative"></span> --}}
                          </span>
                        </span>
                    </label>
                </div>
            </div>
            <a href="#" class="stop-recurring" data-toggle="modal" data-target="#modalRedownload">Stop Recurring</a> <br>

            <button>SAVE</button>
        </div>

        <!-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalRedownload">
        Launch demo modal
    </button> -->

    <!-- Modal Redownload / Generate License -->
    <div class="modal fade" id="modalRedownload" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h2 class="text-center">Stop Subscription</h2>
                <h4>Are you sure want to stop subscription?</h4>
                <div class="modal-btn-container">
                    <button class="modal-download-btn" data-dismiss="modal">No</button>
                    <a href="{{route('stopSubscription')}}">
                        <button class="modal-generate-btn">Yes</button>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <!-- end modal -->

    </div>
@endsection