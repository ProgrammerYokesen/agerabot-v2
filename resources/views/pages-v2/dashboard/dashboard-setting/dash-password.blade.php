@extends('pages-v2.dashboard.dashboard')

@section('dash-content')
    <div class="setting-container">
        <ul class="nav nav-tabs nav-justified" id="myTab" role="tablist">
            <li class="nav-item">
                <a class="nav-link" id="profile-tab" href="{{ route('settingPage') }}">Profile Detail</a>
            </li>
            <li class="nav-item">
                <a class="nav-link active" id="pass-tab" href="javascript:void(0)">Change Password</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="2fa-tab" href="#2fa">2FA</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="terms-tab" href="{{ route('termsPage') }}">Term & Condition</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="plans-tab" href="{{ route('planPage') }}">Plan</a>
            </li>
        </ul>

        <div class="setting-content">
            <form action="post" method="{{ route('submitChangePassword')}}">
                <div class="form-group row align-items-center">
                  <label for="staticEmail" class="col-sm-4 col-form-label">Password Lama</label>
                  <div class="col-sm-8">
                    <input type="password" name="passwordLama" class="form-control input" id="name">
                  </div>
                </div>
                <div class="form-group row align-items-center">
                  <label for="inputPassword" class="col-sm-4 col-form-label">Password Baru</label>
                  <div class="col-sm-8">
                    <input type="password" name="passwordBaru" class="form-control input" >
                  </div>
                </div>
                <div class="form-group row align-items-center">
                  <label for="inputPassword" class="col-sm-4 col-form-label">Konfirmasi Password Baru</label>
                  <div class="col-sm-8">
                    <input type="password" name="confirmasiPasswordBaru" class="form-control input" >
                  </div>
                </div>

                <button>SAVE</button>                
              </form>
        </div>

    </div>
@endsection