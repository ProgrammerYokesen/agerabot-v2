@extends('pages-v2.dashboard.dashboard')

@section('dash-content')
    <div class="setting-container">
        <ul class="nav nav-tabs nav-justified" id="myTab" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" id="profile-tab" href="javascript:void(0)">Profile Detail</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="pass-tab" href="{{ route('passwordPage') }}">Change Password</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="2fa-tab" href="#2fa">2FA</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="terms-tab" href="{{ route('termsPage') }}">Term & Condition</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="plans-tab" href="{{ route('planPage') }}">Plan</a>
            </li>
        </ul>

        <div class="setting-content">
            <form action="post" method="{{ route('submitProfile')}}">
              @csrf
                <div class="form-group row">
                  <label for="staticEmail" class="col-sm-4 col-form-label">Name</label>
                  <div class="col-sm-8">
                    <input type="text" class="form-control input" id="name" value="{{$user->name}}">
                  </div>
                </div>
                <div class="form-group row">
                  <label for="inputPassword" class="col-sm-4 col-form-label">Username</label>
                  <div class="col-sm-8">
                    <input type="text" class="form-control input" id="username" value="{{$user->username}}">
                  </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group row" style="width: 100%;">
                            <label for="staticEmail" class="col-sm-4 col-form-label">Email</label>
                            <div class="col-sm-8">
                              <input type="text" class="form-control input" id="email" value="{{$user->email}}" disabled>
                            </div>
                        </div>
                        <div class="form-group row" style="width: 100%;">
                            <label for="staticEmail" class="col-sm-4 col-form-label">Phone Number</label>
                            <div class="col-sm-8">
                              <input type="text" class="form-control input" id="phone" value="{{$user->phoneNumber}}">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group row" style="width: 100%;">
                            <label for="staticEmail" class="col-sm-4 col-form-label">Country</label>
                            <div class="col-sm-8">
                              <input type="text" class="form-control input" id="country" value="{{$user->country}}">
                            </div>
                        </div>
                        <div class="form-group row" style="width: 100%;">
                            <label for="staticEmail" class="col-sm-4 col-form-label">Upload Photo</label>
                            <div class="col-sm-8">
                              <input type="file" id="photo">
                            </div>
                        </div>
                    </div>
                </div>

                <button>SAVE</button>

                
              </form>
        </div>

    </div>
@endsection