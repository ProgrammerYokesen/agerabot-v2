@extends('pages-v2.dashboard.dashboard')

@section('dash-content')
    <div class="upgrade-container">


        <h4>Upgrade Robot</h4>

        @foreach ($robots as $datum)

            <div class="card" id="robot-upgrade-card">
                <div class="card-body">
                    <div class="upgrade-robots row align-items-center">
                        <div class="col-md-1">
                            <img src="{{ asset('./images/robot.png') }}" alt="robot-icon" width="100">
                        </div>
                        <div class="col-md-2 robots-status">
                            <p>{{ $datum->robotLongName }} <br>{{ $inv->externalId }} </p>
                            <div class="status-label">
                                @if ($datum->status == 'Waiting')
                                    <div class="label-waiting"></div>
                                @elseif ($datum->status == 'Active')
                                    <div class="label-active"></div>
                                @else
                                    <div class="label-inactive"></div>
                                @endif
                                <span class="ml-1">{{ $datum->status }}</span>
                            </div>
                        </div>
                        <div class="col-md-4 feature-status">
                            <div class="feature-label mr-5">
                                @if ($datum->installmentStatus == 1)
                                    <div class="label-waiting"></div>
                                @elseif ($datum->installmentStatus == 2)
                                    <div class="label-active"></div>
                                @else
                                    <div class="label-inactive"></div>
                                @endif
                                <span class="ml-2">Installment</span>
                            </div>
                            <div class="feature-label">
                                @if ($datum->vpsStatus == 1)
                                    <div class="label-waiting"></div>
                                @elseif ($datum->vpsStatus == 2)
                                    <div class="label-active"></div>
                                @else
                                    <div class="label-inactive"></div>
                                @endif
                                <span class="ml-2">VPS</span>
                            </div>
                        </div>
                        <div class="col-md-5 btn-container">
                            <button class="manage-btn mr-3">Manage</button>
                            <button class="download-btn modal-redownload" data-toggle="modal" data-target="#modalRedownload"
                                data-id="{{ $datum->id }}" data-userId="{{ $userId }}"
                                data-packageId="{{ $packageId->packageId }}"
                                data-time="{{ $time }}">Download</button>
                        </div>
                    </div>

                    {{-- THE DETAIL --}}
                    <div class="row my-3 dnone" id="robot-upgrade-detail">
                        <div class="col-md-3 install-status">
                            <p style="font-weight: 700">Installation Status</p>
                            <p>Not Installed</p>
                            <button>INSTALL</button>
                        </div>
                        <div class="col-md-3 vps-status">
                            <p style="font-weight: 700">VPS Status</p>
                            <div class="dropdown">
                                <button class="btn btn-sm dropdown-toggle" type="button" id="dropdownMenuButton"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    VPS Buah-buahan
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <a class="dropdown-item" href="#">None</a>
                                    <a class="dropdown-item" href="#">VPS-Buah-buahan + Maintenance</a>
                                    <a class="dropdown-item" href="#">+ Add new VPS</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 report-btn">
                            <button>Report</button>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
        <!-- Modal Redownload / Generate License -->
        <div class="modal fade" id="modalRedownload" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
                <div class="modal-content">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h2 class="text-center">Re-Download Robot</h2>
                    <h4>Do you want to generate new license key? (2/3)</h4>
                    <div class="modal-btn-container" id="re-download-container">
                        {{-- <button class="modal-download-btn">Download Only</button> --}}
                        {{-- <button class="modal-generate-btn">Generate</button> --}}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('jsDashboard')
    <script>
        $(document).ready(function() {
            $(document).on('click', '.modal-redownload', function() {
                var id = $(this).attr('data-id');
                var packageId = $(this).attr('data-packageId');
                var userId = $(this).attr('data-userId');
                var time = $(this).attr('data-time');
                var code = `SW-${userId}-${packageId}-${time}`
                var generateCode = 'yes';
                var downloadOnly = 'no';
                console.log('tes');
                $('#re-download-container').append(
                    `<a href="/download/robot/` + id + `/` + code + `/` + downloadOnly +
                    `" id="download-button"><button class="modal-download-btn" >Download Only</button></a>
                        <a href="/download/robot/` + id + `/` + code + `/` + generateCode +
                    `" id="download-button"><button class="modal-generate-btn" >Generate</button></a>`
                )
            })
        })

    </script>
@endsection
