@extends('pages-v2.dashboard.dashboard')

@section('dash-content')
    <div class="product-container">

        <div class="row">

            <div class="col-md-4">

                <div class="product-content">
                    <div data-toggle="collapse" href="#collapseSubs" class="toggle_accordion">
                        <img src="{{ asset('images/icons/dash-sub.png') }}" alt="">
                        <h5 class="mb-5">Subscription</h5>
                    </div>

                    <div class="collapse" id="collapseSubs">
                        @foreach ($subscription as $index => $datum)
                            @if ($index % 2 == 0)
                                <div class="sub-even">
                                    <p>{{ $datum->nmPackage }}</p>
                                    <h5><sup>US$</sup> {{ $datum->price }}</h5>
                                    <a href="{{ route('addToCart', $datum->slug) }}">
                                        <button>ORDER</button>
                                    </a>
                                </div>
                            @else
                                <div class="sub-odd">
                                    <p>{{ $datum->nmPackage }}</p>
                                    <h5><sup>US$</sup> {{ $datum->price }}</h5>
                                    <a href="{{ route('addToCart', $datum->slug) }}">
                                        <button>ORDER</button>
                                    </a>
                                </div>
                            @endif

                        @endforeach
                    </div>

                </div>

            </div>

            <div class="col-md-4">

                <div class="product-content">
                    <div data-toggle="collapse" href="#collapseVPS" class="toggle_accordion">
                        <img src="{{ asset('images/icons/dash-vps.png') }}" alt="">
                        <h5 class="mb-5">VPS</h5>
                    </div>
                    <div class="collapse" id="collapseVPS">
                        {{-- ======= --}}

                        <div class="accordion" id="accordionExample">

                            <div id="headingOne" class="product-accordion" data-toggle="collapse" data-target="#collapseOne"
                                aria-expanded="true" aria-controls="collapseOne">
                                <h5 class="product-accordion-item">
                                    VPS Only
                                    <i class="fas fa-chevron-down rotate"></i>
                                </h5>
                            </div>

                            <div id="collapseOne" class="collapse accordion-expand" aria-labelledby="headingOne"
                                data-parent="#accordionExample">
                                <div style="width: 100%">
                                    @foreach ($vps1 as $indicator => $data)

                                        @if ($indicator % 2 == 0)
                                            <div class="sub-even">
                                                @if ($data->interval > 1)
                                                    <p>{{ $data->interval }} Months</p>
                                                @else
                                                    <p>{{ $data->interval }} Month</p>
                                                @endif
                                                <h5><sup>US$</sup> {{ $data->price }}</h5>
                                                <a href="{{ route('addToCartVps', $data->slug) }}">
                                                    <button>ORDER</button>
                                                </a>
                                            </div>
                                        @else
                                            <div class="sub-odd">
                                                <p>{{ $data->interval }} Months</p>
                                                <h5><sup>US$</sup> {{ $data->price }}</h5>
                                                <a href="{{ route('addToCartVps', $data->slug) }}">
                                                    <button>ORDER</button>
                                                </a>
                                            </div>
                                        @endif
                                    @endforeach

                                </div>
                            </div>

                            <div id="headingTwo" class="product-accordion" data-toggle="collapse" data-target="#collapseTwo"
                                aria-expanded="true" aria-controls="collapseTwo">
                                <h5 class="product-accordion-item">
                                    VPS + Maintenance
                                    <i class="fas fa-chevron-down rotate"></i>
                                </h5>
                            </div>

                            <div id="collapseTwo" class="collapse accordion-expand" aria-labelledby="headingTwo"
                                data-parent="#accordionExample">
                                <div style="width: 100%">
                                    @foreach ($vps2 as $index => $datum)
                                        @if ($indicator % 2 == 0)
                                            <div class="sub-even">
                                                @if ($datum->interval > 1)
                                                    <p>{{ $datum->interval }} Months</p>
                                                @else
                                                    <p>{{ $datum->interval }} Month</p>
                                                @endif
                                                <h5><sup>US$</sup> {{ $datum->price }}</h5>
                                                <a href="{{ route('addToCartVps', $datum->slug) }}">
                                                    <button>ORDER</button>
                                                </a>
                                            </div>
                                        @else
                                            <div class="sub-odd">
                                                <p>{{ $datum->interval }} Months</p>
                                                <h5><sup>US$</sup> {{ $datum->price }}</h5>
                                                <a href="{{ route('addToCartVps', $datum->slug) }}">
                                                    <button>ORDER</button>
                                                </a>
                                            </div>
                                        @endif
                                    @endforeach
                                </div>
                            </div>

                        </div>
                        {{-- ========= --}}
                    </div>



                </div>
            </div>

            <div class="col-md-4">
                {{-- <a href="{{ route('addToCartInstall') }}"> --}}
                <div class="product-content">

                    <div data-toggle="collapse" href="#collapseInstall" class="toggle_accordion">
                        <img src="{{ asset('images/icons/dash-install.png') }}" alt="">
                        <h5 class="mb-5">Installation</h5>
                    </div>

                    <div class="collapse" id="collapseInstall">
                        <div class="install_desc">
                            <div class="install_price row">
                                <div class="col-md-8">
                                    <h5>
                                        Installation Quota
                                    </h5>
                                    <p class="text-left">Buy our installation services and let us do the rest for you</p>
                                </div>
                                <div class="col-md-4">
                                    <h5><sup>US$</sup> 1.99</h5>
                                    <a href="{{ route('addToCartInstall') }}">
                                        <button>ORDER</button>
                                    </a>
                                </div>
                            </div>
                            <div class="install_guide">
                                <h5>Do It Yourself - Manual</h5>
                                <p class="text-left">This is the basic guidelines for trading robot installation steps which
                                    the
                                    you can refer to this anytime for any robots. However, if you needs more information or
                                    need
                                    help with the installation, buy installation quota instead.</p>
                                <a href="{{ route('manualPage') }}">Learn More <i class="fas fa-arrow-right"></i></a>
                            </div>
                            <div class="vps_guide">
                                <h5>Do It Yourself - With VPS</h5>
                                <p class="text-left">This is the basic guidelines for renting a VPS for forex trading which
                                    the
                                    user can refer to this anytime for any VPS in Metatrader 4 or 5. However, if you needs
                                    more
                                    information or need help with the installation, buy installation quota instead.</p>
                                <a href="{{ route('manualVpsPage') }}">Learn More <i class="fas fa-arrow-right"></i></a>
                            </div>
                        </div>
                    </div>

                </div>
                {{-- </a> --}}
            </div>

            <!-- Button trigger modal -->
            {{-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#termsCondition">
                Launch demo modal
            </button> --}}

            <!-- Modal Redownload / Generate License -->
            <div class="modal fade" id="modalRedownload" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h2 class="text-center">Re-Download Robot</h2>
                        <h4>Do you want to generate new license key? (2/3)</h4>
                        <div class="modal-btn-container">
                            <button class="modal-download-btn">Download Only</button>
                            <button class="modal-generate-btn">Generate</button>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Modal Install Robot -->
            <div class="modal fade" id="modalInstall" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h2 class="text-center">Installation</h2>
                        <h4>Your installation quota will be used to Aquarius robot. Please confirm to install the robot
                        </h4>
                        <button class="modal-confirm-btn">Confirm</button>
                    </div>
                </div>
            </div>

            <!-- Modal Redownload / Generate License -->
            <div class="modal fade" id="modalNoQuota" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h2 class="text-center">You doesn’t have sufficient <br> installation quota </h2>
                        <h4>Please order installation quota to use this feature</h4>
                        <button class="modal-confirm-btn">Confirm</button>
                    </div>
                </div>
            </div>

            <!-- Modal Terms & Condition -->
            <div class="modal fade" id="termsCondition" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h2 class="text-center">Terms and Condition</h2>
                        <div class="">
                            <h4>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lacus, risus aenean ut
                                pellentesque.
                                Vestibulum a quam ullamcorper vulputate molestie sem lorem in augue.</h4>
                            <p>xxxxxxxx</p>
                            <p>xxxxxxxx</p>
                            <p>xxxxxxxx</p>
                            <p>xxxxxxxx</p>
                            <p>xxxxxxxx</p>
                            <div class="form-group form-check">
                                <input type="checkbox" class="form-check-input" id="exampleCheck1">
                                <label class="form-check-label" for="exampleCheck1">I agree</label>
                            </div>
                        </div>
                        <button class="modal-confirm-btn">I AGREE</button>
                    </div>
                </div>
            </div>

            <!-- Modal Step Install -->
            <div class="modal fade" id="modalStepInstall" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>

                        <div class="install-card">
                            {{-- STEP 1 --}}
                            <div class="card">
                                <img src="{{ asset('images/icons/install-1.svg') }}" alt="label" class="install-label">
                                <div class="card-body">
                                    adasd
                                </div>
                            </div>

                            {{-- STEP 2 --}}
                            <div class="card">
                                <img src="{{ asset('images/icons/install-2.svg') }}" alt="label" class="install-label">
                                <div class="card-body">
                                    This is some text within a card body.
                                </div>
                            </div>

                            {{-- STEP 3 --}}
                            <div class="card">
                                <img src="{{ asset('images/icons/install-3.svg') }}" alt="label" class="install-label">
                                <div class="card-body">
                                    This is some text within a card body.
                                </div>
                            </div>

                            {{-- STEP 4 --}}
                            <div class="card">
                                <img src="{{ asset('images/icons/install-4.svg') }}" alt="label" class="install-label">
                                <div class="card-body">
                                    This is some text within a card body.
                                </div>
                            </div>
                        </div>

                        <button class="modal-confirm-btn">DOWNLOAD</button>
                    </div>
                </div>
            </div>
        @endsection
