@extends('pages-v2.dashboard.dashboard')

@section('dash-content')
    <div class="home-container">
        <div class="row home-status">

            <div class="col-md-4">
                <div class="card">
                    <div class="card-body">
                        <p>Subscription</p>
                        <div class="sub-status row">
                            <p class="col-6">Status</p>
                            <p class="col-6" style="font-weight: 500">: <span
                                    class="subs-type">{{ $subscription->nmPackage }}</span></p>
                        </div>
                        <div class="sub-exp row">
                            <p class="col-6">Exp. till</p>
                            <p class="col-6" style="font-weight: 500">: {{ $subsExpired }}</p>
                        </div>
                        {{-- <button>SUBSCRIBE</button> --}}
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card">
                    <div class="card-body">
                        <p>Installation Quota</p>
                        <h4 class="install-quota">
                            {{ $quota }}
                        </h4>
                        {{-- <button>REFILL QUOTA</button> --}}
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card">
                    <div class="card-body">
                        <p>VPS</p>
                        <div class="sub-status row">
                            <p class="col-6">Status</p>

                            <p class="col-6" style="font-weight: 500">: {{ $data['status'] }}</p>
                        </div>
                        <div class="sub-exp row">
                            <p class="col-6">Exp. till</p>
                            <p class="col-6" style="font-weight: 500">: {{ $data['paymentExpired'] }}</p>
                        </div>
                        {{-- <button>BUY VPS</button> --}}
                    </div>
                </div>
            </div>

        </div>

        <div class="robot-list-table table-responsive">
            <table class="table table-striped text-center">
                <thead>
                    <tr>
                        <th scope="col">My Robot List</th>
                        <th scope="col">Robot License</th>
                        <th scope="col">Last Transaction</th>
                        <th scope="col">Robot Health</th>
                        <th scope="col">Status</th>
                    </tr>
                </thead>
                <tbody>

                    @if ($robotList != null)

                        @foreach ($robotList as $robot)
                            <tr>
                                <td style="font-weight: 700">
                                    <img src="{{ asset('./images/robot.png') }}" alt="" style="width: 30px; height: 30px">
                                    {{ $robot->robotLongName }}
                                </td>
                                <td>{{ $robot->licenseKey }}</td>
                                <td>{{ $robot->last_transaction_at }} </td>
                                <td>{{ $robot->health }}</td>
                                <td class="status-container">
                                    <div class="status-{{ $robot->status }}"></div>
                                    <div>{{ $robot->status }}</div>
                                </td>
                            </tr>
                        @endforeach
                    @endif

                </tbody>
            </table>
        </div>

        <div class="no_robot">
            <img src="{{ asset('images/robot-compare-2.png') }}" alt="">
            <h5>You don’t have any robots installed. <br>
                Please download and install robot in robot list menu.</h5>
            <button>Robot List</button>
        </div>


    </div>
@endsection
