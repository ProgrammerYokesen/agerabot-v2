@extends('pages-v2.dashboard.dashboard')

@section('dash-content')
    <div class="vps-container">

        <div class="vps_none">
            <img src="{{ asset('images/vps.png') }}" alt="vps">
            <p>You don’t have any VPS installed. <br>
                Please buy and install VPS in product menu.</p>
            <button>Get Vps</button>
        </div>

        @for ($i = 0; $i < 3; $i++)

            <div class="card" id="robot-upgrade-card">
                <div class="card-body">
                    <div class="upgrade-robots row align-items-center">
                        <div class="col-md-2 robots-status">
                            <p>VPS Buah-buahan</p>
                            <div class="vps-status-label">
                                <div class="label-active"></div>
                                <span class="ml-1">Active</span>
                            </div>
                        </div>
                        <div class="col-md-2 vps-info">
                            <h5>IP Address</h5>
                            <span>312.312.442.12</span>
                        </div>
                        <div class="col-md-2 vps-info">
                            <h5>Username</h5>
                            <span>r41jdf12</span>
                        </div>
                        <div class="col-md-2 vps-info">
                            <h5>Password</h5>
                            <span class="click-to-view">Click to View</span>
                        </div>
                        <div class="col-md-4 btn-container">
                            <button class="manage-btn mr-3">Manage</button>
                        </div>
                    </div>

                    {{-- THE DETAIL --}}
                    <div class="row my-3 dnone" id="robot-upgrade-detail">
                        <div class="col-md-3 install-status">
                            <p style="font-weight: 700">Installation Status</p>
                            <p>Not Installed</p>
                            <button>INSTALL</button>
                        </div>
                        <div class="col-md-3 vps-status">
                            <p style="font-weight: 700">VPS Status</p>
                            <div class="dropdown">
                                <button class="btn btn-sm dropdown-toggle" type="button" id="dropdownMenuButton"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    VPS Buah-buahan
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <a class="dropdown-item" href="#">None</a>
                                    <a class="dropdown-item" href="#">VPS-Buah-buahan + Maintenance</a>
                                    <a class="dropdown-item" href="#">+ Add new VPS</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 report-btn">
                            <button>Report</button>
                        </div>
                    </div>
                </div>
            </div>

        @endfor


    </div>
@endsection
