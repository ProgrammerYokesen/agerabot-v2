@extends('pages-v2.dashboard.dashboard')

@section('dash-content')
    <div class="transaction-container">
        <div class="order_menu">
            <a class="order_link" href="{{ route('transactionHistory') }}">Unpaid</a>
            <a class="order_link active" href="{{ route('transactionDone') }}">Done</a>
        </div>

        <h5 class="mt-5">Transaction History</h5>
        <div class="history-table">
            <table class="table table-striped table-bordered">
                <thead>
                    <tr class="text-center">
                        <th scope="col">Plan</th>
                        <th scope="col">Subscription date</th>
                        <th scope="col">Active Time</th>
                        <th scope="col">Status</th>
                        <th scope="col">Payment Method</th>
                        <th scope="col"></th>
                    </tr>
                </thead>
                <tbody>

                    @if (count($paidSubs) > 0)
                        @foreach ($paidSubs as $item)
                            <tr>
                                <td>{{ $item->nmPackage }}</td>
                                <td>{{ $item->activeTime }}</td>
                                <td>{{ $item->activeTime }}- {{ $item->expiredDate }}</td>
                                <td>{{ $item->expired }}</td>
                                <td>{{ $item->paymentMethod }}</td>
                                <td></td>
                            </tr>
                        @endforeach
                    @endif

                    @if (count($paidVps) > 0)
                        @foreach ($paidVps as $item)
                            <tr>
                                <td>{{ $item->name }}</td>
                                <td>{{ $item->activeTime }}</td>
                                <td>{{ $item->activeTime }}- {{ $item->expiredDate }}</td>
                                <td>{{ $item->expired }}</td>
                                <td>{{ $item->paymentMethod }}</td>
                                <td></td>
                            </tr>
                        @endforeach
                    @endif
                    @if (count($paidInstall) > 0)
                        @foreach ($paidInstall as $item)
                            <tr>
                                <td>Installation</td>
                                <td>{{ $item->paymentTime }}</td>
                                <td>{{ $item->paymentTime }}- </td>
                                <td>Active</td>
                                <td>{{ $item->paymentMethod }}</td>
                                <td>
                                    <a href="{{ route('addToCartInstall') }}"><button
                                            class="checkout-btn">Reorder</button></a>
                                </td>
                            </tr>
                        @endforeach
                    @endif
                </tbody>
            </table>
        </div>
    </div>
@endsection
