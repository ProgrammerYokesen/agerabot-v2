@extends('pages-v2.dashboard.dashboard')

@section('dash-content')
    <div class="help_container">
        <div class="help_title">
            <img src="{{ asset('images/help.png') }}" alt="help" class="help_icon">
            <h2>We are here to help you</h2>
        </div>

        <p>Our priorities is to help you experiencing the best of our product and services. Should you have any questions,
            facing problems during your journey in Agerabot or in case you need to get any information about us, feel free
            to reach us through:</p>

        <div class="help_contact">
            <img src="{{ asset('images/icons/mail.png') }}" alt="mail" class="contact_img">
            <a href="mailto:support@agerabot.com">support@agerabot.com</a>
        </div>
        <div class="help_contact">
            <img src="{{ asset('images/icons/phone.png') }}" alt="phone" class="contact_img">
            <p>021 - 000000</p>
        </div>
        <div class="help_contact">
            <img src="{{ asset('images/icons/instagram.png') }}" alt="instagram" class="contact_img">
            <a href="#">@agerabot_indonesia</a>
        </div>

        <p>For more details elaboration, asking for guideline or want to talk to us:</p>

        <a href="https://agerabotticketing.freshdesk.com/" target="_blank">
            <button class="help_btn">Create Ticket</button>
        </a>
    </div>
@endsection
