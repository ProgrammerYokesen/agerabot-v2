@extends('pages-v2.dashboard.dashboard')

@section('dash-content')
    <div class="manual_container">
        <div class="manual_title">
            <img src="{{ asset('images/icons/manual-vps.png') }}" alt="manual">
            <h2>With VPS</h2>
        </div>

        <div class="manual_vps">
            <div class="manual_card">
                <div class="card">
                    <div class="card-body">
                        <img src="{{ asset('images/icons/manual1.svg') }}" alt="label" class="manual_label">
                        <div class="manual_video popup-btn">
                            <i class="fas fa-play"></i>
                        </div>
                        <p>Launch the virtual hosting wizard (Navigator — trading account context menu — "Register a Virtual
                            Server").</p>
                    </div>
                </div>
            </div>

            <div class="manual_card">
                <div class="card">
                    <div class="card-body">
                        <img src="{{ asset('images/icons/manual2.svg') }}" alt="label" class="manual_label">
                        <div class="manual_video popup-btn">
                            <i class="fas fa-play"></i>
                        </div>
                        <p>It automatically finds the access point closest to your broker and offers you several service
                            plans. You have to set the data migration mode: indicators and robots, signals or all.</p>
                    </div>
                </div>
            </div>

            <div class="manual_card">
                <div class="card">
                    <div class="card-body">
                        <img src="{{ asset('images/icons/manual3.svg') }}" alt="label" class="manual_label">
                        <div class="manual_video popup-btn">
                            <i class="fas fa-play"></i>
                        </div>
                        <p>Your account is already launched on a virtual machine with all the settings, charts, robots and
                            subscriptions. You can see the remote server name in the Navigator window:</p>
                    </div>
                </div>
            </div>

            <div class="manual_card">
                <div class="card">
                    <div class="card-body">
                        <img src="{{ asset('images/icons/manual4.svg') }}" alt="label" class="manual_label">
                        <div class="manual_video popup-btn">
                            <i class="fas fa-play"></i>
                        </div>
                        <p>In case of any changes in the trading environment, synchronize the
                            desktop
                            platform with the
                            virtual one. Suppose that you have subscribed to a new signal. <br> The appropriate entry
                            appears in
                            the Navigator window. Execute the "Synchronize signal only" command in the context menu to start
                            copying trades on a virtual machine.</p>
                    </div>
                </div>
            </div>

            <div class="manual_card">
                <div class="card">
                    <div class="card-body">
                        <img src="{{ asset('images/icons/manual5.svg') }}" alt="label" class="manual_label">
                        <div class="manual_video popup-btn">
                            <i class="fas fa-play"></i>
                        </div>
                        <p>Do you want to launch a robot? Add it to the desired chart in your desktop platform and execute
                            the "Synchronize experts, indicators and signal" command. <br> Thus, you can launch any robots
                            and
                            copy any signals on hosting.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
