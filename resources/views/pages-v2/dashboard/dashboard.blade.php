<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    {{-- Favicon --}}
    <link rel="icon" type="image/png" href="{{ URL::to('images/favicon.ico') }}">
    <title>Agerabot | User Dashboard</title>

    <!-- ===== BOX ICONS ===== -->
    <link href='https://cdn.jsdelivr.net/npm/boxicons@2.0.5/css/boxicons.min.css' rel='stylesheet'>

    {{-- Bootstrap --}}
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css"
        integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">

    <!-- ===== CSS ===== -->
    <link rel="stylesheet" href="{{ asset('dashboard/css/styles.css') }}">

    {{-- Font Awesome --}}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css">

    {{-- Swiper JS --}}
    <link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.min.css" />




    <meta name="csrf-token" content="{{ csrf_token() }}">

</head>

<body id="body-pd" class="body-pd">
    <header class="header body-pd" id="header">
        <div class="header__toggle">
            <i class='bx bx-menu' id="header-toggle"></i>
        </div>

        <div class="header_right">
            <i class="far fa-bell"></i>
            <div class="header__img">
                <div class="user_img">
                    <img src="{{ asset('dashboard/img/perfil.jpg') }}" alt="">
                </div>
                <div class="doropdown">
                    <ul>
                        <li class="doropdown-link"><a href="{{ route('dashboard') }}"><i
                                    class="mr-3 far fa-address-card"></i> Account</a></li>
                        <li class="doropdown-link"><a href="{{ route('settingPage') }}"><i
                                    class="mr-3 fas fa-cog"></i> Setting</a></li>
                        <li class="doropdown-link"><a href="{{ route('getLogout') }}"><i
                                    class="mr-3 fas fa-sign-out-alt"></i> Logout</a></li>
                    </ul>
                </div>
            </div>
        </div>



    </header>

    <div class="l-navbar show" id="nav-bar">
        <nav class="side-nav">
            <div>
                <a href="{{ route('homePage') }}" class="nav__logo">
                    {{-- <i class='bx bx-layer nav__logo-icon'></i> --}}
                    <img src="{{ asset('images/agerabot-icon.png') }}" alt="icon" width="25">
                    <span class="nav__logo-name">Agerabot</span>
                </a>

                <div class="nav__list">
                    <a id="home" href="{{ route('dashboard') }}"
                        class="nav__link {{ set_active_navbar('dashboard') }}">
                        {{-- <i class='bx bx-grid-alt nav__icon' ></i> --}}
                        <img src="{{ asset('images/icons/home.svg') }}" alt="icon" width="25">
                        <span class="nav__name">Home</span>
                    </a>

                    <a id="myrobot" href="{{ route('myrobot') }}"
                        class="nav__link {{ set_active_navbar('myrobot') }}">
                        {{-- <i class='bx bx-user nav__icon' ></i> --}}
                        <img src="{{ asset('images/icons/myrobot.svg') }}" alt="icon" width="25">
                        <span class="nav__name">Robot List</span>
                    </a>

                    <a id="upgrade" href="{{ route('upgradeRobot') }}"
                        class="nav__link {{ set_active_navbar('upgradeRobot') }}">
                        {{-- <i class='bx bx-message-square-detail nav__icon' ></i> --}}
                        <img src="{{ asset('images/icons/upgrade-robot.svg') }}" alt="icon" width="25">
                        <span class="nav__name">Upgrade Robot</span>
                    </a>

                    <a id="vps" href="{{ route('vpsPage') }}"
                        class="nav__link {{ set_active_navbar('vpsPage') }}">
                        <img src="{{ asset('images/icons/vps.svg') }}" alt="icon" width="25">
                        <span class="nav__name">VPS</span>
                    </a>

                    <a id="product" href="{{ route('productPage') }}"
                        class="nav__link {{ set_active_navbar('productPage') }}">
                        <img src="{{ asset('images/icons/product.svg') }}" alt="icon" width="25">
                        <span class="nav__name">Product</span>
                    </a>

                    <a id="transaction" href="{{ route('transactionHistory') }}"
                        class="nav__link {{ set_active_navbar('transactionHistory') }}">
                        <img src="{{ asset('images/icons/history.svg') }}" alt="icon" width="25">
                        <span class="nav__name">Order</span>
                    </a>

                    <a id="help" href="{{ route('helpPage') }}"
                        class="nav__link {{ set_active_navbar('helpPage') }}">
                        <img src="{{ asset('images/icons/help.svg') }}" alt="icon" width="25">
                        <span class="nav__name">Help</span>
                    </a>
                </div>
            </div>

            {{-- <a href="{{ route('getLogout') }}" class="nav__link">
                <i class='bx bx-log-out nav__icon'></i>
                <span class="nav__name">Log Out</span>
            </a> --}}
        </nav>

        <div class="video-popup">
            <div class="popup-bg"></div>
            <div class="popup-content">
                {{-- <iframe src="{{ asset('video/dummy.mp4') }}" class="video"></iframe> --}}

                <video class="video" controls>
                    <source src="{{ asset('video/dummy.mp4') }}" type="video/mp4">
                </video>
            </div>
        </div>

    </div>

    @yield('dash-content')

    <!--===== MAIN JS =====-->
    <script src="{{ asset('dashboard/js/main.js') }}"></script>


    <script src="https://code.jquery.com/jquery-3.6.0.js"
        integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
    <script defer src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"
        defer></script>
    <script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>

    @yield('jsDashboard')
    <script>
        $(document).ready(function() {
            $(".manage-btn").click(function() {
                // $('#robot-upgrade-card').attr('style', 'min-height: 300px');
                $("#robot-upgrade-detail").toggleClass("dnone");
            });
        });


        $('ul.tabs li').click(function() {
            var tab_id = $(this).attr('href');

            $('ul.tabs li').removeClass('current');
            $('.tab-content').removeClass('current');

            $(this).addClass('current');
            $("#" + tab_id).addClass('current');
        })


        var $cell = $('.cardss');
        //open and close card when clicked on card
        $cell.find('.js-expander').click(function() {

            var $thisCell = $(this).closest('.cardss');

            if ($thisCell.hasClass('is-collapsed')) {
                $cell.not($thisCell).removeClass('is-expanded').addClass('is-collapsed').addClass(
                    'is-inactive');
                $thisCell.removeClass('is-collapsed').addClass('is-expanded');

                if ($cell.not($thisCell).hasClass('is-inactive')) {
                    //do nothing
                } else {
                    $cell.not($thisCell).addClass('is-inactive');
                }

            } else {
                $thisCell.removeClass('is-expanded').addClass('is-collapsed');
                $cell.not($thisCell).removeClass('is-inactive');
            }
        });

        //close card when click on cross
        $cell.find('.js-collapser').click(function() {

            var $thisCell = $(this).closest('.cardss');

            $thisCell.removeClass('is-expanded').addClass('is-collapsed');
            $cell.not($thisCell).removeClass('is-inactive');

        });

    </script>

    <script>
        $(".rotate").click(function() {
            console.log('rotate')
            $(this).toggleClass("down");
        })

    </script>

    <script>
        var swiper = new Swiper('.swiper-manual', {
            pagination: {
                el: '.swiper-pagination',
                clickable: true,
                renderBullet: function(index, className) {
                    return '<span class="' + className + '">' + (index + 1) + '</span>';
                },
            },
        });

        $(document).ready(function() {

            $('.popup-btn').on('click', function() {
                $('.video-popup').fadeIn('slow');
                $('.video')[0].pause();
                return false;
            });

            $('.popup-bg').on('click', function() {
                $('.video-popup').fadeOut('slow');
                $('.video')[0].pause();
                return false;
            });

            $('.close-btn').on('click', function() {
                $('.video-popup').fadeOut('slow');
                $('.video')[0].pause();
                return false;
            });

        });

    </script>


</body>

</html>
