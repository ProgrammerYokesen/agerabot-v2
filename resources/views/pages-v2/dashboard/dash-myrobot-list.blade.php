@extends('pages-v2.dashboard.dash-myrobot')

@section('dashboardRobot')
    <div class="my-robot-header">
        <div class="row">
            <div class="col-md-6">
                {{-- Menu Bawah --}}
                <div class="customize_solution">
                    <ul class="tabs nav nav-justified">
                        <li class=" nav-pill current mt-2">
                            <a href="{{ route('myrobot') }}"><span class="ease-effect">Robot List</span></a>
                        </li>
                        <li class=" nav-pill mt-2">
                            <a href="{{ route('dasboardWishlist') }}"><span class="ease-effect">Wishlist</span></a>
                        </li>
                    </ul>
                </div>
                {{-- End Menu Bawah --}}
            </div>
            <div class="col-md-6">
                <div class="search">
                    <i class="fas fa-search search-icon"></i>
                    <input type="search" id="dashboard-search-robot" class="search-box" placeholder="Search...">
                </div>
            </div>
        </div>
    </div>
    {{-- <div class="tab-content" id="tab-1"> --}}
    <div class="myrobot-list">
        @include('pages-v2.dashboard.dash-myrobot-list-card')
    </div>
    {{-- </div> --}}

    <!-- Modal Terms & Condition -->
    <div class="modal fade" id="termsCondition" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h2 class="text-center">Terms and Condition</h2>
                <div class="text-left">
                    <h4>This is the basic guidelines for trading robot installation steps which you can refer to anytime for any robots. However, if you needs more information or need help with the installation, create ticket or contact us.</h4>
                    <ul class="my-3">
                        <li>Agerabot.com does not promise fixed income, profit or fund management. The robot is meant only a tool (tools) for trading easily.</li>
                        <li>Each robot can only generate license key 3 times. More than that, please contact Agerabot Customer Service for further information.</li>
                        <li>Each robot can only be used for one broker.</li>
                    </ul>
                    <div class="form-group form-check">
                        <input type="checkbox" class="form-check-input" name="dash-agree" id="checkAgree">
                        <label class="form-check-label" for="exampleCheck1">I have read and agree to the terms and conditions</label>
                    </div>
                </div>
                <div id="agreeContainer">

                </div>
            </div>
        </div>
    </div>
    {{-- @if ()
        
    @endif --}}
    <!-- Modal how to install robot? -->
    <div class="modal fade" id="howToDownload" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h2 class="text-center">Do you know how to install the robot?</h2>
                <div class="modal-btn-container">
                    <a href="{{route('manualPage')}}">
                        <button class="modal-download-btn">NO, I NEED A GUIDE</button>
                    </a>
                    <div id="dash-robot-modal-download">

                    </div>
                    {{-- <button class="modal-generate-btn" data-dismiss="modal" data-toggle="modal" data-target="#modalInstall">YES, I’LL DO IT MYSELF</button> --}}
                </div>
            </div>
        </div>
    </div>

    <!-- Modal Install Robot -->
    {{-- <div class="modal fade" id="modalInstall" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h2 class="text-center">Installation</h2>
                <h4>Your installation quota will be used to <strong>Aquarius</strong> robot. Please confirm to install the robot
                </h4>
                @if ($userQuota==0)
                    <button class="modal-confirm-btn">Confirm</button>
                @elseif ($userQuota>0)
                    <div id="dash-robot-modal-download">
                    <button class="modal-confirm-btn">Confirm</button>
                    </div>
                @endif
            </div>
        </div>
    </div> --}}
    
@endsection

@section('jsDashboard')
    <script>
        $(document).ready(function() {
            function fetch_data(query) {
                $.ajax({
                    url: "/dashboard-myrobot/search?query=" + query,
                    success: function(finalRobot) {
                        // console.log(finalRobot)
                        $('#dashboard-robot-list').html('');
                        $('#dashboard-robot-list').html(finalRobot);
                    }
                })
            }
            $(document).on('keyup', '#dashboard-search-robot', function() {
                var query = $('#dashboard-search-robot').val();
                // console.log(query)
                fetch_data(query)
            });

            $(document).on('click', '.dash-robot-download', function() {
                var id = $(this).attr('data-id');
                var packageId = $(this).attr('data-packageId');
                var userId = $(this).attr('data-userId');
                var time = $(this).attr('data-time');
                var generateCode = 'yes';
                // console.log(id, packageId, userId, time)
                var code = `SW-${userId}-${packageId}-${time}`
                // console.log(code)
                $('#agreeContainer').empty()
                $('#agreeContainer').append(
                    `
                        <button id="agreeButton" class="modal-confirm-btn" data-dismiss="modal" data-toggle="modal" data-target="#howToDownload" disabled >CONTINUE</button>
                    `
                )
                $('#dash-robot-modal-download').empty()
                $('#dash-robot-modal-download').append(
                    `<a href="/download/robot/` + id + `/` + code + `/` + generateCode +
                    `" id="download-button"><button class="modal-generate-btn" >YES, I’LL DO IT MYSELF</button></a>`
                )
            })

            $(document).on('click', '#checkAgree', function() {
                if (this.checked) {
                    if ($('#agreeButton').prop("disabled")) {
                        $('#agreeButton').prop("disabled", false);
                    }
                }
            })

            // $(document).on('click', '#download-button', function () {


            // })




            // $('#modalStepInstall').on('show.bs.modal', function(e) {
            // // var button = document.getElementById('dash-robot-download').getAttribute('data-id')
            // var button = $('#dash-robot-download').attr('data-id')
            // console.log(button)
            // $('#dash-robot-modal-download').append(
            //     `<a href="/v2/download/robot/`+button+`"><button class="modal-confirm-btn">DOWNLOAD</button></a>`
            // )   
            // console.log(button)
            // })
        });

    </script>

@endsection
