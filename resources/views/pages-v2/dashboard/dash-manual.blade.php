@extends('pages-v2.dashboard.dashboard')

@section('dash-content')
    <div class="manual_container">
        <div class="manual_back">
            <a href="{{ route('productPage') }}">
                <img src="{{ asset('images/icons/back.svg') }}" alt="back">
            </a>
        </div>
        <div class="manual_title">
            <img src="{{ asset('images/manual.png') }}" alt="manual">
            <h2>Manual</h2>
        </div>

        <!-- Swiper -->
        <div class="swiper-container swiper-manual">
            <div class="swiper-wrapper">

                {{-- SLIDE 1 --}}
                <div class="swiper-slide">
                    <div class="manual_card">
                        <div class="card">
                            <div class="card-body">
                                <img src="{{ asset('images/icons/manual1.svg') }}" alt="label" class="manual_label">
                                <div class="manual_video popup-btn">
                                    <i class="fas fa-play"></i>
                                </div>
                                <p>Click menu file on the top left of Metatrader 4
                                    Then choose the open data folder that will open Windows Explorer</p>
                            </div>
                        </div>
                    </div>

                    <div class="manual_card">
                        <div class="card">
                            <div class="card-body">
                                <img src="{{ asset('images/icons/manual2.svg') }}" alt="label" class="manual_label">
                                <div class="manual_video popup-btn">
                                    <i class="fas fa-play"></i>
                                </div>
                                <p>Click MQL4 folders and double click to enter the folder</p>
                            </div>
                        </div>
                    </div>

                    <div class="manual_card">
                        <div class="card">
                            <div class="card-body">
                                <img src="{{ asset('images/icons/manual3.svg') }}" alt="label" class="manual_label">
                                <div class="manual_video popup-btn">
                                    <i class="fas fa-play"></i>
                                </div>
                                <p>Click folder experts and double click to enter the folder</p>
                            </div>
                        </div>
                    </div>

                    <div class="manual_card">
                        <div class="card">
                            <div class="card-body">
                                <img src="{{ asset('images/icons/manual4.svg') }}" alt="label" class="manual_label">
                                <div class="manual_video popup-btn">
                                    <i class="fas fa-play"></i>
                                </div>
                                <p>Copy and paste file robot that you want to use
                                    First download EA from Agerabot.com, then copy and paste</p>
                            </div>
                        </div>
                    </div>
                </div>

                {{-- SLIDE 2 --}}
                <div class="swiper-slide">
                    <div class="manual_card">
                        <div class="card">
                            <div class="card-body">
                                <img src="{{ asset('images/icons/manual5.svg') }}" alt="label" class="manual_label">
                                <div class="manual_video popup-btn">
                                    <i class="fas fa-play"></i>
                                </div>
                                <p>Check robot that has been downloaded
                                    Find a list of robots that you have downloaded (.ex4 format)</p>
                            </div>
                        </div>
                    </div>

                    <div class="manual_card">
                        <div class="card">
                            <div class="card-body">
                                <img src="{{ asset('images/icons/manual6.svg') }}" alt="label" class="manual_label">
                                <div class="manual_video popup-btn">
                                    <i class="fas fa-play"></i>
                                </div>
                                <p>Turn on the navigator panel on the top with an icon like this
                                    Then right click expert advisors in the navigator panel and click refresh</p>
                            </div>
                        </div>
                    </div>

                    <div class="manual_card">
                        <div class="card">
                            <div class="card-body">
                                <img src="{{ asset('images/icons/manual7.svg') }}" alt="label" class="manual_label">
                                <div class="manual_video popup-btn">
                                    <i class="fas fa-play"></i>
                                </div>
                                <p>Check your Trading Robot List
                                    Make sure you have a list of trading robots shown in the Expert Advisors</p>
                            </div>
                        </div>
                    </div>

                    <div class="manual_card">
                        <div class="card">
                            <div class="card-body">
                                <img src="{{ asset('images/icons/manual8.svg') }}" alt="label" class="manual_label">
                                <div class="manual_video popup-btn">
                                    <i class="fas fa-play"></i>
                                </div>
                                <p>Click menu tools on the top menu
                                    Select options or you can use the shortcut CTR + O</p>
                            </div>
                        </div>
                    </div>
                </div>


                {{-- SLIDE 3 --}}
                <div class="swiper-slide">
                    <div class="manual_card">
                        <div class="card">
                            <div class="card-body" style="align-items: flex-start">
                                <img src="{{ asset('images/icons/manual9.svg') }}" alt="label" class="manual_label">
                                <div class="manual_video popup-btn">
                                    <i class="fas fa-play"></i>
                                </div>
                                <div>
                                    <p>Click advisors expert tab in Windows Options</p>
                                    <p>Tick below options:</p>
                                    <ul>
                                        <li>Allow automated trading</li>
                                        <li>Disabled automated trading when the account has been changed</li>
                                        <li>Disabled automated trading when the profile has been changed</li>
                                        <li>Allow DLL Imports</li>
                                        <li>Allow WebRequest for listed URL</li>
                                    </ul>
                                    <p>Then type or copy paste the URL https://agerabot.com/api/v2/access/robot/license</p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="manual_card">
                        <div class="card">
                            <div class="card-body">
                                <img src="{{ asset('images/icons/manual10.svg') }}" alt="label" class="manual_label">
                                <div class="manual_video popup-btn">
                                    <i class="fas fa-play"></i>
                                </div>
                                <p>Check auto trading and make sure the Auto Trading Button is GREEN, which means the Expert
                                    Advisor is ready to use</p>
                            </div>
                        </div>
                    </div>
                </div>


                {{-- SLIDE 4 --}}
                <div class="swiper-slide">
                    <div class="manual_card">
                        <div class="card">
                            <div class="card-body">
                                <img src="{{ asset('images/icons/manual11.svg') }}" alt="label" class="manual_label">
                                <div class="manual_video popup-btn">
                                    <i class="fas fa-play"></i>
                                </div>
                                <p>Check robot that has been downloaded
                                    Find a list of robots that you have downloaded (.ex4 format)</p>
                            </div>
                        </div>
                    </div>

                    <div class="manual_card">
                        <div class="card">
                            <div class="card-body">
                                <img src="{{ asset('images/icons/manual12.svg') }}" alt="label" class="manual_label">
                                <div class="manual_video popup-btn">
                                    <i class="fas fa-play"></i>
                                </div>
                                <p>Turn on the navigator panel on the top with an icon like this
                                    Then right click expert advisors in the navigator panel and click refresh</p>
                            </div>
                        </div>
                    </div>

                    <div class="manual_card">
                        <div class="card">
                            <div class="card-body" style="align-items: flex-start">
                                <img src="{{ asset('images/icons/manual13.svg') }}" alt="label" class="manual_label">
                                <div class="manual_video popup-btn">
                                    <i class="fas fa-play"></i>
                                </div>
                                <div>
                                    <p>Click the inputs tab -> Content license key can be found on the Agerabot.com website
                                        when you order a trading robot.</p>
                                    <p>Fill in the trading rules data:</p>
                                    <ul>
                                        <li>Contract Size: 100000 or 10000 (filled with numbers without punctuation marks)
                                        </li>
                                        <li>Lot Step: multiples of Lot (filled in 1, or 0.1, or 0.01, with signs decimal
                                            point) </li>
                                        <li>Lot Maximum: Maximum lots per trade</li>
                                        <li>Minimum Lot: Minimum lot per trade</li>
                                    </ul>
                                    <p>Attention: filling errors will result in the Robot not taking a position.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                {{-- SLIDE 5 --}}
                <div class="swiper-slide">
                    <div class="manual_card">
                        <div class="card">
                            <div class="card-body">
                                <img src="{{ asset('images/icons/manual14.svg') }}" alt="label" class="manual_label">
                                <div class="manual_video popup-btn">
                                    <i class="fas fa-play"></i>
                                </div>
                                <p>Notification appears -> You have successfully installed the expert advisor
                                    Make sure the smile icon -> It indicates that the Robot is ready to work</p>
                            </div>
                        </div>
                    </div>

                    <div class="manual_card">
                        <div class="card">
                            <div class="card-body">
                                <img src="{{ asset('images/icons/manual15.svg') }}" alt="label" class="manual_label">
                                <div class="manual_video popup-btn">
                                    <i class="fas fa-play"></i>
                                </div>
                                <p>Take step 11-14
                                    <br> For all PAIR trading at your disposal
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <!-- Add Pagination -->

            <div class="swiper-pagination"></div>

        </div>

    </div>


@endsection
