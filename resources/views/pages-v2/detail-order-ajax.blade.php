<h4>Detail Orders</h4>

<div class="order-name">
    <h4>Premium Account</h4>
    <button>X</button>
</div>
@foreach (Cart::content() as $cart)
    <div class="order-type" id="orderType">

        <p id="orderCartId">{{ $cart->name }}</p>
        <p id="orderCartPrice">${{ $cart->price }}</p>

    </div>

    <div class="coupon-code">
        <p>Enter Coupon Code</p>
        <input type="text" class="coupon-code-input" placeholder="Coupon Code">
    </div>

    <div class="total mt-3">
        <h4>Total</h4>
        <h4>${{ $cart->price }}</h4>
    </div>

    <div class="mt-5" style="margin: 0 auto">
        @if (CRUDBooster::myID())
            <button id="checkoutButton" class="checkout-btn" onclick="checkout({{ $cart->id }})">Checkout</button>
        @else
            <a href="{{ route('registerPage') }}">
                <button class="checkout-btn">Checkout</button>
            </a>
        @endif
    </div>
@endforeach
