{{-- Start Blog List weekly --}}
<div class="row" id="blog-thumbnail">
    @foreach ($finalBlogsData as $item)
        <div class="col-md-4 blog_list">
            <div>
                <a href="{{ route('blogDetailPage', ['slug' => $item->blogSlug]) }}" data-slug="{{ $item->type }}"
                    class="src-blog">
                    <img src="{{ $item->blogImage1 }}" alt="" style="width: 100%"> {{-- Blog Image --}}
                </a>
            </div>
            <div class="blog_title">
                <a href="{{ route('blogDetailPage', ['slug' => $item->blogSlug]) }}">
                    <h5>{{ $item->blogTitle }}</h5> {{-- Blog Title --}}
                </a>
            </div>
            <div class="blog_author">
                <img src="{{ $item->photo }}" alt="">
                <div>
                    <p>By {{ $item->name }}</p> {{-- Blog Author Name --}}
                    <p>{{ $item->readingTime }} mins read</p> {{-- Blog Length --}}
                </div>
            </div>
        </div>
    @endforeach

</div>
{{ $finalBlogsData->links() }}
{{-- End Blog List --}}
