<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Agerabot</title>
    {{-- Favicon --}}
    <link rel="icon" type="image/png" href="{{ URL::to('images/favicon.ico') }}">

    <script src="{{ asset('js/app.js') }}" defer></script>

    {{-- CSS --}}
    <link rel="stylesheet" href="{{ URL::to('css/style.css') }}">

    {{-- Bootstrap --}}
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css"
        integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">

    {{-- Font Awesome --}}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css">

    <meta name="csrf-token" content="{{ csrf_token() }}">

</head>

<body>

    <div class="register-hero">
        <div class="register-logo">
            <a href="{{ route('homePage') }}">
                <img src="{{ asset('./images/logo.svg') }}" alt="" class="register">
            </a>
        </div>
        <div class="register-overlay">
            {{-- <button class="change-view">
          <i class="fas fa-chevron-left"></i>
        </button> --}}
            <div class="register-hero-container">
                <h1 class="mb-3">Register</h1>
                <h5>Already have an account? <a href="{{ route('loginPage') }}">Log in</a></h5>
                <form action="{{ route('processRegistering') }}" method="post">
                    @csrf

                    <div class="form-group">
                        <label for="fullName">Full Name</label>
                        <input type="text" required name="fullName" class="form-control" id="fullName"
                            autocomplete="off" value="{{ old('fullName') }}">
                        @if ($errors->get('fullName'))
                            <p class="text-danger">{{ $errors->get('fullName')[0] }}</p>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Email</label>
                        <input type="email" required name="email" class="form-control" id="exampleInputEmail1"
                            aria-describedby="emailHelp" value="{{ old('email') }}">
                        @if ($errors->get('email'))
                            <p class="text-danger">{{ $errors->get('email')[0] }}</p>
                        @endif
                    </div>
                    {{-- <div class="form-group">
                  <label for="exampleInputEmail1">Phone Number</label>
                  <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
                </div> --}}
                    <div class="form-group">
                        <label for="txtPassword">Password</label>
                        <div class="input-group">
                            <input type="password" required id="txtPassword" class="form-control" name="password"
                                style="border-radius: 5px" value="{{ old('password') }}" />
                            <button type="button" id="btnToggle" class="toggle"><i id="eyeIcon"
                                    class="fa fa-eye"></i></button>
                        </div>
                        @if ($errors->get('password'))
                            <p class="text-danger">{{ $errors->get('password')[0] }}</p>
                        @endif
                    </div>

                    <div class="form-group">
                        <label for="txtRePassword">Confirm Password</label>
                        <div class="input-group">
                            <input type="password" required id="txtRePassword" class="form-control"
                                name="password_confirmation" style="border-radius: 5px"
                                value="{{ old('password_confirmation') }}" />
                            <button type="button" id="btnReToggle" class="toggle"><i id="reEyeIcon"
                                    class="fa fa-eye"></i></button>
                            @if ($errors->get('password_confirmation'))
                                <p class="text-danger">{{ $errors->get('password_confirmation')[0] }}</p>
                            @endif
                        </div>
                    </div>

                    <div class="form-check">
                        <input type="checkbox" required name="disclaimer" class="form-check-input" id="exampleCheck1">
                        <label class="form-check-label" for="exampleCheck1">I agree to the <a
                                href="{{ route('disclaimerPage') }}" target="_blank">End User License Agreement</a>
                            and <a href="{{ route('termsCondition') }}" target="_blank">Terms of Service</a></label>
                    </div>
                    <button type="submit" class="register-btn my-5">REGISTER NOW</button>
                </form>

                {{-- <button class="change-view">TOGGLE</button> --}}
            </div>
        </div>


        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
            integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous">
        </script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous">
        </script>


        <script>
            $(".change-view").click(function() {
                $(".register-hero-container").toggleClass("hide-content");
                $(".login-hero-container").toggleClass("hide-content");
            });

            // Toggle Password
            let passwordInput = document.getElementById('txtPassword'),
                rePasswordInput = document.getElementById('txtRePassword'),
                toggle = document.getElementById('btnToggle'),
                reToggle = document.getElementById('btnReToggle'),
                icon = document.getElementById('eyeIcon');
            reIcon = document.getElementById('reEyeIcon');

            function togglePassword() {
                if (passwordInput.type === 'password') {
                    passwordInput.type = 'text';
                    icon.classList.add("fa-eye-slash");
                    //toggle.innerHTML = 'hide';
                } else {
                    passwordInput.type = 'password';
                    icon.classList.remove("fa-eye-slash");
                    //toggle.innerHTML = 'show';
                }
            }

            function toggleRePassword() {
                if (rePasswordInput.type === 'password') {
                    rePasswordInput.type = 'text';
                    reIcon.classList.add("fa-eye-slash");
                    //toggle.innerHTML = 'hide';
                } else {
                    rePasswordInput.type = 'password';
                    reIcon.classList.remove("fa-eye-slash");
                    //toggle.innerHTML = 'show';
                }
            }

            toggle.addEventListener('click', togglePassword, false);
            reToggle.addEventListener('click', toggleRePassword, false);

        </script>

</body>

</html>
