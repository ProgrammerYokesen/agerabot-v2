@extends('app')

@section('content')
    <div class="checkout-container container">
        <div class="row">
            {{-- LEFT SECTION --}}
            <div class="col-md-7 order-choice">
                <h4>Pick Your Subscription</h4>

                <div class="row" id="cart-ajax">
                    @for ($i = 0; $i < $datasLength; $i++)
                        <div class="col-md-4 col-lg-4 col-sm-4 col-4 sub-choice">

                            <label class="radio-label">

                                <input id="subscriptionChoice" type="radio" name="product" class="card-input-element"
                                    onclick="editCart({{ $datas[$i]->id }})" />


                                @foreach (Cart::content() as $row)
                                    @if ($datas[$i]->id == $row->id)
                                        <div class="card-input  active-card">
                                        @else
                                            <div class="card-input">
                                    @endif
                                @endforeach
                                <p>{{ $datas[$i]->nmPackage }}</p>
                                @if ($datas[$i]->interval != 1)
                                    <p><s>${{ $datas[0]->price * $datas[$i]->interval }}</s></p>
                                @endif
                                <h4>
                                    ${{ $datas[$i]->price }}
                                </h4>
                        </div>

                        </label>

                </div>
                @endfor

            </div>

        </div>
        {{-- END LEFT SECTION --}}

        {{-- RIGHT SECTION --}}
        <div class="col-md-5 order-detail-card">
            <div class="card">
                <div class="card-body" id="detailOrder">

                    <h4>Detail Orders</h4>

                    <div class="order-name">
                        <h4>Premium Account</h4>
                        <button>X</button>
                    </div>
                    @foreach (Cart::content() as $cart)
                        <div class="order-type" id="orderType">

                            <p id="orderCartId">{{ $cart->name }}</p>
                            <p id="orderCartPrice">${{ $cart->price }}</p>

                        </div>

                        <div class="coupon-code">
                            <p>Enter Coupon Code</p>
                            <input type="text" class="coupon-code-input" placeholder="Coupon Code">
                        </div>

                        <!-- <div class="tax">
                                                                                                                                                                                <p>10% Tax</p>
                                                                                                                                                                                <p>${{ $cart->price * 0.1 }}</p>
                                                                                                                                                                            </div> -->
                        <div class="total mt-5">
                            <h4>Total</h4>
                            <h4>${{ $cart->price }}</h4>
                        </div>

                        <div class="mt-5" style="margin: 0 auto">
                            @if (CRUDBooster::myID())
                                <button id="checkoutButton" class="checkout-btn"
                                    onclick="checkout({{ $cart->id }})">Checkout</button>
                            @else
                                <button class="checkout-btn" onclick="modal()">Checkout</button>
                            @endif
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
        {{-- END RIGHT SECTION --}}
    </div>

    <!-- Modal checkout -->
    <div class="modal" tabindex="-1" role="dialog" id="checkoutModal">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content p-0">
                <iframe width="400" height="700" id="iframe-invoice" class="iframe-invoice" title="Invoice" src=""></iframe>
            </div>
        </div>
    </div>
    <!-- Endmodal checkout -->
    <!-- Modal Redownload / Generate License -->
    <div class="modal fade" id="modalRedownload" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h2 class="text-center">Update Subscription</h2>
                <h4>Success updating your subscription</h4>
                <div class="modal-btn-container">
                    <!-- <button class="modal-download-btn" data-dismiss="modal">No</button>
                    <a href="{{ route('stopSubscription') }}">
                        <button class="modal-generate-btn">Yes</button>
                    </a> -->
                </div>
            </div>
        </div>
    </div>
    <!-- end modal -->
    </div>
@endsection

@section('scriptJs')
    <script type="text/javascript">
        function editCart(id) {
            // console.log(id)
            $.ajax({
                    type: "GET",
                    url: "checkout-ajax/" + id,
                    beforeSend: function() {
                        // $('.ajax-load').show();
                    }
                })
                .done(function(data) {
                    if (data.html == " ") {
                        $('.ajax-load').html("No more records found");
                        return;
                    }
                    $('.ajax-load').hide();
                    document.getElementById("cart-ajax").innerHTML = data.html;
                    document.getElementById("detailOrder").innerHTML = data.orderDetail;
                })
                // .fail(function(jqXHR, ajaxOptions, thrownError)
                .fail(function(e) {
                    console.log(e);
                    alert('server not responding...');
                });
        }
        // $('#checkoutModal').modal('show')

        function checkout(id) {
            console.log('checkout');
            $.ajax({
                    type: "GET",
                    url: "xendit/recurring/create/" + id,
                    beforeSend: function() {
                        // $('.ajax-load').show();
                    }
                })
                .done(function(data) {
                    type = data.type;
                    if (type == 'update') {
                        $('#modalRedownload').modal('show');
                        // alert("Update subscription success");
                    } else {
                        data = data.data;
                        console.log(data);
                        loadIframe("iframe-invoice", data.last_created_invoice_url);
                        $('#checkoutModal').modal('show');
                    }

                })
                // .fail(function(jqXHR, ajaxOptions, thrownError)
                .fail(function(e) {
                    console.log(e);
                    alert('server not responding...');
                });
        }

        function loadIframe(iframeName, url) {
            var $iframe = $('#' + iframeName);
            if ($iframe.length) {
                $iframe.attr('src', url);
                return false;
            }
            return true;
        }
        // $('#checkoutModal').modal('show')

        function checkout(id) {
            console.log('checkout');
            $.ajax({
                    type: "GET",
                    url: "xendit/recurring/create/" + id,
                    beforeSend: function() {
                        // $('.ajax-load').show();
                    }
                })
                .done(function(data) {
                    type = data.type;
                    if (type == 'update') {
                        $('#modalRedownload').modal('show');
                        // alert("Update subscription success");
                    } else {
                        data = data.data;
                        console.log(data);
                        loadIframe("iframe-invoice", data.last_created_invoice_url);
                        $('#checkoutModal').modal('show');
                    }

                })
                // .fail(function(jqXHR, ajaxOptions, thrownError)
                .fail(function(e) {
                    console.log(e);
                    alert('server not responding...');
                });
        }

        function loadIframe(iframeName, url) {
            var $iframe = $('#' + iframeName);
            if ($iframe.length) {
                $iframe.attr('src', url);
                return false;
            }
            return true;
        }

        function modal() {
            $.ajax({
                    url: "/login/checkout/session",
                    type: "GET",

                    beforeSend: function() {
                        // $('.ajax-load').show();
                    }
                })
                .done(function(data) {
                    $("#loginModal").modal('toggle');
                })
                .fail(function(e) {
                    console.log(e);

                });
        }

    </script>
@endsection
