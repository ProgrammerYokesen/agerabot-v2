@foreach($datas as $data)
<div class="row vps-card">
    <div class="card">
        <div class="card-body">
            <h5>{{$data->nmVPS}}</h5>
            <p>{{$data->detail}}</p>
        </div>
    </div>
</div>


<div class="row">
    <!-- Foreach vps here -->
    @foreach($data->vps as $vps)
    <div class="col-md-4 col-lg-4 col-sm-4 col-4">

        <label class="radio-label">
            <input type="radio" name="product" class="card-input-element" onclick="editCart({{ $vps->vpsId }})" />
            @foreach (Cart::content() as $row)
            @if ($vps->vpsId == $row->id)
            <div class="panel panel-default card-input active-card">
                @else
                <div class="panel panel-default card-input">
                    @endif
                    @endforeach

                    <p>{{$vps->interval}} Month</p>
                    <h4>
                        ${{$vps->price}}
                    </h4>
                </div>

        </label>

    </div>
    @endforeach
    <!-- End foreach VPs -->

</div>
@endforeach