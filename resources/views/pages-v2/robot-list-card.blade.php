<div id="robot-list-card">
    <div class="row robot-detail-slider">
        @foreach ($dataFinal as $robots => $data)

            <div class="col-md-4 cardss [ is-collapsed ]">
                <div class="robot-detail-slider-bg text-center card__inner"
                    style="margin-top: 100px; margin-bottom: 50px" id="{{ $robots->robotName }}">
                    <div class="robot-wishlist-container">
                        <div class="text-left wishlist-btn">
                            @if (CRUDBooster::myId() == null)
                                <input type="image" name="submit" value="{{ $dataFinal[$robots]['robot'] }}"
                                    src="{{ asset('./images/wishlist.svg') }}" alt="" class="wishlist-img">
                            @else
                                @if ($dataFinal[$robots]['subs'] != false)
                                    <input type="image" name="submit" value="{{ $dataFinal[$robots]['robot'] }}"
                                        src="{{ asset('./images/wishlist-red.svg') }}" alt=""
                                        id="{{ $dataFinal[$robots]['tag'] }}"
                                        onclick="wish({{ $dataFinal[$robots]['robot'] }})">
                                @else
                                    <input type="image" name="submit" value="{{ $dataFinal[$robots]['robot'] }}"
                                        src="{{ asset('./images/wishlist.svg') }}" alt=""
                                        id="{{ $dataFinal[$robots]['tag'] }}"
                                        onclick="wish({{ $dataFinal[$robots]['robot'] }})">
                                @endif
                            @endif
                        </div>
                        <div class="text-right">
                            {{-- <img src="{{ $data['robotImage'] }}" alt="" class="robot-detail-slider-img"> --}}
                            <img src="{{ asset('./images/robot.png') }}" alt="" class="robot-detail-slider-img">
                        </div>
                    </div>
                    <h3 class="mt-2 robotName">{{ $dataFinal[$robots]['robotName'] }}</h3>
                    <p>Monthly Performance:</p>
                    <h5>{{ $dataFinal[$robots]['month_percent'] }}%</h5>
                    <button class="robot-detail-slider-btn [ js-expander ]">SHOW DETAILS</button>
                </div>

                {{-- Data Expand --}}
                <div class="card__expander">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="detail-header row align-items-center">
                                <h5 class="col-md-2" style="padding-left:30px">Best Pair</h5>
                                <h4 class="col-md-10" style="padding-left:30px">{{ $data['pair'] }}</h4>
                            </div>
                            <div class="last-ten row align-items-center">
                                <h5 class="col-md-2">Last Ten</h5>
                                <div class="col-md-8">
                                    <div class="progress">
                                        <div class="progress-bar w-50" role="progressbar" aria-valuenow="50"
                                            aria-valuemin="0" aria-valuemax="100"
                                            style="width:{{ $data['last_percent'] }}%"></div>
                                    </div>
                                </div>
                                <h5 class="col-md-2">{{ $data['last_percent'] }}%</h5>
                            </div>
                            <div class="row pips">
                                <div class="col-md-2"></div>
                                <div class="col-md-8">
                                    <span>Pips: {{ $data['last_total'] }}</span>
                                </div>
                            </div>
                            <div class="last-ten row align-items-center">
                                <h5 class="col-md-2">Weekly</h5>
                                <div class="col-md-8">
                                    <div class="progress">
                                        <div class="progress-bar w-75" role="progressbar" aria-valuenow="75"
                                            aria-valuemin="0" aria-valuemax="100"
                                            style="width:{{ $data['week_percent'] }}%"></div>
                                    </div>
                                </div>
                                <h5 class="col-md-2">{{ $data['week_percent'] }}%</h5>
                            </div>
                            <div class="row pips">
                                <div class="col-md-2"></div>
                                <div class="col-md-8">
                                    <span>Pips: {{ $data['week_total'] }}</span>
                                </div>
                            </div>
                            <div class="last-ten row align-items-center">
                                <h5 class="col-md-2">Monthly</h5>
                                <div class="col-md-8">
                                    <div class="progress">
                                        <div class="progress-bar w-75" role="progressbar" aria-valuenow="75"
                                            aria-valuemin="0" aria-valuemax="100"
                                            style="width:{{ $data['month_percent'] }}%"></div>
                                    </div>
                                </div>
                                <h5 class="col-md-2">{{ $data['month_percent'] }}%</h5>
                            </div>
                            <div class="row pips">
                                <div class="col-md-2"></div>
                                <div class="col-md-8">
                                    <span>Pips: {{ $data['month_total'] }}</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 text-center robot-detail-right">
                            <div>
                                <h1>{{ $data['year_percent'] }}%</h1>
                            </div>
                            <div>
                                <p>Year to date Performance <br> {{ $data['pair'] }}</p>
                            </div>
                            <div style="width: 100%">
                                <button>Subscribe</button>
                                <button class="[ js-collapser ]">Show Less</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{-- </div> --}}
        @endforeach

    </div>
    {{ $dataFinal->links() }}

</div>


