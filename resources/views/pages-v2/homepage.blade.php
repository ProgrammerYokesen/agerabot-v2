@extends('app')

@section('content')
    <div>
        <div class="hero">
            <div class="hero-txt">
                <h1>All in one step</h1>
                <h5>There's a lot of opportunities for you to gain profit when robot-trading. Take this opportunity to learn
                    more about the trading robots on agerabot.com</h5>
                <a href="{{ route('subsPage') }}">
                    <button class="hero-btn">
                        SUBSCRIBE NOW
                    </button>
                </a>
            </div>
            <div class="ungu-ungu"></div>
        </div>
    </div>

    <div class="container text-center section-home">
        <h2>How Trading Robot Serves You?</h2>
        <h4>These are the privileges of using our trading robot</h4>
        <div class="row">
            <div class="col-md-4">
                <div class="card">
                    <div class="card-body">
                        <img src="{{ asset('./images/icon1.svg') }}" alt="icon" class="section-home-icon">
                        <h5>NO MARKET RESEARCH NEEDED</h5>
                        <p>Everything has been analised and well calculated by our trading robots.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card">
                    <div class="card-body">
                        <img src="{{ asset('./images/icon2.svg') }}" alt="icon" class="section-home-icon">
                        <h5>SAVE TRADING TIME</h5>
                        <p>Trading robots will automatically execute your transactions.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card">
                    <div class="card-body">
                        <img src="{{ asset('./images/icon3.svg') }}" alt="icon" class="section-home-icon">
                        <h5>TRADING'S PSYCHOLOGY AVOIDED</h5>
                        <p>Your transactions become more neat and keep the over-transaction away</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- Six Indication Start --}}
    <div class="container indication">
        <h2>6 things that indicate that you need our trading robot</h2>

        <div class="row indication-row">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-body">
                        <img src="{{ asset('images/icons/things-1.svg') }}" alt="img" class="indication-img">
                        <h5>DOESN’T HAVE MUCH TIME FOR TRADING</h5>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card">
                    <div class="card-body">
                        <img src="{{ asset('images/icons/things-2.svg') }}" alt="img" class="indication-img">
                        <h5>POOR MARKET ANALYSIS AND TRADE’S PLANNING</h5>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card">
                    <div class="card-body">
                        <img src="{{ asset('images/icons/things-3.svg') }}" alt="img" class="indication-img">
                        <h5>DIFFICULTIES TO DEFINE STOP LOSS RANGE</h5>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card">
                    <div class="card-body">
                        <img src="{{ asset('images/icons/things-4.svg') }}" alt="img" class="indication-img">
                        <h5>UNABLE TO DETERMINE TRADING TARGET AND TAKE PROFIT</h5>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card">
                    <div class="card-body">
                        <img src="{{ asset('images/icons/things-5.svg') }}" alt="img" class="indication-img">
                        <h5>EXCESSIVE OVER-TRADES</h5>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card">
                    <div class="card-body">
                        <img src="{{ asset('images/icons/things-6.svg') }}" alt="img" class="indication-img">
                        <h5>UNABLE TO SET LIMIT LOSS AND SUFFER BIG LOST</h5>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- Six Indication End --}}

    <div class="section-robot-bg">
        <div class="section-robot">
            <h2>Choose your Trading Robot!</h2>
            <div class="swiper-container swiper-robot">
                <div class="swiper-wrapper">

                    @foreach ($robot as $datum)
                        <div class="swiper-slide">
                            <div class="robot-carousel-bg">
                                <div class="robot-wishlist-container">
                                    <div class="text-left wishlist-btn">
                                        <input type="image" name="submit" src="{{ asset('./images/wishlist.svg') }}"
                                            alt="" class="wishlist-img">
                                    </div>
                                    <div class="text-right">
                                        <img src="{{ asset('./images/zeus.png') }}" alt="" class="robot-carousel-img">
                                    </div>
                                </div>
                                <h3 class="mt-2">{{ $datum['robotLongName'] }}</h3>
                                <p>Monthly Performance:</p>
                                <h5>{{ $datum['month_percent'] }}%</h5>
                                <button class="robot-carousel-btn"
                                    onclick="fetch_data('{{ $datum['robotLongName'] }}' )">SHOW DETAILS</button>
                            </div>
                        </div>
                    @endforeach

                </div>
                <!-- Add Arrows -->
                <div class="swiper-button-next"></div>
                <div class="swiper-button-prev"></div>
            </div>
            <a href="{{ route('robotListPage') }}"><button class="robot-slider-detail">SHOW DETAIL</button></a>
        </div>
    </div>

    <div class="container section-step">
        <h2>How do you start to operate our Trading Robot?</h2>
        <h4>You can start to operate your trading robot in just three steps.</h4>

        <div class="row">
            <div class="col-md-4">
                <div class="card">
                    <div class="card-body">
                        <img src="{{ asset('./images/step1.svg') }}" alt="icon" class="section-step-icon">
                        <div class="step-flex">
                            <img src="{{ asset('./images/step1.png') }}" alt="icon" class="section-step-img">
                            <p>Register</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card">
                    <div class="card-body">
                        <img src="{{ asset('./images/step2.svg') }}" alt="icon" class="section-step-icon">
                        <div class="step-flex">
                            <img src="{{ asset('./images/step2.png') }}" alt="icon" class="section-step-img">
                            <p>Rent</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card">
                    <div class="card-body">
                        <img src="{{ asset('./images/step3.svg') }}" alt="icon" class="section-step-icon">
                        <div class="step-flex">
                            <img src="{{ asset('./images/step3.png') }}" alt="icon" class="section-step-img">
                            <p>Install</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <a href="{{ route('subsPage') }}">
            <button class="subscribe-btn">SUBSCRIBE NOW</button>
        </a>
    </div>

    {{-- Testimonial --}}
    <section class="col-md-12 p-5">
        <div class="swiper-container swiper-testimonial">
            <div class="swiper-wrapper">
                @foreach ($testimoni as $item)
                    <div class="swiper-slide">
                        <div class="container">
                            <div class="row d-flex">
                                <div class="col-md-12 col-lg-7 align-items-center">
                                    <div class="testi-content_block">
                                        <img src="{{ asset('images/quotes.png') }}" class="img-fluid" alt="#">
                                        <h5>{{ $item->quotes }}</h5>
                                        <span>{{ $item->nama }}</span>
                                        <p>{{ $item->pekerjaan }}, {{ $item->negara }}</p>
                                    </div>
                                </div>
                            </div>
                            <div class="testi-img_wrap bg-testi-sewarobot">
                                <img src={{ $item->photo }} class="img-fluid" alt="img" width="250">
                            </div>
                        </div>
                    </div>
                @endforeach

            </div>
            <!-- Add Arrows -->
            <div class="swiper-button-next"></div>
            <div class="swiper-button-prev"></div>
        </div>
    </section>
    <!--//END TESTIMONIAL -->

@endsection

@section('js')
    <script>
        function fetch_data(query) {
            var page = 0
            var sort = 'asc'
            window.location.href = "/search/robot?page=" + page + "&query=" + query + "&sort=" + sort
        }

    </script>
@endsection
