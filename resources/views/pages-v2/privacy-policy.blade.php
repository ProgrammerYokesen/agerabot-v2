@extends('app')

@section('content')
    <div class="howto-container container">
        <div class="row">
            <div class="col-md-3">
                <div class="side-menu">
                    <a href="{{ route('termsCondition') }}">Terms & Condition</a>
                    <a href="{{ route('disclaimerPage') }}">Disclaimer</a>
                    <a href="#">Privacy Policy</a>
                </div>
            </div>
            <div class="col-md-9">
                <div class="howto-content">
                    <h1>Privacy Policy</h1>
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Incidunt inventore libero eveniet nisi ipsam
                        voluptatum quas labore provident explicabo praesentium, dolore assumenda error voluptatem esse
                        possimus
                        facilis magnam fugiat neque eum! Aut debitis illum necessitatibus repellendus, ea, totam fuga ex
                        ullam
                        quos eaque corporis, tempora asperiores nemo rerum? Aspernatur, vero eum. Voluptatibus deleniti
                        praesentium sapiente temporibus eum! Reiciendis, aspernatur sunt! Aut, saepe molestias. Asperiores
                        libero et animi itaque aspernatur consequatur commodi at exercitationem dolores, earum veniam quas
                        tempora quod veritatis aliquam quam delectus, debitis consequuntur cumque obcaecati minima fugit
                        placeat, aliquid voluptatem. Tempore architecto fugiat libero vel sapiente voluptas adipisci
                        cupiditate
                        molestias rem beatae! Praesentium autem veritatis provident libero culpa. Neque sint ut ullam quia
                        architecto animi quibusdam iure, ratione, doloribus, provident voluptatum adipisci? Eius,
                        dignissimos
                        corrupti veniam atque facere velit officiis libero, deleniti similique numquam quasi ipsum magnam
                        impedit amet distinctio. Praesentium, accusamus harum provident vel consectetur dolor, omnis
                        laboriosam
                        odit delectus, alias dolore in officia aperiam obcaecati vero odio itaque corrupti eos accusantium
                        quo?
                        Quas consequatur libero, corrupti, impedit facere velit nulla quo eaque ullam aperiam modi quidem
                        porro
                        quibusdam est inventore? Magni consequatur, inventore asperiores quaerat ut laudantium placeat
                        tempora
                        rem soluta facere consequuntur animi voluptates earum. Distinctio, quas at numquam quaerat fugit
                        amet!
                        Magnam obcaecati tempore modi recusandae praesentium temporibus fuga dolor harum id enim iste
                        delectus
                        sunt libero perferendis, aliquid aliquam dolorem nemo doloremque quibusdam quod nulla soluta
                        possimus
                        eveniet exercitationem. Maxime dignissimos corporis iure error sapiente expedita recusandae neque
                        fugit
                        ut id numquam ex tempora vel, explicabo vero itaque nulla sequi, consequuntur amet? Placeat vero
                        tenetur
                        qui ab praesentium commodi inventore, provident similique asperiores reiciendis ullam molestias
                        obcaecati sit pariatur neque quod. Neque quidem dolorum deserunt ab sapiente ducimus quisquam eum,
                        officiis excepturi tempora minima, cum sint at sequi enim, vitae nisi ad dolores? Esse quam vitae
                        eos
                        laboriosam delectus modi, tenetur inventore iste aspernatur, animi iure omnis atque expedita
                        blanditiis
                        tempore quos odit! Perferendis, tempore eveniet odit in excepturi enim aspernatur voluptatum
                        deleniti
                        quod sit sunt mollitia, placeat delectus dolore minus vitae ipsa. Voluptas eos neque nisi,
                        perspiciatis
                        obcaecati sapiente ut, temporibus ratione, explicabo iste cum mollitia et. Reprehenderit, repellat
                        pariatur laborum earum, ipsa, rem dolore consequuntur amet nobis similique illo. Dolore amet
                        adipisci
                        molestiae hic animi quod asperiores sapiente veritatis, eum, ullam repudiandae dolorum temporibus
                        quas
                        sequi, totam iure aperiam? Placeat optio, a fugiat earum laboriosam amet assumenda tempora
                        cupiditate
                        iusto deserunt nihil consequuntur molestias culpa molestiae cum ducimus cumque porro laborum ipsum
                        at
                        minus animi eum. Adipisci minima architecto iste voluptates alias quia. Eos a cum laborum
                        dignissimos,
                        nemo ducimus harum sapiente libero nam doloremque maiores quibusdam exercitationem distinctio rem
                        obcaecati consequuntur id quisquam repudiandae illum eaque voluptate rerum! Sunt, nobis? Omnis,
                        adipisci
                        placeat temporibus, pariatur aliquid, totam beatae quis obcaecati doloremque molestiae a vitae
                        fugiat.
                        Optio, corporis quas placeat nemo sint unde maiores tenetur asperiores quaerat vero magnam fugit
                        praesentium, explicabo alias a odio beatae veniam veritatis, nisi deleniti magni? Id velit ad
                        voluptatum
                        ullam quidem, facilis, totam fugiat, iusto distinctio voluptate autem ut ex alias. Modi dolorum, sit
                        illo, provident corrupti, ullam alias neque molestiae nobis suscipit harum! Exercitationem eveniet
                        architecto quibusdam beatae est, saepe fuga repellat magnam quos sed aliquid nulla! Commodi unde
                        eaque
                        quis pariatur perspiciatis! Ea tenetur hic explicabo quia repellat incidunt blanditiis sequi
                        expedita,
                        similique excepturi modi nisi vel error temporibus suscipit esse dignissimos laboriosam laudantium
                        illum
                        obcaecati alias reprehenderit, inventore provident? Molestias minima, natus deserunt sed distinctio
                        labore quaerat iure, inventore officia reprehenderit ipsam modi similique magni molestiae quidem
                        incidunt voluptatibus? Aspernatur reiciendis repudiandae, esse sit impedit optio dolorem magnam
                        itaque
                        eius harum officia ipsum dolor enim repellendus quo consequatur delectus tempore veniam soluta nam
                        voluptate cum accusamus tempora dignissimos? Ipsam et a, facilis repellat eum incidunt quas!
                        Sapiente
                        neque laboriosam sit adipisci dolorem odit quibusdam quasi odio assumenda nobis. Quibusdam fugit,
                        blanditiis voluptas sit eveniet repudiandae ex recusandae. Magni similique tenetur qui doloribus?
                        Nemo
                        fugiat fugit ipsum pariatur magnam temporibus. Assumenda illo atque et ratione iste quae odio
                        tenetur
                        repudiandae quia? Dolor pariatur nemo dolore. Culpa iusto dignissimos quaerat officiis, repellendus
                        quisquam illum natus molestias tenetur incidunt esse consequatur eveniet! Vero, sunt. Dignissimos,
                        consequatur? Nobis fuga nam alias corporis nostrum iste quae, ipsum molestiae vel dolorem eos
                        ratione a
                        quisquam esse recusandae sed et repellendus architecto quo similique obcaecati perferendis soluta
                        at.
                        Aliquam enim consequuntur facilis hic harum corporis inventore beatae, dolor nulla excepturi laborum
                        illo, autem maxime minima necessitatibus recusandae dolore? Explicabo, exercitationem quis quasi
                        sapiente debitis perspiciatis, aspernatur reiciendis eum autem recusandae ducimus, officia possimus
                        voluptatum! Praesentium dolor numquam in laboriosam ipsum aliquam neque esse. Aperiam quia repellat
                        ipsam corrupti est quibusdam placeat perspiciatis numquam voluptatibus porro quas, nisi ea vel
                        praesentium officiis sit voluptas esse voluptate laborum animi voluptatem expedita aut, delectus
                        sunt.
                        Eius quam iusto reprehenderit eaque explicabo deleniti sint praesentium dolore quae rerum, harum
                        deserunt asperiores dolores? Quas praesentium esse aut odit expedita. Doloremque molestias
                        architecto
                        aperiam delectus! Autem temporibus impedit voluptatibus iusto dolorum! Corrupti doloribus maxime
                        obcaecati! Inventore exercitationem perferendis similique harum odio eius veniam rem quae commodi,
                        magni
                        optio vitae culpa distinctio? Provident corporis, ut aperiam quas magnam saepe eum. Quasi voluptate,
                        amet quod assumenda veniam nostrum, accusamus quisquam ipsam beatae quis officiis vero suscipit
                        debitis
                        optio, dignissimos earum doloremque voluptates modi. Iste omnis debitis harum ab voluptate, dolore
                        corporis doloremque id porro optio veritatis tempora repellendus! Doloribus iusto vitae eos ipsam
                        optio
                        tenetur animi, voluptatibus provident mollitia nemo voluptatum rem incidunt natus et dolorum
                        praesentium
                        dolore corporis distinctio, sit officia dolores atque pariatur. In impedit temporibus consequatur
                        nulla,
                        eligendi obcaecati excepturi nisi aperiam qui, placeat odit itaque quibusdam magni! Corporis sunt
                        distinctio nihil, deserunt corrupti, dignissimos ipsa laboriosam, veritatis inventore dolor dolorum
                        beatae facere totam laudantium a suscipit nesciunt repellat repudiandae animi modi delectus
                        necessitatibus! Ea ab quasi quibusdam culpa nisi, ex possimus, maxime nobis, ipsum distinctio est
                        veniam! A vero quasi nostrum et recusandae, unde perspiciatis quia nam debitis sint sapiente ut
                        deserunt
                        quidem porro? Provident illo eveniet ex error praesentium. Doloremque, sed iste.</p>
                </div>

            </div>
        </div>

    </div>
@endsection
