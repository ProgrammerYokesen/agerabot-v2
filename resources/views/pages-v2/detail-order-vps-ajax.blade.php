 <!-- foreach here -->
 @foreach (Cart::content() as $cart)
                    <div class="card-body">

                        <h4>Detail Orders</h4>

                        <div class="order-name">
                            <h4 id="vpsName">{{$cart->name}}</h4>
                            <button>X</button>
                        </div>

                        <div class="order-type">
                            <p>{{$cart->qty}} Months</p>
                            <p>${{$cart->price}}</p>
                        </div>

                        <div class="coupon-code">
                            <p>Enter Coupon Code</p>
                            <input type="text" class="coupon-code-input" placeholder="Coupon Code">
                        </div>

                        <!-- <div class="tax">
                            <p>10% Tax</p>
                            <p>$1.5</p>
                        </div> -->

                        <div class="total  mt-5">
                            <h4>Total</h4>
                            <h4>${{$cart->price}}</h4>
                        </div>

                        <div class="mt-5" style="margin: 0 auto">
                        @if (CRUDBooster::myID())
                                <button class="checkout-btn"  onclick="checkout({{ $cart->id }})">Checkout</button>
                            @else
                                <a href="{{ route('registerPage') }}">
                                    <button class="checkout-btn">Checkout</button>
                                </a>
                            @endif
                        </div>
                    </div>
                  </div>
                  @endforeach
                  <!-- endforeach -->