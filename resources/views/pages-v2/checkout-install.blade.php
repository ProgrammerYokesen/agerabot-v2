@extends('app')

@section('content')
<div class="checkout-container container">
    <div class="row">
        <!-- foreach here -->
        @foreach (Cart::content() as $row)
        <div class="col-md-7 order-choice">
            <h4>Choose your additional feature</h4>
            <div class="row install-card">
                <div class="card">
                    <div class="card-body">
                        <div class="install-desc">
                            <h5>Installation</h5>
                            <p>Explanation about installation and maintenance system in Agerabot</p>
                        </div>
                        <div class="install-price">
                            <h5>${{$row->price}}</h5>
                        </div>
                        <div class="install-price">
                            <h5>X</h5>
                        </div>
                        <div class="install-counter">
                            <i class="fas fa-caret-up" id="installUp"></i>
                            <span id="qtyInstall">{{$row->qty}}</span>
                            <i class="fas fa-caret-down" id="installDown"></i>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        @endforeach
        <!-- End foreach -->
        <div class="col-md-5 order-detail-card">
            @foreach (Cart::content() as $row)
            <div class="card">
                <div class="card-body">

                    <h4>Detail Orders</h4>

                    <div class="order-name">
                        <h4>Installation</h4>
                    </div>

                    <div class="order-type">
                        <p id="quotaInstall">x{{$row->qty}} </p>
                        <p id="qtyXHarga">${{$row->price}}</p>
                    </div>

                    <div class="coupon-code">
                        <p>Enter Coupon Code</p>
                        <input type="text" class="coupon-code-input" placeholder="Coupon Code">
                    </div>

                    <!-- <div class="tax">
                        <p>10% Tax</p>
                        <p id="taxInstall">$0</p>
                    </div> -->

                    <div class="total mt-5">
                        <h4>Total</h4>
                        <h4 id="finalPrice">${{$row->price}}</h4>
                    </div>

                    <div class="mt-5" style="margin: 0 auto">
                        <button id="btn-checkout" class="checkout-btn">Checkout</button>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</div>

<!-- Modal checkout -->
<div class="modal" tabindex="-1" role="dialog" id="checkoutModal">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content p-0">
            <iframe width="400" height="700" id="iframe-invoice" class="iframe-invoice" title="Invoice" src=""></iframe>
        </div>
    </div>
</div>
<!-- Endmodal checkout -->
@endsection

@section('js')
<script>
    $(document).on('click', '#installUp', function() {
        var qty = (+$('#qtyInstall').text()) + 1;
        $('#qtyInstall').text(qty)
        // console.log(qty)
        $('#quotaInstall').text(qty)
        var total = 1.99 * qty;
        $('#qtyXHarga').text(total)
        var discount = (10 * total) / 100;
        $('#taxInstall').text(discount)

        $('#finalPrice').text(total)
    })


    $(document).on('click', '#installDown', function() {
        var qty = (+$('#qtyInstall').text()) - 1;
        if (qty < 0) {
            qty = 0
        }
        $('#qtyInstall').text(qty)
        // console.log(qty)
        $('#quotaInstall').text(qty)
        var total = 1.99 * qty;
        $('#qtyXHarga').text(total)
        var discount = (10 * total) / 100;
        $('#taxInstall').text(discount)
        $('#finalPrice').text(total)

    })



    $("#btn-checkout").click(function(e) {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        e.preventDefault();

        var qty = $('#qtyInstall').text();
        console.log(qty);

        $.ajax({
            type: 'POST',
            url: "{{ route('createSubscriptionInstall') }}",
            data: {
                qty: qty
            },
            success: function(data) {
                //   alert(data);
                // console.log(data);
                data = data.data;
                // console.log(data);
                loadIframe("iframe-invoice", data.invoice_url);
                $('#checkoutModal').modal('show');
            }
        });

    });

    function loadIframe(iframeName, url) {
    var $iframe = $('#' + iframeName);
    if ($iframe.length) {
      $iframe.attr('src', url);
      return false;
    }
    return true;
  }
</script>

@endsection