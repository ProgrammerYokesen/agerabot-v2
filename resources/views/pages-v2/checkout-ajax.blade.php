@for($i=0;$i<$datasLength;$i++) <div class="col-md-4 col-lg-4 col-sm-4 col-4">

    <label class="radio-label">

        <input id="subscriptionChoice" type="radio" name="product" class="card-input-element" onclick="editCart({{$datas[$i]->id}})" />


        @foreach(Cart::content() as $row)
        @if($datas[$i]->id == $row->id)
        <div class="card-input  active-card">
            @else
            <div class="card-input">
                @endif
                @endforeach
                <p>{{$datas[$i]->nmPackage}}</p>
                @if($datas[$i]->interval != 1)
                <p><s>${{$datas[0]->price * $datas[$i]->interval}}</s></p>
                @endif
                <h4>
                    ${{$datas[$i]->price}}
                </h4>
            </div>

    </label>

    </div>
    @endfor