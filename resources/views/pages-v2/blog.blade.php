@extends('app')

@section('content')
    <div class="container blog-container">

        {{-- Start Blog Header --}}
        <div class="blog-header">
            <ul>
                <li>
                    <a href="#" class="blog_links active">Weekly Highlight</a>
                </li>
                <li>
                    <a href="#" class="blog_links" onclick="allBlog('all')">All</a>
                </li>
                <li>
                    <a href="#" class="blog_links">Case Studies</a>
                </li>
                <li>
                    <a href="#" class="blog_links">Tips & Trick</a>
                </li>
                <li>
                    <a href="#" class="blog_links">Version Update</a>
                </li>
            </ul>
            <a href="#"><i class="fas fa-search"></i></a>
        </div>
        {{-- End Blog Header --}}

        {{-- Start Featured Blog weekly --}}
        {{-- <div id="mostRead"> --}}
        <div class="row align-items-center featured_blog" id="mostRead">
            <div class="col-md-6">
                <a href="{{ route('blogDetailPage', ['slug' => $mostRead->blogSlug]) }}">
                    <img src="{{ $mostRead->blogImage1 }}" alt="" style="width: 100%"> {{-- Blog Image --}}
                </a>
            </div>
            <div class="col-md-6">
                <div class="featured_blog_r">
                    <a href="{{ route('blogDetailPage', ['slug' => $mostRead->blogSlug]) }}"
                        style="text-decoration:none;">
                        <h5>{{ $mostRead->blogTitle }}</h5> {{-- Blog Title --}}
                    </a>
                    <div class="blog_author">
                        {{-- <img src="{{ $mostRead->photo }}" alt=""> --}}
                        <img src="{{ $mostRead->photo }}" alt="blog_author" width="100"
                            style="width: 50px;height:50px ;border-radius: 50%; object-fit: cover; margin-right: 15px">
                        <div>
                            <p>{{ $mostRead->name }}</p> {{-- Blog Author Name --}}
                            <p>{{ $readingTime }} mins read</p> {{-- Blog Length --}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{-- </div> --}}
        {{-- End Featured Blog --}}
        {{-- Start Blog List weekly --}}
        <div id="blog-thumbnail-wrapper">
            <div class="row" id="blog-thumbnail">
                @foreach ($finalBlogsData as $item)
                    <div class="col-md-4 blog_list">
                        <div>
                            <a href="{{ route('blogDetailPage', ['slug' => $item->blogSlug]) }}" id="{{ $item->type }}">
                                <img src="{{ $item->blogImage1 }}" alt="" style="width: 100%">
                                {{-- Blog Image --}}
                            </a>
                        </div>
                        <div class="blog_title">
                            <a href="{{ route('blogDetailPage', ['slug' => $item->blogSlug]) }}">
                                <h5>{{ $item->blogTitle }}</h5> {{-- Blog Title --}}
                            </a>
                        </div>
                        <div class="blog_author">
                            {{-- <img src="{{ $item->photo }}" alt=""> --}}
                            <img src="{{ $item->photo }}" alt="author" width="100"
                                style="width: 45px;height:45px ;border-radius: 50%; object-fit: cover; margin-right: 15px">
                            <div>
                                <p>By {{ $item->name }}</p> {{-- Blog Author Name --}}
                                <p>{{ $item->readingTime }} mins read</p> {{-- Blog Length --}}
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>

            {{ $finalBlogsData->links() }}
        </div>
        {{-- End Blog List --}}

        {{-- @include('pages-v2.blog-blade-card') --}}

    </div>
@endsection

@section('js')
    <script>
        $(document).on('click', '.pagination a', function(event) {
            event.preventDefault();
            var page = $(this).attr('href').split('page=')[1];
            var slug = $('.src-blog').attr('data-id');
            var pages = $(this).attr('href')
            console.log(slug)
            fetch_data(page);
        });

        function fetch_data(page) {
            // $.ajax({
            //     url: ""
            // })
        }

    </script>
    <script>
        // $(document).ready(function() {
        function allBlog(slug) {
            // console.log(slug);
            $.ajax({
                    url: "/blogs/ajax?slug=" + slug,
                    type: "GET",
                    beforeSend: function() {}
                })
                .done(function(data) {
                    // console.log(data)
                    var url = '{{ route('blogDetailPage', ':slug') }}';
                    url = url.replace(':slug', data[0].blogSlug);
                    var htmlMostRead =
                        `                
                                                                                                    <div class="row align-items-center featured_blog" id="mostRead">
                                                                                                        <div class="col-md-6">
                                                                                                            <a href="` +
                        url +
                        `">
                                                                                                                <img src="` +
                        data[
                            0]
                        .blogImage1 + `" alt="" style="width: 100%"> {{-- Blog Image --}}
                                                                                                            </a>
                                                                                                        </div>
                                                                                                        <div class="col-md-6">
                                                                                                            <div class="featured_blog_r">
                                                                                                                <a href="{{ route('blogDetailPage', ['slug' => `+data[0].blogSlug+`]) }}"
                                                                                                                    style="text-decoration:none;">
                                                                                                                    <h5>` +
                        data[0]
                        .blogTitle +
                        `</h5> {{-- Blog Title --}}
                                                                                                                </a>
                                                                                                                <div class="blog_author">
                                                                                                                    <img src="` +
                        data[
                            0]
                        .photo +
                        `" alt="">
                                                                                                                    <div>
                                                                                                                        <p>` +
                        data[0]
                        .name +
                        `</p> {{-- Blog Author Name --}}
                                                                                                                        <p>` +
                        data[
                            1] + ` mins read</p> {{-- Blog Length --}}
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>  
                                                                                                `
                    $('#mostRead').empty();
                    $("#mostRead").html(htmlMostRead);

                })
                .fail(function(e) {
                    console.log(e);

                });

            $.ajax({
                    url: "/blog?slug=" + slug,
                    type: "GET",
                    beforeSend: function() {}
                })
                .done(function(finalBlogsData) {
                    // if (finalBlogsData == " ") {
                    //     $('.ajax-load').html("No more records found");
                    //     return;
                    // }
                    console.log(finalBlogsData)
                    // $('#blog-thumbnail-wrapper').html('');
                    // $('#blog-thumbnail-wrapper').html(finalBlogsData);
                    document.getElementById("blog-thumbnail-wrapper").innerHTML = finalBlogsData;
                    // document.getElementById("detailOrder").innerHTML = data.orderDetail;
                })
                .fail(function(e) {
                    console.log(e);

                });
        }
        // })

    </script>
@endsection
