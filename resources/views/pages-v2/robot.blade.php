@extends('app')

@section('content')
    <div class="robot-hero">
        <div class="robot-hero-txt">
            <h2>Temukan Robot yang tepat untuk Anda</h2>
            <h5>Kami menyediakan berbagai jenis robot yang sesuai dengan karakter dan tipe trading Anda, dan pastinya robot
                kami memiliki performa yang bisa membantu Anda meraih profit!</h5>
        </div>
        <div class="robot-hero-overlay"></div>
    </div>

    <div class="container robot-menu-container">
        <div class="customize_solution">
            <ul class="tabs nav nav-justified">
                <li class="tab-link current nav-pill mt-2">
                    <a href="{{ route('robotPage') }}">
                        <span class="ease-effect">Top Performance Robot</span>
                    </a>
                </li>
                <li class="tab-link nav-pill mt-2">
                    <a href="{{ route('robotListPage') }}">
                        <span class="ease-effect">Robot Detail List</span>
                    </a>
                </li>
                <li class="tab-link nav-pill mt-2">
                    <a href="{{ route('robotComparePage') }}">
                        <span class="ease-effect">Robot Comparison</span>
                    </a>
                </li>
            </ul>
        </div>

        {{-- Profitable ROBOT --}}
        <div>
            <div class="profitable-robot">
                @if (empty($array))
                    <form action="{{ route('searchProfitableRobot') }}" method="get">
                        @csrf
                        {{-- SELECT PAIR --}}
                        <div class="card mt-5">
                            <div class="card-body">
                                <h5>Select Pair</h5>
                                <div class="row">

                                    @foreach ($pullPair as $datum)
                                        <div class="col-md-4 col-lg-4 col-sm-4 col-3 colpadding">
                                            <label class="robot-radio-label">
                                                <input type="radio" name="pair" value="{{ $datum->stat_pair }}"
                                                    class="card-input-element" />
                                                {{-- <div class="robot-pair" onclick="pair('{{$datum->stat_pair}}')"> --}}
                                                <div class="robot-pair">
                                                    <p>{{ $datum->stat_pair }}</p>
                                                </div>
                                            </label>
                                        </div>
                                    @endforeach

                                </div>
                            </div>
                        </div>
                        {{-- END SELECT PAIR --}}

                        {{-- SELECT TIMEFRAME --}}
                        <div class="card my-5">
                            <div class="card-body">
                                <h5>Select Timeframe</h5>
                                <div class="row">

                                    <div class="col-md-4 col-lg-4 col-sm-4 col-4">
                                        <label class="robot-radio-label">
                                            <input type="radio" name="timeframe" value="week" class="card-input-element" />
                                            <div class="robot-pair">
                                                <p>WEEKLY</p>
                                            </div>
                                        </label>
                                    </div>
                                    <div class="col-md-4 col-lg-4 col-sm-4 col-4">
                                        <label class="robot-radio-label">
                                            <input type="radio" name="timeframe" value="month" class="card-input-element" />
                                            <div class="robot-pair">
                                                <p>MONTHLY</p>
                                            </div>
                                        </label>
                                    </div>
                                    <div class="col-md-4 col-lg-4 col-sm-4 col-4">
                                        <label class="robot-radio-label">
                                            <input type="radio" name="timeframe" value="year" class="card-input-element" />
                                            <div class="robot-pair">
                                                <p>YEAR TO DATE</p>
                                            </div>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {{-- END SELECT TIMEFRAME --}}

                        @if ($errors->all())
                            <div>
                                <p class="error-profitable-robot">Please select pair and timeframe</p>
                            </div>
                        @endif

                        <div class="profitable-search-btn">
                            <button type="submit">Search</button>
                        </div>
                    </form>

                @elseif (!empty($array))
                    <form action="{{ route('searchProfitableRobot') }}" id="robotProfitSearch">
                        @csrf
                        <div class="choose-pair my-5">
                            <div class="filter">
                                <div>
                                    <p>Pair</p>
                                </div>
                                {{-- Sort By: <input type="text" class="filter-box"> --}}
                                <div class="input-group mb-3 ml-3" style="width: 200px">
                                    <select name="pair" class="custom-select profit-pair" id="inputGroupSelect01">
                                        <option selected>Choose...</option>
                                        @foreach ($pullPair as $pair)
                                            <option value="{{ $pair->stat_pair }}">{{ $pair->stat_pair }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="filter">
                                <div>
                                    <p>Timeframe</p>
                                </div>
                                {{-- Sort By: <input type="text" class="filter-box"> --}}
                                <div class="input-group mb-3 ml-3" style="width: 200px">
                                    <select name="timeframe" class="custom-select profit-timeframe" id="inputGroupSelect1"
                                        onchange="this.form.submit()">
                                        <option selected>Choose...</option>
                                        <option value="week">WEEKLY</option>
                                        <option value="month">MONTHLY</option>
                                        <option value="year">YEAR TO DATE</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="row robot-detail-slider">
                        @if ($subscribe == false)
                            @php
                                $key = array_keys($array);
                                $counts = '';
                                if (count($array) > 5) {
                                    $counts = 6;
                                } else {
                                    $counts = count($array);
                                }
                            @endphp
                            @for ($i = 0; $i < $counts; $i++)

                                @if ($i < 3) {{-- @php
                            dd($key[$i], $key, $array[$key[$i]], $array, $array[$key[$i]]['robot']);
                            @endphp --}}
                        {{-- SUBSCRIBE TO REVEAL --}}
                    <div class="col-md-4 col-6 robot-detail-slider-bg text-center">
                        <div class="robot-wishlist-container">
                            <div class="text-right">
                                <img src="{{ asset('./images/robot-hidden.png') }}" alt="" class="robot-detail-hidden">
                            </div>
                        </div>
                        <h3 class="mt-5 text-center">Subscribe to reveal</h3>
                    </div>
                            @else                             
                    <div class="col-md-4 col-6 cardss [ is-collapsed ]">
                        <div class="robot-detail-slider-bg text-center card__inner">
                            <div class="robot-wishlist-container">
                                <div class="text-left wishlist-btn">

                           @if (CRUDBooster::myId()==null)

                                    <input type="image" name="submit"
                                    src="{{ asset('./images/wishlist.svg') }}" alt=""
                                    class="wishlist-img">
                                @else
                                    @if ($array[$key[$i]]['subs'] != false)
                                        {{-- <form action="{{ route('wishlist') }}" method="post"> --}}
                                        {{-- @csrf --}}
                                        {{-- <input type="text" name="robot" value="{{$array[$key[$i]]['robot']}}" style="display:none"> --}}
                                        <input type="image" name="submit"
                                        src="{{ asset('./images/wishlist-red.svg') }}" alt=""
                                        class="{{ $array[$key[$i]]['tag'] }}"
                                        onclick="wish({{ $array[$key[$i]]['robot'] }})">
                                        {{-- </form> --}}
                                    @else
                                        {{-- <form action="{{ route('wishlist') }}" method="post"> --}}
                                        {{-- @csrf --}}
                                        {{-- <input type="text" name="robot" value="{{$array[$key[$i]]['robot']}}" style="display:none"> --}}
                                        <input type="image" name="submit"
                                        src="{{ asset('./images/wishlist.svg') }}" alt=""
                                        class="{{ $array[$key[$i]]['tag'] }}"
                                        onclick="wish({{ $array[$key[$i]]['robot'] }})">
                                        {{-- </form> --}} @endif
                                    @endif
                    </div>
                    <div class="text-right">
                        <img src="{{ asset('./images/robot.png') }}" alt="" class="robot-detail-slider-img">
                    </div>
            </div>
            <h3>{{ $array[$key[$i]]['robotName'] }}</h3>
            @if ($periode == 'week')
                <p>Weekly Performance:</p>
            @elseif ($periode == 'month')
                <p>Monthly Performance:</p>
            @else
                <p>Yearly Performance:</p>
            @endif
            <h5>{{ $array[$key[$i]]['month_percent'] }}%</h5>
            <button class="robot-detail-slider-btn [ js-expander ]">SHOW DETAILS</button>
        </div>

        {{-- Data Expand --}}
        <div class="card__expander">
            <div class="row">
                <div class="col-md-8">
                    <p class="profitable-detail-title ml-2">Last Ten Transaction - per {{ $date }}</p>
                    <div class="last-ten-trans row">

                        @foreach ($array[$key[$i]]['lastTen'] as $data)

                            @if ($data->profit > 0)
                                <button class="col-md-2 col-md-offset-1 last-ten-green">${{ $data->profit }}</button>
                            @else
                                <button class="col-md-2 col-md-offset-1 last-ten-red">${{ $data->profit }}</button>
                            @endif
                        @endforeach


                    </div>
                </div>
                <div class="col-md-4 profitable-detail-percent">
                    <h1>{{ $array[$key[$i]]['last_percent'] }}%</h1>
                    <h5>lorem ipsum</h5>
                </div>
            </div>
            <div class="row mt-5 ml-2">
                <div class="profitable-detail-btn">
                    <button class="mr-3">Subscribe</button>
                    <button class="[ js-collapser ]">Show Less</button>
                </div>
            </div>
        </div>
    </div>
    @endif

    @endfor
@else
    @for ($i = 0; $i <= 5; $i++)
        <div class="col-md-4 robot-detail-slider-bg text-center" style="margin-top: 100px; margin-bottom: 50px">
            <div class="robot-wishlist-container">
                <div class="text-left wishlist-btn">

                    <img src="{{ asset('./images/wishlist.svg') }}" alt="" class="wishlist-img">
                </div>
                <div class="text-right">
                    <img src="{{ asset('./images/robot.png') }}" alt="" class="robot-detail-slider-img">
                    {{-- <img src="{{ $array[$key[$i]]['robotImage'] }}" alt="" class="robot-detail-slider-img"> --}}
                </div>
            </div>
            <h3 class="mt-2">{{ $array[$key[$i]]['robotName'] }}</h3>
            <p>Monthly Performance:</p>
            <h5>{{ $array[$key[$i]]['month_percent'] }}%</h5>
            <button class="robot-detail-slider-btn">SHOW DETAILS</button>
        </div>
    @endfor
    @endif

    </div>

    <div class="text-center mt-5">
        <a href="{{ route('subsPage') }}">
            <button class="subscribe-btn mb-5">SUBSCRIBE NOW</button>
        </a>
        <a href="{{ route('howToPage') }}">
            <button class="subscribe-btn mb-5">LEARN HOW TO USE</button>
        </a>
    </div>
    @endif

    </div>
    </div>


    {{-- ===================================================== --}}

    {{-- <div class="performance_category">
        <button class="select_button">By Popularity</button>
        <button class="select_button">By Performance</button>
    </div> --}}

    <div class="customize_solution my-5">
        <ul class="tabs nav nav-justified" style="justify-content: center">
            <li class="tab-link current nav-pill mt-2">
                <a>
                    <span class="ease-effect">By Popularity</span>
                </a>
            </li>
            <li class="tab-link nav-pill mt-2">
                <a href="{{ route('robotPerformancePage') }}">
                    <span class="ease-effect">By Performance</span>
                </a>
            </li>
        </ul>
    </div>


    @php
    $key = array_keys($popularRobots);
    $counts = '';
    if (count($popularRobots) > 5) {
        $counts = 6;
    } else {
        $counts = count($array);
    }
    @endphp
    <div class="robot_podium">
        @for ($i = 0; $i < 10; $i++)
            @if ($i == 1) {{-- RANK 2 --}}
            <div class="card">
            <div class="card-body">
            <img src="{{ asset('images/icons/rank2.svg') }}" alt="rank"
            class="podium_rank">
            <img src="{{ asset('images/robot.png') }}" alt="" class="podium_image">
            <h5>Kosmobot</h5>
            <h2>577.777</h2>
            <>Downloaded</div>
            </div>
        @elseif ($i == 0)
            {{-- RANK 1 --}}
            <div class="card rank1">
            <div class="card-body">
            <img src="{{ asset('images/icons/rank1.svg') }}" alt="rank"
            class="podium_rank">
            <img src="{{ asset('images/robot.png') }}" alt="" class="podium_rank1">
            <h5>Pegasuso</h5>
            <h2>777.777</h2>
            <h5>Downloaded</h5>
            </div>
            </div>
        @elseif ($i == 2)
            {{-- RANK 3 --}}
            <div class="card">
            <div class="card-body">
            <img src="{{ asset('images/icons/rank3.svg') }}" alt="rank"
            class="podium_rank">
            <img src="{{ asset('images/robot.png') }}" alt="" class="podium_image">
            <h5>Gemini</h5>
            <h2>177.777</h2>
            <h5>Downloaded</h5>
            </div>
            </div> @endif
        @endfor
    </div>

    <div class="robot_leaderboard table-responsive">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th scope="col" style="border-top-left-radius: 20px">Rank</th>
                    <th scope="col">Robot</th>
                    <th scope="col">Downloaded</th>
                    <th scope="col" style="border-top-right-radius: 20px">Favorite</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>4</td>
                    <td><img src={{ asset('images/top-robot.png') }} alt="robot" class="table_robot_img"> Ceme Lot</td>
                    <td>23.386</td>
                    <td>500</td>
                </tr>
                <tr>
                    <td>5</td>
                    <td><img src={{ asset('images/top-robot.png') }} alt="robot" class="table_robot_img"> Kosmobot</td>
                    <td>23.386</td>
                    <td>500</td>
                </tr>
                <tr>
                    <td>6</td>
                    <td><img src={{ asset('images/top-robot.png') }} alt="robot" class="table_robot_img"> Tachdown</td>
                    <td>23.386</td>
                    <td>500</td>
                </tr>
                <tr>
                    <td>7</td>
                    <td><img src={{ asset('images/top-robot.png') }} alt="robot" class="table_robot_img"> Tachdown</td>
                    <td>23.386</td>
                    <td>500</td>
                </tr>
                <tr>
                    <td>8</td>
                    <td><img src={{ asset('images/top-robot.png') }} alt="robot" class="table_robot_img"> Tachdown</td>
                    <td>23.386</td>
                    <td>500</td>
                </tr>
                <tr>
                    <td>9</td>
                    <td><img src={{ asset('images/top-robot.png') }} alt="robot" class="table_robot_img"> Tachdown</td>
                    <td>23.386</td>
                    <td>500</td>
                </tr>
                <tr>
                    <td>10</td>
                    <td><img src={{ asset('images/top-robot.png') }} alt="robot" class="table_robot_img"> Tachdown</td>
                    <td>23.386</td>
                    <td>500</td>
                </tr>

            </tbody>
        </table>
    </div>

    <div class="text-center mt-5">
        <a href="{{ route('subsPage') }}">
            <button class="subscribe-btn mb-5">SUBSCRIBE NOW</button>
        </a>
        <a href="{{ route('howToPage') }}">
            <button class="subscribe-btn mb-5">LEARN HOW TO USE</button>
        </a>
    </div>


    {{-- ===================================================== --}}


    {{-- Menu Bawah --}}
    <div class="customize_solution my-5">
        <ul class="tabs nav nav-justified">
            <li class="tab-link current nav-pill mt-2">
                <a href="{{ route('robotPage') }}">
                    <span class="ease-effect">Top Performance Robot</span>
                </a>
            </li>
            <li class="tab-link nav-pill mt-2">
                <a href="{{ route('robotListPage') }}">
                    <span class="ease-effect">Robot Detail List</span>
                </a>
            </li>
            <li class="tab-link nav-pill mt-2">
                <a href="{{ route('robotComparePage') }}">
                    <span class="ease-effect">Robot Comparison</span>
                </a>
            </li>
        </ul>
    </div>
    {{-- End Menu Bawah --}}

    </div>
@endsection

@section('js')
    <script>
        $(document).ready(function() {
            $('.profit-pair').select2();
            $('.profit-timeframe').select2();
        });

    </script>
    <script>
        function wish(robot) {
            // console.log(robot)
            $.ajax({
                    url: "/wishlist?robot=" + robot,
                    type: "GET",
                    // data: robot,
                    // headers: {
                    //     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    // },
                    beforeSend: function() {
                        // $('.ajax-load').show();
                    }

                })
                .done(function(data) {
                    // console.log(data)
                    if (data.subs == true) {
                        // console.log('masuk')
                        var source = "{{ asset('./images/wishlist-red.svg') }}"
                        $(`.${data.slug}`).attr('src', source)
                    } else {
                        // console.log('keluar')
                        var source = "{{ asset('./images/wishlist.svg') }}"
                        $(`.${data.slug}`).attr('src', source)
                    }
                })
                .fail(function(e) {
                    console.log(e);

                });
        }

    </script>
@endsection
