@extends('app')

@section('content')
    <div class="robot-hero">
        <div class="robot-hero-txt">
            <h2>One more step to own your robot</h2>
            <h5>Start your journey with your trading robot and change your life by earning more profit.</h5>
        </div>
        <div class="subs-hero-overlay"></div>
    </div>

    <div class="subscription_title">
        <h2>Subscription Plan</h2>
        <h5>Choose the option that suits your need.</h5>
    </div>

    <div class="swiper-container swiper-price">
        <div class="swiper-wrapper">


            <div class="swiper-slide">
                <div class="price-card">
                    <div class="price-header">
                        <h5>1 Month</h5>
                        <div class="d-flex" style="align-items: center; justify-content: center">
                            <sup>US$</sup>
                            <h1>4.99</h1>
                        </div>
                    </div>

                    <div class="price-feature">
                        <h5 class="feature-bg">All Robot of your choice</h5>
                        <h5>Top 3 Robot for trading recommendation</h5>
                        <h5 class="feature-bg">Daily Signal Recommendation</h5>
                    </div>

                    <div class="text-center">
                        <button class="price-btn">SUBSCRIBE NOW</button>
                    </div>
                </div>
            </div>
            {{-- ==== --}}
            <div class="swiper-slide">
                <div class="price-card">
                    <div class="price-header">
                        <h5>3 Months</h5>
                        <div class="d-flex" style="align-items: center; justify-content: center">
                            <sup>US$</sup>
                            <h1>14.99</h1>
                        </div>
                    </div>

                    <div class="price-feature">
                        <h5 class="feature-bg">All Robot of your choice</h5>
                        <h5>Top 3 Robot for trading recommendation</h5>
                        <h5 class="feature-bg">Daily Signal Recommendation</h5>
                    </div>

                    <div class="text-center">
                        <button class="price-btn">SUBSCRIBE NOW</button>
                    </div>
                </div>
            </div>
            {{-- ==== --}}
            <div class="swiper-slide">
                <div class="price-card">
                    <div class="price-header">
                        <h5>12 Months</h5>
                        <div class="d-flex" style="align-items: center; justify-content: center">
                            <sup>US$</sup>
                            <h1>59.88</h1>
                        </div>
                    </div>

                    <div class="price-feature">
                        <h5 class="feature-bg">All Robot of your choice</h5>
                        <h5>Top 3 Robot for trading recommendation</h5>
                        <h5 class="feature-bg">Daily Signal Recommendation</h5>
                    </div>

                    <div class="text-center">
                        <button class="price-btn">SUBSCRIBE NOW</button>
                    </div>

                    <div class="best-offer">BEST OFFER</div>
                </div>
            </div>
            {{-- ==== --}}

        </div>
        <!-- Add Arrows -->
        <div class="swiper-button-next"></div>
        <div class="swiper-button-prev"></div>

    </div>
    {{-- END PRICE CAROUSEL --}}


    <div class="container subs-price-container">
        <div class="row price-desktop">
            <!-- Start Price Card -->
            <!-- Start For here -->
            @foreach ($datas as $data)
                <div class="col-md-4">
                    <div class="price-card">
                        <div class="price-header">
                            <h5>{{ $data->nmPackage }}</h5>
                            <div class="d-flex" style="align-items: center; justify-content: center">
                                <sup>US$</sup>
                                <h1>{{ $data->price }}</h1>
                            </div>
                        </div>

                        <div class="price-feature">
                            <h5 class="feature-bg">{{ $data->desc1 }}</h5>
                            <h5>{{ $data->desc2 }}</h5>
                            <h5 class="feature-bg">{{ $data->desc3 }}</h5>
                        </div>

                        <div class="text-center">
                            <button onclick="window.location='{{ route('addToCart', $data->slug) }}'"
                                class="price-btn">SUBSCRIBE NOW</button>
                        </div>
                    </div>
                </div>
            @endforeach
            <!-- End for here -->

        </div> <!-- End Price Card -->

        <div class="subscription_footer">
            <h4>For more information or inquiries,</h4>
            <button>Contact Us</button>
        </div>

    </div> <!-- End Container -->

@endsection
