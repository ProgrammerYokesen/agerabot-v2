@extends('app')

@section('content')
    <div class="blog_detail_container">
        <img src="{{ Storage::url($article->blogImage1) }}" alt="blog_img" class="blog_detail_img">

        <div class="container">
            <div class="blog_detail_title"> {{-- Blog Title --}}
                {{$article->blogTitle}}
            </div>
            <div class="blog_detail_author"> {{-- Blog Author --}}
                <div class="blog_author">
                    <img src="<?php echo ( $article->providerOrigin == 'facebook' || $article->providerOrigin == 'google')? $article->photo : Storage::url($article->photo); ?>" alt=""> {{-- Author Image --}}
                    <div>
                        <p>By {{$article->name}}</p> {{-- Blog Author Name --}}
                        <p>Date, {{$article->release_date}} &bull; {{$article->readingTime}} mins read</p> {{-- Blog Length --}}
                    </div>
                </div>
                <div class="blog_share">
                    <i class="fas fa-share"></i>
                </div>
            </div>
            <div class="blog_detail_content"> {{-- Blog Content --}}
               {!! $article->blogContent !!}
            </div>
            <hr>
            {{-- Comment Section --}}
            <div class="blog_comment">
                <h5>Comment</h5>
                {{-- <form> --}}
                    <div class="form-group">
                        <textarea class="form-control" name="comment" id="comment" rows="5"
                            placeholder="Write your comment here"></textarea>
                        <p class="text-danger d-none" id="comment-blank"><small>*please write the comment first</small> </p>
                        <div class="text-right">
                            <button id="comment-post" onclick="submitComment({{ $article->blogId }})">POST</button>
                        </div>
                    </div>
                {{-- </form> --}}

                <div class="comment_section" id="blog-comment">
                    @if (count($comments)>0)
                        @foreach ($comments as $item)
                            <div class="the_comment">
                                <img class="user_comment_img" src="<?php echo ( $item->providerOrigin == 'facebook' || $item->providerOrigin == 'google')? $item->photo : Storage::url($item->photo); ?>">
                                <div class="user_comment">
                                    <div class="user_comment_name">
                                        <h5>{{$item->name}}</h5> {{-- User Name --}}
                                        <span>{{ $item->time }}</span> {{-- User Comment Time --}}
                                    </div>
                                    <p>
                                        {{$item->comment}}
                                    </p>
                                    <div class="user_feedback">
                                        <div class="user_like">
                                            <i class="far fa-thumbs-up" id="like-{{$item->id}}" onclick="likes({{ $item->id }},1)"></i> {{$item->likesCount}}
                                        </div>
                                        <div class="user_dislike">
                                            <i class="far fa-thumbs-down" id="dislike-{{$item->id}}" onclick="likes({{ $item->id }},2)"></i> {{$item->dislikesCount}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    @endif

                </div>
            </div>
            {{-- End Comment Section --}}
            <hr>
        </div>

        {{-- Start Blog List --}}
        <div class="other_blog_container">
            <h1 class="text-center">Look for another one!</h1>
            <div class="row">
                @foreach ($data as $item)
                    <div class="col-md-4 blog_list">
                        <div>
                            <a href="{{ route('blogDetailPage', ['slug' => $item['slug']]) }}">
                                <img src="{{ Storage::url($item['blogImage']) }}" alt="" style="width: 100%" > {{-- Blog Image --}}
                            </a>
                        </div>
                        <div class="blog_title">
                            <a href="{{ route('blogDetailPage', ['slug' => $item['slug']]) }}" style="text-decoration:none;">
                                <h5>{{$item['title']}}</h5> {{-- Blog Title --}}
                            </a>
                        </div>
                        <div class="blog_author">
                            <img src="<?php echo ( $item['providerOrigin'] == 'facebook' || $item['providerOrigin'] == 'google')? $item['photo'] : Storage::url($item['photo']); ?>" alt="">
                            <div>
                                <p>By {{$item['name']}}</p> {{-- Blog Author Name --}}
                                <p>{{$item['readingTime']}} mins read</p> {{-- Blog Length --}}
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        {{-- End Blog List --}}
    </div>
@endsection

@section('js')
    <script>
        $(document).ready(function(){
            $(document).on('keyup', '#comment', function()
            {
                $('#comment-blank').addClass('d-none');
            })
        })
    </script>
    <script>
        function submitComment(blogId) 
        {
            event.preventDefault();
            var comment = $('#comment').val();
            var data = {comment, blogId}
            // console.log(comment, blogId)
            if(comment && data)
            {
                $.ajax({
                    type:"POST",
                    url: '{{route('submitComment')}}',
                    data,
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    } 
                })
                .done(function(comment)
                {
                    // console.log(comment)
                    $('#comment').val('');
                    $('#blog-comment').append(
                        `
                            <div class="the_comment">
                                <img class="user_comment_img" src="<?php echo ( `+comment.providerOrigin+` == 'facebook' || `+comment.providerOrigin+` == 'google')? `+comment.photo+` : Storage::url(`+comment->photo+`); ?>">
                                <div class="user_comment">
                                    <div class="user_comment_name">
                                        <h5>${comment.name}</h5> {{-- User Name --}}
                                        <span>${ comment.time }</span> {{-- User Comment Time --}}
                                    </div>
                                    <p>
                                        ${comment.comment}
                                    </p>
                                    <div class="user_feedback">
                                        <div class="user_like">
                                            <i class="far fa-thumbs-up"></i> ${comment.likesCount}
                                        </div>
                                        <div class="user_dislike">
                                            <i class="far fa-thumbs-down"></i> ${comment.dislikesCount}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        `
                    );
                })
                .fail(function(e)
                {
                    console.log(e);
                })
            }else {
                $('#comment-blank').removeClass('d-none');
            }
            
        }
    </script>
    <script>
        function likes(commentId,like)
        {
            // console.log(commentId, like);
            var data = {commentId, like}
            $.ajax({
                type:"POST",
                url:'{{route('like')}}',
                data,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            })
            .done(function(data)
            {
                console.log(data)
            })
        }
    </script>
@endsection
