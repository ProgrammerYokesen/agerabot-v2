<nav class="navbar_agerabot">
    <input id="nav-toggle" type="checkbox">
    <a id="logo_agerabot" class="logo" href="{{ route('homePage') }}">
        {{-- <img src="{{ asset('./images/logo.svg') }}" alt=""> --}}
    </a>
    <ul class="links">
        <li><a href="{{ route('homePage') }}" class="nav__link {{ set_active_navbar('homePage') }}">Home</a></li>
        <li><a href="{{ route('profitableRobot') }}"
                class="nav__link {{ set_active_navbar('profitableRobot') }}">Trading
                Robot</a></li>
        <li><a href="{{ route('subsPage') }}"
                class=" nav__link {{ set_active_navbar('subsPage') }}">Subscription</a></li>
        <li>
            <a href="{{ route('blogPage') }}" class=" nav__link {{ set_active_navbar('blogPage') }}">Blog</a>
        </li>

        @if (CRUDBooster::myId())
            <li class="nav__user">
                <a style="display: flex; justify-content: center; align-items:center" href="javascript:void(0)"><i
                        class="fas fa-user-circle mr-3" style="font-size: 24px"></i>
                    {{ CRUDBooster::myName() }}
                </a>
                <div class="doropdown">
                    <ul>
                        <li class="doropdown-link"><a href="{{ route('dashboard') }}"><i
                                    class="mr-2 far fa-address-card"></i> Account</a></li>
                        <li class="doropdown-link"><a href="{{ route('settingPage') }}"><i
                                    class="mr-2 fas fa-cog"></i> Setting</a></li>
                        <li class="doropdown-link"><a href="{{ route('getLogout') }}"><i
                                    class="mr-2 fas fa-sign-out-alt"></i> Logout</a></li>
                    </ul>
                </div>
            </li>
        @else
            <li>
                <a data-toggle="modal" data-target="#loginModal"
                    style="display: flex; justify-content: center; align-items:center" href="#">
                    <i class="fas fa-user-circle mr-3" style="font-size: 24px"></i> Login
                </a>
            </li>
            {{-- <li><a href="{{ route('registerPage') }}">Login</a></li> --}}
        @endif
        <li>
            @php
                foreach (Cart::content() as $key => $value) {
                    $type = $value->options['type'];
                    $id = $value->id;
                    if ($value->options['slug']) {
                        $slug = $value->options['slug'];
                    }
                    // dd($value);
                }
                // dd($type,  $id, $slug);
            @endphp
            @if ($type == 'subs')
                <a class="nav-link" href="{{ route('addToCart', $slug) }}">
                    <img src="{{ asset('images/icons/cart.svg') }}" alt="">
                </a>
            @elseif ($type == 'vps')
                <a class="nav-link" href="{{ route('addToCartVps', $slug) }}">
                    <img src="{{ asset('images/icons/cart.svg') }}" alt="">
                </a>
            @elseif ($type == 'install')
                <a class="nav-link" href="{{ route('addToCartInstall', $slug) }}">
                    <img src="{{ asset('images/icons/cart.svg') }}" alt="">
                </a>
            @elseif ($type == 'unpaidsubs')
                <a class="nav-link" href="{{ route('addToCartUnpaid', $id) }}">
                    <img src="{{ asset('images/icons/cart.svg') }}" alt="">
                </a>
            @elseif ($type == 'unpaidvps')
                <a class="nav-link" href="{{ route('addToCartUnpaidVps', $id) }}">
                    <img src="{{ asset('images/icons/cart.svg') }}" alt="">
                </a>
            @elseif ($type == 'unpaidinstall')
                <a class="nav-link" href="{{ route('addToCartUnpaidInstall', $id) }}">
                    <img src="{{ asset('images/icons/cart.svg') }}" alt="">
                </a>
            @else
                <a class="nav-link" href="{{ route('subsPage') }}">
                    <img src="{{ asset('images/icons/cart.svg') }}" alt="">
                </a>
            @endif

        </li>
    </ul>
    <label for="nav-toggle" class="icon-burger">
        <div class="line"></div>
        <div class="line"></div>
        <div class="line"></div>
    </label>
</nav>

<!-- Modal -->
<div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="loginModalTitle"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal_head">
                <h4>Login and get your robot</h4>
            </div>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <form class="mt-4" action="{{ route('userLogin') }}" method="post">
                @csrf
                <div class="form-group">
                    <label for="exampleInputEmail1">Email Address</label>
                    <input type="email" name="email" class="form-control" id="exampleInputEmail1"
                        aria-describedby="emailHelp" required value="{{ old('email') }}">
                </div>
                <div class="form-group">
                    <label for="Password1">Password</label>
                    <div class="input-group">
                        <input type="password" name="password" class="form-control" id="Password1" required>
                        <button type="button" id="btnToggle" class="toggle" style="padding-top: 7px;"><i id="eyeIcon"
                                class="fa fa-eye"></i></button>
                    </div>
                </div>
                @if ($errors->any())
                    <p class="text-danger">Wrong Email or Password</p>
                @endif
                <div class="login-help">
                    <a href="#">Forgot Password?</a>
                    <a href="{{ route('registerPage') }}">Create New Account</a>
                </div>
                <button type="submit" class="login-modal-btn my-3">LOGIN</button>
                <h5 class="text-center">Or</h5>
                <button type="submit" class="login-fb-btn my-3">Login with Facebook</button>
                <button type="submit" class="login-google-btn my-3">Login with Google</button>
            </form>
        </div>
    </div>
</div>
