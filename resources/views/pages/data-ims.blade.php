<div class="col-md-6 col-xs-12">
  <div class="row">
    <div class="col-md-6">
      <div class="small-box bg-{{$ibreg > Cookie::get('ibreg') ? 'green' : 'teal'}}">
        <div class="inner">
          <h3>{{number_format($ibreg_today,'0','.','.')}}</h3>
          <p>Today Leads</p>
        </div>
        <div class="icon" >
          <i class="fa fa-users" style="color:#ffffff"></i>
        </div>
      </div>
    </div>
    <div class="col-md-6">
      <div class="small-box bg-teal">
        <div class="inner">
          <h3>{{number_format($ibreg_week,'0','.','.')}}</h3>
          <p>This Week Leads</p>
        </div>
        <div class="icon" >
          <i class="fa fa-users" style="color:#ffffff"></i>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-6">
      <div class="small-box bg-teal">
        <div class="inner">
          <h3>{{number_format($ibreg_month,'0','.','.')}}</h3>
          <p>This Month Leads</p>
        </div>
        <div class="icon" >
          <i class="fa fa-users" style="color:#ffffff"></i>
        </div>
      </div>
    </div>
    <div class="col-md-6">
      <div class="small-box bg-teal">
        <div class="inner">
          <h3>{{number_format($ibreg,'0','.','.')}}</h3>
          <p>All Time Leads</p>
        </div>
        <div class="icon" >
          <i class="fa fa-users" style="color:#ffffff"></i>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- ./col -->
<div class="col-md-6 col-xs-12">
  <div class="row">
    <div class="col-md-6">
      <!-- small box -->
      <div class="small-box bg-{{$ibmt4 > Cookie::get('ibmt4') ? 'green' : 'purple'}}">
        <div class="inner">
          <h3>{{number_format($ibmt4_today,'0','.','.')}}</h3>
          <p>Today Metatraders</p>
        </div>
        <div class="icon" >
          <i class="fa fa-cog" style="color:#ffffff"></i>
        </div>
      </div>
    </div>
    <div class="col-md-6">
      <!-- small box -->
      <div class="small-box bg-purple">
        <div class="inner">
          <h3>{{number_format($ibmt4_week,'0','.','.')}}</h3>
          <p>This Week Metatraders</p>
        </div>
        <div class="icon" >
          <i class="fa fa-cog" style="color:#ffffff"></i>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-6">
      <!-- small box -->
      <div class="small-box bg-purple">
        <div class="inner">
          <h3>{{number_format($ibmt4_month,'0','.','.')}}</h3>
          <p>This Month Metatraders</p>
        </div>
        <div class="icon" >
          <i class="fa fa-cog" style="color:#ffffff"></i>
        </div>
      </div>
    </div>
    <div class="col-md-6">
      <!-- small box -->
      <div class="small-box bg-purple">
        <div class="inner">
          <h3>{{number_format($ibmt4,'0','.','.')}}</h3>
          <p>All Time Metatraders</p>
        </div>
        <div class="icon" >
          <i class="fa fa-cog" style="color:#ffffff"></i>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="col-md-6 col-xs-12">
  <div class="row">
    <div class="col-md-6">
      <!-- small box -->
      <div class="small-box bg-{{$ibdep > Cookie::get('ibdep') ? 'green' : 'navy'}}">
        <div class="inner">
          <h3>{{number_format($ibdep_today,'0','.','.')}}</h3>
          <p>Today Deposits</p>
        </div>
        <div class="icon" >
          <i class="fa fa-money" style="color:#ffffff"></i>
        </div>
      </div>
    </div>
    <div class="col-md-6">
      <!-- small box -->
      <div class="small-box bg-navy">
        <div class="inner">
          <h3>{{number_format($ibdep_week,'0','.','.')}}</h3>
          <p>This Week Deposits</p>
        </div>
        <div class="icon" >
          <i class="fa fa-money" style="color:#ffffff"></i>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-6">
      <!-- small box -->
      <div class="small-box bg-navy">
        <div class="inner">
          <h3>{{number_format($ibdep_month,'0','.','.')}}</h3>
          <p>This Month Deposits</p>
        </div>
        <div class="icon" >
          <i class="fa fa-money" style="color:#ffffff"></i>
        </div>
      </div>
    </div>
    <div class="col-md-6">
      <!-- small box -->
      <div class="small-box bg-navy">
        <div class="inner">
          <h3>{{number_format($ibdep,'0','.','.')}}</h3>
          <p>All Time Deposit</p>
        </div>
        <div class="icon" >
          <i class="fa fa-money" style="color:#ffffff"></i>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="col-md-6 col-xs-12">
  <div class="row">
    <div class="col-md-6">
      <!-- small box -->
      <div class="small-box bg-{{$ibdep > Cookie::get('ibdep') ? 'green' : 'orange'}}">
        <div class="inner">
          <h3>{{number_format($today_amount,'0','.','.')}}</h3>
          <p>Today Amount</p>
        </div>
        <div class="icon" >
          <i class="fa fa-dollar" style="color:#ffffff"></i>
        </div>
      </div>
    </div>
    <div class="col-md-6">
      <!-- small box -->
      <div class="small-box bg-{{$week_amount > 0 ? 'orange' : 'maroon'}}">
        <div class="inner">
          <h3>{{number_format($week_amount,'0','.','.')}}</h3>
          <p>This Week Amount</p>
        </div>
        <div class="icon" >
          <i class="fa fa-dollar" style="color:#ffffff"></i>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-6">
      <!-- small box -->
      <div class="small-box bg-{{$month_amount > 0 ? 'orange' : 'maroon'}}">
        <div class="inner">
          <h3>{{number_format($month_amount,'0','.','.')}}</h3>
          <p>This Month Amount</p>
        </div>
        <div class="icon" >
          <i class="fa fa-dollar" style="color:#ffffff"></i>
        </div>
      </div>
    </div>
    <div class="col-md-6">
      <!-- small box -->
      <div class="small-box bg-{{$total_amount > 0 ? 'orange' : 'maroon'}}">
        <div class="inner">
          <h3>{{number_format($total_amount,'0','.','.')}}</h3>
          <p>Total Amount</p>
        </div>
        <div class="icon" >
          <i class="fa fa-dollar" style="color:#ffffff"></i>
        </div>
      </div>
    </div>
  </div>
</div>
@foreach ($ibiklan as $iklan)
  <?php
    if($ibreg_week>0){
      $persen = number_format($iklan['week']/$ibreg_week*100,0,'.','.');
    }else{
      $persen = 0;
    }

    $colorCampaign = 'gray';

    if($iklan['today'] > 0){
      $colorCampaign = 'teal';
    }

    if($iklan['mt4_today'] > 0){
      $colorCampaign = 'purple';
    }

    if($iklan['depo_today'] > 0){
      $colorCampaign = 'navy';
    }

    if($iklan['amount_today'] > 0){
      $colorCampaign = 'orange';
    }

    if($iklan['amount_wd_today'] > 0){
      $colorCampaign = 'danger';
    }

    $campaignName = $iklan['campaign'];
  ?>
  <div class="col-md-2">
    <!-- Info Boxes Style 2 -->
    <div class="info-box bg-{{$iklan['total'] > Cookie::get($campaignName) ? 'green' : $colorCampaign}}">


      <div class="info-box-content" style="margin-left:0px!important;">
        <span class="info-box-text">
        {{$iklan['today']}} | {{$iklan['mt4_today']}} | {{$iklan['depo_today']}} <i class="ion ion-ios-pricetag-outline text-maroon"></i> {{$iklan['campaign']}}</span>
        <span class="info-box-number"><i class="fa fa-users" style="color:#ffffff"></i> {{number_format($iklan['total'],'0','.','.')}} <i class="fa fa-cog" style="color:#ffffff"></i> {{number_format($iklan['metatrader'],'0','.','.')}} <i class="fa fa-money" style="color:#ffffff"></i> {{number_format($iklan['deposit'],'0','.','.')}}</span>

        <div class="progress">
          <div class="progress-bar" style="width: {{$persen}}%;background-color:{{$iklan['total'] > Cookie::get($campaignName) ? '#ffffff' : '#ff0000'}};"></div>
        </div>
        <span class="progress-description">
          {{$iklan['origin']}}
        </span>
      </div>
      <!-- /.info-box-content -->
    </div>
    <!-- /.info-box -->
  </div>
@endforeach
