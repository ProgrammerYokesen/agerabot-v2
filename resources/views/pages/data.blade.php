{{--<div class="row">
  <div class="col-md-4 col-sm-6 col-xs-12">
    <div class="info-box">
      <span class="info-box-icon bg-{{$mql->equity > 456.69 ? 'green' : 'red'}}"><i class="fa fa-dollar"></i></span>

      <div class="info-box-content">
        <h1>{{number_format($mql->equity,'2','.',' ')}} <small>Equity</small>  </h1>
      </div>
      <!-- /.info-box-content -->
    </div>
    <!-- /.info-box -->
  </div>
  <!-- /.col -->
  <div class="col-md-4 col-sm-6 col-xs-12">
    <div class="info-box">
      <span class="info-box-icon bg-{{$mql->balance > 456.69 ? 'green' : 'red'}}"><i class="fa fa-credit-card"></i></span>

      <div class="info-box-content">
        <h1>{{number_format($mql->balance,'2','.',' ')}} <small>Balance</small> </h1>
      </div>
      <!-- /.info-box-content -->
    </div>
    <!-- /.info-box -->
  </div>
  <!-- /.col -->

  <div class="col-md-4 col-sm-6 col-xs-12">
    <div class="info-box">
      <span class="info-box-icon bg-{{$mql->monthly_growth > 0 ? 'green' : 'red'}}"><i class="fa fa-calendar-check-o"></i></span>

      <div class="info-box-content">
        <h1>{{number_format($mql->monthly_growth,'2','.',' ')}} % <small>Monthly Growth</small></h1>
      </div>
      <!-- /.info-box-content -->
    </div>
    <!-- /.info-box -->
  </div>
  <!-- /.col -->

  <div class="col-md-4 col-sm-6 col-xs-12">
    <div class="info-box">
      <span class="info-box-icon bg-{{$mql->roi > 0 ? 'green' : 'red'}}"><i class="fa fa-bar-chart"></i></span>

      <div class="info-box-content">
        <h3>{{number_format($mql->roi,'2','.',' ')}} % <small>ROI</small> </h3>
      </div>
      <!-- /.info-box-content -->
    </div>
    <!-- /.info-box -->
  </div>
  <!-- /.col -->

  <div class="col-md-4 col-sm-6 col-xs-12">
    <div class="info-box">
      <span class="info-box-icon bg-{{$mql->profit_factor > 1 ? 'green' : 'red'}}"><i class="fa fa-rocket"></i></span>

      <div class="info-box-content">
        <h3>{{number_format($mql->profit_factor,'2','.',' ')}} <small>Profit Factor</small> {{number_format($mql->expected_payoff,'2','.',' ')}} <small>Expected Payoff</small></h3>
      </div>
      <!-- /.info-box-content -->
    </div>
    <!-- /.info-box -->
  </div>
  <!-- /.col -->

  <div class="col-md-4 col-sm-6 col-xs-12">
    <div class="info-box">
      <span class="info-box-icon bg-{{$mql->deals_per_week > 75 ? 'green' : 'red'}}"><i class="fa fa-list-ol"></i></span>

      <div class="info-box-content">
        <h3>{{number_format($mql->deals_per_week,'0','.',' ')}} <small>Deals per week</small></h3>
      </div>
      <!-- /.info-box-content -->
    </div>
    <!-- /.info-box -->
  </div>
  <!-- /.col -->

  <div class="col-md-4 col-sm-6 col-xs-12">
    <div class="info-box">
      <span class="info-box-icon bg-{{$mql->rating > 3 ? 'green' : 'gray'}}"><i class="fa fa-star-o"></i></span>

      <div class="info-box-content">
        <h3>
          @for ($i=0; $i < number_format($mql->rating,'0','',''); $i++)
            <i class="fa fa-star" style="background-color:#31c81d"></i>
          @endfor
           {{number_format($mql->rating,'1','.',' ')}} <small>Rating</small></h3>
      </div>
      <!-- /.info-box-content -->
    </div>
    <!-- /.info-box -->
  </div>
  <!-- /.col -->
  <div class="col-md-4 col-sm-6 col-xs-12">
    <div class="info-box">
      <span class="info-box-icon bg-{{$mql->subscriptions_total > 0 ? 'green' : 'gray'}}"><i class="fa fa-user-plus"></i></span>

      <div class="info-box-content">
        <h3>{{number_format($mql->subscriptions_total,'2','.',' ')}} <small>Number of Subscription</small></h3>
      </div>
      <!-- /.info-box-content -->
    </div>
    <!-- /.info-box -->
  </div>
  <!-- /.col -->
  <div class="col-md-4 col-sm-6 col-xs-12">
    <div class="info-box">
      <span class="info-box-icon bg-{{$mql->subscriptions_total > 0 ? 'green' : 'gray'}}"><i class="fa fa-bank"></i></span>

      <div class="info-box-content">
        <h3>{{number_format($mql->subscribers_funds,'2','.',' ')}} <small>Total Funds Subscribe</small></h3>
      </div>
      <!-- /.info-box-content -->
    </div>
    <!-- /.info-box -->
  </div>
  <!-- /.col -->
</div>
<!-- /.row -->
--}}
<div class="row">
  <div class="col-md-6 col-xs-12">
    <div class="row">
      <div class="col-md-6">
        <div class="small-box bg-{{$ibreg > Cookie::get('ibreg') ? 'green' : 'teal'}}">
          <div class="inner">
            <h3>{{number_format($ibreg_today,'0','.','.')}}</h3>
            <p>Today Leads</p>
          </div>
          <div class="icon" >
            <i class="fa fa-users" style="color:#ffffff"></i>
          </div>
        </div>
      </div>
      <div class="col-md-6">
        <div class="small-box bg-{{$ibreg > Cookie::get('ibreg') ? 'green' : 'teal'}}">
          <div class="inner">
            <h3>{{number_format($ibreg_week,'0','.','.')}}</h3>
            <p>This Week Leads</p>
          </div>
          <div class="icon" >
            <i class="fa fa-users" style="color:#ffffff"></i>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-6">
        <div class="small-box bg-{{$ibreg > Cookie::get('ibreg') ? 'green' : 'teal'}}">
          <div class="inner">
            <h3>{{number_format($ibreg_month,'0','.','.')}}</h3>
            <p>This Month Leads</p>
          </div>
          <div class="icon" >
            <i class="fa fa-users" style="color:#ffffff"></i>
          </div>
        </div>
      </div>
      <div class="col-md-6">
        <div class="small-box bg-{{$ibreg > Cookie::get('ibreg') ? 'green' : 'teal'}}">
          <div class="inner">
            <h3>{{number_format($ibreg,'0','.','.')}}</h3>
            <p>All Time Leads</p>
          </div>
          <div class="icon" >
            <i class="fa fa-users" style="color:#ffffff"></i>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- ./col -->
  <div class="col-md-6 col-xs-12">
    <div class="row">
      <div class="col-md-6">
        <!-- small box -->
        <div class="small-box bg-{{$ibmt4 > Cookie::get('ibmt4') ? 'green' : 'purple'}}">
          <div class="inner">
            <h3>{{number_format($ibmt4_today,'0','.','.')}}</h3>
            <p>Today Metatraders</p>
          </div>
          <div class="icon" >
            <i class="fa fa-cog" style="color:#ffffff"></i>
          </div>
        </div>
      </div>
      <div class="col-md-6">
        <!-- small box -->
        <div class="small-box bg-{{$ibmt4 > Cookie::get('ibmt4') ? 'green' : 'purple'}}">
          <div class="inner">
            <h3>{{number_format($ibmt4_week,'0','.','.')}}</h3>
            <p>This Week Metatraders</p>
          </div>
          <div class="icon" >
            <i class="fa fa-cog" style="color:#ffffff"></i>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-6">
        <!-- small box -->
        <div class="small-box bg-{{$ibmt4 > Cookie::get('ibmt4') ? 'green' : 'purple'}}">
          <div class="inner">
            <h3>{{number_format($ibmt4_month,'0','.','.')}}</h3>
            <p>This Month Metatraders</p>
          </div>
          <div class="icon" >
            <i class="fa fa-cog" style="color:#ffffff"></i>
          </div>
        </div>
      </div>
      <div class="col-md-6">
        <!-- small box -->
        <div class="small-box bg-{{$ibmt4 > Cookie::get('ibmt4') ? 'green' : 'purple'}}">
          <div class="inner">
            <h3>{{number_format($ibmt4,'0','.','.')}}</h3>
            <p>All Time Metatraders</p>
          </div>
          <div class="icon" >
            <i class="fa fa-cog" style="color:#ffffff"></i>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="col-md-6 col-xs-12">
    <div class="row">
      <div class="col-md-6">
        <!-- small box -->
        <div class="small-box bg-{{$ibdep > Cookie::get('ibdep') ? 'green' : 'navy'}}">
          <div class="inner">
            <h3>{{number_format($ibdep_today,'0','.','.')}}</h3>
            <p>Today Deposits</p>
          </div>
          <div class="icon" >
            <i class="fa fa-money" style="color:#ffffff"></i>
          </div>
        </div>
      </div>
      <div class="col-md-6">
        <!-- small box -->
        <div class="small-box bg-{{$ibdep > Cookie::get('ibdep') ? 'green' : 'navy'}}">
          <div class="inner">
            <h3>{{number_format($ibdep_week,'0','.','.')}}</h3>
            <p>This Week Deposits</p>
          </div>
          <div class="icon" >
            <i class="fa fa-money" style="color:#ffffff"></i>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-6">
        <!-- small box -->
        <div class="small-box bg-{{$ibdep > Cookie::get('ibdep') ? 'green' : 'navy'}}">
          <div class="inner">
            <h3>{{number_format($ibdep_month,'0','.','.')}}</h3>
            <p>This Month Deposits</p>
          </div>
          <div class="icon" >
            <i class="fa fa-money" style="color:#ffffff"></i>
          </div>
        </div>
      </div>
      <div class="col-md-6">
        <!-- small box -->
        <div class="small-box bg-{{$ibdep > Cookie::get('ibdep') ? 'green' : 'navy'}}">
          <div class="inner">
            <h3>{{number_format($ibdep,'0','.','.')}}</h3>
            <p>All Time Deposit</p>
          </div>
          <div class="icon" >
            <i class="fa fa-money" style="color:#ffffff"></i>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="col-md-6 col-xs-12">
    <div class="row">
      <div class="col-md-6">
        <!-- small box -->
        <div class="small-box bg-{{$ibdep > Cookie::get('ibdep') ? 'green' : 'orange'}}">
          <div class="inner">
            <h3>{{number_format($today_amount,'0','.','.')}}</h3>
            <p>Today Amount</p>
          </div>
          <div class="icon" >
            <i class="fa fa-dollar" style="color:#ffffff"></i>
          </div>
        </div>
      </div>
      <div class="col-md-6">
        <!-- small box -->
        <div class="small-box bg-{{$ibdep > Cookie::get('ibdep') ? 'green' : 'orange'}}">
          <div class="inner">
            <h3>{{number_format($week_amount,'0','.','.')}}</h3>
            <p>This Week Amount</p>
          </div>
          <div class="icon" >
            <i class="fa fa-dollar" style="color:#ffffff"></i>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-6">
        <!-- small box -->
        <div class="small-box bg-{{$ibdep > Cookie::get('ibdep') ? 'green' : 'orange'}}">
          <div class="inner">
            <h3>{{number_format($month_amount,'0','.','.')}}</h3>
            <p>This Month Amount</p>
          </div>
          <div class="icon" >
            <i class="fa fa-dollar" style="color:#ffffff"></i>
          </div>
        </div>
      </div>
      <div class="col-md-6">
        <!-- small box -->
        <div class="small-box bg-{{$ibdep > Cookie::get('ibdep') ? 'green' : 'orange'}}">
          <div class="inner">
            <h3>{{number_format($total_amount,'0','.','.')}}</h3>
            <p>Total Amount</p>
          </div>
          <div class="icon" >
            <i class="fa fa-dollar" style="color:#ffffff"></i>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="row">
  @foreach ($ibiklan as $iklan)
    <?php
      $persen = number_format($iklan['total']/$ibreg*100,'0','.','.');
      $campaignName = $iklan['campaign'];
    ?>
    <div class="col-md-2">
      <!-- Info Boxes Style 2 -->
      <div class="info-box bg-{{$iklan['total'] > Cookie::get($campaignName) ? 'green' : 'gray'}}">
        <div class="info-box-content" style="margin-left:0px!important;">
          <span class="info-box-text"><i class="ion ion-ios-pricetag-outline"></i> {{$iklan['campaign']}}</span>
          <span class="info-box-number"><i class="fa fa-users" style="color:#1dc8a6"></i> {{number_format($iklan['total'],'0','.','.')}} <i class="fa fa-cog" style="color:#67679e"></i> {{number_format($iklan['metatrader'],'0','.','.')}} <i class="fa fa-money" style="color:#000080"></i> {{number_format($iklan['deposit'],'0','.','.')}}</span>

          <div class="progress">
            <div class="progress-bar" style="width: {{$persen}}%;background-color:{{$iklan['total'] > Cookie::get($campaignName) ? '#008000' : '#ff0000'}};"></div>
          </div>
          <span class="progress-description">
            <i class="fa fa-buysellads"></i> {{$iklan['origin']}}
          </span>
        </div>
        <!-- /.info-box-content -->
      </div>
      <!-- /.info-box -->
    </div>
  @endforeach

</div>
<div class="row">
  <div class="col-md-6">
    <p style="color:#fff">last update : {{date('H:i:s d-m-Y')}}</p>
  </div>
</div>
