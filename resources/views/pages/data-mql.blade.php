{{--
<div class="col-md-4 col-sm-6 col-xs-12">
  <div class="info-box">
    <span class="info-box-icon bg-{{$mql->equity > $mql->balance ? 'green' : 'red'}}"><i class="fa fa-dollar"></i></span>

    <div class="info-box-content">
      <h1><strong>{{number_format($mql->equity,'2','.',' ')}}</strong> <small>Equity | Last update {{date('H:i d/m',strtotime('+7hours'))}}</small></h1>
    </div>
    <!-- /.info-box-content -->
  </div>
  <!-- /.info-box -->
</div>
<!-- /.col -->
<div class="col-md-4 col-sm-6 col-xs-12">
  <div class="info-box">
    <span class="info-box-icon bg-{{$mql->balance > (456.69 + (456.69 * $mql->monthly_growth / 100)) ? 'green' : 'red'}}"><i class="fa fa-credit-card"></i></span>

    <div class="info-box-content">
      <h3>{{number_format($mql->balance,'2','.',' ')}} <small>Balance | benchmark {{number_format((456.69 + (456.69 * $mql->monthly_growth / 100)),2,'.',' ')}}</small> </h3>
    </div>
    <!-- /.info-box-content -->
  </div>
  <!-- /.info-box -->
</div>
<!-- /.col -->

<div class="col-md-4 col-sm-6 col-xs-12">
  <div class="info-box">
    <span class="info-box-icon bg-{{$mql->monthly_growth > 0 ? 'green' : 'red'}}"><i class="fa fa-calendar-check-o"></i></span>

    <div class="info-box-content">
      <h3>{{number_format($mql->monthly_growth,'2','.',' ')}} % <small>Monthly Growth</small></h3>
    </div>
    <!-- /.info-box-content -->
  </div>
  <!-- /.info-box -->
</div>
<!-- /.col -->

<div class="col-md-4 col-sm-6 col-xs-12">
  <div class="info-box">
    <span class="info-box-icon bg-{{$mql->roi > 0 ? 'green' : 'red'}}"><i class="fa fa-bar-chart"></i></span>

    <div class="info-box-content">
      <h3>{{number_format($mql->roi,'2','.',' ')}} % <small>ROI</small> </h3>
    </div>
    <!-- /.info-box-content -->
  </div>
  <!-- /.info-box -->
</div>
<!-- /.col -->

<div class="col-md-4 col-sm-6 col-xs-12">
  <div class="info-box">
    <span class="info-box-icon bg-{{$mql->profit_factor > 1 ? 'green' : 'red'}}"><i class="fa fa-rocket"></i></span>

    <div class="info-box-content">
      <h3>{{number_format($mql->profit_factor,'2','.',' ')}} <small>Profit Factor</small> {{number_format($mql->expected_payoff,'2','.',' ')}} <small>Expected Payoff</small></h3>
    </div>
    <!-- /.info-box-content -->
  </div>
  <!-- /.info-box -->
</div>
<!-- /.col -->

<div class="col-md-4 col-sm-6 col-xs-12">
  <div class="info-box">
    <span class="info-box-icon bg-{{$mql->deals_per_week > 75 ? 'green' : 'red'}}"><i class="fa fa-list-ol"></i></span>

    <div class="info-box-content">
      <h3>{{number_format($mql->deals_per_week,'0','.',' ')}} <small>Deals per week</small></h3>
    </div>
    <!-- /.info-box-content -->
  </div>
  <!-- /.info-box -->
</div>
<!-- /.col -->

<div class="col-md-4 col-sm-6 col-xs-12">
  <div class="info-box">
    <span class="info-box-icon bg-{{$mql->rating > 3 ? 'green' : 'gray'}}"><i class="fa fa-star-o"></i></span>

    <div class="info-box-content">
      <h3>
        @for ($i=0; $i < number_format($mql->rating,'0','',''); $i++)
          <i class="fa fa-star" style="background-color:#31c81d"></i>
        @endfor
         {{number_format($mql->rating,'1','.',' ')}} <small>Rating</small></h3>
    </div>
    <!-- /.info-box-content -->
  </div>
  <!-- /.info-box -->
</div>
<!-- /.col -->
<div class="col-md-4 col-sm-6 col-xs-12">
  <div class="info-box">
    <span class="info-box-icon bg-{{$mql->subscriptions_total > 0 ? 'green' : 'gray'}}"><i class="fa fa-user-plus"></i></span>

    <div class="info-box-content">
      <h3>{{number_format($mql->subscriptions_total,'2','.',' ')}} <small>Number of Subscription</small></h3>
    </div>
    <!-- /.info-box-content -->
  </div>
  <!-- /.info-box -->
</div>
<!-- /.col -->
<div class="col-md-4 col-sm-6 col-xs-12">
  <div class="info-box">
    <span class="info-box-icon bg-{{$mql->subscriptions_total > 0 ? 'green' : 'gray'}}"><i class="fa fa-bank"></i></span>

    <div class="info-box-content">
      <h3>{{number_format($mql->subscribers_funds,'2','.',' ')}} <small>Total Funds Subscribe</small></h3>
    </div>
    <!-- /.info-box-content -->
  </div>
  <!-- /.info-box -->
</div>
<!-- /.col -->
--}}
