<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <title>Signal Yokesen</title>
  <meta name="csrf-token" content="oeTCbnIwODJhwWVAwTaMD282iAIZ5HN7HXanL4yy"/>
  <meta name='generator' content='CRUDBooster 5.4.6'/>
  <meta name='robots' content='noindex,nofollow'/>
  <link rel="shortcut icon" href="https://yokesen.com/view/justmanonearth/uploads/2016/12/icon-yokesen.png">
  <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
  <!-- Bootstrap 3.4.1 -->
  <link href="https://signal.yokesen.com/vendor/crudbooster/assets/adminlte/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
  <!-- Font Awesome Icons -->
  <link href="https://signal.yokesen.com/vendor/crudbooster/assets/adminlte/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
  <!-- Ionicons -->
  <link href="https://signal.yokesen.com/vendor/crudbooster/ionic/css/ionicons.min.css" rel="stylesheet" type="text/css"/>
  <!-- Theme style -->
  <link href="https://signal.yokesen.com/vendor/crudbooster/assets/adminlte/dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css"/>
  <link href="https://signal.yokesen.com/vendor/crudbooster/assets/adminlte/dist/css/skins/_all-skins.min.css" rel="stylesheet" type="text/css"/>

  <!-- support rtl-->

  <link rel='stylesheet' href='https://signal.yokesen.com/vendor/crudbooster/assets/css/main.css?r=1578108569'/>

  <!-- load css -->
  <style type="text/css">
  </style>

  <style type="text/css">
  .dropdown-menu-action {
    left: -130%;
  }

  .btn-group-action .btn-action {
    cursor: default
  }

  #box-header-module {
    box-shadow: 10px 10px 10px #dddddd;
  }

  .sub-module-tab li {
    background: #F9F9F9;
    cursor: pointer;
  }

  .sub-module-tab li.active {
    background: #ffffff;
    box-shadow: 0px -5px 10px #cccccc
  }

  .nav-tabs > li.active > a, .nav-tabs > li.active > a:focus, .nav-tabs > li.active > a:hover {
    border: none;
  }

  .nav-tabs > li > a {
    border: none;
  }

  .breadcrumb {
    margin: 0 0 0 0;
    padding: 0 0 0 0;
  }

  .form-group > label:first-child {
    display: block
  }
  .inner>h1{
    font-size: 6em;
    font-weight: bold;
  }
  </style>

</head>
<body class="skin-blue">
  <div id='app' class="wrapper">


    <!-- Content Header (Page header) -->


    <!-- Main content -->
    <section class="content">
      {{--
      <div class="row" id="data-mql5">
        <div class="col-md-4 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-{{$mql->equity > 456.69 ? 'green' : 'red'}}"><i class="fa fa-dollar"></i></span>

            <div class="info-box-content">
              <h3>- <small>Equity</small>  </h3>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-4 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-{{$mql->balance > 456.69 ? 'green' : 'red'}}"><i class="fa fa-credit-card"></i></span>

            <div class="info-box-content">
              <h3>- <small>Balance</small> </h3>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->

        <div class="col-md-4 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-{{$mql->monthly_growth > 0 ? 'green' : 'red'}}"><i class="fa fa-calendar-check-o"></i></span>

            <div class="info-box-content">
              <h3>- % <small>Monthly Growth</small></h3>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->

        <div class="col-md-4 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-{{$mql->roi > 0 ? 'green' : 'red'}}"><i class="fa fa-bar-chart"></i></span>

            <div class="info-box-content">
              <h3>- % <small>ROI</small> </h3>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->

        <div class="col-md-4 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-{{$mql->profit_factor > 1 ? 'green' : 'red'}}"><i class="fa fa-rocket"></i></span>

            <div class="info-box-content">
              <h3>- <small>Profit Factor</small> -<small>Expected Payoff</small></h3>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->

        <div class="col-md-4 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-{{$mql->deals_per_week > 75 ? 'green' : 'red'}}"><i class="fa fa-list-ol"></i></span>

            <div class="info-box-content">
              <h3>- <small>Deals per week</small></h3>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->

        <div class="col-md-4 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-{{$mql->rating > 3 ? 'green' : 'gray'}}"><i class="fa fa-star-o"></i></span>

            <div class="info-box-content">
              <h3>
                 - <small>Rating</small></h3>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-4 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-{{$mql->subscriptions_total > 0 ? 'green' : 'gray'}}"><i class="fa fa-user-plus"></i></span>

            <div class="info-box-content">
              <h3>- <small>Number of Subscription</small></h3>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-4 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-{{$mql->subscriptions_total > 0 ? 'green' : 'gray'}}"><i class="fa fa-bank"></i></span>

            <div class="info-box-content">
              <h3>- <small>Total Funds Subscribe</small></h3>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <!--END OF MQL5-->
--}}
      <div class="row" id="data-ims">
        <div class="col-md-6 col-xs-12">
          <div class="row">
            <div class="col-md-6">
              <div class="small-box bg-{{$ibreg > Cookie::get('ibreg') ? 'green' : 'teal'}}">
                <div class="inner">
                  <h3>{{number_format($ibreg_today,'0','.','.')}}</h3>
                  <p>Today Leads</p>
                </div>
                <div class="icon" >
                  <i class="fa fa-users" style="color:#ffffff"></i>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="small-box bg-{{$ibreg > Cookie::get('ibreg') ? 'green' : 'teal'}}">
                <div class="inner">
                  <h3>{{number_format($ibreg_week,'0','.','.')}}</h3>
                  <p>This Week Leads</p>
                </div>
                <div class="icon" >
                  <i class="fa fa-users" style="color:#ffffff"></i>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <div class="small-box bg-{{$ibreg > Cookie::get('ibreg') ? 'green' : 'teal'}}">
                <div class="inner">
                  <h3>{{number_format($ibreg_month,'0','.','.')}}</h3>
                  <p>This Month Leads</p>
                </div>
                <div class="icon" >
                  <i class="fa fa-users" style="color:#ffffff"></i>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="small-box bg-{{$ibreg > Cookie::get('ibreg') ? 'green' : 'teal'}}">
                <div class="inner">
                  <h3>{{number_format($ibreg,'0','.','.')}}</h3>
                  <p>All Time Leads</p>
                </div>
                <div class="icon" >
                  <i class="fa fa-users" style="color:#ffffff"></i>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-md-6 col-xs-12">
          <div class="row">
            <div class="col-md-6">
              <!-- small box -->
              <div class="small-box bg-{{$ibmt4 > Cookie::get('ibmt4') ? 'green' : 'purple'}}">
                <div class="inner">
                  <h3>-</h3>
                  <p>Today Metatraders</p>
                </div>
                <div class="icon" >
                  <i class="fa fa-cog" style="color:#ffffff"></i>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <!-- small box -->
              <div class="small-box bg-{{$ibmt4 > Cookie::get('ibmt4') ? 'green' : 'purple'}}">
                <div class="inner">
                  <h3>-</h3>
                  <p>This Week Metatraders</p>
                </div>
                <div class="icon" >
                  <i class="fa fa-cog" style="color:#ffffff"></i>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <!-- small box -->
              <div class="small-box bg-{{$ibmt4 > Cookie::get('ibmt4') ? 'green' : 'purple'}}">
                <div class="inner">
                  <h3>-</h3>
                  <p>This Month Metatraders</p>
                </div>
                <div class="icon" >
                  <i class="fa fa-cog" style="color:#ffffff"></i>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <!-- small box -->
              <div class="small-box bg-{{$ibmt4 > Cookie::get('ibmt4') ? 'green' : 'purple'}}">
                <div class="inner">
                  <h3>-</h3>
                  <p>All Time Metatraders</p>
                </div>
                <div class="icon" >
                  <i class="fa fa-cog" style="color:#ffffff"></i>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="col-md-6 col-xs-12">
          <div class="row">
            <div class="col-md-6">
              <!-- small box -->
              <div class="small-box bg-{{$ibdep > Cookie::get('ibdep') ? 'green' : 'navy'}}">
                <div class="inner">
                  <h3>-</h3>
                  <p>Today Deposits</p>
                </div>
                <div class="icon" >
                  <i class="fa fa-money" style="color:#ffffff"></i>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <!-- small box -->
              <div class="small-box bg-{{$ibdep > Cookie::get('ibdep') ? 'green' : 'navy'}}">
                <div class="inner">
                  <h3>-</h3>
                  <p>This Week Deposits</p>
                </div>
                <div class="icon" >
                  <i class="fa fa-money" style="color:#ffffff"></i>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <!-- small box -->
              <div class="small-box bg-{{$ibdep > Cookie::get('ibdep') ? 'green' : 'navy'}}">
                <div class="inner">
                  <h3>-</h3>
                  <p>This Month Deposits</p>
                </div>
                <div class="icon" >
                  <i class="fa fa-money" style="color:#ffffff"></i>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <!-- small box -->
              <div class="small-box bg-{{$ibdep > Cookie::get('ibdep') ? 'green' : 'navy'}}">
                <div class="inner">
                  <h3>-</h3>
                  <p>All Time Deposit</p>
                </div>
                <div class="icon" >
                  <i class="fa fa-money" style="color:#ffffff"></i>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="col-md-6 col-xs-12">
          <div class="row">
            <div class="col-md-6">
              <!-- small box -->
              <div class="small-box bg-{{$ibdep > Cookie::get('ibdep') ? 'green' : 'orange'}}">
                <div class="inner">
                  <h3>-</h3>
                  <p>Today Amount</p>
                </div>
                <div class="icon" >
                  <i class="fa fa-dollar" style="color:#ffffff"></i>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <!-- small box -->
              <div class="small-box bg-{{$ibdep > Cookie::get('ibdep') ? 'green' : 'orange'}}">
                <div class="inner">
                  <h3>-</h3>
                  <p>This Week Amount</p>
                </div>
                <div class="icon" >
                  <i class="fa fa-dollar" style="color:#ffffff"></i>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <!-- small box -->
              <div class="small-box bg-{{$ibdep > Cookie::get('ibdep') ? 'green' : 'orange'}}">
                <div class="inner">
                  <h3>-</h3>
                  <p>This Month Amount</p>
                </div>
                <div class="icon" >
                  <i class="fa fa-dollar" style="color:#ffffff"></i>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <!-- small box -->
              <div class="small-box bg-{{$ibdep > Cookie::get('ibdep') ? 'green' : 'orange'}}">
                <div class="inner">
                  <h3>-</h3>
                  <p>Total Amount</p>
                </div>
                <div class="icon" >
                  <i class="fa fa-dollar" style="color:#ffffff"></i>
                </div>
              </div>
            </div>
          </div>
        </div>

        @for ($i=0; $i < 18; $i++)
          <div class="col-md-2">
            <!-- Info Boxes Style 2 -->
            <div class="info-box bg-{{$iklan['total'] > Cookie::get($campaignName) ? 'green' : 'gray'}}">

              <div class="info-box-content" style="margin-left:0px!important;" >
                <span class="info-box-text"><i class="ion ion-ios-pricetag-outline"></i> {{$iklan['campaign']}}</span>
                <span class="info-box-number"><i class="fa fa-users" style="color:#1dc8a6"></i> {{number_format($iklan['total'],'0','.','.')}} <i class="fa fa-cog" style="color:#67679e"></i> {{number_format($iklan['metatrader'],'0','.','.')}} <i class="fa fa-money" style="color:#000080"></i> {{number_format($iklan['deposit'],'0','.','.')}}</span>

                <div class="progress">
                  <div class="progress-bar" style="width: {{$persen}}%;background-color:{{$iklan['total'] > Cookie::get($campaignName) ? '#008000' : '#ff0000'}};"></div>
                </div>
                <span class="progress-description">
                  {{$iklan['origin']}}
                </span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
        @endfor
      </div>

    </section>


  </div>
  <script src="/vendor/crudbooster/assets/adminlte/plugins/jQuery/jquery-2.2.3.min.js"></script>
  <script type="text/javascript">
  $(document).ready(function(){
    setInterval(function() {
      $.ajax({
        type:"GET",
        url: "/data/ims",
        beforeSend: function()
        {
          $('.ajax-load').show();
        }
      })
      .done(function(data)
      {
        console.log(data);
        if(data.html == " "){
          $('.ajax-load').html("No more records found");
          return;
        }
        $('.ajax-load').hide();

        document.getElementById("data-ims").innerHTML = data.html;

      })
      .fail(function(jqXHR, ajaxOptions, thrownError)
      {

      });
    }, 60000);

    setInterval(function() {
      $.ajax({
        type:"GET",
        url: "/data/mql",
        beforeSend: function()
        {
          $('.ajax-load').show();
        }
      })
      .done(function(data)
      {
        console.log(data);
        if(data.html == " "){
          $('.ajax-load').html("No more records found");
          return;
        }
        $('.ajax-load').hide();

        document.getElementById("data-mql5").innerHTML = data.html;

      })
      .fail(function(jqXHR, ajaxOptions, thrownError)
      {

      });
    }, 60000);

  });
  </script>
</body>
</html>
