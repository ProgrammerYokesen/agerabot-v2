@extends("crudbooster::admin_template")

@section('content')
  <div class="row">
          <div class="col-md-12">
            <div class="box">
              <div class="box-header with-border">
                <h3 class="box-title">Best of the Best</h3>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                <table id="example1" class="table table-bordered table-striped">
                  <tr>
                    <th>Pair</th>
                    <th>Robot</th>
                    <th>last %</th>
                    <th>week %</th>
                    <th>month %</th>
                    <th>YTD %</th>
                  </tr>
                  @foreach($best as $isi)
                    <?php
                      if ($isi['data']->last_percent < 50) {
                        $warna_last = "danger";
                        $warni_last = "red";
                      }elseif($isi['data']->last_percent > 49 && $isi['data']->last_percent < 65){
                        $warna_last = "warning";
                        $warni_last = "orange";
                      }elseif($isi['data']->last_percent > 64 && $isi['data']->last_percent < 75){
                        $warna_last = "info";
                        $warni_last = "navy";
                      }elseif($isi['data']->last_percent > 74 && $isi['data']->last_percent < 85){
                        $warni_last = "teal";
                        $warna_last = "primary";
                      }elseif($isi['data']->last_percent > 84 && $isi['data']->last_percent < 101){
                        $warna_last = "success";
                        $warni_last = "green";
                      }

                      if ($isi['data']->week_percent < 50) {
                        $warna_week = "danger";
                        $warni_week = "red";
                      }elseif($isi['data']->week_percent > 49 && $isi['data']->week_percent < 65){
                        $warna_week = "warning";
                        $warni_week = "orange";
                      }elseif($isi['data']->week_percent > 64 && $isi['data']->week_percent < 75){
                        $warna_week = "info";
                        $warni_week = "navy";
                      }elseif($isi['data']->week_percent > 74 && $isi['data']->week_percent < 85){
                        $warni_week = "teal";
                        $warna_week = "primary";
                      }elseif($isi['data']->week_percent > 84 && $isi['data']->week_percent < 101){
                        $warna_week = "success";
                        $warni_week = "green";
                      }

                      if ($isi['data']->month_percent < 50) {
                        $warna_month = "danger";
                        $warni_month = "red";
                      }elseif($isi['data']->month_percent > 49 && $isi['data']->month_percent < 65){
                        $warna_month = "warning";
                        $warni_month = "orange";
                      }elseif($isi['data']->month_percent > 64 && $isi['data']->month_percent < 75){
                        $warna_month = "info";
                        $warni_month = "navy";
                      }elseif($isi['data']->month_percent > 74 && $isi['data']->month_percent < 85){
                        $warni_month = "teal";
                        $warna_month = "primary";
                      }elseif($isi['data']->month_percent > 84 && $isi['data']->month_percent < 101){
                        $warna_month = "success";
                        $warni_month = "green";
                      }

                      if ($isi['data']->year_percent < 50) {
                        $warna_year = "danger";
                        $warni_year = "red";
                      }elseif($isi['data']->year_percent > 49 && $isi['data']->year_percent < 65){
                        $warna_year = "warning";
                        $warni_year = "orange";
                      }elseif($isi['data']->year_percent > 64 && $isi['data']->year_percent < 75){
                        $warna_year = "info";
                        $warni_year = "navy";
                      }elseif($isi['data']->year_percent > 74 && $isi['data']->year_percent < 85){
                        $warni_year = "teal";
                        $warna_year = "primary";
                      }elseif($isi['data']->year_percent > 84 && $isi['data']->year_percent < 101){
                        $warna_year = "success";
                        $warni_year = "green";
                      }

                     ?>
                    <tr>
                      <td>{{$isi['pair']}}</td>
                      <td>{{$isi['robotName']}} ({{$isi['timeframe']}})</td>
                      <td>
                        <span class="badge bg-{{$isi['data']->last_percent > 50 ? 'green' : 'red'}}">{{$isi['data']->last_percent}} %</span> | {{$isi['data']->last_total}}$
                        <div class="progress progress-xs">
                          <div class="progress-bar progress-bar-{{$warna_last}}" style="width: {{$isi['data']->last_percent}}%"></div>
                        </div>
                      </td>
                      <td>
                        <span class="badge bg-{{$isi['data']->week_percent > 50 ? 'green' : 'red'}}">{{$isi['data']->week_percent}} %</span> | {{$isi['data']->week_total}}$
                        <div class="progress progress-xs">
                          <div class="progress-bar progress-bar-{{$warna_week}}" style="width: {{$isi['data']->week_percent}}%"></div>
                        </div>
                      </td>
                      <td>
                        <span class="badge bg-{{$isi['data']->month_percent > 50 ? 'green' : 'red'}}">{{$isi['data']->month_percent}} %</span> | {{$isi['data']->month_total}}$
                        <div class="progress progress-xs">
                          <div class="progress-bar progress-bar-{{$warna_month}}" style="width: {{$isi['data']->month_percent}}%"></div>
                        </div>
                      </td>
                      <td>
                        <span class="badge bg-{{$isi['data']->year_percent > 50 ? 'green' : 'red'}}">{{$isi['data']->year_percent}} %</span> | {{$isi['data']->year_total}}$
                        <div class="progress progress-xs">
                          <div class="progress-bar progress-bar-{{$warna_year}}" style="width: {{$isi['data']->year_percent}}%"></div>
                        </div>
                      </td>
                    </tr>
                  @endforeach
                </table>
              </div>
              <!-- /.box-body -->
              <div class="box-footer clearfix">

              </div>
            </div>
            <!-- /.box -->
          </div>
        </div>


@endsection
