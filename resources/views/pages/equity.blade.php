@foreach ($data as $key => $value)
  <div class="row">
    <div class="col-md-6 col-xs-12">
      <!-- small box -->
      <div class="small-box bg-{{$value->equity > $value->equity_before ? "green" : "red"}}">
        <div class="inner">
          <h1>{{number_format($value->equity,'2','.',' ')}}</h1>

          <p>Equity {{$value->mt4id}}</p>
        </div>

      </div>
    </div>
    <!-- ./col -->
    <div class="col-md-6 col-xs-12">
      <!-- small box -->
      <div class="small-box bg-aqua">
        <div class="inner">
          <h1>{{number_format($value->balance,'2','.',' ')}}</h1>

          <p>Balance {{$value->mt4id}}</p>
        </div>

      </div>
    </div>
  </div>
@endforeach
