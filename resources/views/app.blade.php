<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Agerabot</title>
    {{-- Favicon --}}
    <link rel="icon" type="image/png" href="{{ URL::to('images/favicon.ico') }}">

    <script src="{{ asset('js/app.js') }}" defer></script>

    {{-- CSS --}}
    <link rel="stylesheet" href="{{ URL::to('css/style.css') }}">

    {{-- Bootstrap --}}
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css"
        integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">

    {{-- Font Awesome --}}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css">

    {{-- Swiper JS --}}
    <link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.css" />
    <link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.min.css" />

    {{-- select2 --}}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/css/select2.min.css"
        integrity="sha512-nMNlpuaDPrqlEls3IX/Q56H36qvBASwb3ipuo3MxeWbsQB1881ox0cRv7UPTgBlriqoynt35KjEwgGUeUXIPnw=="
        crossorigin="anonymous" />
    <meta name="csrf-token" content="{{ csrf_token() }}">


</head>

<body>


    @include('components.navbar')
    @yield('content')
    @include('components.footer')


    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous">
    </script>
    <script defer src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous">
    </script>
    <script defer src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/js/select2.min.js"
        integrity="sha512-2ImtlRlf2VVmiGZsjm9bEyhjGW4dU7B6TNwh/hx/iSByxNENtj3WVE6o/9Lj4TJeVXPi4bnOIMXFIJJAeufa0A=="
        crossorigin="anonymous"></script>

    @yield('js')
    {{-- Swiper JS --}}
    <script src="https://unpkg.com/swiper/swiper-bundle.js"></script>
    <script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>
    {{-- select2 --}}

    <script src="{{ asset('js/swiper.js') }}"></script>

    <script>
        $(function() {
            $(document).scroll(function() {
                var $nav = $(".navbar_agerabot");
                var $logo = $("#logo_agerabot");
                $nav.toggleClass('scrolled', $(this).scrollTop() > $nav.height());

                if ($(window).scrollTop() > $nav.height()) {
                    $logo.removeClass('logo').addClass("logo-scrolled");
                } else {
                    $logo.removeClass('logo-scrolled').addClass("logo");
                }

                // $nav.removeClass('logo', $(this).scrollTop() > $nav.height());
                // $nav.addClass('logo-scrolled', $(this).scrollTop() > $nav.height());
            });
        });

        $('ul.tabs li').click(function() {
            var tab_id = $(this).attr('href');

            $('ul.tabs li').removeClass('current');
            $('.tab-content').removeClass('current');

            $(this).addClass('current');
            $("#" + tab_id).addClass('current');
        })


        // SHOW DETAIL ON ROBOT PAGE ===============================================
        var $cell = $('.cardss');

        //open and close card when clicked on card
        $cell.find('.js-expander').click(function() {

            var $thisCell = $(this).closest('.cardss');

            if ($thisCell.hasClass('is-collapsed')) {
                $cell.not($thisCell).removeClass('is-expanded').addClass('is-collapsed').addClass(
                    'is-inactive');
                $thisCell.removeClass('is-collapsed').addClass('is-expanded');

                if ($cell.not($thisCell).hasClass('is-inactive')) {
                    //do nothing
                } else {
                    $cell.not($thisCell).addClass('is-inactive');
                }

            } else {
                $thisCell.removeClass('is-expanded').addClass('is-collapsed');
                $cell.not($thisCell).removeClass('is-inactive');
            }
        });

        //close card when click on cross
        $cell.find('.js-collapser').click(function() {

            var $thisCell = $(this).closest('.cardss');

            $thisCell.removeClass('is-expanded').addClass('is-collapsed');
            $cell.not($thisCell).removeClass('is-inactive');

        });
        // =========================================================================

        // Toggle Password
        let passwordInput = document.getElementById('Password1'),
            toggle = document.getElementById('btnToggle'),
            icon = document.getElementById('eyeIcon');

        function togglePassword() {
            if (passwordInput.type === 'password') {
                passwordInput.type = 'text';
                icon.classList.add("fa-eye-slash");
                //toggle.innerHTML = 'hide';
            } else {
                passwordInput.type = 'password';
                icon.classList.remove("fa-eye-slash");
                //toggle.innerHTML = 'show';
            }
        }

        toggle.addEventListener('click', togglePassword, false);

    </script>
    @yield('scriptJs')

    @if ($errors->any())
        <script>
            $(function() {
                $("#loginModal").modal('toggle');
            });

        </script>
    @endif

</body>

</html>
