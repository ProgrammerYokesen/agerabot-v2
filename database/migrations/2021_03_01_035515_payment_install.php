<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PaymentInstall extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('paymentInstall', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('userId');
            $table->string('externalId');
            $table->integer('quotaTotal');
            $table->float('priceTotal');
            $table->string('orderId');
            $table->text('invoiceUrl');
            $table->string('status', 20);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('paymentInstall');
    }
}
