<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBlogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blogs', function (Blueprint $table) {
            $table->increments('id');
            $table->text('blogTitle')->nullable();
            $table->text('blogSlug')->nullable();
            $table->longText('blogContent')->nullable();
            $table->string('blogImage1')->nullable();
            $table->string('blogImage2')->nullable();
            $table->string('blogCallToAction')->nullable();
            $table->string('blogRedirect')->nullable();
            $table->string('blogVideo')->nullable();
            $table->text('blogMetaDescription')->nullable();
            $table->string('blogMetaKeywords')->nullable();
            $table->text('blogAuthor')->nullable();
            $table->string('blogCode')->nullable();
            $table->string('blogStatus')->nullable();
            $table->integer('blogLike');
            $table->integer('blogRead');
            $table->datetime('release_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blogs');
    }
}
