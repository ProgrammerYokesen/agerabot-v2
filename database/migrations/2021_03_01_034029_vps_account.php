<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class VpsAccount extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vpsAccount', function (Blueprint $table) {
            $table->increments('id');
            $table->string('orderId');
            $table->integer('userId');
            $table->string('ip');
            $table->string('username');
            $table->string('password');
            $table->string('uuid');
            $table->dateTime('expiredDate');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vpsAccount');
    }
}
