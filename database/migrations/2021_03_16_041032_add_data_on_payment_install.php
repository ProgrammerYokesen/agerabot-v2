<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDataOnPaymentInstall extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('paymentinstall', function (Blueprint $table) {
            //
            $table->dateTime('paymentTime')->nullable();
            $table->boolean('payment')->default(0);
            $table->string('paymentMethod');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('paymentinstall', function (Blueprint $table) {
            //
        });
    }
}
