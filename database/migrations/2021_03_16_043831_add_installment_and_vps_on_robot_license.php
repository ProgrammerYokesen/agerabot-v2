<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddInstallmentAndVpsOnRobotLicense extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('robotlicense', function (Blueprint $table) {
            //
            $table->boolean('installment')->default(0);
            $table->boolean('vps')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('robotLicense', function (Blueprint $table) {
            //
        });
    }
}
