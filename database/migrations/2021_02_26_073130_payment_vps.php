<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PaymentVps extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('paymentVps', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('vpsId');
            $table->integer('vpsDetailId');
            $table->integer('userId');
            $table->string('externalId');
            $table->string('orderId');
            $table->text('paymentUrl');
            $table->dateTime('paymentTime')->nullable();
            $table->boolean('payment')->default(0);
            $table->string('status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('paymentVps');
    }
}
