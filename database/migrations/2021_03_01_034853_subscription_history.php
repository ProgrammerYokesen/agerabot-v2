<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SubscriptionHistory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subscriptionHistory', function (Blueprint $table) {
            $table->increments('id');
            $table->string('orderId');
            $table->dateTime('dateSubs');
            $table->text('description');
            $table->dateTime('activeTime');
            $table->float('subTotal');
            $table->float('total');
            $table->string('status', 20);
            $table->string('paymentMethod');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subscriptionHistory');
    }
}
