/*

Script  : Contact Form
Version : 1.0
Author  : Surjith S M
URI     : http://themeforest.net/user/surjithctly

Copyright © All rights Reserved
Surjith S M / @surjithctly

*/

$(function () {

    "use strict";


    /* ================================================
   jQuery Validate - Reset Defaults
   ================================================ */

    $.validator.setDefaults({
        ignore: [],
        highlight: function (element) {
            //$(element).closest('.form-group').addClass('has-error');
            $(element).addClass('is-invalid');
        },
        unhighlight: function (element) {
            //$(element).closest('.form-group').removeClass('has-error');
            $(element).removeClass('is-invalid');
        },
        errorElement: 'div',
        errorClass: 'invalid-feedback',
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length || element.parent('label').length) {
                error.insertAfter(element.parent());
            } else {
                error.insertAfter(element);
            }
        }
    });

    /*
    VALIDATE
    -------- */

    let $loginform = $("#loginform");
    let $jsloginbtn = $("#js-login-btn");
    let $jsloginresult = $("#js-login-result");

    $loginform.submit(function (e) {
        e.preventDefault();
    }).validate({
        rules: {
            username: "required",
            password: "required"
        },
        messages: {
            username: "Your username / email please",
            password: "Please enter your password"
        },
        submitHandler: function (form) {

            $jsloginbtn.attr("disabled", true);

            /*
            CHECK PAGE FOR REDIRECT (Thank you page)
            ---------------------------------------- */

            let redirect = $loginform.data('redirect');
            let noredirect = false;
            if (redirect == 'none' || redirect == "" || redirect == null) {
                noredirect = true;
            }

            $jsloginresult.html('<p class="help-block text-center mt-3 mb-0">Please wait...</p>');

            /*
            FETCH SUCCESS / ERROR MSG FROM HTML DATA-ATTR
            --------------------------------------------- */

            let success_msg = $jsloginresult.data('success-msg');
            let error_msg = $jsloginresult.data('error-msg');

            let token = $("meta[name='csrf-token']").attr("content");

            /*
             AJAX POST
             --------- */

            $.ajax({
                type: "POST",
                data: {
                    "_token": token,
                    "username": $('#username').val(),
                    "password": $('#password').val()
                },
                url: $('#loginform').data('url'),
                cache: false,
                success: function (data) {
                    if (data.status == 'success') {
                        if (noredirect) {
                            $loginform[0].reset();
                            $jsloginresult.fadeIn('slow').html('<div class="mt-3 mb-0 alert alert-success text-center">' + success_msg + '</div>').delay(3000).fadeOut('slow');
                        } else {
                            window.location.href = redirect;
                        }
                    } else {
                        $jsloginresult.fadeIn('slow').html('<div class="mt-3 mb-0 alert alert-danger text-center">' + error_msg + '</div>').delay(3000).fadeOut('slow');
                    }
                    $jsloginbtn.attr("disabled", false);
                },
                error: function (data) {
                    $jsloginresult.fadeIn('slow').html('<div class="mt-3 mb-0 alert alert-danger text-center"> Cannot access Server</div>').delay(3000).fadeOut('slow');
                    $jsloginbtn.attr("disabled", false);
                    if (window.console) {
                        console.log('Ajax Error: ' + data.statusText);
                    }

                }
            });
            return false;

        }
    });

})
