$(document).ready(function() {
	const pathArray = window.location.pathname.split( '/' );
	
	$('li.nav-item[menu="'+pathArray[1]+'"]').addClass('active');

    if(window.matchMedia("(max-width: 767px)").matches){
	    let swiper = new Swiper('.swiper-product', {
	    	effect: 'coverflow',
	    	coverflowEffect: {
	        	rotate: 50,
	        	stretch: 0,
	        	depth: 100,
	        	modifier: 1,
	        	slideShadows: false,
	      	},
	      	slidesPerView: 1,
	      	spaceBetween: 30,
			grabCursor: true,
	      	loop: true,
	      	navigation: {
	        	nextEl: '.swiper-button-next',
	        	prevEl: '.swiper-button-prev',
	      	},
	    });
    } else {
	    let swiper = new Swiper('.swiper-product', {
			effect: 'coverflow',
	    	coverflowEffect: {
	        	rotate: 0,
	        	stretch: 0,
	        	depth: 100,
	        	modifier: 1,
	        	slideShadows: false,
	      	},
	      	slidesPerView: 3,
	      	spaceBetween: 50,
			grabCursor: true,
	      	loop: true,
	      	navigation: {
	        	nextEl: '.swiper-button-next',
	        	prevEl: '.swiper-button-prev',
	      	},
	    });
    }

    let swiperTestimonial = new Swiper('.swiper-testimonial', {
      	slidesPerView: 1,
      	spaceBetween: 30,
		grabCursor: true,
      	loop: true,
      	speed: 2000,
      	navigation: {
        	nextEl: '.swiper-button-next',
        	prevEl: '.swiper-button-prev',
      	},
      	autoplay: {
    		delay: 5000,
  		},
    });

	let swiperPrice = new Swiper('.swiper-price', {
		slidesPerView: 1,
		spaceBetween: 30,
	  grabCursor: true,
		loop: true,
		speed: 2000,
		navigation: {
		  nextEl: '.swiper-button-next',
		  prevEl: '.swiper-button-prev',
		},
		autoplay: {
		  delay: 5000,
		},
  });
});